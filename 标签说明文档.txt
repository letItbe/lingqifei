网站通用标签：
{$installdir}:网站的安装目录
{$installdir_template}:模板目录
{$sitename}:网站名称
{$companyname}:网站公司名称
{$address}:网站联系地址
{$NetAddress}:网站地址
{$contact}:网站联系人
{$tel}:网站电话
{$fax}:网站传真
{$phone}:网站手机
{$email}:网站邮箱
{$postcode}:网站邮政编码
{$icp}:网站ICP备案号
{$copyright}:网站版权
{$meta_keywords}:网站keywords
{$meta_description}:网站description
{$skin_css}:网站的风格CSS


{$channeldir}：频道目录
{$addir}：广告目录
{$pagetitle}：网页标题
{$location}：显示你现在的位置
{$showpage}、{$ShowPage_en}：显示中英文分页标签

会员中心：
{$truename}
{$u_email}
{$u_phone}

{$yn(判断语句,真,假)}
{$islogin(登陆成功信息$$$登陆失败信息)}
【menulist(MenuID,spaceStr)】{$autoid}{$menuname}{$menuurl}{$menualt}【/menulist】
【channel(channelid)】{$channelname}{$channelurl}【/channel】
如果MenuID为空则为所有，spaceStr为分割字符，主要为去掉最后一个
{$showclass(channelid)}:显示为ul、li格式的无限分级
【classlist(ichannelid,classid)】{$classid}{$classname}{$classpic}{$classurl}【/classlist】

调查：
{$vote} 替换调查
调查内容页：
{$votetitle}：调查标题
{$votetotal}：投票总数
【voteitem】参数【/voteitem】,参数列表：
{$itemnum}：选项序号
{$itemtitle}：选项标题
{$itemnum}：选项投票数
{$itemper}：百分比

公告自定义列表标签
【announcelist(AnnNum,TitleLen,ContentLen,pagenum)】循环内容
//参数注释：AnnNum：调用多少条(0为无限制)，TitleLen：标题的长度(0为无限制)，ContentLen：内容长度(0为无限制)，pagenum：多少条每页(0为不分页)
循环内容可用标签:
{$announceurl}：公告连接地址
{$announcetitle}：公告标题
{$announcecontent}：公告内容
{$announcetime}：公告发布时间
【/announcelist】 


友情连接标签说明：
【friendsitelist(Kindid,SiteNum,orderType)】
参数说明：Kindid 分类ID，SiteNum 调用条数(0为所有)，orderType 排序方式1为升 2为降
循环内容可用标签：
{$friendsitename}：链接的名称
{$friendsitelogo}：链接的LOGO，88*31
{$friendsiteurl}：链接的地址
{$friendsiteintro}：链接介绍
{$friendsitelogourl}：图片或者flash的地址
【/friendsitelist】


留言页标签说明:

【guestlist(topnum,pagenum)】循环标签【/guestlist】
循环标签：
{$gname}：名称
{$gtitle}:标题
{$gtel}：电话
{$goicq}：MSN/QQ
{$gmail}：EMAIL
{$gtime}：留言时间
{$gip}：留言IP
{$gface}：头像数字
{$gcontent}：留言信息
{$greply}：管理员回复信息



招聘页标签：
列表标签：【joblist(topnum,pagenum)】循环标签【/joblist】
循环标签：
{$joburl}：链接地址
{$jobname}：招聘职位
{$jobnum}：招聘人数
{$jobsex}：性别要求
{$jobage}：年龄要求
{$jobsalary}：薪资待遇
{$jobaddress}：工作地点
{$jobtime}：发表时间
{$jobdetial(titlelen)}：详细介绍
详细页标签：
{$jobname}、{$jobsex}、{$jobage}、{$jobsalary}、{$jobnum}、{$jobaddress}、{$jobaddTime}、{$jobdetial}、{$accepturl}应聘地址

单页文档可用标签：
【likepage(likestr)】{$infourl}{$infotitle}【/likepage】
{$showinfo_content(id,lenstr)} 得到指定ID的内容(iD可以为数字或则标识)，lenstr为字符个数。0为全部
{$field(字段)} 具体的字段内容


文章中心：

文章列表： 
【articlelist(channelid, arrchildid|classid, IncludeChild, ItemNum, IsHot, IsElite, OrderType, UsePage, TitleLen, ContentLen, IncludePic)】【Cols=2|<hr>】【Rows=2|<hr><hr>】 循环内容【/articlelist】
循环列表可用标签
{$autoid}：序号，从1开始
{$classid}：分类ID
{$classname}：分类名称
{$classurl}：分类URL
{$articleid}:文章ID
{$articleurl}：文章URL
{$author}：作者
{$copyfrom}：来源
{$hits}：点击率
{$property}：顶热荐
{$updatedate}、{$updatetime}：长日期和长时间
{$year}、{$month}、{$day}：时间
{$smalltitle}：简短标题
{$title}：标题
{$titleall}：全标题
{$content}：内容
{$intro}：简介
{$articlepic}：默认图片纯地址格式
{$articlepic(width,height)}：默认图片IMG标签格式


文章内容页：
显示相关列表：【relatearticle(ichannelid,arrClassID, ArticleNum, TitleLen, OrderType)】循环内容【/relatearticle】

循环列表可用标签：
{$smalltitle}：简短标题
{$title}：标题
{$articleurl}：文章URL
{$classid}：分类ID
{$classname}：分类名称
{$classurl}：分类URL
{$updatetime}：长时间
{$year}、{$month}、{$day}：时间



内容页可用标签字段：

{$articleid}：文章ID
{$classid}：分类的ID
{$classname}：分类名称
{$classurl}：分类URL
{$author}：作者
{$copyfrom}：来源
{$hits}：点击率
{$updatedate}、{$updatetime}长日期和长时间
{$year}、{$month}、{$day}：时间

{$keyword}：关键词
{$intro}：文章简介，主要用于meta
{$title}：标题
{$content}：内容


{$prevarticle(titleLen)}：显示上一条新闻
{$nextarticle(titleLen)}：显示下一条新闻



软件中心：
列表页标签：
【softlist(ichannelid, arrClassID, IncludeChild, ItemNum, IsHot, IsElite, OrderType, UsePage, TitleLen, ContentLen)】循环内容【/softlist】
循环内容可用标签：
{$autoid}：
{$classid}：
{$classname}：
{$classurl}：

{$softid}：
{$softname}：
{$softnameall}：
{$softurl}：
{$softintro}：
{$softdetial}：
{$property}：

{$softversion}、{$softsize}、{$softsize_m}、{$updatedate}、{$updatetime}、{$copyrighttype}、{$operatingsystem}、{$softtype}、{$softlanguage}、{$hits}、{$demourl}
{$softpic(width,height)}

内容页可用标签：
{$softid}：
{$classid}、{$classname}、{$classurl}
{$softname}、{$keyword}、{$softintro}、{$softdetial}
{$softvision}、{$softsize}、{$softsize_m}、{$operatingsystem}、{$hits}、{$author}、{$copyfrom}、{$softtype}、{$softlanguage}、$copyrighttype}、{$updatedate}、{$updatetime}、{$demourl}
{$downloadurl}
{$softpic(width,height)}
{$prevsoft(titleLen)}：显示上一软件
{$nextsoft(titleLen)}：显示下一软件

显示相关软件【relatesoft(SoftNum,TitleLen,OrderType)】{$classid}{$classname}{$classurl}{$softname}{$softnameall}{$softurl}{$updatetime}{$month}{$day}{$year}【/relatesoft】

产品中心：
【productlist(channelid,arrClassID,IncludeChild,ProductNum,IsHot,IsElite,OrderType,UsePage,TitleLen,ContentLen,IsPicUrl)】循环内容【/productlist】
循环内容：
{$autoid}
{$classid}、{$classname}、{$classurl}
{$productid}、{$producturl}、{$productname}、{$productintro}、{$productexplain}、{$productthumb(width,height)}
{$price}、{$price_original}、{$price_member}
{$producername}、{$trademarkname}、{$productmodel}、{$productstandard}、{$unit}
{$updatedate}、{$updatetime}、{$hits}、{$property}


内容页：
{$classid}、{$classname}、{$classurl}
{$productid}：
{$productname}、{$productintro}、{$productexplain}
{$price}、{$price_original}、{$price_member}
{$productnum}、{$productmodel}、{$productstandard}、{$producername}、{$trademarkname}、{$unit}
{$hits}、{$updatedate}、{$updatetime}、{$productthumb(width,height)}
{$prevproduct(titlelen)}
{$nextproduct(titlelen)}


【relateproduct(ichannelid,arrClassID, ProductNum, TitleLen, OrderType)】循环内容【/relateproduct】
{$classid}：
{$classname}：
{$classurl}：
{$productname}：
{$productnameall}：
{$producturl}：
{$updatetime}：


图片中心：
【photolist(channelid,arrClassID,IncludeChild, ItemNum, IsHot, IsElite, OrderType, UsePage, TitleLen, ContentLen, IsPicUrl)】【Cols=2|<hr>】【Rows=2|<hr><hr>】循环内容{$Number}{$ClassID}{$ClassName}{$PhotoID}{$PhotoUrl}{$UpdateDate}{$UpdateTime}{$Author}{$CopyFrom}{$Hits}{$Keyword}【/photolist】
循环内容可用标签:
{$autoid}：序号，从1开始
{$classid}：分类ID
{$classname}：分类名称
{$photoid}：图片ID
{$photourl}:
{$updatedate}、{$updatetime}
{$author}、{$copyfrom}、{$hits}、{$keyword}、{$property}
{$photoname}
{$photonameall}
{$photointro}
{$photothumb(W,H)}

【relatephoto(ichannelid,arrClassID, PhotoNum, TitleLen, OrderType)】循环内容
循环内容可用标签：
{$photoname}、{$photonameall}、{$photourl}、{$classid}、{$classname}、{$classurl}、{$updatetime}、{$year}、{$month}、{$day}
【/relatephoto】

图片内容页可用标签：

{$photoid}：图片ID
{$classid}：分类ID
{$classname}：分类名称
{$classurl}：分类的URL地址
{$author}：作者
{$copyfrom}：来源
{$hits}：点击率
{$updatedate}：更新时间长日期
{$updatetime}：更新时间长时间
{$keyword}：关键词
{$title}：图片名称
{$photointro}：图片简介
{$content}：图片详细介绍
{$prevphoto(titlelen)}：上一条图片
{$nextphoto(titlelen)}：下一条图片
【relatephoto(ichannelid,arrClassID, PhotoNum, TitleLen, OrderType)】{$photoname}{$photonameall}{$photourl}{$classid}{$classname}{$classurl}{$hits}{$updatetime}{$year}{$month}{$day}【/relatephoto】
图片地址中的可用标签：
{$photourl}：得到图片地址中的第一组图片的名称及地址
【photourllist】{$arrphotonumber}{$arrphotoname}{$arrphotourl}【\/photourllist】
{$arrphotonumber}：图片地址中的ID
{$arrphotoname}：图片地址中的名称
{$arrphotourl}：图片地址中的图片地址
{$geturlarray(arrpname,arrpvalue)}：得到一个以参数为名称的数组
arrpname：数组图片名称
arrpvalue：数组图片地址