<!--#include file="../Sys_Start.asp"-->
<!--#include file="../include/PW_FSO.asp"-->
<!--#include file="../include/PW_Channel.asp"-->
<!--#include file="../include/PW_Class.asp"-->
<!--#include file="../include/PW_Common_Front.asp"-->
<!--#include file="../include/PW_models_front.asp"-->
<%
If Action = "Save" then
	Dim GuestName, GuestTel, GuestOicq, GuestEmail, GuestFace, GuestTitle, GuestContent, CheckCode
	UserID = PE_Clng(GetForm("userid"))
	GuestName = ReplaceBadChar(GetForm("uname"))
	GuestTel = ReplaceBadChar(GetForm("tel"))
	GuestOicq = ReplaceBadChar(GetForm("oicq"))
	GuestEmail = GetForm("email")
	GuestFace = PE_Clng(GetForm("face"))
	GuestTitle = ReplaceBadChar(GetForm("title"))
	GuestContent = PE_HTMLEncode(GetForm("content"))
	CheckCode = GetForm("code")
	if CheckCode = "" then
		ErrMsg="验证码不能为空！"
		Call ShowErrMsg(ErrMsg, ComeUrl)
		Response.End()
	end if
	if CheckCode <> Session("CheckCode") then
		ErrMsg="您输入的验证码和系统产生的不一致，请重新输入!"
		Call ShowErrMsg(ErrMsg, ComeUrl)
		Response.End()
	end if
	if GuestName ="" Or GuestContent="" then
		ErrMsg="你的姓名和留言内容不能为空!"
		Call ShowErrMsg(ErrMsg, ComeUrl)
		Response.End()
	end if
	if GuestTitle = "" then
		GuestTitle = "无标题!"
	end if
	
	Conn.execute("Insert Into PW_GuestBook(UserID,GuestName,GuestTel,GuestOicq,GuestEmail,GuestFace,GuestTitle,GuestContent,GuestDatetime,GuestIP) Values("& UserID &",'"& GuestName &"','"& GuestTel &"','"& GuestOicq &"','"& GuestEmail &"','"& GuestFace &"','"& GuestTitle &"','"& GuestContent &"','"& now &"','"& UserTrueIP &"')")
	
	Call ShowSuccessMsg("留言发布成功！", ComeUrl)
Else
	Dim strParameter,strList,strTemp
	UserID=PE_Clng(GetValue("UserID"))
	strFileName="guestbook.asp"
	
	if UserID>0 then
		strFileName=strFileName&"?UserID="&UserID
	End if
	
	strHtml = ReadFileContent(Template_Dir &"plus/guestbook.html")
	
	Call ReplaceCommonLabel
	strHtml = PE_Replace(strHtml, "{$pagetitle}", "在线留言_" & sitename)
	strHtml = PE_Replace(strHtml,"{$location}",strNavPath & "&nbsp;" & strNavLink & "&nbsp;在线留言")
	strHtml = PE_Replace(strHtml,"{$showpage}",ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, "条留言", False))
	Call GetModelFront_Html
	Response.Write strHtml
	
	Call CloseConn
end If


%>