<!--#include file="../Sys_Start.asp"-->
<!--#include file="../include/PW_FSO.asp"-->
<!--#include file="../include/PW_Channel.asp"-->
<!--#include file="../include/PW_Class.asp"-->
<!--#include file="../include/PW_Common_Front.asp"-->
<!--#include file="../include/PW_models_front.asp"-->
<%

ChannelID = 0
Dim ID, AnnounceNum
Dim sqlAnnounce, rsAnnounce, strAnnounce
ID = PE_CLng(GetUrl("ID"))

PageTitle = "网站公告"

strHtml = ReadFileContent(Template_Dir &"plus/announce.html")
Call ReplaceCommonLabel
Call GetModelFront_Html

strNavPath = strNavPath & strNavLink & "&nbsp;" & PageTitle

strHtml = PE_Replace(strHtml, "{$pagetitle}", "网站公告_"&sitename)
strHTML = PE_Replace(strHTML, "{$location}", strNavPath)


sqlAnnounce = "select * from PW_Announce where IsSelected=" & PE_True & " and (OutTime=0 or OutTime>DateDiff(" & PE_DatePart_D & ",DateAndTime, " & PE_Now & ")) "
If ID > 0 Then
    sqlAnnounce = sqlAnnounce & " and ID=" & ID
End If
sqlAnnounce = sqlAnnounce & " order by ID Desc"
Set rsAnnounce = Server.CreateObject("ADODB.Recordset")
rsAnnounce.Open sqlAnnounce, Conn, 1, 1
If rsAnnounce.BOF And rsAnnounce.EOF Then
    strAnnounce = strAnnounce & "<p>&nbsp;&nbsp;没有公告</p>"
Else
    AnnounceNum = rsAnnounce.RecordCount
    Dim i
    Do While Not rsAnnounce.EOF
        strAnnounce = strAnnounce & "<table width='100%'  border='0' cellspacing='0' cellpadding='0' style='word-break:break-all;Width:fixed'>"
        strAnnounce = strAnnounce & "<tr><td align='center' height='24' class='AnnounceTitle'>" & rsAnnounce("title") & "</td></tr>"
        strAnnounce = strAnnounce & "<tr><td align='left' valign='top'>" & rsAnnounce("Content") & "</td></tr>"
        strAnnounce = strAnnounce & "<tr><td align='right' valign='top'><p align=''>" & rsAnnounce("Author") & "&nbsp;&nbsp;<br>" & FormatDateTime(rsAnnounce("DateAndTime"), 1) & "</td></tr>"
        strAnnounce = strAnnounce & "</table>"
        rsAnnounce.MoveNext
        i = i + 1
        If i < AnnounceNum Then strAnnounce = strAnnounce & "<br>"
    Loop
End If
rsAnnounce.Close
Set rsAnnounce = Nothing

strHTML = PE_Replace(strHTML, "{$announcelist}", strAnnounce)
Response.Write strHTML
Call CloseConn
%>