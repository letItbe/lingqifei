<!--#include file="CommonCode.asp"-->
<%
ArticleID = PE_CLng(GetValue("ArticleID"))
If ArticleID = 0 Then
    FoundErr = True
    ErrMsg = ErrMsg & "<li>请指定ArticleID！</li>"
End If
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
    Response.End
End If

Dim sql

Conn.Execute ("update PW_Article set Hits=Hits+1 where ArticleID=" & ArticleID)

sql = "select * from PW_Article where Deleted=" & PE_False & " and ArticleID=" & ArticleID & " and ChannelID=" & ChannelID & ""
Set rsArticle = Conn.Execute(sql)
If rsArticle.BOF And rsArticle.EOF Then
    FoundErr = True
    ErrMsg = ErrMsg & "<li>你要找的" & ChannelShortName & "不存在，或者已经被管理员删除！</li>"
Else
    ClassID = rsArticle("ClassID")
    If ClassID > 0 Then
        Call GetClass
	else
		article_template = temparticle
    End If
End If
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
    rsArticle.Close
    Set rsArticle = Nothing
    Response.End
End If

If rsArticle("LinkUrl") <> "" And rsArticle("LinkUrl") <> "http://" Then
    Response.Write "<script language='javascript'>window.location.href='" & rsArticle("LinkUrl") & "';</script>"
Else
    ArticleTitle = Replace(Replace(Replace(Replace(rsArticle("Title") & "", "&nbsp;", " "), "&quot;", Chr(34)), "&gt;", ">"), "&lt;", "<")
	

	strHtml = GetTemplate(article_template)
    Call PW_Content.GetHtml_Article
	Call GetModelFront_Html()
    Response.Write strHtml
End If
rsArticle.Close
Set rsArticle = Nothing
Set PW_Content = Nothing
Call CloseConn

%>