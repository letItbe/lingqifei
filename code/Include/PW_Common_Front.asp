<!--#include file="PW_info.asp"-->
<%
Dim strHtml, strNavPath, strNavLink, PageTitle, strPageTitle,strTemplate
Dim iMod, PrevChannelID
Dim strYear, strMonth, strDay, strListStr_Font
Dim strList_Title
strNavLink = "&gt;&gt;"
strNavPath = "您当前的位置：<a href='/'>网站首页</a>"
strYear = "年"
strMonth = "月"
strDay = "日"

Function getMonth(Timestr)
	Dim tStr
	Timestr = formatdatetime(Timestr,1)
	tStr = Right("0"&Month(Timestr),2)
	getMonth = tStr
End Function

Function getDay(Timestr)
	Dim tStr
	Timestr = formatdatetime(Timestr,1)
	tStr = Right("0"&Day(Timestr),2)
	getDay = tStr
End Function

Function getYear(Timestr)
	Dim tStr
	Timestr = formatdatetime(Timestr,1)
	tStr = Year(Timestr)
	getYear = tStr
End Function

'=================================================
'函数名：GetContent
'作  用：替换并得到内容
'参  数：contentstr ---- 内容字符串
'=================================================
function GetContent(contentstr)
	Dim tmpContentStr
	tmpContentStr=PE_Replace(contentstr,InstallDir&UploadDir&"/",InstallDir&UploadDir&"/")
	GetContent = tmpContentStr
End Function


Function GetKeywords(strSplit, strKeyword)
    GetKeywords = PE_Replace(Mid(strKeyword, 2, Len(strKeyword) - 2), "|", strSplit)
End Function

Function GetInfoList_StrNoItem(arrClassID, IsHot, IsElite)
    Dim strNoItem, strThis
    strThis = ""
    If arrClassID <> "0" Then
        strThis = "此栏目下"
    End If
    If IsHot = False And IsElite = False Then
        strNoItem = "<li>" & strThis & "没有" & ChannelShortName & "</li>"
    ElseIf IsHot = True And IsElite = False Then
        strNoItem = "<li>" & strThis & "没有热门" & ChannelShortName & "</li>"
    ElseIf IsHot = False And IsElite = True Then
        strNoItem = "<li>" & strThis & "没有推荐" & ChannelShortName & "</li>"
    Else
        strNoItem = "<li>" & strThis & "没有热门推荐" & strElite & ChannelShortName & "</li>"
    End If
    GetInfoList_StrNoItem = strNoItem
End Function

'=================================================
'函数名：GetInfoList_GetStrTitle
'作  用：得到标题
Function GetInfoList_GetStrTitle(Title, TitleLen, TitleFontType, TitleFontColor)
    Dim strTitle
    If TitleLen > 0 Then
        strTitle = GetSubStr(Title, TitleLen, True)
    Else
        strTitle = Title
    End If
    Select Case TitleFontType
    Case 1
        strTitle = "<b>" & strTitle & "</b>"
    Case 2
        strTitle = "<em>" & strTitle & "</em>"
    Case 3
        strTitle = "<b><em>" & strTitle & "</em></b>"
    End Select
    If TitleFontColor <> "" Then
        strTitle = "<font color=""" & TitleFontColor & """>" & strTitle & "</font>"
    End If
    GetInfoList_GetStrTitle = strTitle
End Function

'=================================================
'函数名：GetInfoList_GetStrContent
'作  用：得到列表页内容
function GetInfoList_GetStrContent(Content,ContentLen)
    Dim StrContent
	Content = GetContent(Content)
    If ContentLen > 0 Then
		Content = nohtml(Content)
        StrContent = GetSubStr(Content, ContentLen, True)
    Else
        StrContent = Content
    End If
	GetInfoList_GetStrContent = StrContent
End Function

Function GetClassUrl(iClassID)
    Dim strClassUrl
    If UseCreateHTML > 0 Then
        strClassUrl = ChannelUrl&"/list_"& iClassID &".html"
    Else
        strClassUrl = ChannelUrl_ASPFile & "/list.asp?ClassID=" & iClassID
    End If
    GetClassUrl = strClassUrl
End Function

'**************************************************
'函数名：GetListFileName
'作  用：获得列表文件名称(栏目有分页时)
'参  数：iClassID ---- 栏目ID
'        iCurrentPage ---- 列表当前页数
'返回值：列表文件名称
'**************************************************
Public Function GetListFileName(iClassID, iCurrentPage, iPages)
	If iCurrentPage = 1 Then
		GetListFileName = "List_" & iClassID
	Else
		GetListFileName = "List_" & iClassID & "_" & iPages - iCurrentPage + 1
	End If
End Function

Function GetArticleUrl(ByVal tUpdateTime, ByVal tArticleID, ByVal tInfoPurview)
    If IsNull(tInfoPurview) Then tInfoPurview = "0"
    If UseCreateHTML > 0 And tInfoPurview = "0" Then
        GetArticleUrl = ChannelUrl & "/" & GetItemFileName(tUpdateTime, tArticleID)
    Else
        GetArticleUrl = ChannelUrl_ASPFile & "/detial.asp?ArticleID=" & tArticleID
    End If
End Function

Function GetSoftUrl(ByVal tUpdateTime, ByVal tSoftID)
    If UseCreateHTML > 0 Then
        GetSoftUrl = ChannelUrl & "/" & GetItemFileName(tUpdateTime, tSoftID)
    Else
        GetSoftUrl = ChannelUrl_ASPFile & "/detial.asp?SoftID=" & tSoftID
    End If
End Function

Function GetPhotoUrl(ByVal tUpdateTime, ByVal tPhotoID, ByVal tInfoPurview)
    If IsNull(tInfoPurview) Then tInfoPurview = "0"
    If UseCreateHTML > 0 And tInfoPurview = "0" Then
        GetPhotoUrl = ChannelUrl & "/" & GetItemFileName(tUpdateTime, tPhotoID)
    Else
        GetPhotoUrl = ChannelUrl_ASPFile & "/detial.asp?PhotoID=" & tPhotoID
    End If
End Function

Function GetProductUrl(ByVal tUpdateTime, ByVal tProductID, ByVal tInfoPurview)
    If IsNull(tInfoPurview) Then tInfoPurview = "0"
    If UseCreateHTML > 0  And tInfoPurview = "0" Then
        GetProductUrl = ChannelUrl & "/" & GetItemFileName(tUpdateTime, tProductID)
    Else
        GetProductUrl = ChannelUrl_ASPFile & "/detial.asp?ProductID=" & tProductID
    End If
End Function

'**************************************************
'函数名：GetItemFileName
'作  用：获得项目名称
'参  数：UpdateTime ---- 更新时间
'        ItemID ---- 内容ID（ArticleID/SoftID/PhotoID)
'返回值：获得项目名称
'**************************************************
Public Function GetItemFileName(UpdateTime, ItemID)
        GetItemFileName = Year(UpdateTime) & Right("0" & Month(UpdateTime), 2) & Right("0" & Day(UpdateTime), 2) & ItemID &".html"
End Function

'**************************************************
'函数名：ReplaceKeyLink
'作  用：替换站内链接
'参  数：iText-----输入字符串
'返回值：替换后字符串
'**************************************************
Function ReplaceKeyLink(iText)
    Dim rText, rsKey, sqlKey, i, Keyrow, Keycol, LinkUrl, recStr
	Set rsKey = Server.CreateObject("Adodb.RecordSet")
	sqlKey = "Select Linkword,Linkaddress,OpenType,Replacenumber,Priority from PW_KeyLink where isUse=1 order by Priority"
	rsKey.Open sqlKey, Conn, 1, 1
	If Not (rsKey.BOF And rsKey.EOF) Then
		recStr = rsKey.GetString(, , "|||", "@@@", "")
		rsKey.Close
		Set rsKey = Nothing
	Else
		rsKey.Close
		Set rsKey = Nothing
		ReplaceKeyLink = iText
		Exit Function
	End If
    rText = iText
    Keyrow = Split(recStr, "@@@")
    For i = 0 To UBound(Keyrow) - 1
        Keycol = Split(Keyrow(i), "|||")
        If UBound(Keycol) >= 3 Then
            If Keycol(2) = 0 Then
                LinkUrl = "<a class=""keylink"" href=""" & Keycol(1) & """>" & Keycol(0) & "</a>"
            Else
                LinkUrl = "<a class=""keylink"" href=""" & Keycol(1) & """ target=""_blank"">" & Keycol(0) & "</a>"
            End If
            rText = PE_Replace_keylink(rText, Keycol(0), LinkUrl, Keycol(3))
        End If
    Next
    ReplaceKeyLink = rText
End Function

'**************************************************
'函数名：PE_Replace_keylink
'作  用：使用正则替换将HTML代码中的非HTML标签内容进行替换
'参  数：expression ---- 主数据
'        find ---- 被替换的字符
'        replacewith ---- 替换后的字符
'        replacenum  ---- 替换次数
'返回值：容错后的替换字符串,如果 replacewith 空字符,被替换的字符 替换成空
'**************************************************
Function PE_Replace_keylink(ByVal expression, ByVal find, ByVal replacewith, ByVal replacenum)
    If IsNull(expression) Or IsNull(find) Or IsNull(replacewith) Then
        PE_Replace_keylink = ""
        Exit Function
    End If

    Dim newStr
    If PE_Clng(replacenum) > 0 Then
        PE_Replace_keylink = Replace(expression, find, replacewith, 1, replacenum)
    Else
        regEx.Pattern = "([][$( )*+.?\\^{|])"  '正则表达式中的特殊字符，要进行转义
        find = regEx.Replace(find, "\$1")
        replacewith = Replace(replacewith, "$", "&#36;") '对$进行处理，特殊情况
        regEx.Pattern = "(>[^><]*)" & find & "([^><]*<)(?!/a)"
        newStr = regEx.Replace(">" & expression & "<", "$1" & replacewith & "$2")
        PE_Replace_keylink = Mid(newStr, 2, Len(newStr) - 2)
    End If
End Function


Function GetKeywords(strSplit, strKeyword)
    GetKeywords = PE_Replace(Mid(strKeyword, 2, Len(strKeyword) - 2), "|", strSplit)
End Function

'==================================================
'函数名：YN

'功能：     条件判断函数,可以根据条件运算参数的运算来输出相应的结果
'condition: 条件运算参数,根据运行结果,如果是真则输出Fir,否则输出Sec
'Fir:       条件成立的时候输出Fir的内容
'Sec :      条件不成立的时候输出Sec的内容
'==================================================

Function YN(Condition, Fir, Sec)
    If Condition = "" Or IsNull(Condition) Then '条件判断参数为空,则返回Sec的内容
        YN = Sec
	Elseif LCase(Condition)="true" Then
	    YN=Fir 
	Elseif LCase(Condition)="false" Then
	    YN=Sec
    Else
        regEx.Pattern = "^[0-9\<\>\=\%\+\-\*\/\""]+$"    '匹配只是数字还有运算符
        Dim Temp, result
        Temp = regEx.Test(Condition)  '判断是否只有数字和运算符
        If Temp = True Then           '如果只有数字和运算符
		    Condition = Replace(Condition,"%"," mod ")
            result = Eval(Condition)  '执行算术运算
            If (result) Then
                YN = Fir           '计算结果为真,返回条件1
            Else
                YN = Sec             '计算结果为假,返回条件2
            End If
        ElseIf InStr(Condition, "=") Then   '字符串允许等于判断

            Dim Tempequal
            Tempequal = Split(Condition, "=")
            If Tempequal(0) = Tempequal(1) Then
                YN = Fir
            Else
                YN = Sec
            End If
        ElseIf InStr(Condition, "<>") Then   '字符串允许不等于判断
            Dim Tempuneuqal
            Tempuneuqal = Split(Condition, "<>")
            If Tempuneuqal(0) <> Tempuneuqal(1) Then
                YN = Fir
            Else
                YN = Sec
            End If

        Else                            '其他情况都设置成非法参数
            YN = "参数类型不正确"
        End If
    End If
End Function

'**************************************************
'函数名：GetTemplate
'作  用：获得模版内容
'参  数：Tempdir ---- 模版路径
Function GetTemplate(Tempdir)
	if trim(Tempdir) = "" then
		GetTemplate = "找不到模版！"
		exit function
	end if
	Tempdir = Replace(Tempdir,"{@TemplateDir}",Template_Dir)
	GetTemplate = ReadFileContent(Tempdir)
end Function
'==================================================
'函数名：IsLogin
'功能： 判断当前用户是否登录,是的话返回第一个参数,否则返回第二个参数
'==================================================

Function IsLogin(str,Tips)
    If CheckUserLogined() = True Then
        IsLogin = str
    Else
        IsLogin = Tips
    End If
End Function

'==================================================
'函数名：ShowMenuList
'功能：循环导航栏目列表
Function ShowMenuList(TempArr,repeatStr)
	Dim SqlMenu, rsMenu,StrMenuList
	Dim MenuID,spaceStr
	Dim AutoID, totalMenu
	AutoID = 0
	If Trim(TempArr) = "" then
		spaceStr = ""
		MenuID = ""
	Else
		If Instr(TempArr,",") > 0 then
			TempArr = Split(TempArr,",")
			MenuID = TempArr(0)
			spaceStr = TempArr(1)
		End if	
	End if
	SqlMenu = "Select * from PW_Menu where 1=1 "
	If MenuID <> "" and PE_Clng(MenuID) <> 0 then
		SqlMenu = SqlMenu & " And MenuID in (" & FilterArrNull(MenuID, ",") & ")"
	End if
	SqlMenu = SqlMenu & " Order by MenuOrder asc"
	Set rsMenu = Server.CreateObject("Adodb.recordset")
	rsMenu.open SqlMenu,conn,1,3
	If rsMenu.Bof and rsMenu.Eof then
		StrMenuList =""
	Else
		totalMenu = rsMenu.RecordCount
		Do While Not rsMenu.eof
			StrMenuList = StrMenuList & repeatStr 
			StrMenuList = PE_Replace(StrMenuList,"{$autoid}",AutoID)
			StrMenuList = PE_Replace(StrMenuList,"{$menuname}",rsMenu("MenuName"))
			StrMenuList = PE_Replace(StrMenuList,"{$menuurl}",rsMenu("MenuUrl"))
			StrMenuList = PE_Replace(StrMenuList,"{$menualt}",rsMenu("MenuIntro"))
			If AutoID + 1 < totalMenu Then
				StrMenuList = StrMenuList & spaceStr
			End If
			rsMenu.MoveNext
			AutoID = AutoID + 1
		Loop
	End If
	rsMenu.Close
	Set rsMenu = Nothing
	ShowMenuList = StrMenuList
End Function

Function ShowChannel(TempArr,repeatStr)
	Dim cRs,strChannel,tchannelurl
	TempArr = PE_Clng(Trim(TempArr))
	if TempArr<=0 then
		ShowChannel = "频道参数指定错误！"
		exit function
	End if
	Set cRs = Conn.execute("Select * from PW_Channel Where ChannelID="&TempArr&" and Disabled=false")
	If cRs.Bof and cRs.Eof then
		ShowChannel = "没有找到改频道"
		exit function
	Else
		Do while Not cRs.eof
			strChannel = strChannel & repeatStr
			if UseCreateHTML > 0 then
				tchannelurl = InstallDir& HTMLDIR &"/"&cRs("ChannelDir")&"/"
			ELse
				tchannelurl = InstallDir&cRs("ChannelDir")&"/"
			End if
			strChannel = PE_Replace(strChannel,"{$channelurl}",tchannelurl)
			strChannel = PE_Replace(strChannel,"{$channelname}",cRs("ChannelName"))
			cRs.movenext
		loop
	End if
	cRs.Close
	Set cRs = Nothing
	ShowChannel = strChannel
End Function

'==================================================
'函数名：ShowAnnounce
'作  用：显示公告
'参  数：AnnNum 公告条数
'        titleLen 公告标题长度 0为显示全部
'        ContentLen 公告内容长度 0为显示全部
'        PageNum   每页多少条 0为不分页
'        repeatStr  循环的字符串 可包含标签
'==================================================
function ShowAnnounce(AnnNum,titleLen,ContentLen,PageNum,repeatStr)
	Dim sqlAnnounce, rsAnnounce,strAnnounce
	Dim titleStr,ContentStr,timeStr
	PageNum = PE_Clng(PageNum)
    If AnnNum > 0 And AnnNum <= 20 Then
        sqlAnnounce = "select top " & AnnNum
    Else
        sqlAnnounce = "select "
    End If
	Set rsAnnounce=server.CreateObject("Adodb.recordset")
    sqlAnnounce = sqlAnnounce & " * from PW_Announce where IsSelected=" & PE_True & " and (OutTime=0 or OutTime>DateDiff(" & PE_DatePart_D & ",DateAndTime, " & PE_Now & ")) order by ID Desc"
    rsAnnounce.open sqlAnnounce,conn,1,1
    If rsAnnounce.BOF And rsAnnounce.EOF Then
        strAnnounce = "&nbsp;&nbsp;没有公告"
    Else
		if PageNum>0 then
			totalPut = rsAnnounce.RecordCount
			MaxPerPage=PageNum
			If CurrentPage < 1 Then
				CurrentPage = 1
			End If
			If (CurrentPage - 1) * MaxPerPage > totalPut Then
				If (totalPut Mod MaxPerPage) = 0 Then
					CurrentPage = totalPut \ MaxPerPage
				Else
					CurrentPage = totalPut \ MaxPerPage + 1
				End If
			End If
			If CurrentPage > 1 Then
				If (CurrentPage - 1) * MaxPerPage < totalPut Then
					rsAnnounce.Move (CurrentPage - 1) * MaxPerPage
				Else
					CurrentPage = 1
				End If
			End If
		End if
        Dim AnnounceNum
        AnnounceNum = 0
		Do while not rsAnnounce.eof
			strAnnounce = strAnnounce&repeatStr
			if titleLen>0 then
				titleStr = GetSubStr(rsAnnounce("Title"),titleLen,True)
			else
				titleStr = rsAnnounce("Title")
			end if
			if ContentLen>0 then
				ContentStr = GetSubStr(nohtml(rsAnnounce("Content")),ContentLen,True)
			else
				ContentStr = GetContent(rsAnnounce("Content"))
			end if
			timeStr = rsAnnounce("DateAndTime")
			strAnnounce = PE_Replace(strAnnounce,"{$announcetitle}",titleStr)
			strAnnounce = PE_Replace(strAnnounce,"{$announcecontent}",ContentStr)
			strAnnounce = PE_Replace(strAnnounce,"{$announcetime}",timeStr)
			strAnnounce = PE_Replace(strAnnounce,"{$announceurl}",strInstallDir&"plus/Announce.asp?Vid="&rsAnnounce("ID"))
		rsAnnounce.movenext
			if PageNum>0 then
				AnnounceNum = AnnounceNum + 1
				If AnnounceNum >= PageNum Then Exit Do
			end if
		loop
	end if
	rsAnnounce.close
	set rsAnnounce=Nothing
	ShowAnnounce = strAnnounce
End function
'==================================================
'函数名：GetVote
'作  用：显示网站调查
'参  数：无
'==================================================
Function GetVote()
    Dim strVoteBody
	Dim sqlVote, rsVote, i, strVote
	sqlVote = "select * from PW_Vote where IsSelected=" & PE_True & " order by ID Desc"
	Set rsVote = Conn.Execute(sqlVote)
	If rsVote.BOF And rsVote.EOF Then
		strVote = "&nbsp;没有任何调查"
	Else
		Dim j: j = 1
		Dim strVoteContent
		strVoteContent = "<form id='VoteForm{$lid}' name='VoteForm{$lid}' method='post' action='{$strInstallDir}plus/vote.asp' target='_blank'><div class='vt'>{$Title}</div><div class='vc'><ul>{$VoteBody}</ul><input name='VoteType' type='hidden'value='{$VoteType}'><input name='Action' type='hidden' value='Vote'><input name='ID' type='hidden' value='{$ID}'></div><div align='center' class='submit'><a href='javascript:VoteForm{$lid}.submit();'><img src='{$strInstallDir}images/Vote/voteSubmit.gif' width='52' height='18' border='0'></a><a href='{$strInstallDir}plus/Vote.asp?ID={$ID}&Action=Show' target='_blank'><img src='{$strInstallDir}images/Vote/voteView.gif' width='52' height='18' border='0'></a></div></form>"
			
		Do While Not rsVote.EOF
			If rsVote("VoteType") = "Single" Then
				strVoteBody = ""
				For i = 1 To 20
					If Trim(rsVote("Select" & i) & "") = "" Then Exit For
					strVoteBody = strVoteBody & "<li><input type='radio' name='VoteOption' value='" & i & "' style='border:0'>" & rsVote("Select" & i) & "</li>"
				Next
			Else
				strVoteBody = ""
				For i = 1 To 20
					If Trim(rsVote("Select" & i) & "") = "" Then Exit For
					strVoteBody = strVoteBody & "<li><input type='checkbox' id='VoteForm"& j &"vote"& i &"' onClick=""CheckNum('"&rsVote("VoteNum")&"','VoteForm"&j&"','VoteForm"& j &"vote"& i &"')""  name='VoteOption' value='" & i & "' style='border:0'>" & rsVote("Select" & i) & "</li>"
				Next
			End If
			strVote = strVote & Replace(Replace(Replace(Replace(Replace(Replace(strVoteContent, "{$lid}", j), "{$strInstallDir}", strInstallDir), "{$Title}", rsVote("Title")), "{$VoteBody}", strVoteBody), "{$VoteType}", rsVote("VoteType")), "{$ID}", rsVote("ID"))
			rsVote.MoveNext
			j = j + 1
		Loop
	End If
	rsVote.Close
	Set rsVote = Nothing
    strVote = strVote & "<script language='javascript'>" & vbCrLf
    strVote = strVote & "function CheckNum(num,obj,item){" & vbCrLf
    strVote = strVote & " var count;" & vbCrLf
    strVote = strVote & " count=0" & vbCrLf
    strVote = strVote & " for(var i=0 ;i<document.getElementById(obj).elements.length;i++){" & vbCrLf
    strVote = strVote & " if(document.getElementById(obj).elements[i].checked==true){" & vbCrLf
    strVote = strVote & " count++;" & vbCrLf
    strVote = strVote & " if (count>num&&num!=0){" & vbCrLf
    strVote = strVote & "  alert('最多只能选择'+num+'个');" & vbCrLf
    strVote = strVote & "  document.getElementById(item).checked=false;" & vbCrLf
    strVote = strVote & "  }" & vbCrLf
    strVote = strVote & " }" & vbCrLf
    strVote = strVote & " }" & vbCrLf
    strVote = strVote & " }" & vbCrLf
    strVote = strVote & "</script>" & vbCrLf
    GetVote = strVote
End Function


'==================================================
'函数名：ShowFriendSite
'作  用：显示友情连接
'参  数：Kindid 分类ID
'        SiteNum 友情连接条数 0为所有
'        orderType   排序方式 1为升2为降
'        repeatstr   循环字符串
Function ShowFriendSite(Kindid,SiteNum,orderType, repeatstr)
	Dim RsFriendSite, SqlFriendSite, strFriendSite, i
	Dim strLogo
    Kindid = PE_Clng(Kindid)
    SiteNum = PE_Clng(SiteNum)
	if orderType<>1 And orderType<>2 then
		orderType=1
	End if
	if SiteNum > 0 then
		SqlFriendSite="Select top "& SiteNum &" * from PW_FriendSite where 1=1 "
	else
		SqlFriendSite="Select  * from PW_FriendSite where 1=1 "
	end if
	if Kindid > 0 then
		SqlFriendSite = SqlFriendSite &" and KindID="&Kindid
	end if
	SqlFriendSite = SqlFriendSite & " order by Elite "&PE_OrderType&","
    Select Case OrderType
    Case 1
        SqlFriendSite = SqlFriendSite & "ID asc"
    Case 2
        SqlFriendSite = SqlFriendSite & "ID desc"
    End Select
	Set RsFriendSite = Conn.Execute(SqlFriendSite)
	If RsFriendSite.BOF And RsFriendSite.EOF Then
		strFriendSite=""
	else
		Do While Not RsFriendSite.EOF
			If RsFriendSite("LogoUrl") = "" Or RsFriendSite("LogoUrl") = "http://" Then
				strLogo = "<img src='" & strInstallDir & "images/nologo.gif' width='88' height='31' border='0'>"
			Else
				If LCase(Right(RsFriendSite("LogoUrl"), 3)) = "swf" Then
					strLogo = "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0' width='88' height='31'><param name='movie' value='" & RsFriendSite("LogoUrl") & "'><param name='quality' value='high'><embed src='" & RsFriendSite("LogoUrl") & "' pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash' type='application/x-shockwave-flash' width='88' height='31'></embed></object>"
				Else
					strLogo = "<img src='" & RsFriendSite("LogoUrl") & "' width='88' height='31' border='0'>"
				End If
			End If
	
			strFriendSite = strFriendSite&repeatstr
			strFriendSite = PE_Replace(strFriendSite,"{$friendsitename}",RsFriendSite("SiteName"))
			strFriendSite = PE_Replace(strFriendSite,"{$friendsitelogo}",strLogo)
			strFriendSite = PE_Replace(strFriendSite,"{$friendsiteurl}",RsFriendSite("SiteUrl"))
			strFriendSite = PE_Replace(strFriendSite,"{$friendsiteintro}",RsFriendSite("SiteIntro"))
			strFriendSite = PE_Replace(strFriendSite,"{$friendsitelogourl}",RsFriendSite("LogoUrl"))
		RsFriendSite.movenext
		loop
	end if
	RsFriendSite.close
	set RsFriendSite = Nothing
	
	ShowFriendSite = strFriendSite
	
End Function

Function ShowClassList(inChannelID,inClassID,inStyleName,repeatStr)
	Dim iChannelID, iClassID,iStyleName
	Dim ChildNum
	Dim sqlClass,rsClass,strClasslist, strClassurl
	'arrTemp = PE_Replace(arrTemp," ","")
	'arrTemp = Split(arrTemp, ",")
'    If UBound(arrTemp) <> 1 Then
'        ShowClassList = "分类标签参数个数错误，请检查模板。"
'        Exit Function
'    End If
'    iChannelID = PE_Clng(arrTemp(0))
'    iClassID = PE_Clng(arrTemp(1))
    iChannelID =ChannelID
	if inClassID > 0 then
		 iClassID = inClassID
	Else
		iClassID  = ClassID
	End if 
   
	'StyleName = PE_Clng(arrTemp(2))
	ChildNum = 0	
	Call GetChannel(iChannelID)
	PrevChannelID = iChannelID	
    sqlClass = "select ClassID,ClassName,Depth,ParentPath,NextID,ClassType,Child,LinkUrl,ClassPicUrl,Meta_Description from PW_Class where ChannelID=" & iChannelID
	if iClassID > 0 then
		ChildNum = Conn.execute("Select Child from PW_Class where ClassID="&iClassID&" and ChannelID="&iChannelID)(0)
		if PE_Clng(ChildNum) > 0 then
			sqlClass = sqlClass & " and ParentID="&iClassID
		End if
	Else
		sqlClass = sqlClass & " and ParentID=0"
	End if
	sqlClass = sqlClass & " order by RootID,OrderID"
    Set rsClass = Conn.Execute(sqlClass)
    If rsClass.BOF And rsClass.EOF Then
        strClasslist = "没有任何栏目"
    Else
        Do While Not rsClass.EOF
			strClasslist = strClasslist & repeatStr
            If rsClass("ClassType") = 1 Then
                strClassurl = GetClassUrl(rsClass("ClassID"))
            Else
                strClassurl = rsClass("LinkUrl")
            End If
            If rsClass("ClassID") = ClassID Then
              	strClasslist = PE_Replace(strClasslist,"{$stylename}",inStyleName)
            Else
              	strClasslist = PE_Replace(strClasslist,"{$stylename}","")
            End If
			strClasslist = PE_Replace(strClasslist,"{$classid}",rsClass("ClassID"))
			strClasslist = PE_Replace(strClasslist,"{$classname}",rsClass("ClassName"))
			strClasslist = PE_Replace(strClasslist,"{$classpic}",rsClass("ClassPicUrl"))
			strClasslist = PE_Replace(strClasslist,"{$classurl}",strClassurl)
			strClasslist = PE_Replace(strClasslist,"{$classdesc}",rsClass("Meta_Description"))
			
			rsClass.movenext
        Loop
    End If
    rsClass.Close
    Set rsClass = Nothing
    ShowClassList = strClasslist	
End Function

Function ShowClass(iChannelID)
	Dim sqlClass,rsClass,strClass, strClassurl
    iChannelID = PE_Clng(iChannelID)
	Call GetChannel(iChannelID)
	PrevChannelID = iChannelID
	sqlClass = "Select * from PW_Class where ChannelID="& iChannelID &" and ParentID=0 order by RootID,OrderID"
	Set rsClass = Conn.Execute(sqlClass)
	if rsClass.Eof and rsClass.Bof then
		strClass = "没有任何栏目"
	Else
		strClass = "<ul class='classone'>"&vbcrlf
		Do while Not rsClass.Eof
            If rsClass("ClassType") = 1 Then
                strClassurl = GetClassUrl(rsClass("ClassID"))
            Else
                strClassurl = rsClass("LinkUrl")
            End If
			strClass = strClass&"<li><a href='"& strClassurl &"'>"&rsClass("ClassName")&"</a>"
			strClass = strClass&GetChildClass(iChannelID,rsClass("ClassID"))
			strClass = strClass&"</li>"&vbcrlf
		rsClass.movenext
		loop
		strClass = strClass&"</ul>"&vbcrlf
	End If
	ShowClass = strClass
End Function

Function GetChildClass(iChannelID,ClassID)
	Dim Rs,Sql,strChildClass,strClassUrl
	iChannelID = PE_clng(iChannelID)
	ClassID = PE_Clng(ClassID)
	Call GetChannel(iChannelID)
	
	Sql = "Select * from PW_Class Where  ChannelID="& iChannelID &" and ParentID="& ClassID &" order by RootID,OrderID"
	Set Rs = Conn.Execute(Sql)
	if Rs.eof And Rs.bof then
		strChildClass=""
	Else
		strChildClass = "<ul class='classtwo'>"&vbcrlf
		Do while Not Rs.eof
            If Rs("ClassType") = 1 Then
                strClassUrl = GetClassUrl(Rs("ClassID"))
            Else
                strClassUrl = Rs("LinkUrl")
            End If
			strChildClass = strChildClass&"<li><a href='"& strClassurl &"'>"&Rs("ClassName")&"</a>"
			strChildClass = strChildClass&GetChildClass(iChannelID,Rs("ClassID"))
			strChildClass = strChildClass&"</li>"&vbcrlf
			Rs.movenext
		Loop
		strChildClass = strChildClass&"</ul>"&vbcrlf
	End if
	GetChildClass = strChildClass
End Function
'人才招聘列表
Function GetListJob(strTemp, strList)
	Dim Pagenum, Topnum
	Dim RsJob,SqlJob
	Dim ListStr,List_JobStr, SexStr, StrLen
	strTemp = Split(strTemp, ",")
    If UBound(strTemp) <> 1 Then
        GetListJob = "人才招聘列表标签参数个数不对，请检查模板！"
        Exit Function
    End If
	Topnum = PE_Clng(strTemp(0))
	Pagenum = PE_Clng(strTemp(1))
	If Topnum > 0 then
		SqlJob = "Select top " & Topnum &" * "
	else
		SqlJob = "Select * "
	End if
	SqlJob = SqlJob &" from PW_Job where JobShow="& PE_True & " order by JobID desc"
	Set  RsJob=server.CreateObject("Adodb.recordset")
	RsJob.open SqlJob,conn,1,3
	if RsJob.eof and RsJob.bof then
		 List_JobStr="暂时没有相关信息！"
		 totalPut = 0
		 MaxPerPage=Pagenum
	else
		if Pagenum > 0 then
			MaxPerPage=Pagenum
			totalPut = RsJob.RecordCount
			If CurrentPage < 1 Then
				CurrentPage = 1
			End If
			If (CurrentPage - 1) * MaxPerPage > totalPut Then
				If (totalPut Mod MaxPerPage) = 0 Then
					CurrentPage = totalPut \ MaxPerPage
				Else
					CurrentPage = totalPut \ MaxPerPage + 1
				End If
			End If
			If CurrentPage > 1 Then
				If (CurrentPage - 1) * MaxPerPage < totalPut Then
					RsJob.Move (CurrentPage - 1) * MaxPerPage
				Else
					CurrentPage = 1
				End If
			End If
	
			Dim JobNum
			JobNum = 0
		end if

        Do While Not RsJob.EOF	
			SexStr=PE_Clng(RsJob("JobSex"))
			Select case SexStr
				Case 0
					SexStr="男"
				Case 1
					SexStr="女"
				Case 2
					SexStr="男女不限"
			End Select
			ListStr=PE_Replace(strList,"{$joburl}",InstallDir&"plus/job.asp?Action=Show&ID="&RsJob("JobID"))
			ListStr=PE_Replace(ListStr,"{$jobname}",RsJob("JobName"))
			ListStr=PE_Replace(ListStr,"{$jobnum}",RsJob("JobRequireNum"))
			ListStr=PE_Replace(ListStr,"{$jobsex}",SexStr)
			ListStr=PE_Replace(ListStr,"{$jobage}",RsJob("JobAge"))
			ListStr=PE_Replace(ListStr,"{$jobsalary}",RsJob("JobSalary"))
			ListStr=PE_Replace(ListStr,"{$jobaddress}",RsJob("JobAddress"))
			ListStr=PE_Replace(ListStr,"{$jobtime}",RsJob("JobDate"))

			regEx.Pattern = "\{\$jobdetial\((.*?)\)\}"
			Set Matches = regEx.Execute(strHtml)
			For Each Match In Matches
				StrLen = PE_CLng(Match.SubMatches(0))
				if StrLen > 0 then
					ListStr=PE_Replace(ListStr,Match.value,GetSubStr(RsJob("JobDetial"),StrLen,True))
				else
					ListStr=PE_Replace(ListStr,Match.value,RsJob("JobDetial"))
				End if
			Next
			List_JobStr=List_JobStr & ListStr
		RsJob.movenext
		if Pagenum > 0 then
			JobNum = JobNum + 1
			If JobNum >= MaxPerPage Then Exit Do
		end if
		loop
	end if
	RsJob.close
	set RsJob=Nothing
	GetListJob = List_JobStr
End Function

Function GetList_Gbook(strTemp, strList)
	Dim Topnum,Pagenum
	Dim RsGuest,SqlGuest,QuerySql
	Dim ListStr,List_GbookStr
	strTemp = Split(strTemp, ",")
    If UBound(strTemp) <> 1 Then
        GetList_Gbook = "留言列表标签参数个数不对，请检查模板！"
        Exit Function
    End If
	Topnum = PE_Clng(strTemp(0))
	Pagenum = PE_Clng(strTemp(1))
	QuerySql=""
	if UserID>0 then
		QuerySql = QuerySql& " and UserID="&UserID
	End if
	if Topnum > 0 then
		SqlGuest = "Select top "& Topnum
	Else
		SqlGuest = "Select "
	End if
	SqlGuest = SqlGuest&" * from PW_GuestBook where GuestIsPassed="& PE_True & QuerySql &" order by GuestDatetime desc,GuestID desc"
	Set  RsGuest=server.CreateObject("Adodb.recordset")
	RsGuest.open SqlGuest,conn,1,3
	if RsGuest.eof and RsGuest.bof then
		 List_GbookStr="暂时没有相关信息！"
		 totalPut = 0
		 MaxPerPage=Pagenum
	else
		if Pagenum > 0 then
			MaxPerPage=Pagenum
			totalPut = RsGuest.RecordCount
			If CurrentPage < 1 Then
				CurrentPage = 1
			End If
			If (CurrentPage - 1) * MaxPerPage > totalPut Then
				If (totalPut Mod MaxPerPage) = 0 Then
					CurrentPage = totalPut \ MaxPerPage
				Else
					CurrentPage = totalPut \ MaxPerPage + 1
				End If
			End If
			If CurrentPage > 1 Then
				If (CurrentPage - 1) * MaxPerPage < totalPut Then
					RsGuest.Move (CurrentPage - 1) * MaxPerPage
				Else
					CurrentPage = 1
				End If
			End If
	
			Dim GuestNum
			GuestNum = 0
		end if
		Dim SexStr,GuestReplyStr
        Do While Not RsGuest.EOF	
			ListStr=PE_Replace(strList,"{$gname}",RsGuest("GuestName"))
			ListStr=PE_Replace(ListStr,"{$gtitle}",RsGuest("GuestTitle"))
			ListStr=PE_Replace(ListStr,"{$gtel}",RsGuest("GuestTel"))
			ListStr=PE_Replace(ListStr,"{$goicq}",RsGuest("GuestOicq"))
			ListStr=PE_Replace(ListStr,"{$gmail}",RsGuest("GuestEmail"))
			ListStr=PE_Replace(ListStr,"{$gtime}",RsGuest("GuestDatetime"))
			ListStr=PE_Replace(ListStr,"{$gip}",RsGuest("GuestIP"))
			ListStr=PE_Replace(ListStr,"{$gface}",Right("0"&RsGuest("GuestFace"),2))
			ListStr=PE_Replace(ListStr,"{$gcontent}",RsGuest("GuestContent"))
			if RsGuest("GuestReply")="" Or IsNull(RsGuest("GuestReply"))  then
				GuestReplyStr=""
			else
				GuestReplyStr="<table cellpadding=0 cellspacing=0 border=0 width=100% align=center class='Admin_reply'><tr><td class='Admin_reply_title'>管理员于 <span class=Admin_reply_time>"& RsGuest("GuestReplyDatetime") &"</span>回复：</td></tr><tr><td class='Admin_replycontent'>"& RsGuest("GuestReply") &"</td></tr></table>"
			end if
			ListStr=PE_Replace(ListStr,"{$greply}",GuestReplyStr)
			List_GbookStr=List_GbookStr+ListStr
		RsGuest.movenext
		if Pagenum > 0 then
			GuestNum = GuestNum + 1
			If GuestNum >= MaxPerPage Then Exit Do
		end if
		loop
	end if
	RsGuest.close
	set RsGuest=Nothing
	GetList_Gbook = List_GbookStr
End Function



Function GetTemplate(tempfile)
	if tempfile="" or isnull(tempfile) then
		GetTemplate = "找不到指定的模板！"
		exit function
	end if
	tempfile = Replace(tempfile,"{@TemplateDir}",Template_Dir)
	GetTemplate = ReadFileContent(tempfile)
End Function



Function GetIncludeFileContent()
	Dim strTemp
	'替换包含文件标签
	regEx.Pattern = "\{@Include\((.*?)\)\}"
    Set Matches = regEx.Execute(strHtml)
	For Each Match In Matches
		strTemp = Match.SubMatches(0)
		strHtml = Replace(strHtml, Match.Value, ReadFileContent(InStallDir&"template/"& TemplateDir &"/"& strTemp ))
	Next
	
	if regEx.Test(strHtml) then
		strHtml = GetIncludeFileContent()
	End if
	GetIncludeFileContent = strHtml
End Function



Sub ReplaceCommonLabel()
	Dim strTemp,arrTemp,LoopTemp
	Dim strMenuList,strAnnounce,strFriendSite,strClasslist, strChannel
	'替换包含文件
	strHtml = GetIncludeFileContent()
	
    strHtml = PE_Replace(strHtml, "{$installdir}", InStallDir)
    strHtml = PE_Replace(strHtml, "{$channeldir}", InStallDir&ChannelDir&"/")
    strHtml = PE_Replace(strHtml, "{$installdir_template}", Template_Dir)
    strHtml = PE_Replace(strHtml, "{$addir}", ADDir)
    strHtml = PE_Replace(strHtml, "{$sitename}", SiteName)
    strHtml = PE_Replace(strHtml, "{$companyname}", CompanyName)
    strHtml = PE_Replace(strHtml, "{$address}", Address)
	strHtml = PE_Replace(strHtml, "{$NetAddress}", NetAddress)
    strHtml = PE_Replace(strHtml, "{$contact}", contact)
    strHtml = PE_Replace(strHtml, "{$tel}", Tel)
    strHtml = PE_Replace(strHtml, "{$fax}", Fax)
    strHtml = PE_Replace(strHtml, "{$phone}", Phone)
    strHtml = PE_Replace(strHtml, "{$email}", Email)
    strHtml = PE_Replace(strHtml, "{$postcode}", PostCode)
    strHtml = PE_Replace(strHtml, "{$copyright}", Copyright)
    strHtml = PE_Replace(strHtml, "{$meta_keywords}", Meta_Keywords)
    strHtml = PE_Replace(strHtml, "{$meta_description}", Meta_Description)
	strHtml = PE_Replace(strHtml, "{$pagetitle}", SiteName)
	
    strHtml = PE_Replace(strHtml, "{$qq}", QQ)
	
    strHtml = PE_Replace(strHtml, "{$skin_css}", "<link href=""" & strInstallDir & "template/Style.css"" rel=""stylesheet"" type=""text/css"">")
    '替换{$yn(Condition,Fir,Sec)}标签
    Dim strYN
    regEx.Pattern = "\{\$yn\((.*?)\)\}"
    Set Matches = regEx.Execute(strHtml)
    For Each Match In Matches
    arrTemp = Split(Match.SubMatches(0), ",")
	If UBound(arrTemp) <> 2 Then 
		strYN = "函数式标签：{$yn(参数列表)}的参数个数不对。请检查模板中的此标签。"
	Else
		strYN = YN(arrTem(0),arrTem(1),arrTem(2)) 
	End If
	strHtml = PE_Replace(strHtml, Left(Match.value,len(Match.value)-1), strYN)
    Next
	
	If UserLogined = False Then
		strHtml = PE_Replace(strHtml, "{$truename}", "")
		strHtml = PE_Replace(strHtml, "{$u_email}", "")
		strHtml = PE_Replace(strHtml, "{$u_phone}", "")
	Else
		Call GetUser(UserID)
		strHtml = PE_Replace(strHtml, "{$truename}", UserName)
		strHtml = PE_Replace(strHtml, "{$u_email}", U_Email)
		strHtml = PE_Replace(strHtml, "{$u_phone}", U_Phone)
	End if
	
	'替换是否登陆
    Dim strIsLogin
    regEx.Pattern = "\{\$islogin\(([\s\S]*?)\)\}"
    Set Matches = regEx.Execute(strHtml)
    For Each Match In Matches
        arrTemp = Split(Match.SubMatches(0), "$$$")
	    If UBound(arrTemp) <> 1 Then
            strIsLogin = "函数式标签：{$islogin(参数列表)}的参数个数不对。请检查模板中的此标签。"
        Else
        strIsLogin = IsLogin(arrTemp(0),arrTemp(1))
        End If
	strHtml = PE_Replace(strHtml, Match.value, strIsLogin)	
    Next
	'替换指定频道
	regEx.Pattern="\【channel\((.*?)\)\】([\s\S]*?)\【\/channel\】"
	Set Matches = regEx.Execute(strHtml)
	for Each Match In Matches
		strTemp = Match.value
		arrTemp = Match.SubMatches(0)
		LoopTemp= Match.SubMatches(1)
		strChannel = ShowChannel(arrTemp,LoopTemp)
		strHtml = PE_Replace(strHtml,strTemp,strChannel)
	Next
	'替换栏目导航列表
	regEx.Pattern="\【menulist\((.*?)\)\】([\s\S]*?)\【\/menulist\】"
	Set Matches = regEx.Execute(strHtml)
	for Each Match In Matches
		strTemp = Match.value
		arrTemp = Match.SubMatches(0)
		LoopTemp= Match.SubMatches(1)
		strMenuList = ShowMenuList(arrTemp,LoopTemp)
		strHtml = PE_Replace(strHtml,strTemp,strMenuList)
	Next
	'替换分类列表
    regEx.Pattern = "\{\$showclass\((.*?)\)\}"
	Set Matches = regEx.Execute(strHtml)
    For Each Match In Matches
		strTemp = Match.value
		arrTemp = Match.SubMatches(0)
		strHtml = PE_Replace(strHtml, strTemp, ShowClass(PE_Clng(arrTemp)))
    Next
	regEx.Pattern="\【classlist\((.*?)\)\】([\s\S]*?)\【\/classlist\】"
	Set Matches = regEx.Execute(strHtml)
	for Each Match in Matches
		strTemp = Match.value
		arrTemp=Split(Match.SubMatches(0),",")
		LoopTemp=Match.SubMatches(1)
		if UBound(arrTemp)<>2 then
			strClasslist = "标签classlist参数不正确！请检查模板中的此标签."
		Else
			strClasslist = ShowClassList(arrTemp(0),arrTemp(1),arrTemp(2),loopTemp)
		End If
		strHtml = PE_Replace(strHtml,strTemp,strClasslist)
	Next
	'替换公告
	regEx.Pattern="\【announcelist\((.*?)\)\】([\s\S]*?)\【\/announcelist\】"
	Set Matches = regEx.Execute(strHtml)
	for Each Match In Matches
		strTemp = Match.value
		arrTemp=Split(Match.SubMatches(0),",")
		LoopTemp=Match.SubMatches(1)
		if UBound(arrTemp)<>3 then
			strAnnounce="标签announcelist参数不正确！请检查模板中的此标签."
		else
			strAnnounce = ShowAnnounce(PE_Clng(arrTemp(0)),PE_Clng(arrTemp(1)),PE_Clng(arrTemp(2)),PE_Clng(arrTemp(3)),LoopTemp)
		end if
		strHtml = PE_Replace(strHtml,strTemp,strAnnounce)
	Next

	'替换友情连接
	regEx.Pattern="\【friendsitelist\((.*?)\)\】([\s\S]*?)\【\/friendsitelist\】"
	Set Matches = regEx.Execute(strHtml)
	for Each Match In Matches
		strTemp = Match.value
		arrTemp=Split(Match.SubMatches(0),",")
		LoopTemp=Match.SubMatches(1)
		if UBound(arrTemp)<>2 then
			strFriendSite="标签friendsitelist参数不正确！请检查模板中的此标签."
		else
			strFriendSite = ShowFriendSite(PE_Clng(arrTemp(0)),PE_Clng(arrTemp(1)),PE_Cbool(arrTemp(2)),LoopTemp)
		end if
		strHtml = PE_Replace(strHtml,strTemp,strFriendSite)
	Next
	'替换调查
	strHtml = PE_Replace(strHtml,"{$vote}",GetVote())
	'替换招聘列表
	regEx.Pattern = "\【joblist\((.*?)\)\】([\s\S]*?)\【\/joblist\】"
	Set Matches = regEx.Execute(strHtml)
	For Each Match In Matches
		strTemp=Match.value
		arrTemp=Match.SubMatches(0)
		LoopTemp=Match.SubMatches(1)
		strHtml = PE_Replace(strHtml,strTemp,GetListJob(arrTemp,LoopTemp))
	Next
	'替换留言列表
	regEx.Pattern = "\【guestlist\((.*?)\)\】([\s\S]*?)\【\/guestlist\】"
	Set Matches = regEx.Execute(strHtml)
	For Each Match In Matches
		strTemp=Match.value
		arrTemp=Match.SubMatches(0)
		LoopTemp=Match.SubMatches(1)
		strHtml = PE_Replace(strHtml,strTemp,GetList_Gbook(arrTemp,LoopTemp))
	Next
	'替换单页内容
	strHtml = GetInfo_Html(strHtml)
	
End Sub

%>