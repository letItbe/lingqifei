<%

Function SaveAccept_Job()
	Dim Jobname,Name,Sex,Marryed,Birthday,Stature,School,Studydegree,Specialty,GradTime,Address,Edulevel,Experience,Phone,Email,CheckCode
	Dim oRs,Sql
	CheckCode = ReplaceBadChar(GetForm("CheckCode"))
    If CheckCode = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>验证码不能为空！</li>"
    End If
	If CheckCode <> Session("CheckCode") then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>您输入的验证码和系统产生的不一致，请重新输入。</li>"		
	End If
	If Action <>"SaveAcc" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>本页面禁止从外部提交数据！</li>"	
	End If
    If FoundErr = True Then
        Exit function
    End If
	
	Jobname = ReplaceBadChar(GetForm("Jobname"))
	Name = ReplaceBadChar(GetForm("Name"))
	Sex = ReplaceBadChar(GetForm("Sex"))
	Marryed = ReplaceBadChar(GetForm("Marryed"))
	Birthday = ReplaceBadChar(GetForm("Birthday"))
	Stature = ReplaceBadChar(GetForm("Stature"))
	School = ReplaceBadChar(GetForm("School"))
	Studydegree = ReplaceBadChar(GetForm("Studydegree"))
	Specialty = ReplaceBadChar(GetForm("Specialty"))
	GradTime = ReplaceBadChar(GetForm("GradTime"))
	Address = ReplaceBadChar(GetForm("Address"))
	Edulevel = PE_HTMLEncode(GetForm("Edulevel"))
	Experience = PE_HTMLEncode(GetForm("Experience"))
	Phone = ReplaceBadChar(GetForm("Phone"))
	Email = ReplaceBadChar(GetForm("Email"))
	If Jobname="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>职位名称不能为空！</li>"		
	End if
	If Name="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>姓名不能为空！</li>"		
	End if	
	If Sex="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>请选择性别！</li>"		
	End if	
	If Marryed="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>请选择婚姻状况！</li>"		
	End if	
	If Birthday="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>请输入出生日期！</li>"		
	End if	
	If Stature="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>请输入身高！</li>"		
	End if	
	If Studydegree="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>学历不能为空！</li>"		
	End if		
	If Experience="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>工作经验不能为空！</li>"		
	End if	
	If Phone="" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<br><li>联系电话不能为空！</li>"		
	End if	
    If FoundErr = True Then
        Exit function
    End If
	Set oRs=Server.CreateObject("Adodb.recordset")
	Sql="Select * from PW_Sys_AcceptJob where 1=0 order by ID desc"
	oRs.open Sql,conn,1,3
	oRs.addnew
	oRs("JobName")=Jobname
	oRs("Name")=Name
	oRs("Sex")=Sex
	oRs("Birthday")=Birthday
	oRs("Stature")=Stature
	oRs("Marryed")=Marryed
	oRs("School")=School
	oRs("Studydegree")=Studydegree
	oRs("Specialty")=Specialty
	oRs("GradTime")=GradTime
	oRs("Address")=Address
	oRs("Edulevel")=Edulevel
	oRs("Experience")=Experience	
	oRs("Phone")=Phone
	oRs("Email")=Email	
	oRs("Adddate")=Date()
	oRs.update
	oRs.close
	Set oRs=Nothing
	Response.write "<script language='javascript'>alert('谢谢你对本站的支持！\n你的简历我们已经收到,我们会尽快和你取得联系！');location.href='"& Post_SuccessPage &"'</script>"
End Function

If FoundErr=True Then
	Call WriteErrMsg(ErrMsg,Post_ErrorPage)
End if
%>
