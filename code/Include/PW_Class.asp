<%
'定义栏目设置相关的变量
Dim ClassID, ClassName,RootID, Depth, ParentID, ParentPath, Child, arrChildID, ClassPicUrl,list_template, article_template
Dim ItemCount, ClassShowType
Dim Meta_Keywords_Class, Meta_Description_Class


Sub GetClass()
    ClassName = ""
    Meta_Keywords_Class = ""
    Meta_Description_Class = ""
    ParentID = 0
    ParentPath = "0"
    Child = 0
    arrChildID = ""
    MaxPerPage = 20
    If ClassID > 0 Then
        Dim tClass
        Set tClass = Conn.Execute("select * from PW_Class where ChannelID=" & ChannelID & " and ClassID=" & ClassID & "")
        If tClass.BOF And tClass.EOF Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>找不到指定的栏目</li>"
        Else
            ClassName = tClass("ClassName")
            Meta_Keywords_Class = tClass("Meta_Keywords")
            Meta_Description_Class = tClass("Meta_Description")
            ParentID = tClass("ParentID")
            ParentPath = tClass("ParentPath")
            Child = tClass("Child")
            arrChildID = tClass("arrChildID")
            ClassPicUrl = tClass("ClassPicUrl")
			MaxPerPage = tClass("MaxPerPage")
			list_template = tClass("Templatedir")
			article_template = tClass("Templatearticle")
            strNavPath = strNavPath & "&nbsp;" & strNavLink & "&nbsp;"
            strPageTitle = strPageTitle & "_"
            If ParentID > 0 Then
                Dim sqlPath, rsPath
                sqlPath = "select ClassID,ClassName from PW_Class where ClassID in (" & ParentPath & ") order by Depth"
                Set rsPath = Conn.Execute(sqlPath)
                Do While Not rsPath.EOF
					strNavPath = strNavPath & "<a href='" & GetClassUrl(rsPath("ClassID")) & "'>" & rsPath("ClassName") & "</a>&nbsp;" & strNavLink & "&nbsp;"
                    strPageTitle = strPageTitle & rsPath("ClassName") & "_"
                    rsPath.MoveNext
                Loop
                rsPath.Close
                Set rsPath = Nothing
            End If
            strNavPath = strNavPath & "<a href='" & GetClassUrl(ClassID) & "'>" & ClassName & "</a>"
            strPageTitle = strPageTitle & ClassName
		end if
        tClass.Close
        Set tClass = Nothing
    End If
End Sub

%>