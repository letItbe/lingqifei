<%
Const MaxTotalSize = 104857600    '上传数据限制，最大上传100M
Const NoAllowExt = "asa|asax|ascs|ashx|asmx|asp|aspx|axd|cdx|cer|config|cs|csproj|idc|licx|rem|resources|resx|shtm|shtml|soap|stm|vb|vbproj|vsdisco|webinfo|php"    '不允许上传类型(黑名单)
Const NeedCheckFileMimeExt = "gif|jpg|jpeg|jpe|bmp|png|swf|mid|mp3|wmv|asf|avi|mpg|ram|rm|ra|rar|exe|doc|zip"
Dim AdminLogined
Sub Execute()
    If ObjInstalled_FSO = False Then
        Response.Write "您的服务器不支持FSO，或者FSO已经改名，所以不能上传！"
        Exit Sub
    End If	    
    If CheckLogin() = False Then
        Response.Write "请先登录！"
        Exit Sub
    End If
    Dim Forms, Files
    Dim oUpFilestream   '上传的数据流
    
    '********************************************
    '以下代码是对提交的数据进行分析
    '********************************************
    Dim RequestBinDate, sSpace, bCrLf, sInfo, iInfoStart, iInfoEnd, tStream, iStart
    Dim sFormValue, sFileName
    Dim iFindStart, iFindEnd
    Dim iFormstart, iFormEnd, sFormName
    Dim FileInfo(6)
        '代码开始
    If Request.TotalBytes < 1 Then  '如果没有数据上传
        FoundErr = True
        ErrMsg = "没有数据上传"
        Exit Sub
    End If
        
    If Request.TotalBytes > MaxTotalSize Then  '如果上传的数据超出限制大小
        FoundErr = True
        ErrMsg = "上传的数据超出限制大小"
        Exit Sub
    End If
        
    Set Forms = Server.CreateObject("Scripting.Dictionary")
    Forms.CompareMode = 1
    Set Files = Server.CreateObject("Scripting.Dictionary")
    Files.CompareMode = 1
    Set tStream = Server.CreateObject("ADODB.Stream")
    Set oUpFilestream = Server.CreateObject("ADODB.Stream")
    oUpFilestream.Type = 1
    oUpFilestream.Mode = 3
    oUpFilestream.Open
    oUpFilestream.Write Request.BinaryRead(Request.TotalBytes)
    oUpFilestream.Position = 0
    RequestBinDate = oUpFilestream.Read
    iFormEnd = oUpFilestream.size
    bCrLf = ChrB(13) & ChrB(10)
    '取得每个项目之间的分隔符
    sSpace = LeftB(RequestBinDate, InStrB(1, RequestBinDate, bCrLf) - 1)
    iStart = LenB(sSpace)
    iFormstart = iStart + 2
    '分解项目
    
    Do
        iInfoEnd = InStrB(iFormstart, RequestBinDate, bCrLf & bCrLf) + 3
        tStream.Type = 1
        tStream.Mode = 3
        tStream.Open
        oUpFilestream.Position = iFormstart
        oUpFilestream.CopyTo tStream, iInfoEnd - iFormstart
        tStream.Position = 0
        tStream.Type = 2
        tStream.Charset = "gb2312"
        sInfo = tStream.ReadText
                
        '取得表单项目名称
        iFindStart = InStr(22, sInfo, "name=""", 1) + 6
        iFindEnd = InStr(iFindStart, sInfo, """", 1)
        sFormName = Mid(sInfo, iFindStart, iFindEnd - iFindStart)
            
        iFormstart = InStrB(iInfoEnd, RequestBinDate, sSpace) - 1
        If InStr(45, sInfo, "filename=""", 1) > 0 Then   '如果是文件
            '取得文件属性
            iFindStart = InStr(iFindEnd, sInfo, "filename=""", 1) + 10
            iFindEnd = InStr(iFindStart, sInfo, """" & vbCrLf, 1)
            sFileName = Mid(sInfo, iFindStart, iFindEnd - iFindStart)
            FileInfo(0) = sFormName
            FileInfo(1) = GetFileName(sFileName)
            FileInfo(2) = GetFilePath(sFileName)
            FileInfo(3) = GetFileExt(sFileName)
            iFindStart = InStr(iFindEnd, sInfo, "Content-Type: ", 1) + 14
            iFindEnd = InStr(iFindStart, sInfo, vbCr)
            FileInfo(4) = Mid(sInfo, iFindStart, iFindEnd - iFindStart)
            FileInfo(5) = iInfoEnd
            FileInfo(6) = iFormstart - iInfoEnd - 2
            Files.Add sFormName, FileInfo
        Else    '如果是表单项目
            tStream.Close
            tStream.Type = 1
            tStream.Mode = 3
            tStream.Open
            oUpFilestream.Position = iInfoEnd
            oUpFilestream.CopyTo tStream, iFormstart - iInfoEnd - 2
            tStream.Position = 0
            tStream.Type = 2
            tStream.Charset = "gb2312"
            sFormValue = tStream.ReadText
            If Forms.Exists(sFormName) Then
                Forms(sFormName) = Forms(sFormName) & ", " & sFormValue
            Else
                Forms.Add sFormName, sFormValue
            End If
        End If
        tStream.Close
        iFormstart = iFormstart + iStart + 2
        '如果到文件尾了就退出
    Loop Until (iFormstart + 2) >= iFormEnd
    RequestBinDate = ""
    Set tStream = Nothing
    '********************************************
    '数据分析结束
    '********************************************
	
    Dim SavePath, dirMonth, tmpPath
    If fso.FolderExists(Server.MapPath(InstallDir)) = False Then fso.CreateFolder Server.MapPath(InstallDir)
	Dim FileType,FieldName
    FileType = LCase(Trim(Forms("FileType")))
    FieldName = Trim(Forms("FieldName"))	
	UpFileType = Replace(UpFileType," ","")
	Dim arrFileType
	arrFileType = Split(UpFileType, "$")		
	Select Case FileType
	Case "adpic"
		UpFileType = Trim(arrFileType(0)) & "|" & Trim(arrFileType(1))
	Case "softpic","articlepic", "productpic","photopic","fieldphoto"
		UpFileType = Trim(arrFileType(0))
	Case "flinkpic"
		UpFileType = Trim(arrFileType(0)) & "|" & Trim(arrFileType(1))
	Case "photos"
		UpFileType = Trim(arrFileType(0))
	Case "soft"
		UpFileType = Trim(arrFileType(1)) & "|" & Trim(arrFileType(2)) & "|" & Trim(arrFileType(3)) & "|" & Trim(arrFileType(4))
	Case "field"
		UpFileType = Trim(arrFileType(1)) & "|" & Trim(arrFileType(2)) & "|" & Trim(arrFileType(3)) & "|" & Trim(arrFileType(4))
	Case Else
		UpFileType = ""
	End Select	
	if FileType = "adpic" then
		SavePath = InstallDir & ADDir &"/"
	else
		SavePath = InstallDir & UploadDir& "/"
	End if
	If fso.FolderExists(Server.MapPath(SavePath)) = False Then fso.CreateFolder Server.MapPath(SavePath)
    Response.Write "<html>" & vbCrLf
    Response.Write "<head>" & vbCrLf
    Response.Write "<title>上传文件结果</title>" & vbCrLf
    Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
    Response.Write "<link rel='stylesheet' type='text/css' href='../sys_manage/images/Upload.css'>" & vbCrLf
    Response.Write "</head>" & vbCrLf
    Response.Write "<body leftmargin='5' topmargin='0'>" & vbCrLf
	
	Dim FileCount,EnableUpload,i
	Dim strJS, dtNow,msg
    Dim FormNames,oFilestream,oFileInfo
	Dim IsThumb
    Dim cFileName, cFilePath, cFileExt, cFileMIME, cFileStart, cFileSize
	
    FileCount = 0
    FormNames = Files.Keys
    IsThumb = Trim(Forms("IsThumb"))
    If IsNumeric(IsThumb) Then
       IsThumb = CLng(IsThumb)
    Else
        IsThumb = 0
    End If
	
    For i = 0 To Files.Count - 1
        On Error Resume Next
        EnableUpload = False        
        dtNow = Now()
        oFileInfo = Files.Item(FormNames(i))
        cFileName = oFileInfo(1)
        cFilePath = oFileInfo(2)
        cFileExt = oFileInfo(3)
        cFileMIME = oFileInfo(4)
        cFileStart = oFileInfo(5)
        cFileSize = oFileInfo(6)
        If cFileSize < 10 Then
            FoundErr = True
            If Not (FileType = "photos") Then
                msg = "请先选择你要上传的文件！"
            End If
        Else
            If cFileSize > (MaxFileSize * 1024) Then
                msg = "文件大小超过了限制，最大只能上传" & CStr(MaxFileSize) & "K的文件！"
                FoundErr = True
            Else
                If CheckFileExt(UpFileType, cFileExt) = False Or CheckFileExt(NoAllowExt, cFileExt) = True Or IsValidStr(cFileExt) = False Then
                    FoundErr = True
                    If cFileName <> "" Then
                        If (FileType = "photos") Then
                            Response.Write "<li>第 " & i + 1 & " 个文件不允许上传！\n\n只允许上传这几种文件类型：" & UpFileType & "</li>"
                        Else
                            msg = "这种文件类型不允许上传！\n\n只允许上传这几种文件类型：" & UpFileType
                        End If
                    End If
                Else
                    If Left(LCase(cFileMIME), 5) = "text/" And CheckFileExt(NeedCheckFileMimeExt, cFileExt) = True Then
                        FoundErr = True
                        If (FileType = "photos") Then
                            Response.Write "<li>第 " & i + 1 & " 个文件是用文本文件伪造的图片文件或压缩文件，为了系统安全，不允许上传这种类型的文件！</li>"
                        Else
                            msg = "为了系统安全，不允许上传用文本文件伪造的图片文件！"
                        End If
                    Else
                        EnableUpload = True
                    End If
                End If
            End If
        End If

        If EnableUpload = True Then
            dirMonth = Year(dtNow) & Right("0" & Month(dtNow), 2) & "/"
			if FileType = "adpic" then
            	tmpPath = SavePath & "UploadPic/"
            else
				tmpPath = SavePath & dirMonth
			End if
			If fso.FolderExists(Server.MapPath(tmpPath)) = False Then fso.CreateFolder Server.MapPath(tmpPath)
            Randomize
            strFileName = GetNumString()
            tmpPath = tmpPath & strFileName & "." & cFileExt
            
            Set oFilestream = Server.CreateObject("ADODB.Stream")
            oFilestream.Type = 1
            oFilestream.Mode = 3
            oFilestream.Open
            oUpFilestream.Position = cFileStart
            oUpFilestream.CopyTo oFilestream, cFileSize
            oFilestream.SaveToFile Server.MapPath(tmpPath)   '保存文件
            oFilestream.Close
            Set oFilestream = Nothing
            
            FileCount = FileCount + 1
            
            Select Case FileType
            Case "photos"
                Response.Write "<li>第 " & i + 1 & " 张图片上传成功！</li>" & vbCrLf
                
                strJS = strJS & "  var url" & i & "='图片地址'+(parent.document.myform.PhotoUrl.length+1)+'|" & tmpPath & "'; " & vbCrLf
                strJS = strJS & "parent.document.myform.PhotoUrl.options[parent.document.myform.PhotoUrl.length]=new Option(url" & i & ",url" & i & ");" & vbCrLf
				
				If IsThumb = i Then
					strJS = strJS & "parent.document.myform.PhotoThumb.value='" & tmpPath & "'; " & vbCrLf
				End If
            Case "soft"
                Response.Write "文件上传成功！ <a href='javascript:history.go(-1)'>继续上传</a>"
                strJS = strJS & "var url='下载地址'+(parent.document.myform.DownloadUrl.length+1)+'|" & tmpPath & "'; " & vbCrLf
                strJS = strJS & "parent.document.myform.DownloadUrl.options[parent.document.myform.DownloadUrl.length]=new Option(url,url);" & vbCrLf
                strJS = strJS & "parent.document.myform.SoftSize.value='" & CStr(Round(cFileSize / 1024)) & "';" & vbCrLf
                Exit For
            Case "adpic"
                If cFileExt = "swf" Then
                    strJS = strJS & "parent.document.myform.FlashUrl.value='" & tmpPath & "';" & vbCrLf
                    strJS = strJS & "parent.ADTypeChecked(1);" & vbCrLf
                Else
                    strJS = strJS & "parent.document.myform.ImgUrl.value='" & tmpPath & "';" & vbCrLf
                    strJS = strJS & "parent.ADTypeChecked(0);" & vbCrLf
                End If
                strJS = strJS & "history.go(-1);" & vbCrLf
                Exit For
            Case "fieldphoto" 
                Response.Write "图片上传成功！ <a href='javascript:history.go(-1)'>继续上传</a>" & vbCrLf
                strJS = strJS & "parent.document.myform."&FieldName&".value='" & tmpPath & "';" & vbCrLf	
                strJS = strJS & "history.go(-1);" & vbCrLf							
                Exit For
			Case "articlepic"	
                Response.Write "图片上传成功！ <a href='javascript:history.go(-1)'>继续上传</a>" & vbCrLf
                strJS = strJS & "parent.document.myform.DefaultPicUrl.value='" & tmpPath & "';" & vbCrLf	
                strJS = strJS & "history.go(-1);" & vbCrLf							
                Exit For
			Case "softpic"	
                Response.Write "图片上传成功！ <a href='javascript:history.go(-1)'>继续上传</a>" & vbCrLf
                strJS = strJS & "parent.document.myform.SoftPicUrl.value='" & tmpPath & "';" & vbCrLf	
                strJS = strJS & "history.go(-1);" & vbCrLf							
                Exit For
			Case "productpic"	
                Response.Write "图片上传成功！ <a href='javascript:history.go(-1)'>继续上传</a>" & vbCrLf
                strJS = strJS & "parent.document.myform.ProductThumb.value='" & tmpPath & "';" & vbCrLf	
                strJS = strJS & "history.go(-1);" & vbCrLf							
                Exit For
			Case "photopic"	
                Response.Write "图片上传成功！ <a href='javascript:history.go(-1)'>继续上传</a>" & vbCrLf
                strJS = strJS & "parent.document.myform.PhotoThumb.value='" & tmpPath & "';" & vbCrLf	
                strJS = strJS & "history.go(-1);" & vbCrLf							
                Exit For
			Case "flinkpic"	
                Response.Write "图片上传成功！ <a href='javascript:history.go(-1)'>继续上传</a>" & vbCrLf
                strJS = strJS & "parent.document.myform.LogoUrl.value='" & tmpPath & "';" & vbCrLf	
                strJS = strJS & "history.go(-1);" & vbCrLf							
                Exit For
            Case "field" 
                Response.Write "上传成功！ <a href='javascript:history.go(-1)'>继续上传</a>" & vbCrLf
                strJS = strJS & "parent.document.myform."&FieldName&".value='" & tmpPath & "';" & vbCrLf	
                strJS = strJS & "history.go(-1);" & vbCrLf							
                Exit For	
            End Select
        End If
    Next
	
    Response.Write "<SCRIPT language='javascript'>" & vbCrLf
	Response.Write strJS
	If FoundErr = True Then
		If msg <> "" Then Response.Write "alert('" & msg & "');" & vbCrLf
		Response.Write "history.go(-1);" & vbCrLf
	End If
    Response.Write "</script>"
    Response.Write "</body></html>"
	
    Forms.RemoveAll
    Set Forms = Nothing
    Files.RemoveAll
    Set Files = Nothing
    oUpFilestream.Close
    Set oUpFilestream = Nothing
    Call ClearAspFile(SavePath & dirMonth)
End Sub


Sub ShowUploadForm()
    If CheckLogin() = False Then
		Response.Write "请先登录！"
		Exit Sub
	End If
    Response.Write "<html>" & vbCrLf
    Response.Write "<head>" & vbCrLf
    Response.Write "<title>上传文件</title>" & vbCrLf
    Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
    Response.Write "<link rel='stylesheet' type='text/css' href='../sys_manage/images/Upload.css'>" & vbCrLf
    Response.Write "<SCRIPT language=javascript>" & vbCrLf
    Response.Write "function check() " & vbCrLf
    Response.Write "{" & vbCrLf
    Response.Write "    var strFileName=document.form1.FileName.value;" & vbCrLf
    Response.Write "    if (strFileName=='')" & vbCrLf
    Response.Write "    {" & vbCrLf
    Response.Write "        alert('请选择要上传的文件');" & vbCrLf
    Response.Write "        document.form1.FileName.focus();" & vbCrLf
    Response.Write "        return false;" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "</SCRIPT>" & vbCrLf
    Response.Write "</head>" & vbCrLf
    Response.Write "<body class='Filebg' leftmargin='0' topmargin='0'><table width='100%' border='0' cellpadding='0' cellspacing='0'><tr><td>" & vbCrLf
	
	Dim FileType,FieldName,i
    
	FileType = LCase(GetValue("DialogType"))
    FieldName = GetValue("FieldName")
	
	If FileType = "photos" then
		Response.Write "<form action='Upfile.asp' method='post' name='form1' enctype='multipart/form-data'>" & vbCrLf
		For i = 0 To 9
			Response.Write "  <input name='IsThumb' type='radio' value='" & i & "'"
			If i = 0 Then Response.Write " checked"
			Response.Write ">"
			Response.Write "  <input name='FileName" & i & "' type='FILE' class='FileButton' size='28'>" & vbCrLf
			If (i + 1) Mod 2 = 0 Then Response.Write "  <br>" & vbCrLf
		Next
		Response.Write "<font style='font-size:9pt'>若选中文件名前的单选框，则表示将此图片设为缩略图。</font>&nbsp;&nbsp;" & vbCrLf
		Response.Write "&nbsp;&nbsp;<input type='submit' name='Submit' value='开始上传'>" & vbCrLf
		Response.Write "  <input name='FileType' type='hidden' id='FileType' value='photos'>" & vbCrLf
    Else
    	Response.Write "<form action='Upfile.asp' method='post' name='form1' onSubmit='return check()' enctype='multipart/form-data'>" & vbCrLf
    	Response.Write "  <input name='FileName' type='FILE' class='FileButton' size='30'>" & vbCrLf
        Response.Write "  <input type='submit' name='Submit' value='上传'>" & vbCrLf
		Response.Write "  <input name='FileType' type='hidden' id='FileType' value='" & FileType & "'>" & vbCrLf
		Response.Write "  <input name='FieldName' type='hidden' id='FieldName' value='" & FieldName & "'>" & vbCrLf			
		Response.Write "</form>" & vbCrLf
    End If
    Response.Write "</td></tr></table>" & vbCrLf
    Response.Write "</body>" & vbCrLf
    Response.Write "</html>" & vbCrLf
End Sub

Function CheckLogin()
    Dim AdminName, AdminPassword, RndPassword
    Dim sqlUser,rsUser
    AdminName = ReplaceBadChar(Trim(Request.Cookies(Site_Sn)("AdminName")))
    AdminPassword = ReplaceBadChar(Trim(Request.Cookies(Site_Sn)("AdminPassword")))
    RndPassword = ReplaceBadChar(Trim(Request.Cookies(Site_Sn)("RndPassword")))
    
    If AdminName = "" Or AdminPassword = "" Or RndPassword = "" Then
        CheckLogin = False
    Else
        '验证管理员帐号及密码并检测是否为多人同时使用
        sqlUser = "select * from PW_Admin where AdminName='" & AdminName & "' and Password='" & AdminPassword & "'"
        Set rsUser = Conn.Execute(sqlUser)
        If rsUser.BOF And rsUser.EOF Then
            AdminLogined = False
        Else
            If rsUser("EnableMultiLogin") <> True And Trim(rsUser("RndPassword")) <> RndPassword Then
                AdminLogined = False
            Else
                AdminLogined = True
            End If
        End If
        rsUser.Close
        Set rsUser = Nothing
    End If
    If AdminLogined = True Then
        CheckLogin = True
        Exit Function
    End If
End Function

%>