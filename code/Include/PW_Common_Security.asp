<%
'**************************************************
'函数名：GetForm
'作  用：得到表单的获取值
'参  数：str  ----要获取的标单的名称如：（"Action"）
'返回值：表单的值
'**************************************************
Function GetForm(ByVal str)
	Dim Strer
	Strer=Trim(Request.Form(str))
	GetForm=Strer
end function 


'**************************************************
'函数名：GetUrl
'作  用：得到超链接的获取值
'参  数：str  ----要获取的链接变量的名称如：（"Action"）
'返回值：表单的值
'**************************************************
Function GetUrl(ByVal str)
	Dim Strer
	Strer=Trim(Request.QueryString(str))
	GetUrl=Strer
end function 


'**************************************************
'函数名：GetValue
'作  用：得到获取值
'参  数：str  ----要获取的链接变量的名称如：（"Action"）
'返回值：表单的值
'**************************************************
Function GetValue(ByVal str)
	Dim Strer
	Strer=Trim(Request(str))
	GetValue=Strer
end function 
'**************************************************
'函数名：PE_CBool
'作  用：将字符转为布尔弄变量
'参  数：strBool---- 字符
'返回值：True/False
'**************************************************
Function PE_CBool(strBool)
    If strBool = True Or LCase(Trim(strBool)) = "true" Or LCase(Trim(strBool)) = "yes" Or Trim(strBool) = "1" Then
        PE_CBool = True
    Else
        PE_CBool = False
    End If
End Function


'**************************************************
'函数名：PE_CLng
'作  用：将字符转为整型数值
'参  数：str1 ---- 字符
'返回值：如果传入的参数不是数值，返回0，其他情况返回对应的数值
'**************************************************
Function PE_CLng(ByVal str1)
    If IsNumeric(str1) Then
        PE_CLng = Fix(CDbl(str1))
    Else
        PE_CLng = 0
    End If
End Function


'**************************************************
'函数名：PE_CLng1
'作  用：将字符转为整型数值
'参  数：str1 ---- 字符
'返回值：如果传入的参数不是数值，返回1，其他情况返回对应的数值
'**************************************************
Function PE_CLng1(ByVal str1)
    If IsNumeric(str1) Then
        PE_CLng1 = CLng(str1)
        If PE_CLng1 <= 0 Then PE_CLng1 = 1
    Else
        PE_CLng1 = 1
    End If
End Function

'**************************************************
'函数名：PE_CDbl
'作  用：将字符转为双精度数值
'参  数：str1 ---- 字符
'返回值：如果传入的参数不是数值，返回0，其他情况返回对应的数值
'**************************************************
Function PE_CDbl(ByVal str1)
    If IsNumeric(str1) Then
        PE_CDbl = CDbl(str1)
    Else
        PE_CDbl = 0
    End If
End Function

'**************************************************
'函数名：PE_CDate
'作  用：将字符转为日期
'参  数：str1 ---- 字符
'返回值：如果参数不是日期型字符，则返回当前时间，否则返回对应的日期型数据
'**************************************************
Function PE_CDate(ByVal str1)
    If IsDate(str1) Then
        PE_CDate = CDate(str1)
    Else
        PE_CDate = Now
    End If
End Function


'**************************************************
'函数名：IsValidEmail
'作  用：检查Email地址合法性
'参  数：email ----要检查的Email地址
'返回值：True  ----Email地址合法
'        False ----Email地址不合法
'**************************************************
Function IsValidEmail(Email)
    regEx.Pattern = "^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$"
    IsValidEmail = regEx.Test(Email)
End Function


'**************************************************
'函数名：ReplaceUrlBadChar
'作  用：过滤Url中非法的SQL字符
'参  数：strChar-----要过滤的字符
'返回值：过滤后的字符
'**************************************************
Function ReplaceUrlBadChar(strChar)
    If strChar = "" Or IsNull(strChar) Then
        ReplaceUrlBadChar = ""
        Exit Function
    End If
    Dim strBadChar, arrBadChar, tempChar, i
    strBadChar = "+,',(,),<,>,[,],{,},\,;," & Chr(34) & "," & Chr(0) & ",--"
    arrBadChar = Split(strBadChar, ",")
    tempChar = strChar
    For i = 0 To UBound(arrBadChar)
        tempChar = Replace(tempChar, arrBadChar(i), "")
    Next
    tempChar = Replace(tempChar, "@@", "@")
    ReplaceUrlBadChar = tempChar
End Function


'**************************************************
'函数名：ReplaceBadChar
'作  用：过滤非法的SQL字符
'参  数：strChar-----要过滤的字符
'返回值：过滤后的字符
'**************************************************
Function ReplaceBadChar(strChar)
    If strChar = "" Or IsNull(strChar) Then
        ReplaceBadChar = ""
        Exit Function
    End If
    Dim strBadChar, arrBadChar, tempChar, i
    strBadChar = "+,',%,^,&,?,(,),<,>,[,],{,},/,\,;,:," & Chr(34) & "," & Chr(0) & ",--"
    arrBadChar = Split(strBadChar, ",")
    tempChar = strChar
    For i = 0 To UBound(arrBadChar)
        tempChar = Replace(tempChar, arrBadChar(i), "")
    Next
    tempChar = Replace(tempChar, "@@", "@")
    ReplaceBadChar = tempChar
End Function


'**************************************************
'函数名：CheckBadChar
'作  用：检查是否包含非法的SQL字符
'参  数：strChar-----要检查的字符
'返回值：True  ----字符合法
'        False ----字符不合法
'**************************************************
Function CheckBadChar(strChar)
    Dim strBadChar, arrBadChar, i
    strBadChar = "@@,+,',%,^,&,?,(,),<,>,[,],{,},/,\,;,:," & Chr(34) & ",--,union,select,insert,delete,from"
    arrBadChar = Split(strBadChar, ",")
    If strChar = "" Then
        CheckBadChar = False
    Else
        Dim tempChar
        tempChar = LCase(strChar)
        For i = 0 To UBound(arrBadChar)
            If InStr(tempChar, arrBadChar(i)) > 0 Then
                CheckBadChar = False
                Exit Function
            End If
        Next
    End If
    CheckBadChar = True
End Function

'**************************************************
'函数名：ReplaceLabelBadChar
'作  用：函数标签过滤非法的SQL字符
'参  数：strChar-----要过滤的字符
'返回值：过滤后的字符
'**************************************************
Function ReplaceLabelBadChar(strChar)
    If strChar = "" Or IsNull(strChar) Then
        ReplaceLabelBadChar = ""
        Exit Function
    End If
    Dim strBadChar, arrBadChar, tempChar, i
    strBadChar = "+,',%,^,&,?,(,),<,>,[,],{,},/,\,;,:," & Chr(34) & "," & Chr(0)
	arrBadChar = Split(strBadChar, ",")
    tempChar = strChar
    For i = 0 To UBound(arrBadChar)
        tempChar = Replace(tempChar, arrBadChar(i), "")
    Next
    tempChar = Replace(tempChar, "@@", "@")
    Dim oldString
    oldString = ""
    Do While oldString <> tempChar
        oldString = tempChar
        regEx.Pattern = "(select|union|update|insert|delete|exec|from|PW_admin|--)?"
        tempChar = regEx.Replace(tempChar, "")
    Loop
    ReplaceLabelBadChar = tempChar
End Function


Function CheckUserBadChar(strChar)
    Dim strBadChar, arrBadChar, i
    strBadChar = "',%,^,&,?,(,),<,>,[,],{,},/,\,;,:," & Chr(34) & ",*,|,"",.,#,union,select,insert,delete,from,pe_admin"
    arrBadChar = Split(strBadChar, ",")
    If strChar = "" Then
        CheckUserBadChar = False
    Else
        Dim tempChar
        tempChar = LCase(strChar)
        For i = 0 To UBound(arrBadChar)
            If InStr(tempChar, arrBadChar(i)) > 0 Then
                CheckUserBadChar = False
                Exit Function
            End If
        Next
    End If
    CheckUserBadChar = True
    
End Function
'**************************************************
'函数名：IsValidID
'作  用：检查传过来的ＩＤ是否是合法ＩＤ或者ＩＤ串
'参  数：Check_ID ---- ID 字符串
'返回值：True  ---- 合法ID
'**************************************************
Function IsValidID(Check_ID)
    Dim FixID, i
    If IsNull(Check_ID) Or Check_ID = "" Then
        IsValidID = False
        Exit Function
    End If
    FixID = Replace(Check_ID, "|", "")
    FixID = Replace(FixID, ",", "")
    FixID = Replace(FixID, "-", "")
    FixID = Trim(Replace(FixID, " ", ""))
    If FixID = "" Or IsNull(FixID) Then
        IsValidID = False
    Else
        For i = 1 To Len(FixID) Step 100
            If Not IsNumeric(Mid(FixID, i, 100)) Then
                IsValidID = False
                Exit Function
            End If
        Next
        IsValidID = True
    End If
End Function

'**************************************************
'函数名：PE_ConvertBR
'作  用：将文本区域内的<BR>替换换行
'参  数：fString ---- 要处理的字符串
'返回值：处理后的字符串
'**************************************************
Function PE_ConvertBR(ByVal fString)
    If IsNull(fString) Or Trim(fString) = "" Then
        PE_ConvertBR = ""
        Exit Function
    End If
    fString = Replace(fString, "</P><P>", Chr(10) & Chr(10))
    fString = Replace(fString, "<BR>", Chr(10))
    fString = Replace(fString, "<br>", Chr(10))
    PE_ConvertBR = fString
End Function

'**************************************************
'函数名：PE_HTMLEncode
'作  用：将html 标记替换成 能在IE显示的HTML
'参  数：fString ---- 要处理的字符串
'返回值：处理后的字符串
'**************************************************
Function PE_HTMLEncode(ByVal fString)
    If IsNull(fString) Or Trim(fString) = "" Then
        PE_HTMLEncode = ""
        Exit Function
    End If
    fString = Replace(fString, ">", "&gt;")
    fString = Replace(fString, "<", "&lt;")
    fString = Replace(fString, Chr(32), "&nbsp;")
    fString = Replace(fString, Chr(9), "&nbsp;")
    fString = Replace(fString, Chr(34), "&quot;")
    fString = Replace(fString, Chr(39), "&#39;")
    fString = Replace(fString, Chr(13), "")
    fString = Replace(fString, Chr(10) & Chr(10), "</P><P>")
    fString = Replace(fString, Chr(10), "<BR>")

    PE_HTMLEncode = fString
End Function


Function Html2Js(str)
    If str <> "" Then
        str = Replace(str, Chr(34), "\""")
        str = Replace(str, Chr(13), "\n")
        str = Replace(str, Chr(10), "\r")
    End If
    Html2Js = str
End Function


'**************************************************
'函数名：PE_HtmlDecode
'作  用：还原Html标记,配合PE_HTMLEncode 使用
'参  数：fString ---- 要处理的字符串
'返回值：处理后的字符串
'**************************************************
Function PE_HtmlDecode(ByVal fString)
    If IsNull(fString) Or Trim(fString) = "" Then
        PE_HtmlDecode = ""
        Exit Function
    End If
    fString = Replace(fString, "&gt;", ">")
    fString = Replace(fString, "&lt;", "<")
    fString = Replace(fString, "&nbsp;", " ")
    fString = Replace(fString, "&quot;", Chr(34))
    fString = Replace(fString, "&#39;", Chr(39))
    fString = Replace(fString, "<BR>", Chr(10))
    fString = Replace(fString, "</P><P>", Chr(10) & Chr(10))
    PE_HtmlDecode = fString
End Function


'**************************************************
'函数名：nohtml
'作  用：过滤html 元素
'参  数：str ---- 要过滤字符
'返回值：没有html 的字符
'**************************************************
Function nohtml(ByVal str)
    If IsNull(str) Or Trim(str) = "" Then
        nohtml = ""
        Exit Function
    End If
    regEx.Pattern = "(\<.[^\<]*\>)"
    str = regEx.Replace(str, "")
    regEx.Pattern = "(\<\/[^\<]*\>)"
    str = regEx.Replace(str, "")
    regEx.Pattern = "\[NextPage(.*?)\]"   '解决“当在文章模块的频道中发布的是图片并使用分页标签[NextPage]或内容开始的前几行就使用分页标签时，一旦使用搜索来搜索该文时，搜索页就会显示分页标签”的问题
    str = regEx.Replace(str, "")
    
    str = Replace(str, "'", "")
    str = Replace(str, Chr(34), "")
    str = Replace(str, vbCrLf, "")
    str = Trim(str)
    nohtml = str
End Function


'**************************************************
'函数名：IsValidStr
'作  用：检查字符是否在有效范围内
'参  数：str ----要检查的字符
'返回值：True  ----字符合法
'        False ----字符不合法
'**************************************************
Function IsValidStr(ByVal str)
    Dim i, c
    For i = 1 To Len(str)
        c = LCase(Mid(str, i, 1))
        If InStr("abcdefghijklmnopqrstuvwxyz1234567890", c) <= 0 Then
            IsValidStr = False
            Exit Function
        End If
    Next
    If IsNumeric(Left(str, 1)) Then
        IsValidStr = False
    Else
        IsValidStr = True
    End If
End Function


Function IsValidPhone(Phone)
    Dim i, c
    IsValidPhone = True
    For i = 1 To Len(Phone)
        c = LCase(Mid(Phone, i, 1))
        If InStr("-()", c) <= 0 And Not IsNumeric(c) Then
            IsValidPhone = False
            Exit Function
        End If
    Next
End Function

'=================================================
'函数名：FilterJS()
'作  用：过滤非法JS字符
'参  数：strInput 需要过滤的内容
'=================================================
Function FilterJS(ByVal strInput)
    If IsNull(strInput) Or Trim(strInput) = "" Then
        FilterJS = ""
        Exit Function
    End If
    Dim reContent

    ' 替换掉HTML字符实体(Character Entities)名字和分号之间的空白字符，比如：&auml    ;替换成&auml;
    regEx.Pattern = "(&#*\w+)[\x00-\x20]+;"
    strInput = regEx.Replace(strInput, "$1;")

    ' 将无分号结束符的数字编码实体规范成带分号的标准形式
    regEx.Pattern = "(&#x*[0-9A-F]+);*"
    strInput = regEx.Replace(strInput, "$1;")

    ' 将&nbsp; &lt; &gt; &amp; &quot;字符实体中的 & 替换成 &amp; 以便在进行HtmlDecode时保留这些字符实体
    'RegEx.Pattern = "&(amp|lt|gt|nbsp|quot);"
    'strInput = RegEx.Replace(strInput, "&amp;$1;")

    ' 将HTML字符实体进行解码，以消除编码字符对后续过滤的影响
    'strInput = HtmlDecode(strInput);

    ' 将ASCII码表中前32个字符中的非打印字符替换成空字符串，保留 9、10、13、32，它们分别代表 制表符、换行符、回车符和空格。
    regEx.Pattern = "[\x00-\x08\x0b-\x0c\x0e-\x19]"
    strInput = regEx.Replace(strInput, "")  
       
    oldhtmlString = ""
    Do While oldhtmlString <> strInput
        oldhtmlString = strInput
        regEx.Pattern = "(<[^>]+src[\x00-\x20]*=[\x00-\x20]*[^>]*?)&#([^>]*>)"  '过虑掉 src 里的 &#
        strInput = regEx.Replace(strInput, "$1&amp;#$2")
        regEx.Pattern = "(<[^>]+style[\x00-\x20]*=[\x00-\x20]*[^>]*?)&#([^>]*>)"  '过虑掉style 里的 &#
        strInput = regEx.Replace(strInput, "$1&amp;#$2")
        regEx.Pattern = "(<[^>]+style[\x00-\x20]*=[\x00-\x20]*[^>]*?)\\([^>]*>)"   '替换掉style中的 "\" 
        strInput = regEx.Replace(strInput, "$1/$2")  
    Loop
    ' 替换以on和xmlns开头的属性，动易系统的几个JS需要保留
    regEx.Pattern = "on(load\s*=\s*""*'*resizepic\(this\)'*""*)"
    strInput = regEx.Replace(strInput, "off$1")
    regEx.Pattern = "on(mousewheel\s*=\s*""*'*return\s*bbimg\(this\)'*""*)"
    strInput = regEx.Replace(strInput, "off$1")

    regEx.Pattern = "(<[^>]+[\x00-\x20""'/])(on|xmlns)([^>]*)>"
    strInput = regEx.Replace(strInput, "$1pe$3>")

    regEx.Pattern = "off(load\s*=\s*""*'*resizepic\(this\)'*""*)"
    strInput = regEx.Replace(strInput, "on$1")
    regEx.Pattern = "off(mousewheel\s*=\s*""*'*return\s*bbimg\(this\)'*""*)"
    strInput = regEx.Replace(strInput, "on$1")

    
    ' 替换javascript
    regEx.Pattern = "([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`'""]*)[\x00-\x20]*j[\x00-\x20]*a[\x00-\x20]*v[\x00-\x20]*a[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:"
    strInput = regEx.Replace(strInput, "$1=$2nojavascript...")

    ' 替换vbscript
    regEx.Pattern = "([a-z]*)[\x00-\x20]*=[\x00-\x20]*([`'""]*)[\x00-\x20]*v[\x00-\x20]*b[\x00-\x20]*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:"
    strInput = regEx.Replace(strInput, "$1=$2novbscript...")

    '替换style中的注释部分，比如：<div style="xss:expres/*comment*/sion(alert(x))">
    regEx.Pattern = "(<[^>]+style[\x00-\x20]*=[\x00-\x20]*[^>]*?)/\*[^>]*\*/([^>]*>)"
    strInput = regEx.Replace(strInput, "$1$2")
    ' 替换expression
    regEx.Pattern = "(<[^>]+)style[\x00-\x20]*=[\x00-\x20]*([`'""]*).*[eｅＥ][xｘＸ][pｐＰ][rｒＲ][eｅＥ][sｓＳ][sｓＳ][iｉＩ][oｏＯ][nｎＮ][\x00-\x20]*[\(\（][^>]*>"
    strInput = regEx.Replace(strInput, "$1>")

    ' 替换behaviour
    regEx.Pattern = "(<[^>]+)style[\x00-\x20]*=[\x00-\x20]*([`'""]*).*behaviour[^>]*>>"
    strInput = regEx.Replace(strInput, "$1>")
    ' 替换behavior
    regEx.Pattern = "(<[^>]+)style[\x00-\x20]*=[\x00-\x20]*([`'""]*).*behavior[^>]*>>"
    strInput = regEx.Replace(strInput, "$1>")

    ' 替换script
    regEx.Pattern = "(<[^>]+)style[\x00-\x20]*=[\x00-\x20]*([`'""]*).*s[\x00-\x20]*c[\x00-\x20]*r[\x00-\x20]*i[\x00-\x20]*p[\x00-\x20]*t[\x00-\x20]*:*[^>]*>"
    strInput = regEx.Replace(strInput, "$1>")

    ' 替换namespaced elements 不需要
    regEx.Pattern = "</*\w+:\w[^>]*>"
    strInput = regEx.Replace(strInput, "　")

    Dim oldhtmlString
    oldhtmlString = ""
    Do While oldhtmlString <> strInput
        oldhtmlString = strInput
        '实行严格过滤
        regEx.Pattern = "</*(applet|meta|xml|blink|link|style|script|embed|object|iframe|frame|frameset|ilayer|layer|bgsound|title|base)[^>]*>?"
        strInput = regEx.Replace(strInput, "　")
        '过滤掉SHTML的Include包含文件漏洞
        regEx.Pattern = "<!--\s*#include[^>]*>"
        strInput = regEx.Replace(strInput, "noshtml")
    Loop
    FilterJS = strInput
End Function


'**************************************************
'函数名：DelRightComma
'作  用：删除字符串（如："1,3,5,8"）右侧多余的逗号以消除SQL查询时出错的问题，Comma：逗号。
'参  数：str ---- 待处理的字符串
'**************************************************
Function DelRightComma(ByVal str)
    str = Trim(str)
    If Right(str, 1) = "," Then
        str = Left(str, Len(str) - 1)
    End If
    DelRightComma = str
End Function


'**************************************************
'函数名：FilterArrNull
'作  用：过滤数组空字符
'**************************************************
Function FilterArrNull(ByVal ArrString, ByVal CompartString)
    Dim arrContent, arrTemp, i

    If CompartString = "" Or ArrString = "" Then
        FilterArrNull = ArrString
        Exit Function
    End If
    If InStr(ArrString, CompartString) = 0 Then
        FilterArrNull = ArrString
        Exit Function
    Else
        arrContent = Split(ArrString, CompartString)
        For i = 0 To UBound(arrContent)
            If Trim(arrContent(i)) <> "" Then
                If arrTemp = "" Then
                    arrTemp = Trim(arrContent(i))
                Else
                    arrTemp = arrTemp & CompartString & Trim(arrContent(i))
                End If
            End If
        Next
    End If
    FilterArrNull = arrTemp
End Function
%>