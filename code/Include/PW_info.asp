<%
'==================================================
'函数名：getDefaultInfoID  {$getDefaultInfoID(标识名)}
'作  用：得到默认ID
'参  数：likestr 标识
Function getDefaultInfoID(likestr)
	Dim oRs,Sql,tid
	if likestr = "" then
		Sql="Select ID from PW_Info order by OrderID asc"
	Else
		Sql="Select ID from PW_Info where Pagelike='"& trim(likestr) &"' order by OrderID asc"
	End if
	Set oRs=conn.execute(Sql)
	if not oRs.eof then
		tid = oRs("ID")
	else
		tid = 0
	End if
	oRs.close
	set oRs=Nothing
	getDefaultInfoID = tid
End Function

'==================================================
'函数名：Showlikepage
'作  用：显示单页相关文档
'参  数：likestr 标识
'        loopstr 循环字符串
Function Showlikepage(likestr,loopstr)
	Dim oRs,Sql,tmpStr, furl
	if likestr = "" then
		Showlikepage = ""
		exit function
	end if
	Sql="Select ID,Title,filename from PW_Info where Pagelike='"& likestr &"' order by OrderID asc"
	Set oRs=conn.execute(Sql)
	if not oRs.eof then
		do while not oRs.eof
			tmpStr = tmpStr & loopstr
			If UseCreateHTML > 0 Then
				furl = InStallDir&oRs("filename")
			Else
				furl = InStallDir&"plus/info.asp?id="&oRs("ID")
			End if
			tmpStr = PE_Replace(tmpStr, "{$infourl}", furl)
			tmpStr = PE_Replace(tmpStr, "{$infotitle}", oRs("Title"))
			oRs.movenext
		loop
	else
		Showlikepage =	""
	end if
	oRs.close
	set oRs=Nothing
	Showlikepage = tmpStr
End Function


'==================={$Showinfo_Content(id,len)}===============================
'函数名：Showinfo_Content
'作  用：显示单页文档内容
'参  数：id 如果ID为数字，则查找数字，否则将查找 关联标识
Function Showinfo_Content(id,lenstr)
	Dim oRs
	If PE_Clng(id)<=0 then
		id = getDefaultInfoID(id)
	end if
	if id<=0 then
		Showinfo_Content = ""	
		exit function
	end if
	Set oRs=conn.execute("Select Content from PW_Info where ID = "&id)
	if oRs.eof and oRs.bof then
		Showinfo_Content = "没找到指定信息！"
	else
		if lenstr>0 then
			Showinfo_Content = GetSubStr(nohtml(GetContent(oRs("Content"))),lenstr,true)
		else
			Showinfo_Content = GetContent(oRs("Content"))
		end if
	end if
	oRs.close
	set oRs=Nothing
End function


'函数名：GetInfo_Html
'作  用：替换单页相关的内容为HTML
function GetInfo_Html(strHtml)
	'''''替换相同标识列表
	dim strParameter,arrTemp
    regEx.Pattern = "\【likepage\((.*?)\)\】([\s\S]*?)\【\/likepage\】"
	Set Matches = regEx.Execute(strHtml)
	For Each Match In Matches
		strParameter = Match.SubMatches(0)
		strHtml = PE_Replace(strHtml, Match.Value, Showlikepage(strParameter, Match.SubMatches(1)))
	Next
	'替换单页内容标签
	regEx.Pattern = "\{\$showinfo_content\((.*?)\)\}"
	Set Matches = regEx.Execute(strHtml)
	For Each Match In Matches
		strParameter = Replace(Match.SubMatches(0)," ","")
		arrTemp = split(strParameter,",")
		strHtml = PE_Replace(strHtml, Match.Value, Showinfo_Content(arrTemp(0),PE_Clng(arrTemp(1))))
	Next
	GetInfo_Html = strHtml
end function

%>