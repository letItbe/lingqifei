<%

Dim PhotoID, rsPhoto, PhotoName, PhotoUrl

Class Photo

'定义其它全局的变量
Private rsClass

'初始化需要用到的一些变量
Public Sub Init()
    FoundErr = False
    ErrMsg = ""
    PrevChannelID = ChannelID
    ChannelShortName = "图片"

	Call GetChannel(ChannelID)
	strNavPath = strNavPath & "&nbsp;" & strNavLink & "&nbsp;<a href='"
	If UseCreateHTML > 0 Then
		strNavPath = strNavPath & ChannelUrl & "/Index.html"
	Else
		strNavPath = strNavPath & ChannelUrl_ASPFile & "/Index.asp"
	End If
	strNavPath = strNavPath & "'>" & ChannelName & "</a>"
	strPageTitle = ChannelName&"_"&SiteName

	End Sub
Private Function GetSqlStr(iChannelID, arrClassID, IncludeChild, IsHot, IsElite, OrderType, IsPicUrl)
    Dim strSql, IDOrder
    If IsValidID(iChannelID) = False Then
        iChannelID = 0
    Else
        iChannelID = ReplaceLabelBadChar(iChannelID)
    End If  
    If IsValidID(arrClassID) = False Then
        arrClassID = 0
    Else
        arrClassID = ReplaceLabelBadChar(arrClassID)
    End If
    strSql = strSql & " from PW_Photo P left join PW_Class C on P.ClassID=C.ClassID"
    strSql = strSql & " where P.Deleted=" & PE_False & " "
    If iChannelID > 0 Then
        strSql = strSql & " and P.ChannelID=" & iChannelID
    End If
    If arrClassID <> "0" Then
        If InStr(arrClassID, ",") = 0 And IncludeChild = True Then
            Dim trs
            Set trs = Conn.Execute("select arrChildID from PW_Class where ClassID=" & PE_CLng(arrClassID) & "")
            If trs.BOF And trs.EOF Then
                arrClassID = "0"
            Else
                If IsNull(trs(0)) Or Trim(trs(0)) = "" Then
                    arrClassID = "0"
                Else
                    arrClassID = trs(0)
                End If
            End If
            Set trs = Nothing
        End If
        
        If InStr(arrClassID, ",") > 0 Then
            strSql = strSql & " and P.ClassID in (" & FilterArrNull(arrClassID, ",") & ")"
        Else
            If PE_CLng(arrClassID) > 0 Then strSql = strSql & " and P.ClassID=" & PE_CLng(arrClassID)
        End If
    End If
    If IsHot = True Then
        strSql = strSql & " and P.Hits>=" & HitsOfHot
    End If
    If IsElite = True Then
        strSql = strSql & " and P.Elite=" & PE_True
    End If

    If IsPicUrl = True Then
        strSql = strSql & " and P.PhotoThumb<>'' "
    End If

    strSql = strSql & " order by P.OnTop " & PE_OrderType & ","
    Select Case PE_CLng(OrderType)
    Case 1, 2

    Case 3
        strSql = strSql & "P.UpdateTime desc,"
    Case 4
        strSql = strSql & "P.UpdateTime asc,"
    Case 5
        strSql = strSql & "P.Hits desc,"
    Case 6
        strSql = strSql & "P.Hits asc,"
    Case Else
        
    End Select
    If OrderType = 2 Then
        IDOrder = "asc"
    Else
        IDOrder = "desc"
    End If
    strSql = strSql & "P.PhotoID " & IDOrder
    GetSqlStr = strSql
End Function







Public Function GetCustomFromTemplate(strValue)   '得到自定义列表的版面设计的HTML代码
    Dim strCustom, strParameter
	strCustom = strValue
    regEx.Pattern = "【photolist\((.*?)\)】([\s\S]*?)【\/photolist】"
    Set Matches = regEx.Execute(strCustom)
    For Each Match In Matches
        strParameter = Replace(Match.SubMatches(0), Chr(34), " ")
        strCustom = PE_Replace(strCustom, Match.value, GetCustomFromLabel(strParameter, Match.SubMatches(1)))
    Next
    GetCustomFromTemplate = strCustom
End Function



Private Function GetCustomFromLabel(strTemp, strList)
    Dim arrTemp
    Dim strPhotoPic, strPicTemp, arrPicTemp
    Dim iChannelID, arrClassID, IncludeChild, ItemNum, IsHot, IsElite,  OrderType, UsePage, TitleLen, ContentLen
    Dim iCols, iColsHtml, iRows, iRowsHtml, iNumber
    Dim IncludePic ,strlinkaddress 
    If strTemp = "" Or strList = "" Then GetCustomFromLabel = "": Exit Function

    iCols = 1: iRows = 1: iColsHtml = "": iRowsHtml = ""
    regEx.Pattern = "【(cols|rows)=(\d{1,2})\s*(?:\||｜)(.+?)】"
    Set Matches = regEx.Execute(strList)
    For Each Match In Matches
        If LCase(Match.SubMatches(0)) = "cols" Then
            If Match.SubMatches(1) > 1 Then iCols = Match.SubMatches(1)
            iColsHtml = Match.SubMatches(2)
        ElseIf LCase(Match.SubMatches(0)) = "rows" Then
            If Match.SubMatches(1) > 1 Then iRows = Match.SubMatches(1)
            iRowsHtml = Match.SubMatches(2)
        End If
        strList = regEx.Replace(strList, "")
    Next
    
    arrTemp = Split(strTemp, ",")
    If UBound(arrTemp) <> 10 Then
        GetCustomFromLabel = "自定义列表标签：【photolist(参数列表)】列表内容【/photolist】的参数个数不对。请检查模板中的此标签。"
        Exit Function
    End If
        
    Select Case Trim(arrTemp(0))
    Case "channelid"
        iChannelID = ChannelID
    Case Else
        iChannelID = PE_CLng(arrTemp(0))
    End Select

    Select Case Trim(arrTemp(1))
    Case "arrchildid"
        arrClassID = arrChildID
    Case "classid"
        arrClassID = ClassID
    Case Else
        arrClassID = arrTemp(1)
    End Select
    arrClassID = Replace(Trim(arrClassID), "|", ",")
    IncludeChild = PE_CBool(arrTemp(2))
    ItemNum = PE_CLng(arrTemp(3))
    IsHot = PE_CBool(arrTemp(4))
    IsElite = PE_CBool(arrTemp(5))
	OrderType = PE_CLng(arrTemp(6))
    UsePage = PE_CBool(arrTemp(7))
    TitleLen = PE_CLng(arrTemp(8))
    ContentLen = PE_CLng(arrTemp(9))
    If UBound(arrTemp) = 10  then
        IncludePic = PE_CBool(arrTemp(10))
    Else
        IncludePic = False	    
    End If
    FoundErr = False
    If iChannelID <> PrevChannelID Or ChannelID = 0 Then
        Call GetChannel(iChannelID)
    End If
    PrevChannelID = iChannelID
    If FoundErr = True Then
        GetCustomFromLabel = ErrMsg
        Exit Function
    End If
    
    Dim rsField, ArrField, iField
    Set rsField = Conn.Execute("select FieldName,LabelName,FieldType  from PW_Field where ChannelID=" & iChannelID & "")
    If Not (rsField.BOF And rsField.EOF) Then
        ArrField = rsField.getrows(-1)
    End If
    Set rsField = Nothing

    Dim sqlCustom, rsCustom, iCount, strCustomList, strThisClass, strLink
    iCount = 0
    sqlCustom = ""
    strThisClass = ""
    strCustomList = ""
    
    sqlCustom = "select "
    If ItemNum > 0 Then
        sqlCustom = sqlCustom & "top " & ItemNum & " "
    End If
    If ContentLen > 0 Then
        sqlCustom = sqlCustom & "P.PhotoIntro,"
    End If
    If IsArray(ArrField) Then
        For iField = 0 To UBound(ArrField, 2)
            sqlCustom = sqlCustom & "P." & ArrField(0, iField) & ","
        Next
    End If
    sqlCustom = sqlCustom & "P.PhotoID,P.ChannelID,P.ClassID,P.PhotoName,P.Keyword,P.PhotoThumb"

    sqlCustom = sqlCustom & ",P.Author,P.CopyFrom,P.UpdateTime,P.Hits,P.OnTop,P.Elite,P.InfoPurview,C.ClassName"
    sqlCustom = sqlCustom & GetSqlStr(iChannelID, arrClassID, IncludeChild, IsHot, IsElite, OrderType, IncludePic)

    Set rsCustom = Server.CreateObject("ADODB.Recordset")
    rsCustom.Open sqlCustom, Conn, 1, 1
    If rsCustom.BOF And rsCustom.EOF Then
        totalPut = 0
        strCustomList = GetInfoList_StrNoItem(arrClassID, IsHot, IsElite)
        rsCustom.Close
        Set rsCustom = Nothing
        GetCustomFromLabel = strCustomList
        Exit Function
    End If

    If UsePage = True Then
        totalPut = rsCustom.RecordCount
        If CurrentPage < 1 Then
            CurrentPage = 1
        End If
        If (CurrentPage - 1) * MaxPerPage > totalPut Then
            If (totalPut Mod MaxPerPage) = 0 Then
                CurrentPage = totalPut \ MaxPerPage
            Else
                CurrentPage = totalPut \ MaxPerPage + 1
            End If
        End If
        If CurrentPage > 1 Then
            If (CurrentPage - 1) * MaxPerPage < totalPut Then
                iMod = 0
                rsCustom.Move (CurrentPage - 1) * MaxPerPage - iMod
            Else
                CurrentPage = 1
            End If
        End If
    End If
    Do While Not rsCustom.EOF
        If iChannelID = 0 Then
            If rsCustom("ChannelID") <> PrevChannelID Then
                Call GetChannel(rsCustom("ChannelID"))
                PrevChannelID = rsCustom("ChannelID")
            End If
        End If
        strTemp = strList

        If UsePage = True Then
            iNumber = (CurrentPage - 1) * MaxPerPage + iCount + 1
        Else
            iNumber = iCount + 1
        End If

        strTemp = PE_Replace(strTemp, "{$autoid}", iNumber)
        strTemp = PE_Replace(strTemp, "{$classid}", rsCustom("ClassID"))
        strTemp = PE_Replace(strTemp, "{$classname}", rsCustom("ClassName"))
        strTemp = PE_Replace(strTemp, "{$classurl}", GetClassUrl(rsCustom("ClassID")))
		
        strTemp = PE_Replace(strTemp, "{$photoid}", rsCustom("PhotoID"))
        'If InStr(strTemp, "{$photourl}") > 0 Then strTemp = PE_Replace(strTemp, "{$photourl}", GetPhotoUrl(rsCustom("UpdateTime"), rsCustom("PhotoID"),rsCustom("InfoPurview")))
        If InStr(strTemp, "{$updatedate}") > 0 Then strTemp = PE_Replace(strTemp, "{$updatedate}", FormatDateTime(rsCustom("UpdateTime"), 2))
        strTemp = PE_Replace(strTemp, "{$updatetime}", rsCustom("updatetime"))
        strTemp = PE_Replace(strTemp, "{$author}", rsCustom("author"))
        strTemp = PE_Replace(strTemp, "{$copyfrom}", rsCustom("copyfrom"))
        strTemp = PE_Replace(strTemp, "{$hits}", rsCustom("hits"))
        strTemp = PE_Replace(strTemp, "{$keyword}", GetKeywords(",", rsCustom("Keyword")))
		if InStr(strTemp, "{$property}") >0 then
			Dim PropertyStr
			PropertyStr = ""
			if rsCustom("OnTop") = True then
				PropertyStr = PropertyStr&"<font color=""green""> 顶</font>"
			end if
			if rsCustom("Elite") = True then
				PropertyStr = PropertyStr&"<font color=""blue""> 荐</font>"
			end if
			if rsCustom("Hits") > HitsOfHot then
				PropertyStr = PropertyStr&"<font color=""red""> 荐</font>"
			end if
			strTemp = PE_Replace(strTemp, "{$property}", PropertyStr)	
		end if
        
        If TitleLen > 0 Then
            strTemp = PE_Replace(strTemp, "{$photoname}", GetSubStr(rsCustom("PhotoName"), TitleLen, True))
        Else
            strTemp = PE_Replace(strTemp, "{$photoname}", rsCustom("PhotoName"))
        End If
        strTemp = PE_Replace(strTemp, "{$photonameall}", rsCustom("PhotoName"))
        If ContentLen > 0 Then
            If InStr(strTemp, "{$photointro}") > 0 Then strTemp = PE_Replace(strTemp, "{$photointro}", Left(nohtml(rsCustom("PhotoIntro")), ContentLen))
	    Else
            strTemp = PE_Replace(strTemp, "{$photointro}", "")
        End If
        
		
        '替换图片缩略图
        regEx.Pattern = "\{\$photothumb\((.*?)\)\}"
        Set Matches = regEx.Execute(strTemp)
        For Each Match In Matches
            arrPicTemp = Split(Match.SubMatches(0), ",")
            strPhotoPic = GetPhotoThumb(Trim(rsCustom("PhotoThumb")), PE_CLng(arrPicTemp(0)), PE_CLng(arrPicTemp(1)))
            strTemp = Replace(strTemp, Match.value, strPhotoPic)
        Next
        
		
		
        If IsArray(ArrField) Then
            For iField = 0 To UBound(ArrField, 2)
                Select Case ArrField(2, iField)
                Case 8,9,10
                    strTemp = PE_Replace(strTemp, ArrField(1, iField), PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField)))))
                Case 4
                    If PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField))))="" or IsNull(PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField))))) or PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField))))="http://" Then
                        strTemp = PE_Replace(strTemp, ArrField(1, iField), "")	
                    Else 
                        strTemp = PE_Replace(strTemp, ArrField(1, iField), "<img  class='fieldImg' src='" &PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField))))&"' border=0>")	
                    End If
                Case Else
                    strTemp = PE_Replace(strTemp, ArrField(1, iField), PE_HTMLEncode(rsCustom(Trim(ArrField(0, iField)))))			
                End Select 
           Next
        End If

        strCustomList = strCustomList & strTemp
        rsCustom.MoveNext
        iCount = iCount + 1
        If iCols > 1 And iCount Mod iCols = 0 Then strCustomList = strCustomList & iColsHtml
        If iRows > 1 And iCount Mod iCols * iRows = 0 Then strCustomList = strCustomList & iRowsHtml
        If UsePage = True And iCount >= MaxPerPage Then Exit Do
    Loop
    rsCustom.Close
    Set rsCustom = Nothing
    
    GetCustomFromLabel = strCustomList
End Function



Public Function GetCorrelativeFromTemplate(strValue)  
    Dim strCorrelative, strParameter
    strCorrelative = strValue
    regEx.Pattern = "【relatephoto\((.*?)\)】([\s\S]*?)【\/relatephoto】"
    Set Matches = regEx.Execute(strCorrelative)
    For Each Match In Matches
        strParameter = Replace(Match.SubMatches(0), Chr(34), " ")
        strCorrelative = PE_Replace(strCorrelative, Match.Value, GetCorrelative(strParameter, Match.SubMatches(1)))
    Next
    GetCorrelativeFromTemplate = strCorrelative
End Function

'=================================================
'函数名：GetCorrelative
'作  用：显示相关图片 之中参数(iChannelID,arrClassID, PhotoNum, TitleLen, OrderType)
'=================================================
private Function GetCorrelative(strTemp, strList)
    Dim arrTemp
	Dim iChannelID,arrClassID,MaxNum,PhotoNum,TitleLen,OrderType
    Dim rsCorrelative, sqlCorrelative, strCorrelative, iCols, iTemp ,strlinkaddress
    Dim strKey, arrKey, i
	strCorrelative=""
    arrTemp = Split(strTemp, ",")
    If UBound(arrTemp) <> 5 Then
        GetCorrelative = "相关文章标签：【relatephoto(参数列表)】列表内容【/relatephoto】的参数个数不对。请检查模板中的此标签。"
        Exit Function
    End If
    iChannelID = Replace(iChannelID,"|",",")
    Select Case iChannelID
    Case "channelid"
        iChannelID = ChannelID
    Case else
        If IsValidID(iChannelID) = False Then
            iChannelID = 0
        End If  
    End Select	
    Select Case Trim(arrTemp(1))
    Case "arrchildid"
        arrClassID = arrChildID
    Case "classid"
        arrClassID = ClassID
    Case Else
        arrClassID = arrTemp(1)
    End Select
    arrClassID = Replace(arrClassID,"|",",")	
	PhotoNum = PE_CLng(arrTemp(2))
    TitleLen = PE_CLng(arrTemp(3))
	OrderType = PE_CLng(arrTemp(4))
    If IsValidID(arrClassID) = False Then
        arrClassID = 0
    End If  
    If PhotoNum > 0 And PhotoNum <= 20 Then
        sqlCorrelative = "select top " & PhotoNum
    Else
        sqlCorrelative = "Select Top 5 "
    End If
    strKey = Mid(rsPhoto("Keyword"), 2, Len(rsPhoto("Keyword")) - 2)
    If InStr(strKey, "|") > 1 Then
        arrKey = Split(strKey, "|")
        MaxNum = UBound(arrKey)   
        If MaxNum > 4 Then MaxNum = 4
        strKey = "((P.Keyword like '%|" & Replace(Replace(arrKey(0), "［", ""), "］", "") & "|%')"
        For i = 1 To MaxNum
            strKey = strKey & " or (P.Keyword like '%|" & Replace(Replace(arrKey(i), "［", ""), "］", "") & "|%')"
        Next
        strKey = strKey & ")"
    Else
        strKey = "(P.Keyword like '%|" & strKey & "|%')"
    End If
    sqlCorrelative = sqlCorrelative & " P.PhotoID,P.ClassID,P.PhotoName,P.UpdateTime,P.Hits,P.InfoPurview,C.ClassName from PW_Photo P left join PW_Class C on P.ClassID=C.ClassID where 1=1"
    If InStr(iChannelID, ",") > 0 Then
        sqlCorrelative = sqlCorrelative & " and P.ChannelID in (" & FilterArrNull(iChannelID, ",") & ")"
    Else
        If PE_CLng(iChannelID) > 0 Then sqlCorrelative = sqlCorrelative & " and P.ChannelID=" & PE_CLng(iChannelID)
    End If	
    If arrClassID <> "0" Then
        If InStr(arrClassID, ",") > 0 Then
            sqlCorrelative = sqlCorrelative & " and P.ClassID in (" & FilterArrNull(arrClassID, ",") & ")"
        Else
            If PE_CLng(arrClassID) > 0 Then sqlCorrelative = sqlCorrelative & " and P.ClassID=" & PE_CLng(arrClassID)
        End If
    End If
    sqlCorrelative = sqlCorrelative & " and P.Deleted=" & PE_False & ""
    sqlCorrelative = sqlCorrelative & " and " & strKey & " and P.PhotoID<>" & PhotoID & " Order by "
    Select Case PE_CLng(OrderType)
    Case 1
        sqlCorrelative = sqlCorrelative & "P.PhotoID desc"
    Case 2
        sqlCorrelative = sqlCorrelative & "P.PhotoID asc"
    Case 3
        sqlCorrelative = sqlCorrelative & "P.UpdateTime desc"
    Case 4
        sqlCorrelative = sqlCorrelative & "P.UpdateTime asc"
    Case 5
        sqlCorrelative = sqlCorrelative & "P.Hits desc"
    Case 6
        sqlCorrelative = sqlCorrelative & "P.Hits asc"
    Case Else
        sqlCorrelative = sqlCorrelative & "P.PhotoID desc"
    End Select
    Set rsCorrelative = Conn.Execute(sqlCorrelative)
    If TitleLen < 0 Or TitleLen > 255 Then TitleLen = 50
    If rsCorrelative.BOF And rsCorrelative.EOF Then
        strCorrelative = "没有相关信息！"
    Else
        Do While Not rsCorrelative.EOF
			strTemp = strList
			If TitleLen > 0 Then
				strTemp = PE_Replace(strTemp, "{$photoname}", GetSubStr(rsCorrelative("PhotoName"), TitleLen, false))
			Else
				strTemp = PE_Replace(strTemp, "{$photoname}", rsCorrelative("PhotoName"))
			End If
			strTemp = PE_Replace(strTemp, "{$photonameall}", rsCorrelative("PhotoName"))
			'strTemp = PE_Replace(strTemp, "{$photourl}", GetPhotoUrl(rsCorrelative("UpdateTime"), rsCorrelative("PhotoID"),rsCorrelative("InfoPurview")))
			strTemp = PE_Replace(strTemp, "{$classid}", rsCorrelative("ClassID"))
			strTemp = PE_Replace(strTemp, "{$classname}", rsCorrelative("ClassName"))
			strTemp = PE_Replace(strTemp, "{$classurl}", GetClassUrl(rsCorrelative("ClassID")))
			strTemp = PE_Replace(strTemp, "{$updatetime}", rsCorrelative("UpdateTime"))
			strTemp = PE_Replace(strTemp, "{$hits}", rsCorrelative("Hits"))
			If InStr(strHtml, "{$year}") > 0 Then strTemp = PE_Replace(strTemp, "{$year}", getYear(rsCorrelative("UpdateTime")))
			If InStr(strHtml, "{$month}") > 0 Then strTemp = PE_Replace(strTemp, "{$month}", getMonth(rsCorrelative("UpdateTime")))	
			If InStr(strHtml, "{$day}") > 0 Then strTemp = PE_Replace(strTemp, "{$day}", getDay(rsCorrelative("UpdateTime")))	
        strCorrelative = strCorrelative & strTemp
        rsCorrelative.MoveNext
    Loop
	End if
    rsCorrelative.Close
    Set rsCorrelative = Nothing
    
    GetCorrelative = strCorrelative
End function
	
'=================================================
'函数名：GetPrevPhoto
'作  用：显示上一张图片
'参  数：TitleLen   ----标题最多字符数，一个汉字=两个英文字符
'        LinkAddress 链接地址
'=================================================
Private Function GetPrevPhoto(TitleLen)
    Dim rsPrev, sqlPrev, strPrev
    strPrev = Replace("<li>上一{$ItemUnit}： ", "{$ItemUnit}", ChannelItemUnit & ChannelShortName)
    sqlPrev = "Select Top 1 PhotoID,PhotoName,UpdateTime,InfoPurview from PW_Photo Where ChannelID=" & ChannelID & " and Deleted=" & PE_False & " and ClassID=" & rsPhoto("ClassID") & " and PhotoID<" & rsPhoto("PhotoID") & " order by PhotoID DESC"
    Set rsPrev = Conn.Execute(sqlPrev)
    If TitleLen < 0 Or TitleLen > 255 Then TitleLen = 50
    If rsPrev.EOF Then
        strPrev = strPrev & "没有了"
    Else
        strPrev = strPrev & "<a href='" & GetPhotoUrl(rsPrev("UpdateTime"), rsPrev("PhotoID"),rsPrev("InfoPurview")) &"'"
        strPrev = strPrev & " title='" & rsPrev("PhotoName") &"'>" & GetSubStr(rsPrev("PhotoName"), TitleLen, True) & "</a>"
    End If
    rsPrev.Close
    Set rsPrev = Nothing
    strPrev = strPrev & "</li>"
    GetPrevPhoto = strPrev
End Function


'=================================================
'函数名：GetNextArticle
'作  用：显示下一篇文章
'参  数：TitleLen   ----标题最多字符数，一个汉字=两个英文字符
'=================================================
Private Function GetNextPhoto(TitleLen,LinkAddress)
    Dim rsNext, sqlNext, strNext
    strNext = Replace("<li>下一{$ItemUnit}： ", "{$ItemUnit}", ChannelItemUnit & ChannelShortName)
    sqlNext = "Select Top 1 PhotoID,PhotoName,UpdateTime,InfoPurview from PW_Photo Where ChannelID=" & ChannelID & " and Deleted=" & PE_False & " and ClassID=" & rsPhoto("ClassID") & " and PhotoID>" & rsPhoto("PhotoID") & " order by PhotoID ASC"
    Set rsNext = Conn.Execute(sqlNext)
    If TitleLen < 0 Or TitleLen > 255 Then TitleLen = 50
    If rsNext.EOF Then
        strNext = strNext & "没有了"
    Else
        strNext = strNext & "<a href='" & GetPhotoUrl(rsNext("UpdateTime"), rsNext("PhotoID"),rsNext("InfoPurview")) &"'"
        strNext = strNext & " title='" & rsNext("PhotoName") &"'>" & GetSubStr(rsNext("PhotoName"), TitleLen, True) & "</a>"
    End If
    rsNext.Close
    Set rsNext = Nothing
    strNext = strNext & "</li>"
    GetNextPhoto = strNext
End Function


Private Function GetHits()
    Dim strHits
    If UseCreateHTML > 0 Then
        strHits = "<script language='javascript' src='" & ChannelUrl_ASPFile & "/GetHits.asp?PhotoID=" & PhotoID & "'></script>"
    Else
        strHits = rsPhoto("Hits")
    End If
    GetHits = strHits
End Function

Public Sub ReplaceViewPhoto()
	Dim FoundErr, ErrMsg
    FoundErr = False
    ErrMsg = ""
	if rsPhoto("InfoPurview") <>"0" then
		Dim ErrMsg_NoLogin, ErrMsg_PurviewCheckedErr
		ErrMsg_NoLogin = Replace(Replace(Replace("<br>&nbsp;&nbsp;&nbsp;&nbsp;你还没注册？或者没有登录？这{$ItemUnit}要求至少是本站的注册会员才能阅读！<br><br>&nbsp;&nbsp;&nbsp;&nbsp;如果你还没注册，请赶紧<a href='{$InstallDir}Reg/User_Reg.asp'><font color=red>点此注册</font></a>吧！<br><br>&nbsp;&nbsp;&nbsp;&nbsp;如果你已经注册但还没登录，请赶紧<a href='{$InstallDir}User/User_Login.asp'><font color=red>点此登录</font></a>吧！<br><br>","{$ItemUnit}",ChannelItemUnit & ChannelShortName),"{$ChannelItemUnit}",ChannelItemUnit),"{$InstallDir}",strInstallDir)
        If UserLogined <> True Then
            FoundErr = True
            ErrMsg = ErrMsg & ErrMsg_NoLogin
        Else
            Call GetUser(UserID)
            ErrMsg_PurviewCheckedErr = "<li>对不起，您没有查看此栏目内容的权限！</li>"
			if FoundInArr(userflag,rsPhoto("InfoPurview"),",") = false then
				FoundErr = True
				ErrMsg = ErrMsg & ErrMsg_PurviewCheckedErr
			end if
		End if
	end if
    If FoundErr = True Then
		strHtml = PE_Replace(strHtml, "{$photourl}", "")
        regEx.Pattern = "\{\$geturlarray\((.*?)\)\}"
        strHtml = regEx.Replace(strHtml, ErrMsg)
		regEx.Pattern = "【photourllist\((.*?)\)】([\s\S]*?)【\/photourllist】"
		response.Write 12
        strHtml = regEx.Replace(strHtml, "")
    Else
        Call ReplacePhotoContent()
    End If
End Sub

Private Function ReplacePhotoContent()
    Dim arrTemp, strPhotoContent, strUrlList
    regEx.Pattern = "【photourllist】([\s\S]*?)【\/photourllist】"
    Set Matches = regEx.Execute(strHtml)
    For Each Match In Matches
		strHtml = PE_Replace(strHtml,Match.Value,GetUrlList(Match.SubMatches(0)))
    Next
    regEx.Pattern = "\{\$geturlarray\((.*?)\)\}"
    Set Matches = regEx.Execute(strHtml)
    For Each Match In Matches
        arrTemp = Split(Match.SubMatches(0), ",")
        If UBound(arrTemp) <> 1 Then
            strUrlList = "函数式标签：{$geturlarray(参数列表)}的参数个数不对。请检查模板中的此标签。"
        Else
            strUrlList = GetUrlArray(arrTemp(0),arrTemp(1))
        End If
        strHtml = PE_Replace(strHtml, Match.value, strUrlList)
    Next
    strHtml = PE_Replace(strHtml, "{$photourl}", GetFirstPhotoUrl())
    ReplacePhotoContent = strHtml
End Function

Private Function GetUrlList(strList)
	Dim iTemp, arrPhotoUrls, PhotoUrl, arrUrls, strTemp
    If rsPhoto("PhotoUrl") & "" = "" Then
        GetUrlList = ""
        Exit Function
    End If
    arrPhotoUrls = Split(rsPhoto("PhotoUrl"), "$$$")
    For iTemp = 0 To UBound(arrPhotoUrls)
        arrUrls = Split(arrPhotoUrls(iTemp), "|")
        If UBound(arrUrls) = 1 Then
            If arrUrls(1) <> "" And arrUrls(1) <> "http://" Then
				If FoundInArr("gif,jpg,jpeg,jpe,bmp,png,swf", LCase(Mid(arrUrls(1), InStrRev(arrUrls(1), ".") + 1)), ",") = True Then
					PhotoUrl = arrUrls(1)
				End If
            End If
        End If
		strTemp = strTemp & strList                
		strTemp = PE_Replace(strTemp, "{$arrphotonumber}", iTemp)
		strTemp = PE_Replace(strTemp, "{$arrphotoname}", arrUrls(0))
		strTemp = PE_Replace(strTemp, "{$arrphotourl}", PhotoUrl)
    Next
    GetUrlList = strTemp
End Function

Private Function GetUrlArray(arrurlname,arrurl)
    Dim strArray, arrPhotoUrls, iTemp, arrUrls, PhotoUrl
    strArray = "<script language='javascript'>" & vbCrLf
    strArray = strArray & "var "& arrurlname &"=new Array();" & vbCrLf
    strArray = strArray & "var "& arrurl &"=new Array();" & vbCrLf
    'arrPhotoUrls = Split(GetPhotoUrl(rsPhoto("PhotoUrl")), "$$$")
	arrPhotoUrls = Split(rsPhoto("PhotoUrl"), "$$$")
    For iTemp = 0 To UBound(arrPhotoUrls)
        arrUrls = Split(arrPhotoUrls(iTemp), "|")
        If UBound(arrUrls) = 1 Then
            If arrUrls(1) <> "" And arrUrls(1) <> "http://" Then
				If FoundInArr("gif,jpg,jpeg,jpe,bmp,png,swf", LCase(Mid(arrUrls(1), InStrRev(arrUrls(1), ".") + 1)), ",") = True Then
					PhotoUrl = arrUrls(1)
				End If
                strArray = strArray & arrurlname &"[" & iTemp & "]='" & arrUrls(0) & "';" & vbCrLf
                strArray = strArray & arrurl &"[" & iTemp & "]='" & PhotoUrl & "';" & vbCrLf
            End If
        End If
    Next
    strArray = strArray & "</script>" & vbCrLf
    GetUrlArray = strArray
End Function

Private Function GetFirstPhotoUrl()
    Dim arrPhotoUrls, arrUrls,photourl
	'photourl = GetPhotoUrl(rsPhoto("PhotoUrl"))
	'photourl = GetPhotoUrl(rsPhoto("UpdateTime"), rsPhoto("PhotoID"),rsPhoto("InfoPurview"))
	photourl =rsPhoto("PhotoUrl")
    If InStr(photourl, "$$$") > 0 Then
        arrPhotoUrls = Split(photourl, "$$$")
        arrUrls = Split(arrPhotoUrls(0), "|")
    Else
        arrUrls = Split(photourl, "|")
    End If
    If UBound(arrUrls) <> 1 Then
        GetFirstPhotoUrl = ""
    Else
        If arrUrls(1) = "" Or LCase(arrUrls(1)) = "http://" Then
            GetFirstPhotoUrl = ""
        Else
			'GetFirstPhotoUrl = arrUrls(1)&"','"&arrUrls(0)
			GetFirstPhotoUrl = arrUrls(1)
        End If
    End If
End Function

Public Sub GetHtml_Index()

    Call GetChannel(ChannelID)
    ClassID = 0
	strHtml = GetTemplate(tempindex)
    Call ReplaceCommonLabel
    strHtml = Replace(strHtml, "{$pagetitle}", strPageTitle)
    strHtml = Replace(strHtml, "{$location}", strNavPath)
    strHtml = GetCustomFromTemplate(strHtml)
    If ChannelID <> PrevChannelID Then
        Call GetChannel(ChannelID)
        PrevChannelID = ChannelID
    End If

    If UseCreateHTML > 0 Then
        If InStr(strHtml, "{$showpage}") > 0 Then strHtml = Replace(strHtml, "{$showpage}", ShowPage_Html(ChannelUrl & "/", 0, ".html", strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName))
        If InStr(strHtml, "{$showpage_en}") > 0 Then strHtml = Replace(strHtml, "{$showpage_en}", ShowPage_en_Html(ChannelUrl & "/", 0, ".html", strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName))
    Else
        If InStr(strHtml, "{$showpage}") > 0 Then strHtml = Replace(strHtml, "{$showpage}", ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName, False))
        If InStr(strHtml, "{$showpage_en}") > 0 Then strHtml = Replace(strHtml, "{$ShowPage_en}", ShowPage_en(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName, False))
    End If
End Sub



Public Sub GetHtml_Class()
	If list_template="" then
		list_template = templist
	End if
	strHtml = GetTemplate(list_template)
    strHtml = PE_Replace(strHtml, "{$classid}", ClassID)
    strHtml = PE_Replace(strHtml, "{$meta_keywords}", Meta_Keywords_Class)
    strHtml = PE_Replace(strHtml, "{$meta_description}", Meta_Description_Class)
    Call ReplaceCommonLabel
    strHtml = PE_Replace(strHtml, "{$classid}", ClassID)
    strHtml = PE_Replace(strHtml, "{$pagetitle}", strPageTitle)
    strHtml = PE_Replace(strHtml, "{$location}", strNavPath)
    strHtml = GetCustomFromTemplate(strHtml)
    If ChannelID <> PrevChannelID Then
        Call GetChannel(ChannelID)
        PrevChannelID = ChannelID
    End If
    Dim strPath
    strPath = ChannelUrl & "/"

    strHtml = PE_Replace(strHtml, "{$classname}", ClassName)
    strHtml = PE_Replace(strHtml, "{$classpicurl}", ClassPicUrl)
    strHtml = PE_Replace(strHtml, "{$classurl}", GetClassUrl(ClassID))
	IF UseCreateHTML > 0 then
		If InStr(strHtml, "{$showpage}") > 0 Then strHtml = Replace(strHtml, "{$showpage}", ShowPage_Html(strPath, ClassID, ".html", "", totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName))
        If InStr(strHtml, "{$showpage_en}") > 0 Then strHtml = Replace(strHtml, "{$showpage_en}", ShowPage_en_Html(strPath, ClassID, ".html", "", totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName))
	else
		If InStr(strHtml, "{$showpage}") > 0 Then strHtml = Replace(strHtml, "{$showpage}", ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName, False))
        If InStr(strHtml, "{$showpage_en}") > 0 Then strHtml = Replace(strHtml, "{$showpage_en}", ShowPage_en(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName, False))	
	end IF

End Sub


Public Sub GetHtml_Photo()
    strHtml = GetCustomFromTemplate(strHtml)  '必须先解析自定义列表标签
    If PrevChannelID <> ChannelID Then
        Call GetChannel(ChannelID)
    End If
    strHtml = PE_Replace(strHtml, "{$photoid}", PhotoID)
    strHtml = PE_Replace(strHtml, "{$classid}", ClassID)
    strHtml = PE_Replace(strHtml, "{$meta_keywords}", GetKeywords(",", rsPhoto("Keyword")))
    strHtml = PE_Replace(strHtml, "{$meta_description}", rsPhoto("PhotoIntro"))
    Call ReplaceCommonLabel   '解析通用标签，包含自定义标签
    strHtml = GetCustomFromTemplate(strHtml)  '必须先解析自定义列表标签
    If PrevChannelID <> ChannelID Then
        Call GetChannel(ChannelID)
    End If
    strHtml = PE_Replace(strHtml, "{$photoid}", PhotoID)
    strHtml = Replace(strHtml, "{$pagetitle}", PhotoName & "_" & SiteName)
    strHtml = Replace(strHtml, "{$location}", strNavPath & "&nbsp;" & strNavLink & "&nbsp;详细内容")
    If InStr(strHtml, "{$MY_") > 0 Then
        Dim rsField
        Set rsField = Conn.Execute("select * from PW_Field where ChannelID=" & ChannelID & "")
        Do While Not rsField.EOF
            If rsField("FieldType") = 8 Or rsField("FieldType") = 9  Or rsField("FieldType") = 10 Then
                strHtml = PE_Replace(strHtml, rsField("LabelName"), PE_HTMLDecode(rsPhoto(Trim(rsField("FieldName")))))
            Else
                strHtml = PE_Replace(strHtml, rsField("LabelName"), PE_HTMLEncode(rsPhoto(Trim(rsField("FieldName")))))		
            End If	
            rsField.MoveNext
        Loop
        Set rsField = Nothing
    End If
    strHtml = PE_Replace(strHtml, "{$classid}", ClassID)
    strHtml = PE_Replace(strHtml, "{$classname}", ClassName)
	strHtml = PE_Replace(strHtml, "{$classurl}", GetClassUrl(ClassID))
	

	strHtml = PE_Replace(strHtml, "{$photoname}", PhotoName)
    If InStr(strHtml, "{$author}") > 0 Then strHtml = PE_Replace(strHtml, "{$author}", rsPhoto("Author"))
	If InStr(strHtml, "{$copyfrom}") > 0 Then strHtml = PE_Replace(strHtml, "{$copyfrom}", rsPhoto("CopyFrom"))
	If InStr(strHtml, "{$hits}") > 0 Then strHtml = PE_Replace(strHtml, "{$hits}", GetHits())
	If InStr(strHtml, "{$updatedate}") > 0 Then strHtml = PE_Replace(strHtml, "{$updatedate}", FormatDateTime(rsPhoto("UpdateTime"), 2))
	strHtml = PE_Replace(strHtml, "{$updatetime}", rsPhoto("updatetime"))

	If InStr(strHtml, "{$month}") > 0 Then strHtml = PE_Replace(strHtml, "{$month}", getMonth(rsPhoto("UpdateTime")))	
	If InStr(strHtml, "{$day}") > 0 Then strHtml = PE_Replace(strHtml, "{$day}", getDay(rsPhoto("UpdateTime")))	
	If InStr(strHtml, "{$year}") > 0 Then strHtml = PE_Replace(strHtml, "{$year}", getYear(rsPhoto("UpdateTime")))

	If InStr(strHtml, "{$photointro}") > 0 Then strHtml = PE_Replace(strHtml, "{$photointro}", Trim(rsPhoto("PhotoIntro")))
    If InStr(strHtml, "{$content}") > 0 Then strHtml = PE_Replace(strHtml, "{$content}", ReplaceKeyLink(GetContent(rsPhoto("Photodetial"))))


	'替换图片缩略图
	Dim arrPicTemp,strPhotoPic
	regEx.Pattern = "\{\$photothumb\((.*?)\)\}"
	Set Matches = regEx.Execute(strHtml)
	For Each Match In Matches
		arrPicTemp = Split(Match.SubMatches(0), ",")
		strPhotoPic = GetPhotoThumb(Trim(rsPhoto("PhotoThumb")), PE_CLng(arrPicTemp(0)), PE_CLng(arrPicTemp(1)))
		strHtml = Replace(strHtml, Match.value, strPhotoPic)
	Next
		

	'''替换上一条信息
	regEx.Pattern = "\{\$prevphoto\((.*?)\)\}"
	Set Matches = regEx.Execute(strHtml)
	For Each Match In Matches
		strParameter = PE_Clng(Match.SubMatches(0))
		if strParameter<=0 and strParameter>255 then
			strParameter = 50
		end if
		strTemp = GetPrevPhoto(strParameter)
		strHtml = PE_Replace(strHtml, Match.Value, strTemp)
	Next
	'''替换下一条信息
	regEx.Pattern = "\{\$nextphoto\((.*?)\)\}"
	Set Matches = regEx.Execute(strHtml)
	For Each Match In Matches
		strParameter = PE_Clng(Match.SubMatches(0))
		if strParameter<=0 and strParameter>255 then
			strParameter = 50
		end if
		strTemp = GetNextPhoto(strParameter)
		strHtml = PE_Replace(strHtml, Match.Value, strTemp)
	Next
	'''替换相关图片
	strHtml = GetCorrelativeFromTemplate(strHtml)
	
end Sub


Private Function GetPhotoThumb(PhotoThumb, PhotoThumbWidth, PhotoThumbHeight)
    Dim strPhotoThumb, FileType, strPicUrl

    If PhotoThumb = "" Then
        strPhotoThumb = strPhotoThumb & "<img src='" & strPicUrl & strInstallDir & "images/nopic.gif' "
        If PhotoThumbWidth > 0 Then strPhotoThumb = strPhotoThumb & " width='" & PhotoThumbWidth & "'"
        If PhotoThumbHeight > 0 Then strPhotoThumb = strPhotoThumb & " height='" & PhotoThumbHeight & "'"
        strPhotoThumb = strPhotoThumb & " border='0'>"
    Else
        strPicUrl = GetContent(PhotoThumb)
        FileType = LCase(Mid(PhotoThumb, InStrRev(PhotoThumb, ".") + 1))
        If FileType = "swf" Then
            strPhotoThumb = strPhotoThumb & "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0' "
            If PhotoThumbWidth > 0 Then strPhotoThumb = strPhotoThumb & " width='" & PhotoThumbWidth & "'"
            If PhotoThumbHeight > 0 Then strPhotoThumb = strPhotoThumb & " height='" & PhotoThumbHeight & "'"
            strPhotoThumb = strPhotoThumb & "><param name='movie' value='" & strPicUrl & "'><param name='quality' value='high'><embed src='" & strPicUrl & "' pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash' type='application/x-shockwave-flash' "
            If PhotoThumbWidth > 0 Then strPhotoThumb = strPhotoThumb & " width='" & PhotoThumbWidth & "'"
            If PhotoThumbHeight > 0 Then strPhotoThumb = strPhotoThumb & " height='" & PhotoThumbHeight & "'"
            strPhotoThumb = strPhotoThumb & "></embed></object>"
        ElseIf FileType = "gif" Or FileType = "jpg" Or FileType = "jpeg" Or FileType = "jpe" Or FileType = "bmp" Or FileType = "png" Then
            strPhotoThumb = strPhotoThumb & "<img src='" & strPicUrl & "' "
            If PhotoThumbWidth > 0 Then strPhotoThumb = strPhotoThumb & " width='" & PhotoThumbWidth & "'"
            If PhotoThumbHeight > 0 Then strPhotoThumb = strPhotoThumb & " height='" & PhotoThumbHeight & "'"
            strPhotoThumb = strPhotoThumb & " border='0'>"
        Else
            strPhotoThumb = strPhotoThumb & "<img src='" & strInstallDir & "images/nopic.gif' "
            If PhotoThumbWidth > 0 Then strPhotoThumb = strPhotoThumb & " width='" & PhotoThumbWidth & "'"
            If PhotoThumbHeight > 0 Then strPhotoThumb = strPhotoThumb & " height='" & PhotoThumbHeight & "'"
            strPhotoThumb = strPhotoThumb & " border='0'>"
        End If
    End If
    GetPhotoThumb = strPhotoThumb
End Function

End Class
%>