<%
'**************************************************
'方法名：PopCalendarInit
'作  用：调用日期js
'**************************************************
Public Sub PopCalendarInit()
    Response.Write "<script language='JavaScript' src='PopCalendar.js'></script>" & vbCrLf
    Response.Write "<script language='JavaScript'>" & vbCrLf
    Response.Write "    PopCalendar = getCalendarInstance()" & vbCrLf
    Response.Write "    PopCalendar.startAt = 0 // 0 - sunday ; 1 - monday" & vbCrLf
    Response.Write "    PopCalendar.showWeekNumber = 0 // 0 - don't show; 1 - show" & vbCrLf
    Response.Write "    PopCalendar.showTime = 0 // 0 - don't show; 1 - show" & vbCrLf
    Response.Write "    PopCalendar.showToday = 0 // 0 - don't show; 1 - show" & vbCrLf
    Response.Write "    PopCalendar.showWeekend = 1 // 0 - don't show; 1 - show" & vbCrLf
    Response.Write "    PopCalendar.showHolidays = 1 // 0 - don't show; 1 - show" & vbCrLf
    Response.Write "    PopCalendar.showSpecialDay = 1 // 0 - don't show, 1 - show" & vbCrLf
    Response.Write "    PopCalendar.selectWeekend = 0 // 0 - don't Select; 1 - Select" & vbCrLf
    Response.Write "    PopCalendar.selectHoliday = 0 // 0 - don't Select; 1 - Select" & vbCrLf
    Response.Write "    PopCalendar.addCarnival = 0 // 0 - don't Add; 1- Add to Holiday" & vbCrLf
    Response.Write "    PopCalendar.addGoodFriday = 0 // 0 - don't Add; 1- Add to Holiday" & vbCrLf
    Response.Write "    PopCalendar.language = 0 // 0 - Chinese; 1 - English" & vbCrLf
    Response.Write "    PopCalendar.defaultFormat = 'yyyy-mm-dd' //Default Format dd-mm-yyyy" & vbCrLf
    Response.Write "    PopCalendar.fixedX = -1 // x position (-1 if to appear below control)" & vbCrLf
    Response.Write "    PopCalendar.fixedY = -1 // y position (-1 if to appear below control)" & vbCrLf
    Response.Write "    PopCalendar.fade = .5 // 0 - don't fade; .1 to 1 - fade (Only IE) " & vbCrLf
    Response.Write "    PopCalendar.shadow = 1 // 0  - don't shadow, 1 - shadow" & vbCrLf
    Response.Write "    PopCalendar.move = 1 // 0  - don't move, 1 - move (Only IE)" & vbCrLf
    Response.Write "    PopCalendar.saveMovePos = 1  // 0  - don't save, 1 - save" & vbCrLf
    Response.Write "    PopCalendar.centuryLimit = 40 // 1940 - 2039" & vbCrLf
    Response.Write "    PopCalendar.initCalendar()" & vbCrLf
    Response.Write "</script>" & vbCrLf
End Sub
'**************************************************
'函数名：ShowJS_Main
'作  用：页面管理js(多项诓全选,删除提示)
'参  数：ItemName ---- 项目名称
'返回值：javascript 验证
'**************************************************
Public Sub ShowJS_Main(ItemName)
    Response.Write "<SCRIPT language=javascript>" & vbCrLf
    Response.Write "function unselectall(){" & vbCrLf
    Response.Write "    if(document.myform.chkAll.checked){" & vbCrLf
    Response.Write " document.myform.chkAll.checked = document.myform.chkAll.checked&0;" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "function CheckAll(form){" & vbCrLf
    Response.Write "  for (var i=0;i<form.elements.length;i++){" & vbCrLf
    Response.Write "    var e = form.elements[i];" & vbCrLf
    Response.Write "    if (e.Name != 'chkAll'&&e.disabled==false)" & vbCrLf
    Response.Write "       e.checked = form.chkAll.checked;" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "function ConfirmDel(){" & vbCrLf
    Response.Write " if(document.myform.Action.value=='Del'){" & vbCrLf
    Response.Write "     if(confirm('确定要删除选中的" & ItemName & "吗？'))" & vbCrLf
    Response.Write "         return true;" & vbCrLf
    Response.Write "     else" & vbCrLf
    Response.Write "         return false;" & vbCrLf
    Response.Write " }" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "</SCRIPT>" & vbCrLf
End Sub

Sub ShowContentManagePath(RootName)
    Response.Write "您现在的位置：&nbsp;" & ChannelName & "管理&nbsp;&gt;&gt;&nbsp;<a href='" & FileName & "'>" & RootName & "</a>&nbsp;&gt;&gt;&nbsp;"
    If ClassID > 0 Then
        If ParentID > 0 Then
            Dim sqlPath, rsPath
            sqlPath = "select ClassID,ClassName from PW_Class where ClassID in (" & ParentPath & ") order by Depth"
            Set rsPath = Conn.Execute(sqlPath)
            Do While Not rsPath.EOF
                Response.Write "<a href='" & FileName & "&ClassID=" & rsPath(0) & "'>" & rsPath(1) & "</a>&nbsp;&gt;&gt;&nbsp;"
                rsPath.MoveNext
            Loop
            rsPath.Close
            Set rsPath = Nothing
        End If
        Response.Write "<a href='" & FileName & "&ClassID=" & ClassID & "'>" & ClassName & "</a>&nbsp;&gt;&gt;&nbsp;"
    End If
	If Keyword = "" Then
		Response.Write "所有" & ChannelShortName & "！"
	Else
		Select Case strField
		Case "Title"
			Response.Write "标题中含有 <font color=red>" & Keyword & "</font> "
		Case "Content"
			Response.Write "内容中含有 <font color=red>" & Keyword & "</font> "
		Case "Keyword"
			Response.Write "关键字为 <font color=red>" & Keyword & "</font> "
		Case "UpdateTime"
			Response.Write "更新时间为 <font color=red>" & Keyword & "</font> "
		End Select
		Response.Write "的" & ChannelShortName & "！"
	End If
End Sub

Sub ShowManagePath(ActStr)
	Response.Write "您现在的位置：网站" & ItemName & "管理&nbsp;&gt;&gt;&nbsp;"
	if ActStr="" then
		Response.Write "所有"&ItemName
	else
		Response.Write ActStr&ItemName
	end if 
End Sub

'**************************************************
'函数名：GetRootClass
'作  用：显示栏目标题栏
'参  数：ChannelID ---- 频道ID
'        RootID ---- 根栏目ID
'        FileName ---- 栏目文件名
'返回值：栏目标题栏
'**************************************************
Public Function GetRootClass()
    Dim sqlRoot, rsRoot, strRoot
    sqlRoot = "select ClassID,ClassName,RootID,Child from PW_Class where ChannelID=" & ChannelID & " and ParentID=0 order by RootID"
    Set rsRoot = Conn.Execute(sqlRoot)
    If rsRoot.BOF And rsRoot.EOF Then
        strRoot = "还没有任何栏目，请首先添加栏目。"
    Else
        strRoot = "|&nbsp;"
        Do While Not rsRoot.EOF
            If rsRoot(2) = RootID Then
                strRoot = strRoot & "<a href='" & FileName & "&ClassID=" & rsRoot(0) & "'><font color=red>" & rsRoot(1) & "</font></a> | "
            Else
                strRoot = strRoot & "<a href='" & FileName & "&ClassID=" & rsRoot(0) & "'>" & rsRoot(1) & "</a> | "
            End If
            rsRoot.MoveNext
        Loop
    End If
    rsRoot.Close
    Set rsRoot = Nothing
    GetRootClass = strRoot
End Function


'**************************************************
'函数名：GetChild_Root
'作  用：显示栏目子栏目标题栏
'参  数：ChannelID ---- 频道ID
'        RootID ---- 根栏目ID
'        ClassID ---- 栏目ID
'        ParentPath ---- 父路径
'        Depth ---- 栏目深度
'        FileName ---- 栏目文件名
'返回值：子栏目标题栏
'**************************************************
Public Function GetChild_Root()
    Dim sqlChild, rsChild, arrParentPath, isCurrent, strChild, i
    If RootID <= 0 Then
        GetChild_Root = ""
        Exit Function
    End If
    sqlChild = "select ClassID,ClassName,Child from PW_Class where ChannelID=" & ChannelID & " and Depth=1 and RootID=" & RootID & " order by OrderID"
    Set rsChild = Conn.Execute(sqlChild)
    If Not (rsChild.BOF And rsChild.EOF) Then
        i = 1
        arrParentPath = Split(ParentPath, ",")
        strChild = "<tr class='tdbg'><td>"
        Do While Not rsChild.EOF
            If Depth <= 1 Then
                If rsChild(0) = ClassID Then
                    isCurrent = True
                Else
                    isCurrent = False
                End If
            Else
                If PE_CLng(arrParentPath(2)) = rsChild(0) Then
                    isCurrent = True
                Else
                    isCurrent = False
                End If
            End If
            If isCurrent = True Then
                strChild = strChild & "&nbsp;&nbsp;<a href='" & FileName & "&ClassID=" & rsChild(0) & "'><font color='red'>" & rsChild(1) & "</font></a>"
            Else
                strChild = strChild & "&nbsp;&nbsp;<a href='" & FileName & "&ClassID=" & rsChild(0) & "'>" & rsChild(1) & "</a>"
            End If
            If rsChild(2) > 0 Then
                strChild = strChild & "(" & rsChild(2) & ")"
            End If
            If i Mod 8 = 0 Then
                strChild = strChild & "<br>"
            Else
                strChild = strChild & "&nbsp;&nbsp;"
            End If
            rsChild.MoveNext
            i = i + 1
        Loop
        strChild = strChild & "</td></tr>"
    End If
    rsChild.Close
    Set rsChild = Nothing
    GetChild_Root = strChild
End Function

'**************************************************
'函数名：GetAuthorList
'作  用：显示作者
'参  数：ChannelID ---- 频道ID
'        UserName ---- 用户名称
'返回值：【未知】【佚名】【管理员】
'**************************************************
Public Function GetAuthorList(UserName)
    Dim strAuthorList

    strAuthorList = "<font color='blue'><="
    strAuthorList = strAuthorList & "【<font color='green' onclick=""document.myform.Author.value='未知'"" style=""cursor:hand;"">未知</font>】"
    strAuthorList = strAuthorList & "【<font color='green' onclick=""document.myform.Author.value='佚名'"" style=""cursor:hand;"">佚名</font>】"
    strAuthorList = strAuthorList & "【<font color='green' onclick=""document.myform.Author.value='管理员'"" style=""cursor:hand;"">管理员</font>】"
    strAuthorList = strAuthorList & "【<font color='green' onclick=""document.myform.Author.value='" & UserName & "'"" style=""cursor:hand;"">" & UserName & "</font>】"
    strAuthorList = strAuthorList & "</font>"
    GetAuthorList = strAuthorList
End Function


'**************************************************
'函数名：GetCopyFromList
'作  用：显示来源
'参  数：FilePrefix ----访问身份 Admin,User
'        ChannelID ---- 频道ID
'返回值：<=【本站原创】【更多】
'**************************************************
Public Function GetCopyFromList()
    Dim strCopyFromList
    strCopyFromList = "<font color='blue'><="
    strCopyFromList = strCopyFromList & "【<font color='green' onclick=""document.myform.CopyFrom.value='不详'"" style=""cursor:hand;"">不详</font>】"
    strCopyFromList = strCopyFromList & "【<font color='green' onclick=""document.myform.CopyFrom.value='本站原创'"" style=""cursor:hand;"">本站原创</font>】"
    strCopyFromList = strCopyFromList & "【<font color='green' onclick=""document.myform.CopyFrom.value='互联网'"" style=""cursor:hand;"">互联网</font>】"
    strCopyFromList = strCopyFromList & "</font>"
    GetCopyFromList = strCopyFromList
End Function



'**************************************************
'函数名：ShowClassPath
'作  用：显示栏目路径
'参  数：无
'返回值：显示栏目
'**************************************************
Public Function ShowClassPath()
    If ParentPath = "" Or IsNull(ParentPath) Then
        ShowClassPath = "不属于任何栏目"
        Exit Function
    End If
    Dim strPath
    If Depth > 0 Then
        Dim rsPath
        Set rsPath = Conn.Execute("select * from PW_Class where ClassID in (" & ParentPath & ") order by Depth")
        Do While Not rsPath.EOF
            strPath = strPath & rsPath("ClassName") & " >> "
            rsPath.MoveNext
        Loop
        rsPath.Close
        Set rsPath = Nothing
    End If
    strPath = strPath & ClassName
    ShowClassPath = strPath
End Function

function SetPhotoUrl(PhotoUrl)
	Dim PhotoUrlStr
	PhotoUrlStr=PE_Replace(PhotoUrl,InstallDir&UploadDir&"/",InstallDir&UploadDir&"/")
	SetPhotoUrl = PhotoUrlStr
End Function

function GetPhotoUrl(PhotoUrl)
	Dim PhotoUrlStr
	PhotoUrlStr=PE_Replace(PhotoUrl,InstallDir&UploadDir&"/",InstallDir&UploadDir&"/")
	GetPhotoUrl = PhotoUrlStr
End Function

'**************************************************
'函数名：ShowInfoPurview
'作  用：显示阅读权限
'参  数：无
'返回值：显示栏目

Public Function ShowInfoPurview(purstr)
	Dim strInfoPurview ,rsGroup, i
	if FoundInArr(Modules,"User", ",") and FoundInArr(FieldShow,"Flag", ",") then
		strInfoPurview = strInfoPurview & "    <tr class='tdbg'>"& vbCrLf
		strInfoPurview = strInfoPurview & "        <td align='right' class='tdbg5'>阅读权限：</td>"& vbCrLf
		strInfoPurview = strInfoPurview & "        <td>"& vbCrLf
		strInfoPurview = strInfoPurview & "         <table width='100%' align='right'><tr>"
		strInfoPurview = strInfoPurview & "           <td><input type='checkbox' name='GroupID' value='0'"
		If FoundInArr(purstr, "0", ",") = True Then
            strInfoPurview = strInfoPurview & " checked"
        End If
		strInfoPurview = strInfoPurview & ">游客</td>"
		
		Set rsGroup = Conn.Execute("select GroupID,GroupName from PW_UserGroup order by GroupID asc")
		Do While Not rsGroup.EOF
			strInfoPurview = strInfoPurview & "           <td><input type='checkbox' name='GroupID' value='"& rsGroup("GroupID") &"'"
		If FoundInArr(purstr, rsGroup("GroupID"), ",") = True Then
            strInfoPurview = strInfoPurview & " checked"
        End If
		strInfoPurview = strInfoPurview & ">"& rsGroup("GroupName") &"</td>"
		rsGroup.movenext
		i = i + 1
		If i Mod 6 = 0 And Not rsGroup.EOF Then
            strInfoPurview = strInfoPurview & "</tr><tr>"
        End If
		loop
		rsGroup.close
		set rsGroup=Nothing
		strInfoPurview = strInfoPurview & "  </tr>"
		strInfoPurview = strInfoPurview & "  </table>"
		strInfoPurview = strInfoPurview &  "</td>"
		strInfoPurview = strInfoPurview & "    </tr>"& vbCrLf
	Else
		strInfoPurview = ""
	end if
	ShowInfoPurview = strInfoPurview
End function

function ShowFckEditor(ToolbarSet,Cvalue,Cfiled,width,height)
	Dim oFCKeditor
	Set oFCKeditor = New FCKeditor
	oFCKeditor.BasePath = InStallDir&"PW_Editor/" 
	oFCKeditor.ToolbarSet = ToolbarSet
	oFCKeditor.Width = width
	oFCKeditor.Height = height
	oFCKeditor.Value = Cvalue
	oFCKeditor.Create Cfiled
End function

'**************************************************
'函数名：GetClass_Option
'作  用：栏目下拉菜单
'参  数：ShowType ---- 显示类型
'        CurrentID ---- 当前栏目ID
'返回值：栏目下拉菜单
'**************************************************
Function GetClass_Option(ShowType,CurrentID)
    Dim rsClass, sqlClass, strClass_Option, tmpDepth, i, ClassNum
    Dim arrShowLine(20)
    ClassNum = 1
    ShowType = PE_Clng(ShowType)
    For i = 0 To UBound(arrShowLine)
        arrShowLine(i) = False
    Next
    sqlClass = "Select * from PW_Class where ChannelID=" & ChannelID & " and ClassType<>2  order by RootID,OrderID"
    Set rsClass = Conn.Execute(sqlClass)
    If rsClass.BOF And rsClass.EOF Then
        	strClass_Option = strClass_Option & ""
    Else
        Do While Not rsClass.EOF
            ClassNum = ClassNum + 1
            tmpDepth = rsClass("Depth")
            If rsClass("NextID") > 0 Then
                arrShowLine(tmpDepth) = True
            Else
                arrShowLine(tmpDepth) = False
            End If
            strClass_Option = strClass_Option & "<option value='" & rsClass("ClassID") & "'"
            If FoundInArr(CurrentID, rsClass("ClassID"), ",") Then
                strClass_Option = strClass_Option & " selected"
            End If
            strClass_Option = strClass_Option & ">"
            
            If tmpDepth > 0 Then
                For i = 1 To tmpDepth
                    strClass_Option = strClass_Option & "&nbsp;&nbsp;"
                    If i = tmpDepth Then
                        If rsClass("NextID") > 0 Then
                            strClass_Option = strClass_Option & "├&nbsp;"
                        Else
                            strClass_Option = strClass_Option & "└&nbsp;"
                        End If
                    Else
                        If arrShowLine(i) = True Then
                            strClass_Option = strClass_Option & "│"
                        Else
                            strClass_Option = strClass_Option & "&nbsp;"
                        End If
                    End If
                Next
            End If
            strClass_Option = strClass_Option & rsClass("ClassName")
            strClass_Option = strClass_Option & "</option>"
            ClassNum = ClassNum + 1
            rsClass.MoveNext
        Loop
    End If
    rsClass.Close
    Set rsClass = Nothing
	if ShowType = 1 then
		strClass_Option = strClass_Option & "<option value='-1'"
		If PE_CLng(CurrentID) = -1 Then strClass_Option = strClass_Option & " selected"
		strClass_Option = strClass_Option & ">不指定任何栏目</option>"
	end if
    GetClass_Option = strClass_Option
End Function

Sub ShowTabs_MyField_Add()
    Dim rsField
    Set rsField = Conn.Execute("select * from PW_Field where ChannelID=" & ChannelID & " Order by FieldID")
    Do While Not rsField.EOF
		If FoundInArr(FieldShow, rsField("FieldName"), ",") then
			Call WriteFieldHTML(rsField("FieldName"), rsField("Title"), rsField("Tips"), rsField("FieldType"), rsField("DefaultValue"), rsField("Options"), rsField("EnableNull"))
		End If
        rsField.MoveNext
    Loop
    Set rsField = Nothing
End Sub

Sub ShowTabs_MyField_Modify(rsInfo)
    Dim rsField
    Set rsField = Conn.Execute("select * from PW_Field where ChannelID=" & ChannelID & "")
    Do While Not rsField.EOF
		If FoundInArr(FieldShow, rsField("FieldName"), ",") then
			Call WriteFieldHTML(rsField("FieldName"), rsField("Title"), rsField("Tips"), rsField("FieldType"), rsInfo(Trim(rsField("FieldName"))), rsField("Options"), rsField("EnableNull"))
		End If
        rsField.MoveNext
    Loop
    Set rsField = Nothing
End Sub

'**************************************************
'函数名：WriteFieldHTML
'作  用：显示自定义字段表单
'参  数：FieldName ----自定义字段名称
'        Title ---- 标题
'        Tips ---- 附加提示
'        FieldType ---- 字段类型  1--单行文本  2--多行文本  3--下拉列表  4--图片  5--文件  6--日期  7--数字
'        strValue ---- 默认值
'        Options ---- 列表项目
'        EnableNull ---- 是否可以为空
'返回值：自定义字段表单
'**************************************************
Sub WriteFieldHTML(FieldName, Title, Tips, FieldType, strValue, Options, EnableNull)
    Dim FieldUpload, ChannelUpload, UserUpload,rsFieldUpload,sqlFieldUpload   
    Dim strEnableNull
	Dim EditorTxt
    If EnableNull = False Then
        strEnableNull = " <font color='#FF0000'>*</font>"
    End If
    Response.Write "<tr class='tdbg'><td width='120' align='right' class='tdbg5'>" & Title & "：</b><td colspan='5'>"
    Select Case FieldType
    Case 10    '文本编辑器
        'Response.Write "<textarea name='" & FieldName & "' cols='80' rows='10'>" & strValue & "</textarea>" & strEnableNull
		Response.Write  ShowFckEditor("Default","" & strValue & "","" & FieldName & "","100%","200") & "" & strEnableNull
    Case 1,8    '单行文本框
        Response.Write "<input type='text' name='" & FieldName & "' size='35' maxlength='255' value='" & strValue & "'>" & strEnableNull
    Case 2,9    '多行文本框
        Response.Write "<textarea name='" & FieldName & "' cols='80' rows='10'>" & strValue & "</textarea>" & strEnableNull
    Case 3    '下拉列表
        Response.Write "<select name='" & FieldName & "'>"
        Dim arrOptions, i
        arrOptions = Split(Options, vbCrLf)
        For i = 0 To UBound(arrOptions)
            Response.Write "<option value='" & arrOptions(i) & "'"
            If arrOptions(i) = strValue Then Response.Write " selected"
            Response.Write ">" & arrOptions(i) & "</option>"
        Next
        Response.Write "</select>" & strEnableNull
    Case 4   '图片  					
        If strValue = "" Then
            Response.Write "<input type='text' id='"&FieldName&"' name='"&FieldName&"'  size='45' maxlength='255' value='http://'><br>" & strEnableNull
        Else
            Response.Write "<input type='text' name='" & FieldName & "' id='" & FieldName & "' size='45' maxlength='255' value='" & strValue & "'><br>" & strEnableNull
        End If
        Response.Write "<iframe style='top:2px;' id='uploadPhoto' src='upload.asp?ChannelID=" & ChannelID & "&dialogtype=fieldphoto&FieldName="& FieldName &"' frameborder=0 scrolling=no width='650' height='25'></iframe>"
    Case 5   '文件
        If strValue = "" Then
            Response.Write "<input type='text' id='"&FieldName&"' name='"&FieldName&"'  size='45' maxlength='255' value='http://'><br>" & strEnableNull
        Else
            Response.Write "<input type='text' name='" & FieldName & "' id='" & FieldName & "' size='45' maxlength='255' value='" & strValue & "'><br>" & strEnableNull
        End If
        Response.Write "<iframe style='top:2px' id='uploadsoft' src='upload.asp?ChannelID=" & ChannelID & "&dialogtype=field&FieldName="& FieldName &"' frameborder=0 scrolling=no width='650' height='25'></iframe>"	
    Case 6    '日期
        If strValue = "" Then
            Response.Write "<input type='text' name='" & FieldName & "' size='20' maxlength='20' value='" & Now() & "'>" & strEnableNull
        Else
            Response.Write "<input type='text' name='" & FieldName & "' size='20' maxlength='20' value='" & strValue & "'>" & strEnableNull
        End If
    Case 7    '数字
        If strValue = "" Then
            Response.Write "<input type='text' name='" & FieldName & "'  onkeyup=""value=value.replace(/[^\d]/g,'')"" size='20' maxlength='20' value='0'>" & strEnableNull
        Else
            Response.Write "<input type='text' name='" & FieldName & "'  onkeyup=""value=value.replace(/[^\d]/g,'')""  size='20' maxlength='20' value='" & PE_Clng(strValue) & "'>" & strEnableNull
        End If		
    End Select
    If IsNull(Tips) = False And Tips <> ""  and (FieldType <> 4 and FieldType <> 5) Then
        Response.Write "&nbsp;" & PE_HTMLEncode(Tips)
    End If
    Response.Write "</td></tr>"
End Sub

%>