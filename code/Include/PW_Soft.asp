<%

Dim SoftID, SoftName, SoftUrl
Dim rsSoft


Class Soft

Sub Init()
    FoundErr = False
    ErrMsg = ""
    ChannelShortName = "软件"
	PrevChannelID = ChannelID
    
    Call GetChannel(ChannelID)
    If Trim(ChannelName) <> "" Then
        strNavPath = strNavPath & "&nbsp;" & strNavLink & "&nbsp;<a href='"
        If UseCreateHTML > 0 Then
            strNavPath = strNavPath & ChannelUrl & "/Index.html"
        Else
            strNavPath = strNavPath & ChannelUrl_ASPFile & "/Index.asp"
        End If
        strNavPath = strNavPath & "'>" & ChannelName & "</a>"
        strPageTitle = ChannelName&"_"&SiteName
    End If
End Sub

Private Function GetSqlStr(iChannelID, arrClassID, IncludeChild, IsHot, IsElite, OrderType, IsPicUrl)
    Dim strSql, IDOrder
    If IsValidID(iChannelID) = False Then
        iChannelID = 0
    Else
        iChannelID = ReplaceLabelBadChar(iChannelID)
    End If  
    If IsValidID(arrClassID) = False Then
        arrClassID = 0
    Else
        arrClassID = ReplaceLabelBadChar(arrClassID)
    End If	
    strSql = strSql & " from PW_Soft S left join PW_Class C on S.ClassID=C.ClassID"
    strSql = strSql & " where S.Deleted=" & PE_False & " "
    If iChannelID > 0 Then
        strSql = strSql & " and S.ChannelID=" & iChannelID
    End If
    If arrClassID <> "0" Then
        If InStr(arrClassID, ",") = 0 And IncludeChild = True Then
            Dim trs
            Set trs = Conn.Execute("select arrChildID from PW_Class where ClassID=" & PE_CLng(arrClassID) & "")
            If trs.BOF And trs.EOF Then
                arrClassID = "0"
            Else
                If IsNull(trs(0)) Or Trim(trs(0)) = "" Then
                    arrClassID = "0"
                Else
                    arrClassID = trs(0)
                End If
            End If
            Set trs = Nothing
        End If
        
        If InStr(arrClassID, ",") > 0 Then
            strSql = strSql & " and S.ClassID in (" & FilterArrNull(arrClassID, ",") & ")"
        Else
            If PE_CLng(arrClassID) > 0 Then strSql = strSql & " and S.ClassID=" & PE_CLng(arrClassID)
        End If
    End If
    If IsHot = True Then
        strSql = strSql & " and S.Hits>=" & HitsOfHot
    End If
    If IsElite = True Then
        strSql = strSql & " and S.Elite=" & PE_True
    End If

    If IsPicUrl = True Then
        strSql = strSql & " and S.SoftPicUrl<>'' "
    End If

    strSql = strSql & " order by S.OnTop " & PE_OrderType & ","
    Select Case PE_CLng(OrderType)
    Case 1, 2

    Case 3
        strSql = strSql & "S.UpdateTime desc,"
    Case 4
        strSql = strSql & "S.UpdateTime asc,"
    Case 5
        strSql = strSql & "S.Hits desc,"
    Case 6
        strSql = strSql & "S.Hits asc,"
    Case Else

    End Select
    If OrderType = 2 Then
        IDOrder = "asc"
    Else
        IDOrder = "desc"
    End If
    strSql = strSql & "S.SoftID " & IDOrder
    GetSqlStr = strSql
End Function


'''标签【softlist(参数列表)】【/softlist】 
Public Function GetCustomFromTemplate(strValue)   
    Dim strCustom, strParameter
	strCustom = strValue
    regEx.Pattern = "【softlist\((.*?)\)】([\s\S]*?)【\/softlist】"
    Set Matches = regEx.Execute(strCustom)
    For Each Match In Matches
        strParameter = Replace(Match.SubMatches(0), Chr(34), " ")
        strCustom = PE_Replace(strCustom, Match.value, GetCustomFromLabel(strParameter, Match.SubMatches(1)))
    Next
    GetCustomFromTemplate = strCustom
End Function

'参数列表：iChannelID, arrClassID, IncludeChild, ItemNum, IsHot, IsElite, OrderType, UsePage, TitleLen, ContentLen
Private Function GetCustomFromLabel(strTemp, strList)
    Dim arrTemp
    Dim strSoftPic, strPicTemp, arrPicTemp
	Dim iChannelID, arrClassID, IncludeChild, ItemNum, IsHot, IsElite, OrderType, UsePage, TitleLen, ContentLen
	Dim iCols, iColsHtml, iRows, iRowsHtml, iNumber
    Dim IncludePic
    If strTemp = "" Or strList = "" Then GetCustomFromLabel = "": Exit Function

    iCols = 1: iRows = 1: iColsHtml = "": iRowsHtml = ""
    regEx.Pattern = "【(cols|rows)=(\d{1,2})\s*(?:\||｜)(.+?)】"
    Set Matches = regEx.Execute(strList)
    For Each Match In Matches
        If LCase(Match.SubMatches(0)) = "cols" Then
            If Match.SubMatches(1) > 1 Then iCols = Match.SubMatches(1)
            iColsHtml = Match.SubMatches(2)
        ElseIf LCase(Match.SubMatches(0)) = "rows" Then
            If Match.SubMatches(1) > 1 Then iRows = Match.SubMatches(1)
            iRowsHtml = Match.SubMatches(2)
        End If
        strList = regEx.Replace(strList, "")
    Next
    arrTemp = Split(strTemp, ",")
    If UBound(arrTemp) <> 10 and UBound(arrTemp) <> 9 Then
        GetCustomFromLabel = "自定义列表标签：【softlist(参数列表)】列表内容【/softlist】的参数个数不对。请检查模板中的此标签。"
        Exit Function
    End If
    Select Case Trim(arrTemp(0))
    Case "channelid"
        iChannelID = ChannelID
    Case Else
        iChannelID = PE_CLng(arrTemp(0))
    End Select
    Select Case Trim(arrTemp(1))
    Case "arrchildid"
        arrClassID = arrChildID
    Case "classid"
        arrClassID = ClassID
    Case Else
        arrClassID = arrTemp(1)
    End Select
    arrClassID = Replace(Trim(arrClassID), "|", ",")
    IncludeChild = PE_CBool(arrTemp(2))
    ItemNum = PE_CLng(arrTemp(3))
    IsHot = PE_CBool(arrTemp(4))
    IsElite = PE_CBool(arrTemp(5))
	OrderType = PE_CLng(arrTemp(6))
    UsePage = PE_CBool(arrTemp(7))
    TitleLen = PE_CLng(arrTemp(8))
    ContentLen = PE_CLng(arrTemp(9))
    If UBound(arrTemp) = 10  then
        IncludePic = PE_CBool(arrTemp(10))
    Else
        IncludePic = False	    
    End If
    FoundErr = False
	
    If iChannelID <> PrevChannelID Or ChannelID = 0 Then
        Call GetChannel(iChannelID)
		PrevChannelID = iChannelID
    End If
    If FoundErr = True Then
        GetCustomFromLabel = ErrMsg
        Exit Function
    End If
	
    Dim rsField, ArrField, iField
    Set rsField = Conn.Execute("select FieldName,LabelName from PW_Field where ChannelID=" & iChannelID & "")
    If Not (rsField.BOF And rsField.EOF) Then
        ArrField = rsField.getrows(-1)
    End If
    Set rsField = Nothing
    Dim sqlCustom, rsCustom, iCount, strCustomList
    iCount = 0
    sqlCustom = ""
    strCustomList = ""
	
    sqlCustom = "select "
    If ItemNum > 0 Then
        sqlCustom = sqlCustom & "top " & ItemNum & " "
    End If
    If ContentLen > 0 Then
        sqlCustom = sqlCustom & "S.SoftIntro,"
    End If
    If IsArray(ArrField) Then
        For iField = 0 To UBound(ArrField, 2)
            sqlCustom = sqlCustom & "S." & ArrField(0, iField) & ","
        Next
    End If
    sqlCustom = sqlCustom & "S.ChannelID,S.SoftID,S.ClassID,S.SoftName,S.SoftIntro,S.DownloadUrl,S.SoftVersion,S.Author,S.Keyword,S.UpdateTime,S.CopyrightType,S.OperatingSystem,S.SoftType,S.SoftLanguage"
    sqlCustom = sqlCustom & ",S.Hits,S.OnTop,S.Elite,S.SoftSize,S.SoftPicUrl,C.ClassName,S.DownloadUrl,S.demourl"
    sqlCustom = sqlCustom & GetSqlStr(iChannelID, arrClassID, IncludeChild, IsHot, IsElite, OrderType, IncludePic)
    Set rsCustom = Server.CreateObject("ADODB.Recordset")
    rsCustom.Open sqlCustom, Conn, 1, 1
    If rsCustom.BOF And rsCustom.EOF Then
        totalPut = 0
        strCustomList = GetInfoList_StrNoItem(arrClassID, IsHot, IsElite)
        rsCustom.Close
        Set rsCustom = Nothing
        GetCustomFromLabel = strCustomList
        Exit Function
    End If

    If UsePage = True Then
        totalPut = rsCustom.RecordCount
        If CurrentPage < 1 Then
            CurrentPage = 1
        End If
        If (CurrentPage - 1) * MaxPerPage > totalPut Then
            If (totalPut Mod MaxPerPage) = 0 Then
                CurrentPage = totalPut \ MaxPerPage
            Else
                CurrentPage = totalPut \ MaxPerPage + 1
            End If
        End If
        If CurrentPage > 1 Then
            If (CurrentPage - 1) * MaxPerPage < totalPut Then
                iMod = 0
                rsCustom.Move (CurrentPage - 1) * MaxPerPage - iMod
            Else
                CurrentPage = 1
            End If
        End If
    End If
    Do While Not rsCustom.EOF
        If iChannelID = 0 Then
            If rsCustom("ChannelID") <> PrevChannelID Then
                Call GetChannel(rsCustom("ChannelID"))
                PrevChannelID = rsCustom("ChannelID")
            End If
        End If
        strTemp = strList

        If UsePage = True Then
            iNumber = (CurrentPage - 1) * MaxPerPage + iCount + 1
        Else
            iNumber = iCount + 1
        End If
        strTemp = PE_Replace(strTemp, "{$autoid}", iNumber)
        strTemp = PE_Replace(strTemp, "{$classid}", rsCustom("ClassID"))
        strTemp = PE_Replace(strTemp, "{$classname}", rsCustom("ClassName"))
        strTemp = PE_Replace(strTemp, "{$classurl}", GetClassUrl(rsCustom("ClassID")))
        strTemp = PE_Replace(strTemp, "{$softid}", rsCustom("SoftID"))
        If TitleLen > 0 Then
            strTemp = Replace(strTemp, "{$softname}", GetSubStr(rsCustom("SoftName"), TitleLen, False))
        Else
            strTemp = Replace(strTemp, "{$softname}", rsCustom("SoftName"))
        End If
		strTemp = PE_Replace(strTemp, "{$softnameall}", rsCustom("SoftName"))
        strTemp = PE_Replace(strTemp, "{$softurl}", GetSoftUrl(rsCustom("UpdateTime"), rsCustom("SoftID")))
        strTemp = PE_Replace(strTemp, "{$softintro}", rsCustom("SoftIntro"))
        If ContentLen > 0 Then
            If InStr(strTemp, "{$softdetial}") > 0 Then strTemp = PE_Replace(strTemp, "{$softdetial}",  GetInfoList_GetStrContent(rsCustom("softdetial"),ContentLen))
		elseif ContentLen = -1 Then
			strTemp = PE_Replace(strTemp, "{$softdetial}", GetContent(rsCustom("softdetial")))
        Else
            strTemp = PE_Replace(strTemp, "{$softdetial}", "")
        End If
		if InStr(strTemp, "{$property}") >0 then
			Dim PropertyStr
			PropertyStr = ""
			if rsCustom("OnTop") = True then
				PropertyStr = PropertyStr&"<font color=""green""> 顶</font>"
			end if
			if rsCustom("Elite") = True then
				PropertyStr = PropertyStr&"<font color=""blue""> 荐</font>"
			end if
			if rsCustom("Hits") > HitsOfHot then
				PropertyStr = PropertyStr&"<font color=""red""> 荐</font>"
			end if
			strTemp = PE_Replace(strTemp, "{$property}", PropertyStr)	
		end if
		If InStr(strHtml, "{$month}") > 0 Then strTemp = PE_Replace(strTemp, "{$month}", getMonth(rsCustom("UpdateTime")))	
		If InStr(strHtml, "{$day}") > 0 Then strTemp = PE_Replace(strTemp, "{$day}", getDay(rsCustom("UpdateTime")))	
		If InStr(strHtml, "{$year}") > 0 Then strTemp = PE_Replace(strTemp, "{$year}", getYear(rsCustom("UpdateTime")))
        If InStr(strTemp, "{$softversion}") > 0 Then strTemp = PE_Replace(strTemp, "{$softversion}", rsCustom("SoftVersion"))
        strTemp = PE_Replace(strTemp, "{$softsize}", rsCustom("SoftSize"))
        If InStr(strTemp, "{$softsize_m}") > 0 Then strTemp = PE_Replace(strTemp, "{$softsize_m}", Round(rsCustom("SoftSize") / 1024, 2))
        If InStr(strTemp, "{$updatedate}") > 0 Then strTemp = PE_Replace(strTemp, "{$updatedate}", FormatDateTime(rsCustom("UpdateTime"), 2))
        strTemp = PE_Replace(strTemp, "{$updatetime}", rsCustom("UpdateTime"))
        If InStr(strTemp, "{$copyrighttype}") > 0 Then strTemp = PE_Replace(strTemp, "{$copyrighttype}", PE_HTMLEncode(rsCustom("CopyrightType")))
        strTemp = PE_Replace(strTemp, "{$operatingsystem}", rsCustom("OperatingSystem"))
        If InStr(strTemp, "{$softtype}") > 0 Then strTemp = PE_Replace(strTemp, "{$softtype}", PE_HTMLEncode(rsCustom("SoftType")))
        If InStr(strTemp, "{$softlanguage}") > 0 Then strTemp = PE_Replace(strTemp, "{$softlanguage}", PE_HTMLEncode(rsCustom("SoftLanguage")))
        strTemp = PE_Replace(strTemp, "{$hits}", rsCustom("Hits"))
        strTemp = PE_Replace(strTemp, "{$demourl}", rsCustom("DemoUrl"))
        regEx.Pattern = "\{\$softpic\((.*?)\)\}"
        Set Matches = regEx.Execute(strTemp)
        For Each Match In Matches
            arrPicTemp = Split(Match.SubMatches(0), ",")
            strSoftPic = GetSoftPicUrl(Trim(rsCustom("SoftPicUrl")), PE_CLng(arrPicTemp(0)), PE_CLng(arrPicTemp(1)))
            strTemp = Replace(strTemp, Match.value, strSoftPic)
        Next
		
		If IsArray(ArrField) Then
            For iField = 0 To UBound(ArrField, 2)
                Select Case ArrField(2, iField)
                Case 8,9
                    strTemp = PE_Replace(strTemp, ArrField(1, iField), PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField)))))
                Case 4
                    If PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField))))="" or IsNull(PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField))))) or PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField))))="http://" Then
                        strTemp = PE_Replace(strTemp, ArrField(1, iField), "")	
                    Else 
                        strTemp = PE_Replace(strTemp, ArrField(1, iField), "<img class='fieldImg' src='" &PE_HTMLDecode(rsCustom(Trim(ArrField(0, iField))))&"' border=0>")	
                    End If
                Case Else
                    strTemp = PE_Replace(strTemp, ArrField(1, iField), PE_HTMLEncode(rsCustom(Trim(ArrField(0, iField)))))				
                End Select 
           Next
        End If
		
        strCustomList = strCustomList & strTemp
        rsCustom.MoveNext
        iCount = iCount + 1
        If iCols > 1 And iCount Mod iCols = 0 Then strCustomList = strCustomList & iColsHtml
        If iRows > 1 And iCount Mod iCols * iRows = 0 Then strCustomList = strCustomList & iRowsHtml
        If UsePage = True And iCount >= MaxPerPage Then Exit Do
    Loop
    rsCustom.Close
    Set rsCustom = Nothing
    
    GetCustomFromLabel = strCustomList
End Function

'解析相关软件标签
Public Function GetCorrelativeFromTemplate(strValue)  
    Dim strCorrelative, strParameter
    strCorrelative = strValue
    regEx.Pattern = "【relatesoft\((.*?)\)】([\s\S]*?)【\/relatesoft】"
    Set Matches = regEx.Execute(strCorrelative)
    For Each Match In Matches
        strParameter = Replace(Match.SubMatches(0), Chr(34), " ")
        strCorrelative = PE_Replace(strCorrelative, Match.Value, GetCorrelative(strParameter, Match.SubMatches(1)))
    Next
    GetCorrelativeFromTemplate = strCorrelative
End Function

'=================================================
'函数名：GetCorrelative
'作  用：显示相关软件 之中参数(SoftNum, TitleLen, OrderType)
'=================================================
private Function GetCorrelative(strTemp, strList)
    Dim arrTemp
	Dim SoftNum,TitleLen,OrderType
    Dim rsCorrelative, sqlCorrelative, strCorrelative
    Dim strKey, arrKey, i, MaxNum
	strCorrelative=""
    arrTemp = Split(strTemp, ",")
    If UBound(arrTemp) <> 2 Then
        GetCorrelative = "相关软件标签：【relatesoft(参数列表)】列表内容【/relatesoft】的参数个数不对。请检查模板中的此标签。"
        Exit Function
    End If
	SoftNum = PE_CLng(arrTemp(0))
    TitleLen = PE_CLng(arrTemp(1))
	OrderType = PE_CLng(arrTemp(2))
    If TitleLen < 0 Or TitleLen > 255 Then TitleLen = 50
    If SoftNum > 0 And SoftNum <= 100 Then
        sqlCorrelative = "select top " & SoftNum
    Else
        sqlCorrelative = "Select Top 5 "
    End If
    strKey = Mid(rsSoft("Keyword"), 2, Len(rsSoft("Keyword")) - 2)
    If InStr(strKey, "|") > 1 Then
        arrKey = Split(strKey, "|")
        MaxNum = UBound(arrKey)
        If MaxNum > 4 Then MaxNum = 4
        strKey = "((S.Keyword like '%|" & Replace(Replace(arrKey(0), "［", ""), "］", "") & "|%')"
        For i = 1 To MaxNum
            strKey = strKey & " or (S.Keyword like '%|" & Replace(Replace(arrKey(i), "［", ""), "］", "") & "|%')"
        Next
        strKey = strKey & ")"
    Else
        strKey = "(S.Keyword like '%|" & strKey & "|%')"
    End If
    sqlCorrelative = sqlCorrelative & " S.SoftID,S.ClassID,S.SoftName,S.SoftVersion,S.UpdateTime,C.ClassName from PW_Soft S left join PW_Class C on S.ClassID=C.ClassID where S.ChannelID=" & ChannelID & " and S.Deleted=" & PE_False & ""
    sqlCorrelative = sqlCorrelative & " and " & strKey & " and S.SoftID<>" & SoftID & " Order by "
    Select Case PE_CLng(OrderType)
    Case 1
        sqlCorrelative = sqlCorrelative & "S.SoftID desc"
    Case 2
        sqlCorrelative = sqlCorrelative & "S.SoftID asc"
    Case 3
        sqlCorrelative = sqlCorrelative & "S.UpdateTime desc"
    Case 4
        sqlCorrelative = sqlCorrelative & "S.UpdateTime asc"
    Case Else
        sqlCorrelative = sqlCorrelative & "S.SoftID desc"
    End Select
    Set rsCorrelative = Conn.Execute(sqlCorrelative)
    If rsCorrelative.BOF And rsCorrelative.EOF Then
        strCorrelative = "没有相关信息！"
    Else
        Do While Not rsCorrelative.EOF
			strTemp = strList
			strTemp = PE_Replace(strTemp, "{$classid}", rsCorrelative("ClassID"))
			strTemp = PE_Replace(strTemp, "{$classname}", rsCorrelative("ClassName"))
			strTemp = PE_Replace(strTemp, "{$classurl}", GetClassUrl(rsCorrelative("ClassID")))
			If TitleLen > 0 Then
				strTemp = PE_Replace(strTemp, "{$softname}", GetSubStr(rsCorrelative("SoftName"), TitleLen, false))
			Else
				strTemp = PE_Replace(strTemp, "{$softname}", rsCorrelative("SoftName"))
			End If
			strTemp = PE_Replace(strTemp, "{$softnameall}", rsCorrelative("SoftName"))
			strTemp = PE_Replace(strTemp, "{$softurl}", GetSoftUrl(rsCorrelative("UpdateTime"), rsCorrelative("SoftID")))
			strTemp = PE_Replace(strTemp, "{$updatetime}", rsCorrelative("UpdateTime"))
			If InStr(strHtml, "{$month}") > 0 Then strTemp = PE_Replace(strTemp, "{$month}", getMonth(rsCorrelative("UpdateTime")))	
			If InStr(strHtml, "{$day}") > 0 Then strTemp = PE_Replace(strTemp, "{$day}", getDay(rsCorrelative("UpdateTime")))	
			If InStr(strHtml, "{$year}") > 0 Then strTemp = PE_Replace(strTemp, "{$year}", getYear(rsCorrelative("UpdateTime")))
			strCorrelative = strCorrelative & strTemp
        rsCorrelative.MoveNext
    Loop
	End if
    rsCorrelative.Close
    Set rsCorrelative = Nothing
    
    GetCorrelative = strCorrelative
End function


'=================================================
'函数名：GetPrevSoft
'作  用：显示上一个软件
'参  数：TitleLen   ----标题最多字符数，一个汉字=两个英文字符
'=================================================
Private Function GetPrevSoft(TitleLen)
    Dim rsPrev, sqlPrev, strPrev
    strPrev = Replace("<li>上一{$ItemUnit}： ", "{$ItemUnit}", ChannelItemUnit & ChannelShortName)
    sqlPrev = "Select Top 1 SoftID,SoftName,UpdateTime from PW_Soft Where ChannelID=" & ChannelID & " and Deleted=" & PE_False & " and ClassID=" & rsSoft("ClassID") & " and SoftID<" & rsSoft("SoftID") & " order by SoftID DESC"
    Set rsPrev = Conn.Execute(sqlPrev)
    If TitleLen < 0 Or TitleLen > 255 Then TitleLen = 50
    If rsPrev.EOF Then
        strPrev = strPrev & "没有了"
    Else
        strPrev = strPrev & "<a href='" & GetSoftUrl(rsPrev("UpdateTime"), rsPrev("SoftID")) &"'"
        strPrev = strPrev & " title='" & rsPrev("SoftName") &"'>" & GetSubStr(rsPrev("SoftName"), TitleLen, True) & "</a>"
    End If
    rsPrev.Close
    Set rsPrev = Nothing
    strPrev = strPrev & "</li>"
    GetPrevSoft = strPrev
End Function


'=================================================
'函数名：GetNextSoft
'作  用：显示下一个软件
'参  数：TitleLen   ----标题最多字符数，一个汉字=两个英文字符
'=================================================
Private Function GetNextSoft(TitleLen)
    Dim rsNext, sqlNext, strNext
    strNext = Replace("<li>下一{$ItemUnit}： ", "{$ItemUnit}", ChannelItemUnit & ChannelShortName)
    sqlNext = "Select Top 1 SoftID,SoftName,UpdateTime from PW_Soft Where ChannelID=" & ChannelID & " and Deleted=" & PE_False & " and ClassID=" & rsSoft("ClassID") & " and SoftID>" & rsSoft("SoftID") & " order by SoftID ASC"
    Set rsNext = Conn.Execute(sqlNext)
    If TitleLen < 0 Or TitleLen > 255 Then TitleLen = 50
    If rsNext.EOF Then
        strNext = strNext & "没有了"
    Else
        strNext = strNext & "<a href='" & GetSoftUrl(rsNext("UpdateTime"), rsNext("SoftID")) &"'"
        strNext = strNext & " title='" & rsNext("SoftName") &"'>" & GetSubStr(rsNext("SoftName"), TitleLen, True) & "</a>"
    End If
    rsNext.Close
    Set rsNext = Nothing
    strNext = strNext & "</li>"
    GetNextSoft = strNext
End Function


Public Sub GetHtml_Index()
    Call GetChannel(ChannelID)
    ClassID = 0
	strHtml = GetTemplate(tempindex)
    Call ReplaceCommonLabel
    strHtml = PE_Replace(strHtml, "{$pagetitle}", strPageTitle)
    strHtml = PE_Replace(strHtml, "{$location}", strNavPath)
    strHtml = GetCustomFromTemplate(strHtml)
    If ChannelID <> PrevChannelID Then
        Call GetChannel(ChannelID)
        PrevChannelID = ChannelID
    End If
    If UseCreateHTML > 0 Then
        If InStr(strHtml, "{$showpage}") > 0 Then strHtml = Replace(strHtml, "{$showpage}", ShowPage_Html(ChannelUrl & "/", 0, ".html", strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName))
        If InStr(strHtml, "{$showpage_en}") > 0 Then strHtml = Replace(strHtml, "{$showpage_en}", ShowPage_en_Html(ChannelUrl & "/", 0, ".html", strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName))
    Else
        If InStr(strHtml, "{$showpage}") > 0 Then strHtml = Replace(strHtml, "{$showpage}", ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName, False))
        If InStr(strHtml, "{$showpage_en}") > 0 Then strHtml = Replace(strHtml, "{$ShowPage_en}", ShowPage_en(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName, False))
    End If
End Sub


Public Sub GetHtml_Class()
	If list_template="" then
		list_template = templist
	End if
	strHtml = GetTemplate(list_template)
    strHtml = PE_Replace(strHtml, "{$ClassID}", ClassID)
    strHtml = PE_Replace(strHtml, "{$meta_keywords}", Meta_Keywords_Class)
    strHtml = PE_Replace(strHtml, "{$meta_description}", Meta_Description_Class)
    Call ReplaceCommonLabel
    strHtml = PE_Replace(strHtml, "{$ClassID}", ClassID)
    strHtml = PE_Replace(strHtml, "{$pagetitle}", strPageTitle)
    strHtml = PE_Replace(strHtml, "{$location}", strNavPath)
    strHtml = GetCustomFromTemplate(strHtml)
    If ChannelID <> PrevChannelID Then
        Call GetChannel(ChannelID)
        PrevChannelID = ChannelID
    End If
    Dim strPath
    strPath = ChannelUrl & "/"
    strHtml = PE_Replace(strHtml, "{$classname}", ClassName)
    strHtml = PE_Replace(strHtml, "{$classpicurl}", ClassPicUrl)
    strHtml = PE_Replace(strHtml, "{$classurl}", GetClassUrl(ClassID))
	IF UseCreateHTML > 0 then
		If InStr(strHtml, "{$showpage}") > 0 Then strHtml = PE_Replace(strHtml, "{$showpage}", ShowPage_Html(strPath, ClassID, ".html", "", totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName))
        If InStr(strHtml, "{$showpage_en}") > 0 Then strHtml = PE_Replace(strHtml, "{$showpage_en}", ShowPage_en_Html(strPath, ClassID, ".html", "", totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName))
	else
		If InStr(strHtml, "{$showpage}") > 0 Then strHtml = PE_Replace(strHtml, "{$showpage}", ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName, False))
        If InStr(strHtml, "{$showpage_en}") > 0 Then strHtml = PE_Replace(strHtml, "{$showpage_en}", ShowPage_en(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName, False))	
	end IF
End Sub



Public Sub GetHtml_Soft()
    strHtml = GetCustomFromTemplate(strHtml)  '必须先解析自定义列表标签
    If ChannelID <> PrevChannelID Then
        Call GetChannel(ChannelID)
        PrevChannelID = ChannelID
    End If
    strHtml = PE_Replace(strHtml, "{$classid}", ClassID)
    strHtml = PE_Replace(strHtml, "{$softid}", SoftID)
    strHtml = PE_Replace(strHtml, "{$meta_keywords}", GetKeywords(",", rsSoft("Keyword")))
    strHtml = PE_Replace(strHtml, "{$meta_description}", rsSoft("softintro"))
   Call ReplaceCommonLabel   '解析通用标签，包含自定义标签
    strHtml = GetCustomFromTemplate(strHtml)  '必须先解析自定义列表标签
    If ChannelID <> PrevChannelID Then
        Call GetChannel(ChannelID)
        PrevChannelID = ChannelID
    End If
    strHtml = PE_Replace(strHtml, "{$classid}", ClassID)
    strHtml = PE_Replace(strHtml, "{$softid}", SoftID)
    strHtml = PE_Replace(strHtml, "{$pagetitle}", SoftName & "_" & SiteName)
    strHtml = PE_Replace(strHtml, "{$location}", strNavPath & "&nbsp;" & strNavLink & "&nbsp;详细内容")

    If InStr(strHtml, "{$MY_") > 0 Then
        Dim rsField
        Set rsField = Conn.Execute("select * from PW_Field where ChannelID=" & ChannelID & "")
        Do While Not rsField.EOF
            If rsField("FieldType") = 8 Or rsField("FieldType") = 9 Then
                strHtml = PE_Replace(strHtml, rsField("LabelName"), PE_HTMLDecode(rsSoft(Trim(rsField("FieldName")))))
            Else
                strHtml = PE_Replace(strHtml, rsField("LabelName"), PE_HTMLEncode(rsSoft(Trim(rsField("FieldName")))))		
            End If	
            rsField.MoveNext
        Loop
        Set rsField = Nothing
    End If
    strHtml = PE_Replace(strHtml, "{$classid}", ClassID)
    strHtml = PE_Replace(strHtml, "{$classname}", ClassName)
	strHtml = PE_Replace(strHtml, "{$classurl}", GetClassUrl(ClassID))
 	strHtml = PE_Replace(strHtml, "{$softname}", rsSoft("SoftName"))	
	strHtml = PE_Replace(strHtml, "{$softintro}", rsSoft("SoftIntro"))
	strHtml = PE_Replace(strHtml, "{$softdetial}", ReplaceKeyLink(GetContent(rsSoft("Softdetial"))))
    strHtml = PE_Replace(strHtml, "{$softversion}", rsSoft("SoftVersion"))
    strHtml = PE_Replace(strHtml, "{$softsize}", rsSoft("SoftSize"))
    strHtml = PE_Replace(strHtml, "{$softsize_m}", Round(rsSoft("SoftSize") / 1024, 2))
    strHtml = PE_Replace(strHtml, "{$operatingsystem}", rsSoft("OperatingSystem"))
	strHtml = PE_Replace(strHtml, "{$hits}", GetHits())
	strHtml = PE_Replace(strHtml, "{$author}", rsSoft("Author"))
	strHtml = PE_Replace(strHtml, "{$copyfrom}", rsSoft("CopyFrom"))
	strHtml = PE_Replace(strHtml, "{$softtype}", PE_HTMLEncode(rsSoft("SoftType")))
	strHtml = PE_Replace(strHtml, "{$softlanguage}", PE_HTMLEncode(rsSoft("SoftLanguage")))
	strHtml = PE_Replace(strHtml, "{$copyrighttype}", PE_HTMLEncode(rsSoft("CopyrightType")))
	strHtml = PE_Replace(strHtml, "{$updatedate}", FormatDateTime(rsSoft("UpdateTime"), 2))
	strHtml = PE_Replace(strHtml, "{$updatetime}", rsSoft("UpdateTime"))
	strHtml = PE_Replace(strHtml, "{$demourl}", rsSoft("DemoUrl"))
    If InStr(strHtml, "{$downloadurl}") > 0 Then strHtml = PE_Replace(strHtml, "{$downloadurl}", GetDownloadUrlList(rsSoft("DownloadUrl")))
    Dim arrTemp,strParameter,strTemp
    Dim strSoftPic
    '替换下载图片
    regEx.Pattern = "\{\$softpic\((.*?)\)\}"
    Set Matches = regEx.Execute(strHtml)
    For Each Match In Matches
        arrTemp = Split(Match.SubMatches(0), ",")
        strSoftPic = GetSoftPicUrl(Trim(rsSoft("SoftPicUrl")), PE_CLng(arrTemp(0)), PE_CLng(arrTemp(1)))
        strHtml = Replace(strHtml, Match.value, strSoftPic)
    Next
	'''替换相关软件
	strHtml = GetCorrelativeFromTemplate(strHtml)
	'''替换上一条
	regEx.Pattern = "\{\$prevsoft\((.*?)\)\}"
	Set Matches = regEx.Execute(strHtml)
	For Each Match In Matches
		strParameter = PE_Clng(Match.SubMatches(0))
		if strParameter<=0 and strParameter>255 then
			strParameter = 50
		end if
		strTemp = GetPrevSoft(strParameter)
		strHtml = PE_Replace(strHtml, Match.Value, strTemp)
	Next
	'''替换下一条
	regEx.Pattern = "\{\$nextsoft\((.*?)\)\}"
	Set Matches = regEx.Execute(strHtml)
	For Each Match In Matches
		strParameter = PE_Clng(Match.SubMatches(0))
		if strParameter<=0 and strParameter>255 then
			strParameter = 50
		end if
		strTemp = GetNextsoft(strParameter)
		strHtml = PE_Replace(strHtml, Match.Value, strTemp)
	Next
End Sub


Private Function GetDownloadUrlList(TempDownloadUrls)
    Dim DownloadUrls, arrDownloadUrls, arrUrls, iTemp, strUrls
    DownloadUrls = TempDownloadUrls
    If DownloadUrls = "" Then
        GetDownloadUrlList = ""
        Exit Function
    End If
    strUrls = ""
        arrDownloadUrls = Split(DownloadUrls, "$$$")
        For iTemp = 0 To UBound(arrDownloadUrls)
            arrUrls = Split(arrDownloadUrls(iTemp), "|")
            If UBound(arrUrls) >= 1 Then
                If arrUrls(1) <> "" Then
                        strUrls = strUrls & "<a href='" & ChannelUrl_ASPFile & "/softdown.asp?UrlID=" & iTemp + 1 & "&SoftID=" & rsSoft("SoftID") & "' target='_blank'>" & arrUrls(0) & "</a>&nbsp;"
                End If
            End If
        Next
        GetDownloadUrlList = strUrls
End Function



Public Function GetDownloadUrl()
    GetDownloadUrl = "ErrorDownloadUrl"
    Call Init
    Dim UrlID, SoftID, ComeUrl, cUrl

    SoftID = PE_CLng(GetUrl("SoftID"))
    UrlID = PE_CLng(GetUrl("UrlID"))
    If SoftID = 0 Then
        FoundErr = True
        Response.Write "<br><li>请指定" & ChannelShortName & "ID！</li>"
        Exit Function
    End If
    If UrlID <= 0 Then UrlID = 1

	ComeUrl = Replace(LCase(Trim(Request.ServerVariables("HTTP_REFERER"))), "http://", "")
	cUrl = LCase(Trim(Request.ServerVariables("SERVER_NAME")))
	If ComeUrl <> "" Then
		If Left(ComeUrl, Len(cUrl)) <> cUrl Then
			FoundErr = True
			Response.Write "<br><li>请勿非法盗链或下载本站软件！</li>"
			Exit Function
		End If
	End If
    
    Dim sqlSoft, rsSoft, LastHitTime
    sqlSoft = "select S.SoftID,S.SoftName,S.DownloadUrl,S.InfoPurview from PW_Soft S left join PW_Class C on S.ClassID=C.ClassID where S.Deleted=" & PE_False & " and S.SoftID=" & SoftID & " and S.ChannelID=" & ChannelID & ""
    Set rsSoft = Server.CreateObject("ADODB.Recordset")
    rsSoft.Open sqlSoft, Conn, 1, 3
    If rsSoft.BOF And rsSoft.BOF Then
        FoundErr = True
        Response.Write "<br><li>找不到指定的" & ChannelShortName & "！</li>"
        rsSoft.Close
        Set rsSoft = Nothing
        Exit Function
    End If

    Dim DownloadUrl, DownloadUrls, arrDownloadUrls, arrUrls, iTemp

    DownloadUrls = GetContent(rsSoft("DownloadUrl"))
    iTemp = UrlID - 1
	arrDownloadUrls = Split(DownloadUrls, "$$$")
	If UBound(arrDownloadUrls) >= iTemp Then
		arrUrls = Split(arrDownloadUrls(iTemp), "|")
		If UBound(arrUrls) >= 1 Then
			DownloadUrl = GetFirstSeparatorToEnd(arrDownloadUrls(iTemp), "|")
		End If
	End If
    
    If DownloadUrl = "" Or DownloadUrl = "http://" Then
        FoundErr = True
        Response.Write "<br><li>找不到有效下载地址！</li>"
        rsSoft.Close
        Set rsSoft = Nothing
        Exit Function
    End If
	Dim ErrMsg_NoLogin, ErrMsg_PurviewCheckedErr
    ErrMsg_NoLogin = Replace(Replace(Replace("<br>&nbsp;&nbsp;&nbsp;&nbsp;你还没注册？或者没有登录？这{$ItemUnit}要求至少是本站的注册会员才能下载！<br><br>&nbsp;&nbsp;&nbsp;&nbsp;如果你还没注册，请赶紧<a href='{$InstallDir}member/Reg.asp'><font color=red>点此注册</font></a>吧！<br><br>&nbsp;&nbsp;&nbsp;&nbsp;如果你已经注册但还没登录，请赶紧<a href='{$InstallDir}member/User_Login.asp'><font color=red>点此登录</font></a>吧！<br><br>", "{$ItemUnit}", ChannelItemUnit & ChannelShortName), "{$ChannelItemUnit}", ChannelItemUnit), "{$InstallDir}", strInstallDir)
	If rsSoft("InfoPurview")<>"0" then
        If UserLogined <> True Then
            FoundErr = True
            ErrMsg = ErrMsg & ErrMsg_NoLogin
        Else
			Call GetUser(UserID)
			ErrMsg_PurviewCheckedErr = "对不起，您没有查看此栏目内容的权限！"
			if FoundInArr(rsSoft("InfoPurview"),GroupID,",") = false then
				FoundErr = True
				ErrMsg = ErrMsg & ErrMsg_PurviewCheckedErr
			end if
		End IF
	end If
    rsSoft.Close
    Set rsSoft = Nothing
    If FoundErr = True Then
        Response.Write ErrMsg
        Exit Function
    End If
    
    Dim sqlHits
    sqlHits = "update PW_Soft set Hits=Hits+1"
    sqlHits = sqlHits & " where SoftID=" & SoftID
    Conn.Execute (sqlHits)

    GetDownloadUrl = DownloadUrl
End Function


Private Function GetSoftPicUrl(SoftPicUrl, SoftPicWidth, SoftPicHeight)
    Dim strSoftPicUrl, FileType, strPicUrl
	
    If SoftPicUrl = "" Then
        strSoftPicUrl = strSoftPicUrl & "<img src='" & strInstallDir & "images/nopic.gif' "
        If SoftPicWidth > 0 Then strSoftPicUrl = strSoftPicUrl & " width='" & SoftPicWidth & "'"
        If SoftPicHeight > 0 Then strSoftPicUrl = strSoftPicUrl & " height='" & SoftPicHeight & "'"
        strSoftPicUrl = strSoftPicUrl & " border='0'>"
    Else
        SoftPicUrl = GetContent(SoftPicUrl)
        FileType = LCase(Mid(SoftPicUrl, InStrRev(SoftPicUrl, ".") + 1))
        If FileType = "swf" Then
            strSoftPicUrl = strSoftPicUrl & "<object classid='clsid:D27CDB6E-AE6D-11cf-96B8-444553540000' codebase='http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=5,0,0,0' "
            If SoftPicWidth > 0 Then strSoftPicUrl = strSoftPicUrl & " width='" & SoftPicWidth & "'"
            If SoftPicHeight > 0 Then strSoftPicUrl = strSoftPicUrl & " height='" & SoftPicHeight & "'"
            strSoftPicUrl = strSoftPicUrl & "><param name='movie' value='" & strPicUrl & "'><param name='quality' value='high'><embed src='" & strPicUrl & "' pluginspage='http://www.macromedia.com/shockwave/download/index.cgi?P1_Prod_Version=ShockwaveFlash' type='application/x-shockwave-flash' "
            If SoftPicWidth > 0 Then strSoftPicUrl = strSoftPicUrl & " width='" & SoftPicWidth & "'"
            If SoftPicHeight > 0 Then strSoftPicUrl = strSoftPicUrl & " height='" & SoftPicHeight & "'"
            strSoftPicUrl = strSoftPicUrl & "></embed></object>"
        ElseIf FileType = "gif" Or FileType = "jpg" Or FileType = "jpeg" Or FileType = "jpe" Or FileType = "bmp" Or FileType = "png" Then
            strSoftPicUrl = strSoftPicUrl & "<img src='" & SoftPicUrl & "' "
            If SoftPicWidth > 0 Then strSoftPicUrl = strSoftPicUrl & " width='" & SoftPicWidth & "'"
            If SoftPicHeight > 0 Then strSoftPicUrl = strSoftPicUrl & " height='" & SoftPicHeight & "'"
            strSoftPicUrl = strSoftPicUrl & " border='0'>"
        Else
            strSoftPicUrl = strSoftPicUrl & "<img src='" & strInstallDir & "images/nopic.gif' "
            If SoftPicWidth > 0 Then strSoftPicUrl = strSoftPicUrl & " width='" & SoftPicWidth & "'"
            If SoftPicHeight > 0 Then strSoftPicUrl = strSoftPicUrl & " height='" & SoftPicHeight & "'"
            strSoftPicUrl = strSoftPicUrl & " border='0'>"
        End If
    End If
    GetSoftPicUrl = strSoftPicUrl
End Function

Private Function GetHits()
    Dim strHits
    If UseCreateHTML > 0 Then
        strHits = "<script language='javascript' src='" & ChannelUrl_ASPFile & "/GetHits.asp?SoftID=" & SoftID & "'></script>"
    Else
        strHits = rsSoft("Hits")
    End If
    GetHits = strHits
End Function
	
End Class
%>