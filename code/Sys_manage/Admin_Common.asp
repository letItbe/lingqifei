<!--#include file="../Sys_Start.asp"-->
<!--#include file="../Include/PW_Channel.asp"-->
<!--#include file="../Include/PW_Common_Manage.asp"-->
<!--#include file="Admin_CommonCode.asp"-->
<%
Server.ScriptTimeOut = 9999999
ChannelID = PE_CLng(Trim(Request("ChannelID")))
If ChannelID > 0 Then
    Call GetChannel(ChannelID)
End If
If NeedCheckComeUrl = True Then
    Call CheckComeUrl
End If

Dim AdminID, AdminName, AdminPassword, RndPassword, AdminLoginCode, AdminPurview, PurviewPassed
Dim AdminPurview_Channel, AdminPurview_Others, AdminPurview_Info

Dim rsGetAdmin, sqlGetAdmin
AdminName = ReplaceBadChar(Trim(Request.Cookies(Site_Sn)("AdminName")))
AdminPassword = ReplaceBadChar(Trim(Request.Cookies(Site_Sn)("AdminPassword")))
RndPassword = ReplaceBadChar(Trim(Request.Cookies(Site_Sn)("RndPassword")))
AdminLoginCode = ReplaceBadChar(Trim(Request.Cookies(Site_Sn)("AdminLoginCode")))
If AdminName = "" Or AdminPassword = "" Or RndPassword = "" Or (EnableSiteManageCode = True And AdminLoginCode <> SiteManageCode) Then
    Call CloseConn
    Response.redirect "Admin_login.asp"
End If

sqlGetAdmin = "select * from PW_Admin where AdminName='" & AdminName & "' and Password='" & AdminPassword & "'"
Set rsGetAdmin = Server.CreateObject("adodb.recordset")
rsGetAdmin.Open sqlGetAdmin, Conn, 1, 1
If rsGetAdmin.BOF And rsGetAdmin.EOF Then
    rsGetAdmin.Close
    Set rsGetAdmin = Nothing
    Call CloseConn
    Response.redirect "Admin_login.asp"
Else
    If rsGetAdmin("EnableMultiLogin") <> True And Trim(rsGetAdmin("RndPassword")) <> RndPassword Then
        Response.write "<br><p align=center><font color='red'>对不起，为了系统安全，本系统不允许两个人使用同一个管理员帐号进行登录！</font></p><p>因为现在有人已经在其他地方使用此管理员帐号进行登录了，所以你将不能继续进行后台管理操作。</p><p>你可以<a href='Admin_Login.asp' target='_top'>点此重新登录</a>。</p>"
        rsGetAdmin.Close
        Set rsGetAdmin = Nothing
        Call CloseConn
        Response.End
    End If
End If
AdminID = rsGetAdmin("ID")
AdminPurview = rsGetAdmin("Purview")
AdminPurview_Channel = rsGetAdmin("Purview_Channel")
AdminPurview_Others = rsGetAdmin("Purview_Others")
AdminPurview_Info = rsGetAdmin("Purview_Info")
rsGetAdmin.Close
Set rsGetAdmin = Nothing


Dim Purview_View,Purview_Add,Purview_Modify,Purview_Del,Purview_Class
Purview_View = purview_ChannelItem(AdminPurview_Channel,"0",ChannelID)
Purview_Add = purview_ChannelItem(AdminPurview_Channel,"1",ChannelID)
Purview_Modify = purview_ChannelItem(AdminPurview_Channel,"2",ChannelID)
Purview_Del = purview_ChannelItem(AdminPurview_Channel,"3",ChannelID)
Purview_Class = purview_ChannelItem(AdminPurview_Channel,"4",ChannelID)

if Purview_View=True or AdminPurview=1 then Purview_View=True
if Purview_Add=True or AdminPurview=1 then Purview_Add=True
if Purview_Modify=True or AdminPurview=1 then Purview_Modify=True
if Purview_Del=True or AdminPurview=1 then Purview_Del=True
if Purview_Class=True or AdminPurview=1 then Purview_Class=True

PurviewPassed = False   '默认设置为没有权限
If AdminPurview = 1 Then   '如果是超级管理员，直接有所有权限
    PurviewPassed = True
elseif PurviewLevel_Others="" and PurviewLevel_Channel=0 then 
	PurviewPassed=True
Else
    '如果是普通管理员，根据文件的设置判断是否有相应的权限
	If PurviewLevel_Others <> "" Then 
		if PurviewLevel_Others="Info" then 
    		Select Case Action
				Case "Add","SaveAdd"
					PurviewPassed = FoundInArr(AdminPurview_Info, "info_Add",",")
				Case "Modify","SaveModify"
					PurviewPassed = FoundInArr(AdminPurview_Info, "info_Modify",",")
				Case "Del"
					PurviewPassed = FoundInArr(AdminPurview_Info, "info_Del",",")
				case Else
					PurviewPassed = True
			end Select 
		else
			PurviewPassed = CheckPurview_Other(AdminPurview_Others, PurviewLevel_Others)
		end if
    else
		if PurviewLevel_Channel<>0 then
			Select Case Action
				Case "Show","Manage"
					PurviewPassed=Purview_View
				Case "Add","SaveAdd" ''添加 
					PurviewPassed=Purview_Add
				case "Modify","SaveModify","SetOnTop","CancelOnTop","SetElite","CancelElite" ''修改 
					PurviewPassed=Purview_Modify
				Case "Del","ConfirmDel","ClearRecyclebin" ''删除 
					PurviewPassed=Purview_Del
				case Else
					PurviewPassed=True
			End Select
		end if
	end if
	
End If

If PurviewPassed = False Then
    Response.write "<br><p align=center><font color='red'>对不起，你没有此项操作的权限。</font></p>"
    Call CloseConn
    Response.End
End If

%>