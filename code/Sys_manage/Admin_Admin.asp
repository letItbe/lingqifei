<!--#include file="Admin_Common.asp"-->
<!--#include file="../Include/PW_MD5.asp"-->
<%
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Channel = 0   '0--不检查，1 检查
Const PurviewLevel_Others = "Admin"   '其他权限

strFileName = "Admin_Admin.asp"

Response.Write "<html><head><title>管理员管理</title><meta http-equiv='Content-Type' content='text/html; charset=gb2312'><link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
Call ShowPageTitle("管 理 员 管 理")
Response.Write "  <tr class='tdbg'>" & vbCrLf
Response.Write "    <td width='70' height='30'><strong>管理导航：</strong></td>" & vbCrLf
Response.Write "    <td height='30'><a href='Admin_Admin.asp'>管理员管理首页</a>&nbsp;|&nbsp;<a href='Admin_Admin.asp?Action=Add'>新增管理员</a>"
Response.Write "    </td>" & vbCrLf
Response.Write "  </tr>" & vbCrLf
Response.Write "</table>" & vbCrLf

Select Case Action
Case "Add"
    Call AddAdmin
Case "SaveAdd"
    Call SaveAdd
Case "ModifyPwd"
    Call ModifyPwd
Case "ModifyPurview"
    Call ModifyPurview
Case "SaveModifyPwd"
    Call SaveModifyPwd
Case "SaveModifyPurview"
    Call SaveModifyPurview
Case "Del"
    Call DelAdmin
Case Else
    Call main
End Select
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn


Sub main()
    Dim rsAdminList, sqlAdminList
    Set rsAdminList = Server.CreateObject("Adodb.RecordSet")
    sqlAdminList = "select * from PW_Admin where Purview<>1 order by id"
    rsAdminList.Open sqlAdminList, Conn, 1, 1
    If rsAdminList.BOF And rsAdminList.EOF Then
        rsAdminList.Close
        Set rsAdminList = Nothing
        Response.Write "没有任何管理员！"
        Exit Sub
    End If
    Response.Write "<SCRIPT language=javascript>" & vbCrLf
    Response.Write "function unselectall(){" & vbCrLf
    Response.Write "    if(document.myform.chkAll.checked){" & vbCrLf
    Response.Write " document.myform.chkAll.checked = document.myform.chkAll.checked&0;" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "}" & vbCrLf
    
    Response.Write "function CheckAll(form){" & vbCrLf
    Response.Write "  for (var i=0;i<form.elements.length;i++){" & vbCrLf
    Response.Write "    var e = form.elements[i];" & vbCrLf
    Response.Write "    if (e.Name != 'chkAll'&&e.disabled!=true)" & vbCrLf
    Response.Write "       e.checked = form.chkAll.checked;" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "}" & vbCrLf
    
    Response.Write "</script>" & vbCrLf
    Response.Write "<br><table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>"
    Response.Write "  <form name='myform' method='Post' action='Admin_Admin.asp' onsubmit=""return confirm('确定要删除选中的管理员吗？');"">"
    Response.Write "     <td>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
    Response.Write "  <tr align='center' class='title' height='22'>"
    Response.Write "    <td  width='30'><strong>选中</strong></td>"
    Response.Write "    <td  width='30' height='22'><strong>序号</strong></td>"
    Response.Write "    <td><strong>管理员名</strong></td>"
    Response.Write "    <td width='55'><strong>多人登录</strong></td>"
    Response.Write "    <td width='95'><strong>最后登录IP</strong></td>"
    Response.Write "    <td width='115'><strong>最后登录时间</strong></td>"
    Response.Write "    <td width='55'><strong>登录次数</strong></td>"
    Response.Write "    <td width='180'><strong>操 作</strong></td>"
    Response.Write "  </tr>"
    Do While Not rsAdminList.EOF
        Response.Write "  <tr align='center' class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"
        Response.Write "    <td width='30'><input name='ID' type='checkbox' id='ID' value='" & rsAdminList("ID") & "'"
        If rsAdminList("AdminName") = AdminName Then Response.Write " disabled"
        Response.Write " onclick='unselectall()'></td>"
        Response.Write "    <td width='30'>" & rsAdminList("ID") & "</td>"
        Response.Write "    <td>"
        If rsAdminList("AdminName") = AdminName Then
            Response.Write "<font color=red><b>" & rsAdminList("AdminName") & "</b></font>"
        Else
            Response.Write rsAdminList("AdminName")
        End If
        Response.Write "</td>"
        Response.Write "<td width='55'>"
        If rsAdminList("EnableMultiLogin") = True Then
            Response.Write "<font color='green'>允许</font>"
        Else
            Response.Write "<font color='red'>不允许</font>"
        End If
        Response.Write "</td>"
        Response.Write "<td width='95'>"
        If rsAdminList("LastLoginIP") <> "" Then
            Response.Write rsAdminList("LastLoginIP")
        Else
            Response.Write "&nbsp;"
        End If
        Response.Write "</td>"
        Response.Write "<td width='115'>"
        If rsAdminList("LastLoginTime") <> "" Then
            Response.Write rsAdminList("LastLoginTime")
        Else
            Response.Write "&nbsp;"
        End If
        Response.Write "</td>"
        Response.Write "<td width='55'>"
        If Trim(rsAdminList("LoginTimes")) <> "" Then
            Response.Write rsAdminList("LoginTimes")
        Else
            Response.Write "0"
        End If
        Response.Write "</td>"
        Response.Write "<td width='180'>"
        Response.Write "<a href='Admin_Admin.asp?Action=ModifyPwd&ID=" & rsAdminList("ID") & "'>修改密码及设置</a>&nbsp;&nbsp;"
        Response.Write "<a href='Admin_Admin.asp?Action=ModifyPurview&ID=" & rsAdminList("ID") & "'>修改权限</a>&nbsp;&nbsp;"
        Response.Write "</td>"
        Response.Write "</tr>"
        rsAdminList.MoveNext
    Loop
    rsAdminList.Close
    Set rsAdminList = Nothing
    
    Response.Write "</table>  "
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>"
    Response.Write "  <tr>"
    Response.Write "    <td width='200' height='30'><input name='chkAll' type='checkbox' id='chkAll' onclick='CheckAll(this.form)' value='checkbox'> 选中本页显示的所有管理员</td>"
    Response.Write "    <td><input name='Action' type='hidden' id='Action' value='Del'>"
    Response.Write "        <input name='Submit' type='submit' id='Submit' value='删除选中的管理员'></td>"
    Response.Write "  </tr>"
    Response.Write "</table>"
    Response.Write "</td>"
    Response.Write "</form></tr></table>"
End Sub


Sub AddAdmin()
    Call ShowJS_Check
    Response.Write "<form method='post' action='Admin_Admin.asp' name='form1' onsubmit='javascript:return CheckAdd();'>"
    Response.Write "  <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border' >"
    Response.Write "    <tr class='title'> "
    Response.Write "      <td height='22' colspan='2'> <div align='center'><strong>新 增 管 理 员</strong></div></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'> "
    Response.Write "      <td width='12%' align='right' class='tdbg'><strong>管理员名：</strong></td>"
    Response.Write "      <td width='88%' class='tdbg'><input name='AdminName' type='text'></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'> "
    Response.Write "      <td width='12%' align='right' class='tdbg'><strong>初始密码：</strong></td>"
    Response.Write "      <td width='88%' class='tdbg'><input type='password' name='Password' onkeyup='javascript:EvalPwdStrength(document.forms[0],this.value);' onmouseout='javascript:EvalPwdStrength(document.forms[0],this.value);' onblur='javascript:EvalPwdStrength(document.forms[0],this.value);'></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'> "
    Response.Write "      <td width='12%' align='right' class='tdbg'><strong>密码强度：</strong></td>"
    Response.Write "      <td width='88%' class='tdbg'>"
    Call ShowPwdStrength
    Response.Write "</td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='12%' align='right' class='tdbg'><strong>确认密码：</strong></td>"
    Response.Write "      <td width='88%' class='tdbg'><input type='password' name='PwdConfirm'></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'> "
    Response.Write "      <td width='12%'>&nbsp;</td>"
    Response.Write "      <td width='88%'><input name='EnableMultiLogin' type='checkbox' value='Yes'>允许多人同时使用此帐号登录</td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td colspan='2'><table id='PurviewDetail' width='100%' border='0' cellspacing='10' cellpadding='0' style='display:'>"
    Response.Write "        <tr>"
    Response.Write "          <td colspan='2' align='center'><strong>管 理 员 权 限 详 细 设 置</strong></td>"
    Response.Write "        </tr>"
    Dim sqlChannel, rsChannel,tChannelID
    sqlChannel = "select * from PW_Channel where  Disabled=" & PE_False & " order by OrderID"
    Set rsChannel = Conn.Execute(sqlChannel)
    Do While Not rsChannel.EOF
        tChannelID = rsChannel("ChannelID")
        ChannelName = Trim(rsChannel("ChannelName"))
        ChannelShortName = Trim(rsChannel("ChannelShortName"))
        Response.Write "<tr valign='top'><td>"
        Response.Write "<fieldset><legend>此管理员在【<font color='red'>" & ChannelName & "</font>】频道的权限：</legend><table width='100%'><tr>"
		Response.write "<td width='100' align='center'><input name='Purview_View' type='checkbox' id='Purview_View' value='"& tChannelID &"'>&nbsp;浏览</td>"
		Response.write "<td width='100' align='center'><input name='Purview_Add' type='checkbox' id='Purview_Add' value='"& tChannelID &"'>&nbsp;添加</td>"
		Response.write "<td width='100' align='center'><input name='Purview_Modify' type='checkbox' id='Purview_Modify' value='"& tChannelID &"'>&nbsp;修改</td>"
		Response.write "<td width='100' align='center'><input name='Purview_Del' type='checkbox' id='Purview_Del' value='"& tChannelID &"'>&nbsp;删除</td>"
		Response.write "<td width='100' align='center'><input name='Purview_Class' type='checkbox' id='Purview_Class' value='"& tChannelID &"'>&nbsp;分类</td>"
		Response.write "</tr></Table></fieldset></td></tr>"
	rsChannel.movenext
	loop
	rsChannel.close
	set rsChannel=nothing
	Response.Write "<tr valign='top'><td align='center'>"&vbcrlf
	Response.Write "<fieldset><legend>此管理员在【单页文档栏目】的权限：</legend>"&vbcrlf
    Response.Write "        <table width='750' border='0' cellspacing='0' cellpadding='0' style='table-layout:fixed'>"&vbcrlf
    Response.Write "          <tr>"&vbcrlf
	Response.write "			<td width='100' align='center'><input type='checkbox' name='info_Purview' value='info_Add' />&nbsp;添加</td>"
	Response.write "			<td width='100' align='center'><input type='checkbox' name='info_Purview' value='info_Modify' />&nbsp;修改</td>"
	Response.write "			<td width='100' align='center'><input type='checkbox' name='info_Purview' value='info_Del' />&nbsp;删除</td>"
    Response.Write "          </tr>"&vbcrlf
    Response.Write "       </table>"&vbcrlf
    Response.Write "</fieldset>"&vbcrlf
	Response.write "</td></tr>"
	Response.Write "<tr valign='top'><td align='center'>"&vbcrlf
	Response.Write "<fieldset><legend>此管理员在【其他模块的】的权限：</legend>"&vbcrlf
    Response.Write "        <table width='750' border='0' cellspacing='0' cellpadding='0' style='table-layout:fixed'>"&vbcrlf
    Response.Write "          <tr>"&vbcrlf
    Response.Write "            <td>"&vbcrlf
	if FoundInArr(Modules,"FriendSite", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='FriendSite'>友情链接管理&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"Vote", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='Vote'>网站调查管理&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"AD", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='AD'>网站广告管理&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"Announce", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='Announce'>网站公告管理&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"Admin", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='Admin'>管理员管理&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"Admin", ",") then
    Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='SiteConfig'>网站系统设置&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"Gbook", ",") then
		Response.Write "           <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='Gbook'>留言板&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"Order", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='Order'>订单管理系统&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"User", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='User'>会员管理系统&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"Database", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='Database'>数据库管理系统&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"Job", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='Job'>招聘系统管理&nbsp;&nbsp;</div>"&vbcrlf
	end if
    Response.Write "          </td></tr>"&vbcrlf
    Response.Write "       </table>"&vbcrlf
    Response.Write "</fieldset>"&vbcrlf
	Response.write "</td></tr>"
    Response.Write "  <tr>"
    Response.Write "  <td height='40' colspan='2' align='center' class='tdbg'><input name='Action' type='hidden' id='Action' value='SaveAdd'>"
    Response.Write "  <input  type='submit' name='Submit' value=' 添 加 ' style='cursor:hand;'>&nbsp;<input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Admin.asp';"" style='cursor:hand;'></td>"
    Response.Write "  </tr>"
    Response.Write "</table></form>"
End Sub


Sub ModifyPwd()
    Dim UserID
    Dim rsAdmin, sqlAdmin
    UserID = GetUrl("ID")
    If UserID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改的管理员ID</li>"
        Exit Sub
    Else
        UserID = PE_CLng(UserID)
    End If
    sqlAdmin = "Select * from PW_Admin where ID=" & UserID
    Set rsAdmin = Server.CreateObject("Adodb.RecordSet")
    rsAdmin.Open sqlAdmin, Conn, 1, 3
    If rsAdmin.BOF And rsAdmin.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>不存在此管理员！</li>"
        rsAdmin.Close
        Set rsAdmin = Nothing
        Exit Sub

    End If
    Call ShowJS_Check
    Response.Write "<form method='post' action='Admin_Admin.asp' name='form1' onsubmit='return CheckModifyPwd();'>"
    Response.Write "  <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
    Response.Write "    <tr class='title'> "
    Response.Write "      <td height='22' colspan='2' align='center'><strong>修 改 管 理 员 密 码</strong></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr> "
    Response.Write "      <td width='40%' class='tdbg'><strong>管理员名：</strong></td>"
    Response.Write "      <td width='60%' class='tdbg'><input type='text' name='AdminName' value='"& rsAdmin("AdminName") &"'><input name='ID' type='hidden' value='" & rsAdmin("ID") & "'></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr>"
    Response.Write "      <td width='40%' class='tdbg'><strong>新 密 码：</strong></td>"
    Response.Write "      <td width='60%' class='tdbg'><input type='password' name='Password' onkeyup='javascript:EvalPwdStrength(document.forms[0],this.value);' onmouseout='javascript:EvalPwdStrength(document.forms[0],this.value);' onblur='javascript:EvalPwdStrength(document.forms[0],this.value);'>"
    Response.Write "      </td>"
    Response.Write "    </tr>"
    Response.Write "    <tr> "
    Response.Write "      <td width='40%' class='tdbg'><strong>密码强度：</strong></td>"
    Response.Write "      <td width='60%' class='tdbg'>"
    Call ShowPwdStrength
    Response.Write "</td>"
    Response.Write "    </tr>"
    Response.Write "    <tr> "
    Response.Write "      <td width='40%' class='tdbg'><strong>确认密码：</strong></td>"
    Response.Write "      <td width='60%' class='tdbg'><input type='password' name='PwdConfirm'></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr class='tdbg'> "
    Response.Write "      <td width='40%'>&nbsp;</td>"
    Response.Write "      <td width='60%'><input name='EnableMultiLogin' type='checkbox' value='Yes'"
    If rsAdmin("EnableMultiLogin") = True Then Response.Write " checked"
    Response.Write ">允许多人同时使用此帐号登录</td>"
    Response.Write "    </tr>"
    Response.Write "    <tr>"
    Response.Write "      <td colspan='2' align='center' class='tdbg'><input name='Action' type='hidden' id='Action' value='SaveModifyPwd'>"
    Response.Write "        <input  type='submit' name='Submit' value='保存修改结果' style='cursor:hand;'>&nbsp;<input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Admin.asp'"" style='cursor:hand;'></td>"
    Response.Write "    </tr>"
    Response.Write "  </table>"
    Response.Write "</form>"

    rsAdmin.Close
    Set rsAdmin = Nothing
End Sub





Sub ModifyPurview()
    Dim UserID
    Dim rsAdmin, sqlAdmin
    Dim PO,PL,PI
    UserID = GetUrl("ID")
    If UserID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改的管理员ID</li>"
        Exit Sub
    Else
        UserID = PE_CLng(UserID)
    End If
    
    
    sqlAdmin = "Select * from PW_Admin where ID=" & UserID
    Set rsAdmin = Server.CreateObject("Adodb.RecordSet")
    rsAdmin.Open sqlAdmin, Conn, 1, 3
    If rsAdmin.BOF And rsAdmin.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>不存在此管理员！</li>"
        rsAdmin.Close
        Set rsAdmin = Nothing
        Exit Sub
    End If
    PO = rsAdmin("Purview_Others")
	PL = rsAdmin("Purview_Channel")
	PI = rsAdmin("Purview_Info")
    Call ShowJS_Check
    
    Response.Write "<form method='post' action='Admin_Admin.asp' name='form1'>"
    Response.Write "  <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
    Response.Write "    <tr class='title'> "
    Response.Write "      <td height='22' colspan='2' align='center'><strong>修 改 管 理 员 权 限</strong></td>"
    Response.Write "    </tr>"
    Response.Write "    <tr> "
    Response.Write "      <td width='12%' class='tdbg'><strong>管理员名：</strong></td>"
    Response.Write "      <td width='88%' class='tdbg'>" & rsAdmin("AdminName") & "<input name='ID' type='hidden' value='" & rsAdmin("ID") & "'></td>"
    Response.Write "    </tr>"

    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td colspan='2'><table id='PurviewDetail' width='100%' border='0' cellspacing='10' cellpadding='0' style='display:'>"
    Response.Write "        <tr>"
    Response.Write "          <td colspan='2' align='center'><strong>管 理 员 权 限 详 细 设 置</strong></td>"
    Response.Write "        </tr>"
    Dim sqlChannel, rsChannel,tChannelID
    sqlChannel = "select * from PW_Channel where  Disabled=" & PE_False & " order by OrderID"
    Set rsChannel = Conn.Execute(sqlChannel)
    Do While Not rsChannel.EOF
        tChannelID = rsChannel("ChannelID")
        ChannelName = Trim(rsChannel("ChannelName"))
        ChannelShortName = Trim(rsChannel("ChannelShortName"))
        Response.Write "<tr valign='top'><td>"
        Response.Write "<fieldset><legend>此管理员在【<font color='red'>" & ChannelName & "</font>】频道的权限：</legend><table width='100%'><tr>"
		Response.write "<td width='100' align='center'><input name='Purview_View' type='checkbox' id='Purview_Add' value='"& tChannelID &"' "& IsRadioChecked(purview_ChannelItem(PL,"0",tChannelID),True) &" >&nbsp;浏览</td>"
		Response.write "<td width='100' align='center'><input name='Purview_Add' type='checkbox' id='Purview_Add' value='"& tChannelID &"' "& IsRadioChecked(purview_ChannelItem(PL,"1",tChannelID),True) &" >&nbsp;添加</td>"
		Response.write "<td width='100' align='center'><input name='Purview_Modify' type='checkbox' id='Purview_Modify' value='"& tChannelID &"' "& IsRadioChecked(purview_ChannelItem(PL,"2",tChannelID),True) &">&nbsp;修改</td>"
		Response.write "<td width='100' align='center'><input name='Purview_Del' type='checkbox' id='Purview_Del' value='"& tChannelID &"' "& IsRadioChecked(purview_ChannelItem(PL,"3",tChannelID),True) &">&nbsp;删除</td>"
		Response.write "<td width='100' align='center'><input name='Purview_Class' type='checkbox' id='Purview_Class' value='"& tChannelID &"' "& IsRadioChecked(purview_ChannelItem(PL,"4",tChannelID),True) &">&nbsp;分类</td>"
		Response.write "</tr></Table></fieldset></td></tr>"
	rsChannel.movenext
	loop
	rsChannel.close
	set rsChannel=nothing
	Response.Write "<tr valign='top'><td align='center'>"&vbcrlf
	Response.Write "<fieldset><legend>此管理员在【单页文档栏目】的权限：</legend>"&vbcrlf
    Response.Write "        <table width='750' border='0' cellspacing='0' cellpadding='0' style='table-layout:fixed'>"&vbcrlf
    Response.Write "          <tr>"&vbcrlf
	Response.write "			<td width='100' align='center'><input type='checkbox' name='info_Purview' value='info_Add' " & IsRadioChecked(FoundInArr(PI, "info_Add",","),True) & " />&nbsp;添加</td>"
	Response.write "			<td width='100' align='center'><input type='checkbox' name='info_Purview' value='info_Modify' " & IsRadioChecked(FoundInArr(PI, "info_Modify",","),True) & " />&nbsp;修改</td>"
	Response.write "			<td width='100' align='center'><input type='checkbox' name='info_Purview' value='info_Del' " & IsRadioChecked(FoundInArr(PI, "info_Del",","),True) & " />&nbsp;删除</td>"
    Response.Write "          </tr>"&vbcrlf
    Response.Write "       </table>"&vbcrlf
    Response.Write "</fieldset>"&vbcrlf
	Response.write "</td></tr>"
	Response.Write "<tr valign='top'><td align='center'>"&vbcrlf
	Response.Write "<fieldset><legend>此管理员在【其他模块的】的权限：</legend>"&vbcrlf
    Response.Write "        <table width='750' border='0' cellspacing='0' cellpadding='0' style='table-layout:fixed'>"&vbcrlf
    Response.Write "          <tr>"&vbcrlf
    Response.Write "            <td>"&vbcrlf
	if FoundInArr(Modules,"FriendSite", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='FriendSite'" & IsRadioChecked(FoundInArr(PO, "FriendSite",","),True) & ">友情链接管理&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"Vote", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='Vote'" & IsRadioChecked(FoundInArr(PO, "Vote",","),True) & ">网站调查管理&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"AD", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='AD'" & IsRadioChecked(FoundInArr(PO, "AD",","),True) & ">网站广告管理&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"Announce", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='Announce'" & IsRadioChecked(FoundInArr(PO, "Announce",","),True) & ">网站公告管理&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"Admin", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='Admin'" & IsRadioChecked(FoundInArr(PO, "Admin",","),True) & ">管理员管理&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"Admin", ",") then
    Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='SiteConfig'" & IsRadioChecked(FoundInArr(PO, "SiteConfig",","),True) & ">网站系统设置&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"Gbook", ",") then
		Response.Write "           <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='Gbook'" & IsRadioChecked(FoundInArr(PO, "Gbook",","),True) & ">留言板&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"Order", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='Order'" & IsRadioChecked(FoundInArr(PO, "Order",","),True) & ">订单管理系统&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"User", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='User'" & IsRadioChecked(FoundInArr(PO, "User",","),True) & ">会员管理系统&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"Database", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='Database'" & IsRadioChecked(FoundInArr(PO, "Database",","),True) & ">数据库管理系统&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"JobSys", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='Job'" & IsRadioChecked(FoundInArr(PO, "Job",","),True) & ">招聘系统管理&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"Nav", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='Nav'" & IsRadioChecked(FoundInArr(PO, "Nav",","),True) & ">栏目导航系统&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"Keywords", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='Keywords'" & IsRadioChecked(FoundInArr(PO, "Keywords",","),True) & ">网站关键字&nbsp;&nbsp;</div>"&vbcrlf
	end if
	if FoundInArr(Modules,"CreateHTML", ",") then
		Response.Write "            <div style='float:left; width:120px;'><input name='AdminPurview_Others' type='checkbox' id='AdminPurview_Others' value='CreateHTML'" & IsRadioChecked(FoundInArr(PO, "CreateHTML",","),True) & ">生成HTML&nbsp;&nbsp;</div>"&vbcrlf
	end if
    Response.Write "          </td></tr>"&vbcrlf
    Response.Write "       </table>"&vbcrlf
    Response.Write "</fieldset>"&vbcrlf
	Response.write "</td></tr>"
    Response.Write "  <tr>"
    Response.Write "  <td height='40' colspan='2' align='center' class='tdbg'><input name='Action' type='hidden' id='Action' value='SaveModifyPurview'>"
    Response.Write "    <input  type='submit' name='Submit' value='保存修改结果' style='cursor:hand;'>&nbsp;<input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Admin.asp'"" style='cursor:hand;'></td>"
    Response.Write "  </tr>"
    Response.Write "</table></form>"
    rsAdmin.Close
    Set rsAdmin = Nothing
End Sub

Sub SaveAdd()
	Dim strAdminName, Password, PwdConfirm, EnableMultiLogin
	Dim rsAdmin, sqlAdmin
	Dim Purview_View, Purview_Add, Purview_Modify, Purview_Del, Purview_Class, AdminPurview_Others, AdminPurview_Channel, info_Purview
    strAdminName = GetForm("AdminName")
    Password = GetForm("Password")
    PwdConfirm = GetForm("PwdConfirm")
    EnableMultiLogin = GetForm("EnableMultiLogin")
    AdminPurview_Others = ReplaceBadChar(GetForm("AdminPurview_Others"))
	Purview_View = ReplaceBadChar(GetForm("Purview_View"))
	Purview_Add = ReplaceBadChar(GetForm("Purview_Add"))
	Purview_Modify = ReplaceBadChar(GetForm("Purview_Modify"))
	Purview_Del = ReplaceBadChar(GetForm("Purview_Del"))
	Purview_Class = ReplaceBadChar(GetForm("Purview_Class"))
 	info_Purview = ReplaceBadChar(GetForm("info_Purview"))
    If strAdminName = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>管理员名不能为空！</li>"
    Else
        If CheckBadChar(strAdminName) = False Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>管理员名中含有非法字符！</li>"
        End If
    End If
    If Password = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>初始密码不能为空！</li>"
    End If
    If PwdConfirm <> Password Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>确认密码必须与初始密码相同！</li>"
    End If
    If CheckBadChar(Password) = False Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>初始密码中含有非法字符！</li>"
    End If
    If FoundErr = True Then
        Exit Sub
    End If
    
	AdminPurview_Channel=Purview_View & "$$$" & Purview_Add & "$$$" & Purview_Modify & "$$$" & Purview_Del & "$$$" & Purview_Class
    sqlAdmin = "Select * from PW_Admin where AdminName='" & strAdminName & "'"
    Set rsAdmin = Server.CreateObject("Adodb.RecordSet")
    rsAdmin.Open sqlAdmin, Conn, 1, 3
    If Not (rsAdmin.BOF And rsAdmin.EOF) Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>数据库中已经存在此管理员！</li>"
        rsAdmin.Close
        Set rsAdmin = Nothing
        Exit Sub
    End If
    rsAdmin.addnew
    rsAdmin("AdminName") = strAdminName
    rsAdmin("Password") = MD5(Password, 16)
    rsAdmin("LoginTimes") = 0
    rsAdmin("Purview") = 2
    rsAdmin("Purview_Others") = AdminPurview_Others
    If EnableMultiLogin = "Yes" Then
        rsAdmin("EnableMultiLogin") = True
    Else
        rsAdmin("EnableMultiLogin") = False
    End If
	rsAdmin("Purview_Channel") = Replace(AdminPurview_Channel," ","")
	rsAdmin("Purview_Info") = Replace(info_Purview," ","")
    rsAdmin.Update
    rsAdmin.Close
    Set rsAdmin = Nothing
    Call main
End Sub
    


Sub SaveModifyPwd()
    Dim UserID, AdminName, Password, PwdConfirm, EnableMultiLogin
    Dim rsAdmin, sqlAdmin
    UserID = GetForm("ID")
	AdminName = GetForm("AdminName")
    Password = GetForm("Password")
    PwdConfirm = GetForm("PwdConfirm")
    EnableMultiLogin = GetForm("EnableMultiLogin")
    If UserID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改的管理员ID</li>"
    Else
        UserID = PE_CLng(UserID)
    End If
    If AdminName = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>管理员名不能为空！</li>"
    Else
        If CheckBadChar(AdminName) = False Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>管理员名中含有非法字符！</li>"
        End If
    End If
    If Password = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>新密码不能为空！</li>"
    End If
    If PwdConfirm <> Password Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>确认密码必须与新密码相同！</li>"
    End If
    If CheckBadChar(Password) = False Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>新密码中含有非法字符！</li>"
    End If
    If FoundErr = True Then
        Exit Sub
    End If
	set rsAdmin =  conn.execute("Select * from PW_Admin where AdminName='"& AdminName &"' and ID<>"& UserID &"")
	if not rsAdmin.eof then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>此管理员已经存在,请换一个名称！</li>"
        Exit Sub
	end if
	rsAdmin.close
	set rsAdmin = nothing
    sqlAdmin = "Select * from PW_Admin where ID=" & UserID
    Set rsAdmin = Server.CreateObject("Adodb.RecordSet")
    rsAdmin.Open sqlAdmin, Conn, 1, 3
    If rsAdmin.BOF And rsAdmin.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>不存在此管理员！</li>"
        rsAdmin.Close
        Set rsAdmin = Nothing
        Exit Sub
    End If
    rsAdmin("AdminName") = AdminName
    rsAdmin("password") = MD5(Password, 16)
    If EnableMultiLogin = "Yes" Then
        rsAdmin("EnableMultiLogin") = True
    Else
        rsAdmin("EnableMultiLogin") = False
    End If
    rsAdmin.Update
    rsAdmin.Close
    Set rsAdmin = Nothing
    Call main
End Sub



Sub SaveModifyPurview()
	Dim UserID, UserName
	Dim rsAdmin, sqlAdmin
	Dim Purview_View, Purview_Add, Purview_Modify, Purview_Del, Purview_Class, AdminPurview_Others, AdminPurview_Channel, info_Purview
    UserID = GetForm("ID")
    AdminPurview_Others = ReplaceBadChar(GetForm("AdminPurview_Others"))
	Purview_View = ReplaceBadChar(GetForm("Purview_View"))
	Purview_Add = ReplaceBadChar(GetForm("Purview_Add"))
	Purview_Modify = ReplaceBadChar(GetForm("Purview_Modify"))
	Purview_Del = ReplaceBadChar(GetForm("Purview_Del"))
	Purview_Class = ReplaceBadChar(GetForm("Purview_Class"))
	info_Purview = ReplaceBadChar(GetForm("info_Purview"))
    If UserID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改的管理员ID</li>"
    Else
        UserID = PE_CLng(UserID)
    End If
    If FoundErr = True Then
        Exit Sub
    End If

	AdminPurview_Channel=Purview_View & "$$$" & Purview_Add & "$$$" & Purview_Modify & "$$$" & Purview_Del & "$$$" & Purview_Class


    sqlAdmin = "Select * from PW_Admin where ID=" & UserID
    Set rsAdmin = Server.CreateObject("Adodb.RecordSet")
    rsAdmin.Open sqlAdmin, Conn, 1, 3
    If rsAdmin.BOF And rsAdmin.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>不存在此管理员！</li>"
        rsAdmin.Close
        Set rsAdmin = Nothing
        Exit Sub
    End If

    rsAdmin("Purview_Others") = AdminPurview_Others
	rsAdmin("Purview_Channel") = Replace(AdminPurview_Channel," ","")
	rsAdmin("Purview_Info") = Replace(info_Purview," ","")
    rsAdmin.Update
    rsAdmin.Close
    Set rsAdmin = Nothing
    Call main
End Sub


Sub DelAdmin()
    Dim UserID
    Dim rsAdmin, sqlAdmin
    UserID = GetForm("ID")
    If IsValidID(UserID) = False Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要删除的管理员ID</li>"
        Exit Sub
    End If
    If InStr(UserID, ",") > 0 Then
        Conn.Execute ("delete from PW_Admin where ID in (" & UserID & ")")
    Else
        Conn.Execute ("delete from PW_Admin where ID=" & UserID & "")
    End If
    Call main
End Sub



Sub ShowJS_Check()
    Response.Write "<SCRIPT language=javascript>" & vbCrLf
    Response.Write "function SelectAll(form){" & vbCrLf
    Response.Write "  for (var i=0;i<form.AdminPurview_Others.length;i++){" & vbCrLf
    Response.Write "    var e = form.AdminPurview_Others[i];" & vbCrLf
    Response.Write "    if (e.disabled==false)" & vbCrLf
    Response.Write "       e.checked = form.chkAll.checked;" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "function CheckAdd(){" & vbCrLf
    Response.Write "  if(document.form1.AdminName.value==''){" & vbCrLf
    Response.Write "      alert('用户名不能为空！');" & vbCrLf
    Response.Write "   document.form1.AdminName.focus();" & vbCrLf
    Response.Write "      return false;" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "  if(document.form1.Password.value==''){" & vbCrLf
    Response.Write "      alert('密码不能为空！');" & vbCrLf
    Response.Write "   document.form1.Password.focus();" & vbCrLf
    Response.Write "      return false;" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "  if((document.form1.Password.value)!=(document.form1.PwdConfirm.value)){" & vbCrLf
    Response.Write "      alert('初始密码与确认密码不同！');" & vbCrLf
    Response.Write "   document.form1.PwdConfirm.select();" & vbCrLf
    Response.Write "   document.form1.PwdConfirm.focus();" & vbCrLf
    Response.Write "      return false;" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "}" & vbCrLf
    
    Response.Write "function CheckModifyPwd(){" & vbCrLf
    Response.Write "  if(document.form1.Password.value==''){" & vbCrLf
    Response.Write "      alert('密码不能为空！');" & vbCrLf
    Response.Write "   document.form1.Password.focus();" & vbCrLf
    Response.Write "      return false;" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "  if((document.form1.Password.value)!=(document.form1.PwdConfirm.value)){" & vbCrLf
    Response.Write "      alert('初始密码与确认密码不同！');" & vbCrLf
    Response.Write "   document.form1.PwdConfirm.select();" & vbCrLf
    Response.Write "   document.form1.PwdConfirm.focus();" & vbCrLf
    Response.Write "      return false;" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "</SCRIPT>" & vbCrLf
End Sub

Sub ShowPwdStrength()
    Response.Write "<script language='JavaScript' src='PwdStrength.js'></script>" & vbCrLf
    Response.Write "<script language='JavaScript'>" & vbCrLf
    Response.Write "<!--" & vbCrLf
    Response.Write "window.onerror = ignoreError;" & vbCrLf
    Response.Write "function ignoreError(){return true;}" & vbCrLf
    Response.Write "function EvalPwdStrength(oF,sP){" & vbCrLf
    Response.Write "  PadPasswd(oF,sP.length*2);" & vbCrLf
    Response.Write "  if(ClientSideStrongPassword(sP,gSimilarityMap,gDictionary)){DispPwdStrength(3,'cssStrong');}" & vbCrLf
    Response.Write "  else if(ClientSideMediumPassword(sP,gSimilarityMap,gDictionary)){DispPwdStrength(2,'cssMedium');}" & vbCrLf
    Response.Write "  else if(ClientSideWeakPassword(sP,gSimilarityMap,gDictionary)){DispPwdStrength(1,'cssWeak');}" & vbCrLf
    Response.Write "  else{DispPwdStrength(0,'cssPWD');}" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "function PadPasswd(oF,lPwd){" & vbCrLf
    Response.Write "  if(typeof oF.PwdPad=='object'){var sPad='IfYouAreReadingThisYouHaveTooMuchFreeTime';var lPad=sPad.length-lPwd;oF.PwdPad.value=sPad.substr(0,(lPad<0)?0:lPad);}" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "function DispPwdStrength(iN,sHL){" & vbCrLf
    Response.Write "  if(iN>3){ iN=3;}for(var i=0;i<4;i++){ var sHCR='cssPWD';if(i<=iN){ sHCR=sHL;}if(i>0){ GEId('idSM'+i).className=sHCR;}GEId('idSMT'+i).style.display=((i==iN)?'inline':'none');}" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "function GEId(sID){return document.getElementById(sID);}" & vbCrLf
    Response.Write "//-->" & vbCrLf
    Response.Write "</script>" & vbCrLf
    Response.Write "<style>" & vbCrLf
    Response.Write "input{FONT-FAMILY:宋体;FONT-SIZE: 9pt;}" & vbCrLf
    Response.Write ".cssPWD{background-color:#EBEBEB;border-right:solid 1px #BEBEBE;border-bottom:solid 1px #BEBEBE;}" & vbCrLf
    Response.Write ".cssWeak{background-color:#FF4545;border-right:solid 1px #BB2B2B;border-bottom:solid 1px #BB2B2B;}" & vbCrLf
    Response.Write ".cssMedium{background-color:#FFD35E;border-right:solid 1px #E9AE10;border-bottom:solid 1px #E9AE10;}" & vbCrLf
    Response.Write ".cssStrong{background-color:#3ABB1C;border-right:solid 1px #267A12;border-bottom:solid 1px #267A12;}" & vbCrLf
    Response.Write ".cssPWT{width:132px;}" & vbCrLf
    Response.Write "</style>" & vbCrLf
    Response.Write "<table cellpadding='0' cellspacing='0' class='cssPWT' style='height:16px'><tr valign='bottom'><td id='idSM1' width='33%' class='cssPWD' align='center'><span style='font-size:1px'>&nbsp;</span><span id='idSMT1' style='display:none;'>弱</span></td><td id='idSM2' width='34%' class='cssPWD' align='center' style='border-left:solid 1px #fff'><span style='font-size:1px'>&nbsp;</span><span id='idSMT0' style='display:inline;font-weight:normal;color:#666'>无</span><span id='idSMT2' style='display:none;'>中</span></td><td id='idSM3' width='33%' class='cssPWD' align='center' style='border-left:solid 1px #fff'><span style='font-size:1px'>&nbsp;</span><span id='idSMT3' style='display:none;'>强</span></td></tr></table>"
End Sub

%>