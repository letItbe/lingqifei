<!--#include file="Admin_Common.asp"-->
<!--#include file="Admin_CommonCode_Content.asp"-->
<!--#include file="../Include/PW_FSO.asp"-->
<%
'**************************************************************
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Others = ""   '其他权限
Const PurviewLevel_Channel = 0   '0--不检查

Response.Write "<html><head><title>频道管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf

Call ShowPageTitle("频 道 管 理")
Response.Write "  <tr class='tdbg'>" & vbCrLf
Response.Write "    <td width='70' height='30'><strong>管理导航：</strong></td>" & vbCrLf
Response.Write "    <td><a href='Admin_Channel.asp'>频道管理首页</a>&nbsp;|&nbsp;<a href='Admin_Channel.asp?Action=Add'>添加新频道</a>&nbsp;</a>"
Response.Write "    </td>" & vbCrLf
Response.Write "  </tr>" & vbCrLf
Response.Write "</table>" & vbCrLf


Action = GetValue("Action")
Select Case Action
Case "Add"
    Call AddChannel
Case "SaveAdd"
    Call SaveAdd
Case "Modify"
    Call Modify
Case "SaveModify"
    Call SaveModify
Case "Disabled"
    Call DisabledChannel(0)
Case "UnDisabled"
    Call DisabledChannel(1)
Case "Del"
    Call DelChannel
Case Else
    Call main
End Select
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn


Sub main()
    Dim rsChannelList, sqlChannelList
    sqlChannelList = "select * from PW_Channel  order by OrderID "
	Set rsChannelList = Conn.Execute(sqlChannelList)
    Response.Write "<br>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
    Response.Write "  <tr class='title' height='22'>"
    Response.Write "    <td width='30' align='center'><strong>ID</strong></td>"
    Response.Write "    <td align='center'><strong>频道名称</strong></td>"
    Response.Write "    <td width='60' align='center'><strong>项目名称</strong></td>"
    Response.Write "    <td width='60' align='center'><strong>功能模块</strong></td>"
    Response.Write "    <td width='60' align='center'><strong>频道状态</strong></td>"
    Response.Write "    <td align='center' width='180'><strong>操作</strong></td>"
    Response.Write "  </tr>" & vbCrLf
    Do While Not rsChannelList.EOF
        Response.Write "  <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"
        Response.Write "    <td align='center'>" & rsChannelList("ChannelID") & "</td>"
        Response.Write "    <td align='center'><a href='Admin_Channel.asp?Action=Modify&iChannelID=" & rsChannelList("ChannelID") & "'>" & rsChannelList("ChannelName") & "</a></td>"
        Response.Write "    <td align='center'>"& rsChannelList("ChannelShortName") &"</td>"
		Response.Write "<td width='54' align='center'>"& GetModuleTypeName(rsChannelList("ModuleType")) &"</td>"
        Response.Write "<td width='54' align='center'>"
        If rsChannelList("Disabled") = True Then
            Response.Write "<font color=red>已禁用</font>"
        Else
            Response.Write "正常"
        End If
        Response.Write "</td>"
        Response.Write "<td align='center'>"
        Response.Write "<a href='Admin_Channel.asp?Action=Modify&iChannelID=" & rsChannelList("ChannelID") & "'>修改</a>&nbsp;&nbsp;"
        If rsChannelList("Disabled") = True Then
            Response.Write "<a href='Admin_Channel.asp?Action=UnDisabled&iChannelID=" & rsChannelList("ChannelID") & "'>启用</a>&nbsp;&nbsp;"
        Else
            Response.Write "<a href='Admin_Channel.asp?Action=Disabled&iChannelID=" & rsChannelList("ChannelID") & "'>禁用</a>&nbsp;&nbsp;"
        End If
        Response.Write "<a href='Admin_Field.asp?ChannelID=" & rsChannelList("ChannelID") & "'>字段管理</a>&nbsp;&nbsp;"
		If rsChannelList("ChannelType") = 0 Then
			Response.Write "<a href='Admin_Channel.asp?Action=Del&iChannelID=" & rsChannelList("ChannelID") & "' onClick=""return confirm('确定要删除此频道吗？');"">删除</a>"
		end if
        Response.Write "</td>"
        Response.Write "</tr>"
        rsChannelList.MoveNext
    Loop
    Response.Write "</table>"
    rsChannelList.Close
    Set rsChannelList = Nothing
End Sub


Sub AddChannel()
    Call ShowChekcFormJS
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_Channel.asp'>频道管理</a>&nbsp;&gt;&gt;&nbsp;添加新频道</td></tr></table>"
    Response.Write "<form method='post' action='Admin_Channel.asp' name='myform' onSubmit='return CheckForm();'>" & vbCrLf
    Response.Write "<table width='100%'  border='0' align='center' cellpadding='5' cellspacing='1' class='border'><tr class='tdbg'><td height='100' valign='top'>" & vbCrLf
    Response.Write "<table width='95%' align='center' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='200' class='tdbg5'><strong> 频道名称：</strong></td>" & vbCrLf
    Response.Write "      <td><input name='ChannelName' type='text' id='ChannelName' size='49' maxlength='30'> <font color='#FF0000'>*</font></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "	<tr class='tdbg'>"& vbCrLf
    Response.Write "	  <td width='200' class='tdbg5'><strong>频道使用的功能模块：</strong><br>(当选择仅作导航时，仅名称有用)</td>"& vbCrLf
    Response.Write "      <td><select name='ModuleType' id='ModuleType'>"
    Response.Write "      <option value='1' selected>文章</option>"
    Response.Write "      <option value='2'>下载</option>"
    Response.Write "      <option value='4'>图片</option>"
    Response.Write "      </select></td>"
    Response.Write "	</tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200' class='tdbg5'><strong>项目名称：</strong><br>例如：频道名称为“新闻中心”，其项目名称为“新闻”或“文章”</td>"
    Response.Write "      <td><input name='ChannelShortName' type='text' id='ChannelShortName' size='20' maxlength='30'> <font color='#FF0000'>*</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200' class='tdbg5'><strong>项目单位：</strong><br>例如：“篇”、“条”、“个”</td>"
    Response.Write "      <td><input name='ChannelItemUnit' type='text' id='ChannelItemUnit' size='10' maxlength='30'></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200' class='tdbg5'><strong>频道目录：</strong></td>"
    Response.Write "      <td><input name='ChannelDir' type='text' id='ChannelDir' size='10' maxlength='30'></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='200' class='tdbg5'><strong> 频道排序：</strong></td>" & vbCrLf
    Response.Write "      <td><input name='OrderID' type='text' id='OrderID' size='10' maxlength='10' value='0'> <font color='#FF0000'>*</font></td>" & vbCrLf
    Response.Write "	</tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200' class='tdbg5'><strong>频道每页信息数：</strong></td>"
    Response.Write "      <td><input name='MaxPerPage_Channel' type='text' id='MaxPerPage_Channel' value='20' size='10' maxlength='3'> <font color='#FF0000'>*</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='200' class='tdbg5'><strong>禁用本频道：</strong></td>" & vbCrLf
    Response.Write "      <td><input type='radio' name='Disabled' value='True'>是 &nbsp;&nbsp;&nbsp;&nbsp;<input name='Disabled' type='radio' value='False' checked>否</td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='200' class='tdbg5'><strong>封面模版：</strong></td>" & vbCrLf
    Response.Write "      <td><input id=""tempindex"" name=""tempindex"" type=""text"" value=""default/list_article.html"" style=""width:300px"" readonly /> <input type='button' name='Button2' class='Button' value='选择模板... ' onclick=""SelectTemplate('tempindex')""> <font color='#FF0000'>*</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='200' class='tdbg5'><strong>列表模版：</strong></td>" & vbCrLf
    Response.Write "      <td><input id=""templist"" name=""templist"" type=""text"" value=""default/list_article.html"" style=""width:300px"" readonly /> <input type='button' name='Button2' class='Button' value='选择模板... ' onclick=""SelectTemplate('templist')""> <font color='#FF0000'>*</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='200' class='tdbg5'><strong>文章模版：</strong></td>" & vbCrLf
    Response.Write "      <td><input id=""temparticle"" name=""temparticle"" type=""text"" value=""default/article_article1.htm"" style=""width:300px"" readonly /> <input type='button' name='Button2' class='Button' value='选择模板... ' onclick=""SelectTemplate('temparticle')""> <font color='#FF0000'>*</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "</table>" & vbCrLf
    Response.Write "</td></tr></table>" & vbCrLf

    Response.Write "<table width='100%' border='0' align='center'>"
    Response.Write "  <tr class='tdbg'>"
    Response.Write "    <td colspan='2' align='center' class='tdbg'><p><input name='Action' type='hidden' id='Action' value='SaveAdd'>"
    Response.Write "      <input  type='submit' name='Submit' value=' 添 加 ' class='submit'> &nbsp; <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Channel.asp'"" style='cursor:hand;' class='submit'></p></td>"
    Response.Write "  </tr>" & vbCrLf
    Response.Write "</table>"
    Response.Write "</form>"
End Sub


Sub Modify()
    Call ShowChekcFormJS
    Dim iChannelID, rsChannel,FieldShow
    iChannelID = GetUrl("iChannelID")
    If iChannelID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改频道ID</li>"
        Exit Sub
    Else
        iChannelID = PE_CLng(iChannelID)
    End If
    Set rsChannel = Conn.Execute("select * from PW_Channel where ChannelID=" & iChannelID)
    If rsChannel.BOF And rsChannel.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的频道！</li>"
        rsChannel.Close
        Set rsChannel = Nothing
        Exit Sub
    End If
	FieldShow = rsChannel("FieldShow")
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_Channel.asp'>频道管理</a>&nbsp;&gt;&gt;&nbsp;修改频道设置：<font color='red'>" & rsChannel("ChannelName") & "</font></td></tr></table>"
    Response.Write "<form method='post' action='Admin_Channel.asp' name='myform' onSubmit='return CheckForm();'>" & vbCrLf
    Response.Write "<table width='100%'  border='0' align='center' cellpadding='5' cellspacing='1' class='border'><tr class='tdbg'><td height='100' valign='top'>" & vbCrLf
    Response.Write "<table width='95%' align='center' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='200' class='tdbg5'><strong> 频道名称：</strong></td>" & vbCrLf
    Response.Write "      <td><input name='ChannelName' type='text' id='ChannelName' size='49' maxlength='30' value='" & rsChannel("ChannelName") & "'> <font color='#FF0000'>*</font></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "	<tr class='tdbg'>"& vbCrLf
    Response.Write "	  <td width='200' class='tdbg5'><strong>频道使用的功能模块：</strong><br>(当选择仅作导航时，仅名称有用)</td>"& vbCrLf
    Response.Write "      <td><select name='ModuleType' id='ModuleType'>"
    Response.Write "      <option value='1'" & IsOptionSelected(rsChannel("ModuleType"), 1) & ">文章</option>"
    Response.Write "      <option value='2'" & IsOptionSelected(rsChannel("ModuleType"), 2) & ">下载</option>"
	if iChannelID = 4 then
		Response.Write "      <option value='3'" & IsOptionSelected(rsChannel("ModuleType"), 3) & ">产品</option>"
	end if
    Response.Write "      <option value='4'" & IsOptionSelected(rsChannel("ModuleType"), 4) & ">图片</option>"
    Response.Write "      </select></td>"
    Response.Write "	</tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200' class='tdbg5'><strong>项目名称：</strong><br>例如：频道名称为“新闻中心”，其项目名称为“新闻”或“文章”</td>"
    Response.Write "      <td><input name='ChannelShortName' type='text' id='ChannelShortName' size='20' maxlength='30' value='" & rsChannel("ChannelShortName") & "'> <font color='#FF0000'>*</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200' class='tdbg5'><strong>项目单位：</strong><br>例如：“篇”、“条”、“个”</td>"
    Response.Write "      <td><input name='ChannelItemUnit' type='text' id='ChannelItemUnit' size='10' maxlength='30'value='" & rsChannel("ChannelItemUnit") & "'></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200' class='tdbg5'><strong>频道目录：</strong></td>"
    Response.Write "      <td><input name='ChannelDir' readonly type='text' id='ChannelDir' size='10' maxlength='30' value='" & rsChannel("ChannelDir") & "'></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='200' class='tdbg5'><strong> 频道排序：</strong></td>" & vbCrLf
    Response.Write "      <td><input name='OrderID' type='text' id='OrderID' size='10' maxlength='10' value='" & rsChannel("OrderID") & "'> <font color='#FF0000'>*</font></td>" & vbCrLf
    Response.Write "	</tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='200' class='tdbg5'><strong>频道每页信息数：</strong></td>"
    Response.Write "      <td><input name='MaxPerPage_Channel' type='text' id='MaxPerPage_Channel' size='10' maxlength='3' value='" & rsChannel("MaxPerPage_Channel") & "'> <font color='#FF0000'>*</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='200' class='tdbg5'><strong>禁用本频道：</strong></td>" & vbCrLf
    Response.Write "      <td><input type='radio' name='Disabled' value='True' " & IsRadioChecked(rsChannel("Disabled"), True) & ">是 &nbsp;&nbsp;&nbsp;&nbsp;<input name='Disabled' type='radio' value='False' " & IsRadioChecked(rsChannel("Disabled"), False) & ">否</td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='200' class='tdbg5'><strong>封面模版：</strong></td>" & vbCrLf
    Response.Write "      <td><input id=""tempindex"" name=""tempindex"" type=""text"" value="""& rsChannel("tempindex") &""" style=""width:300px"" readonly /> <input type='button' name='Button2' class='Button' value='选择模板... ' onclick=""SelectTemplate('tempindex')""> <font color='#FF0000'>*</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='200' class='tdbg5'><strong>列表模版：</strong></td>" & vbCrLf
    Response.Write "      <td><input id=""templist"" name=""templist"" type=""text"" value="""& rsChannel("templist") &""" style=""width:300px"" readonly /> <input type='button' name='Button2' class='Button' value='选择模板... ' onclick=""SelectTemplate('templist')""> <font color='#FF0000'>*</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='200' class='tdbg5'><strong>文章模版：</strong></td>" & vbCrLf
    Response.Write "      <td><input id=""temparticle"" name=""temparticle"" type=""text"" value="""& rsChannel("temparticle") &""" style=""width:300px"" readonly /> <input type='button' name='Button2' class='Button' value='选择模板... ' onclick=""SelectTemplate('temparticle')""> <font color='#FF0000'>*</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='200' class='tdbg5'><strong>系统字段设置：</strong></td>" & vbCrLf
    Response.Write "      <td>" & vbCrLf
	Select case rsChannel("ModuleType")
		Case 1 '''新闻模块
			Response.write "        <table width=""90%"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
			Response.Write "    <tr>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Title' name='F_Show' " & IsFieldSelected(FieldShow, "Title") & ">标题</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='ClassID' name='F_Show' " & IsFieldSelected(FieldShow, "ClassID") & ">归属栏目</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Keyword' name='F_Show' " & IsFieldSelected(FieldShow, "Keyword") & ">关键字</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Intro' name='F_Show' " & IsFieldSelected(FieldShow, "Intro") & ">简介</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Author' name='F_Show' " & IsFieldSelected(FieldShow, "Author") & ">作者</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='CopyFrom' name='F_Show' " & IsFieldSelected(FieldShow, "CopyFrom") & ">来源</td>" & vbCrLf
			Response.Write "    </tr>" & vbCrLf
			Response.Write "    <tr>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='LinkUrl' name='F_Show' " & IsFieldSelected(FieldShow, "LinkUrl") & ">转向链接</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Hits' name='F_Show' " & IsFieldSelected(FieldShow, "Hits") & ">点击数</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='UpdateTime' name='F_Show' " & IsFieldSelected(FieldShow, "UpdateTime") & ">更新时间</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Property' name='F_Show' " & IsFieldSelected(FieldShow, "Property") & ">属性设置</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Content' name='F_Show' " & IsFieldSelected(FieldShow, "Content") & ">文章内容</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='DefaultPicUrl' name='F_Show' " & IsFieldSelected(FieldShow, "DefaultPicUrl") & ">首页图片</td>" & vbCrLf
			Response.Write "    </tr>" & vbCrLf
			Response.Write "    <tr>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Flag' name='F_Show' " & IsFieldSelected(FieldShow, "Flag") & ">阅读权限</td>" & vbCrLf
			Response.Write "    </tr>" & vbCrLf
			Call Channel_Fields(iChannelID,rsChannel("FieldShow"))
			Response.Write "    </table>" & vbCrLf
		case 2  '''下载模块
			Response.write "        <table width=""90%"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
			Response.Write "    <tr>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Title' name='F_Show' " & IsFieldSelected(FieldShow, "Title") & ">标题</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='ClassID' name='F_Show' " & IsFieldSelected(FieldShow, "ClassID") & ">归属栏目</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Keyword' name='F_Show' " & IsFieldSelected(FieldShow, "Keyword") & ">关键字</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Intro' name='F_Show' " & IsFieldSelected(FieldShow, "Intro") & ">简介</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Author' name='F_Show' " & IsFieldSelected(FieldShow, "Author") & ">作者</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='CopyFrom' name='F_Show' " & IsFieldSelected(FieldShow, "CopyFrom") & ">来源</td>" & vbCrLf
			Response.Write "    </tr>" & vbCrLf
			Response.Write "    <tr>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='SoftVersion' name='F_Show' " & IsFieldSelected(FieldShow, "SoftVersion") & ">软件版本</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='DemoUrl' name='F_Show' " & IsFieldSelected(FieldShow, "DemoUrl") & ">演示地址</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Hits' name='F_Show' " & IsFieldSelected(FieldShow, "Hits") & ">下载数</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='UpdateTime' name='F_Show' " & IsFieldSelected(FieldShow, "UpdateTime") & ">更新时间</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='OperatingSystem' name='F_Show' " & IsFieldSelected(FieldShow, "OperatingSystem") & ">操作系统</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='SoftType' name='F_Show' " & IsFieldSelected(FieldShow, "SoftType") & ">软件类型</td>" & vbCrLf
			Response.Write "    </tr>" & vbCrLf
			Response.Write "    <tr>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='SoftLanguage' name='F_Show' " & IsFieldSelected(FieldShow, "SoftLanguage") & ">软件语言</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='CopyrightType' name='F_Show' " & IsFieldSelected(FieldShow, "CopyrightType") & ">授权方式</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='SoftPicUrl' name='F_Show' " & IsFieldSelected(FieldShow, "SoftPicUrl") & ">软件图片</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='DownloadUrl' name='F_Show' " & IsFieldSelected(FieldShow, "DownloadUrl") & ">软件地址</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Property' name='F_Show' " & IsFieldSelected(FieldShow, "Property") & ">属性设置</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Detial' name='F_Show' " & IsFieldSelected(FieldShow, "Detial") & ">详细介绍</td>" & vbCrLf
			Response.Write "    </tr>" & vbCrLf
			Response.Write "    <tr>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Flag' name='F_Show' " & IsFieldSelected(FieldShow, "Flag") & ">阅读权限</td>" & vbCrLf
			Response.Write "    </tr>" & vbCrLf
			Call Channel_Fields(iChannelID,rsChannel("FieldShow"))
			Response.Write "    </table>" & vbCrLf
		case 3  '''产品模块
			Response.write "        <table width=""90%"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
			Response.Write "    <tr>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Title' name='F_Show' " & IsFieldSelected(FieldShow, "Title") & ">标题</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='ClassID' name='F_Show' " & IsFieldSelected(FieldShow, "ClassID") & ">归属栏目</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Keyword' name='F_Show' " & IsFieldSelected(FieldShow, "Keyword") & ">关键字</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='ProductNum' name='F_Show' " & IsFieldSelected(FieldShow, "ProductNum") & ">商品编号</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='ProductModel' name='F_Show' " & IsFieldSelected(FieldShow, "ProductModel") & ">型号</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='ProductStandard' name='F_Show' " & IsFieldSelected(FieldShow, "ProductStandard") & ">规格</td>" & vbCrLf
			Response.Write "    </tr>" & vbCrLf
			Response.Write "    <tr>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='ProducerName' name='F_Show' " & IsFieldSelected(FieldShow, "ProducerName") & ">生产商</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='TrademarkName' name='F_Show' " & IsFieldSelected(FieldShow, "TrademarkName") & ">品牌/商标</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='ProductIntro' name='F_Show' " & IsFieldSelected(FieldShow, "ProductIntro") & ">商品简介</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='ProductExplain' name='F_Show' " & IsFieldSelected(FieldShow, "ProductExplain") & ">详细介绍</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='ProductThumb' name='F_Show' " & IsFieldSelected(FieldShow, "ProductThumb") & ">商品缩略图</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Unit' name='F_Show' " & IsFieldSelected(FieldShow, "Unit") & ">单位</td>" & vbCrLf
			Response.Write "    </tr>" & vbCrLf
			Response.Write "    <tr>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Price' name='F_Show' " & IsFieldSelected(FieldShow, "Price") & ">当前零售价</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Price_Original' name='F_Show' " & IsFieldSelected(FieldShow, "Price_Original") & ">原始零售价</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Price_Member' name='F_Show' " & IsFieldSelected(FieldShow, "Price_Member") & ">会员零售价</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Hits' name='F_Show' " & IsFieldSelected(FieldShow, "Hits") & ">点击数</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Weight' name='F_Show' " & IsFieldSelected(FieldShow, "Weight") & ">商品重量</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='UpdateTime' name='F_Show' " & IsFieldSelected(FieldShow, "UpdateTime") & ">更新时间</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Property' name='F_Show' " & IsFieldSelected(FieldShow, "Property") & ">属性设置</td>" & vbCrLf
			Response.Write "    </tr>" & vbCrLf
			Response.Write "    <tr>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Flag' name='F_Show' " & IsFieldSelected(FieldShow, "Flag") & ">阅读权限</td>" & vbCrLf
			Response.Write "    </tr>" & vbCrLf
			Call Channel_Fields(iChannelID,rsChannel("FieldShow"))
			Response.Write "    </table>" & vbCrLf
		case 4  '''图片模块
			Response.write "        <table width=""90%"" border=""0"" cellspacing=""0"" cellpadding=""0"">"
			Response.Write "    <tr>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Title' name='F_Show' " & IsFieldSelected(FieldShow, "Title") & ">标题</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='ClassID' name='F_Show' " & IsFieldSelected(FieldShow, "ClassID") & ">归属栏目</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Keyword' name='F_Show' " & IsFieldSelected(FieldShow, "Keyword") & ">关键字</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Intro' name='F_Show' " & IsFieldSelected(FieldShow, "Intro") & ">简介</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='PhotoDetial' name='F_Show' " & IsFieldSelected(FieldShow, "PhotoDetial") & ">详细介绍</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Author' name='F_Show' " & IsFieldSelected(FieldShow, "Author") & ">作者</td>" & vbCrLf
			Response.Write "    </tr>" & vbCrLf
			Response.Write "    <tr>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='CopyFrom' name='F_Show' " & IsFieldSelected(FieldShow, "CopyFrom") & ">来源</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='PhotoThumb' name='F_Show' " & IsFieldSelected(FieldShow, "PhotoThumb") & ">图片缩略图</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='PhotoUrl' name='F_Show' " & IsFieldSelected(FieldShow, "PhotoUrl") & ">图片地址</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='PhotoSize' name='F_Show' " & IsFieldSelected(FieldShow, "PhotoSize") & ">图片大小</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Hits' name='F_Show' " & IsFieldSelected(FieldShow, "Hits") & ">点击数</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='UpdateTime' name='F_Show' " & IsFieldSelected(FieldShow, "UpdateTime") & ">更新时间</td>" & vbCrLf
			Response.Write "    </tr>" & vbCrLf
			Response.Write "    <tr>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Property' name='F_Show' " & IsFieldSelected(FieldShow, "Property") & ">属性设置</td>" & vbCrLf
			Response.Write "      <td><input type='checkbox' value='Flag' name='F_Show' " & IsFieldSelected(FieldShow, "Flag") & ">阅读权限</td>" & vbCrLf
			Response.Write "    </tr>" & vbCrLf
			Call Channel_Fields(iChannelID,rsChannel("FieldShow"))
			Response.Write "    </table>" & vbCrLf
	End Select	
	Response.Write "    </td>" & vbCrLf
	Response.Write "    </tr>" & vbCrLf
	Response.Write "</table>" & vbCrLf
		
    Response.Write "</td></tr></table>" & vbCrLf
    Response.Write "<table width='100%' border='0' align='center'><tr><td colspan='2' align='center'>"
    Response.Write "     <input name='iChannelID' type='hidden' id='iChannelID' value='" & rsChannel("ChannelID") & "'>"
    Response.Write "        <input name='Action' type='hidden' id='Action' value='SaveModify'>"
    Response.Write "        <input name='Submit'  type='submit' id='Submit' value='保存修改结果'> &nbsp; <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Channel.asp'"" style='cursor:hand;'></td>"
    Response.Write "  </tr>" & vbCrLf
    Response.Write "</table>"
    Response.Write "</form>"

    rsChannel.Close
    Set rsChannel = Nothing
End Sub


Sub SaveAdd()
    Dim rsChannel
    Dim OrderID
    ChannelName = GetForm("ChannelName")
    ChannelShortName = GetForm("ChannelShortName")
    ChannelItemUnit = GetForm("ChannelItemUnit")
    ModuleType = PE_CLng(GetForm("ModuleType"))
	OrderID = PE_CLng(GetForm("OrderID"))
	ChannelDir = GetForm("ChannelDir")
	tempindex = GetForm("tempindex")
	templist = GetForm("templist")
	temparticle = GetForm("temparticle")
    If ChannelName = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>频道名称不能为空！</li>"
    Else
        ChannelName = ReplaceBadChar(ChannelName)
    End If
	If ChannelShortName = "" Then
		FoundErr = True
		ErrMsg = ErrMsg & "<li>请指定项目名称！</li>"
	End If
    If OrderID = 0 Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请输入频道排序！</li>"
	end if
	If ChannelDir = "" Then
		FoundErr = True
		ErrMsg = ErrMsg & "<li>频道目录不能为空！</li>"
	Else
		If IsValidStr(ChannelDir) = False Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>频道目录名只能为英文字母和数字的组合，且第一个字符必须为英文字母！</li>"
		Else
			If fso.FolderExists(Server.MapPath(InstallDir & ChannelDir)) Then
				FoundErr = True
				ErrMsg = ErrMsg & "<li>频道目录已经存在！请另外指定一个目录。</li>"
			End If
		End If
	End If
    If tempindex = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请选择封面模版！</li>"
	end if
    If templist = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请选择列表模版！</li>"
	end if
    If temparticle = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请选择文章模版！</li>"
	end if
    If FoundErr = True Then
        Exit Sub
    End If
    If ChannelItemUnit = "" Then ChannelItemUnit = "个"
	ChannelID = GetNewID("PW_Channel", "ChannelID")
    Set rsChannel = Server.CreateObject("Adodb.RecordSet")
    rsChannel.Open "Select * from PW_Channel Where ChannelName='" & ChannelName & "'", Conn, 1, 3
    If Not (rsChannel.BOF And rsChannel.EOF) Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>频道名称已经存在！</li>"
        rsChannel.Close
        Set rsChannel = Nothing
        Exit Sub
    End If
    rsChannel.addnew
    rsChannel("ChannelID") = ChannelID
    rsChannel("ChannelName") = ChannelName	
	rsChannel("ChannelShortName") = ChannelShortName	
	rsChannel("ChannelItemUnit") = ChannelItemUnit	
	rsChannel("ChannelDir") = ChannelDir	
	rsChannel("MaxPerPage_Channel") = PE_CLng(GetForm("MaxPerPage_Channel"))	
	rsChannel("OrderID") = OrderID	
	rsChannel("ModuleType") = ModuleType
	rsChannel("Disabled") = PE_CBool(GetForm("Disabled"))	
	rsChannel("Tempindex") = tempindex
	rsChannel("Templist") = templist
	rsChannel("Temparticle") = temparticle
    rsChannel.Update
    rsChannel.Close
    Set rsChannel = Nothing
	Call CreateChannelDir(ChannelID, ChannelDir, ModuleType)
	Call ReloadLeft("Admin_Channel.asp")
End Sub

Sub SaveModify()
    Dim rsChannel, sqlChannel
    Dim OrderID,F_Show
    ChannelID = GetForm("iChannelID")
    ChannelName = GetForm("ChannelName")
    ChannelShortName = GetForm("ChannelShortName")
    ChannelItemUnit = GetForm("ChannelItemUnit")
    ModuleType = PE_CLng(GetForm("ModuleType"))
	OrderID = PE_CLng(GetForm("OrderID"))
	tempindex = GetForm("tempindex")
	templist = GetForm("templist")
	temparticle = GetForm("temparticle")
	F_Show = GetForm("F_Show")
	F_Show = Replace(F_Show," ","")

    If ChannelName = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>频道名称不能为空！</li>"
    Else
        ChannelName = ReplaceBadChar(ChannelName)
    End If
	If ChannelShortName = "" Then
		FoundErr = True
		ErrMsg = ErrMsg & "<li>请指定项目名称！</li>"
	End If
    If OrderID = 0 Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请输入频道排序！</li>"
	end if
    If FoundErr = True Then
        Exit Sub
    End If
    sqlChannel = "Select * from PW_Channel Where ChannelID=" & ChannelID
    Set rsChannel = Server.CreateObject("Adodb.RecordSet")
    rsChannel.Open sqlChannel, Conn, 1, 3
    If rsChannel.BOF And rsChannel.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的频道！</li>"
        rsChannel.Close
        Set rsChannel = Nothing
    Else
		rsChannel("ChannelName") = ChannelName	
		rsChannel("ChannelShortName") = ChannelShortName	
		rsChannel("ChannelItemUnit") = ChannelItemUnit	
		rsChannel("MaxPerPage_Channel") = PE_CLng(GetForm("MaxPerPage_Channel"))	
		rsChannel("OrderID") = OrderID	
		rsChannel("ModuleType") = ModuleType
		rsChannel("Disabled") = PE_CBool(GetForm("Disabled"))	
		rsChannel("Tempindex") = tempindex
		rsChannel("Templist") = templist
		rsChannel("Temparticle") = temparticle
		rsChannel("FieldShow") = F_Show
		rsChannel.Update
		rsChannel.Close
		Set rsChannel = Nothing
    End If
	Call ReloadLeft("Admin_Channel.asp")
End Sub


Sub DisabledChannel(ActionType)
    Dim ChannelID
    ChannelID = GetUrl("iChannelID")
    If ChannelID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定频道ID</li>"
    Else
        ChannelID = PE_CLng(ChannelID)
    End If
    If FoundErr = True Then
        Exit Sub
    End If
    
    If ActionType = 0 Then
        Conn.Execute ("update PW_Channel set Disabled=" & PE_True & " where ChannelID=" & ChannelID)
    Else
        Conn.Execute ("update PW_Channel set Disabled=" & PE_False & " where ChannelID=" & ChannelID)
    End If
    Call ReloadLeft("Admin_Channel.asp")
End Sub


Sub ShowChekcFormJS()
    Response.Write "<script language='JavaScript' type='text/JavaScript'>" & vbCrLf
    Response.Write "function CheckForm(){" & vbCrLf
    Response.Write "  if(document.myform.ChannelName.value==''){" & vbCrLf
    Response.Write "    alert('请输入频道名称！');" & vbCrLf
    Response.Write "    document.myform.ChannelName.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if(document.myform.ChannelShortName.value==''){" & vbCrLf
    Response.Write "    alert('请输入项目名称！');" & vbCrLf
    Response.Write "    document.myform.ChannelShortName.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  if(document.myform.OrderID.value==''){" & vbCrLf
    Response.Write "     alert('请输入栏目排序！');" & vbCrLf
    Response.Write "    document.myform.OrderID.focus();" & vbCrLf
    Response.Write "    return false;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "}" & vbCrLf
	Response.Write "function SelectTemplate(formstr){" & vbCrLf
	Response.Write "  var Templateform = document.getElementById(formstr);" & vbCrLf
	Response.Write "  var arr=showModalDialog('Admin_SelectFile.asp?dialogtype=Template', '', 'dialogWidth:820px; dialogHeight:600px; help: no; scroll: yes; status: no');" & vbCrLf
	Response.Write "  if(arr!=null){" & vbCrLf
	Response.Write "    var ss=arr.split('|');" & vbCrLf
	Response.write "    var regS = new RegExp('"& Template_Dir &"',""gi"");"
	Response.write "    str=ss[0].replace(regS,'{@TemplateDir}'); "
	Response.Write "    Templateform.value=str;" & vbCrLf
	Response.Write "  }" & vbCrLf
	Response.Write "}" & vbCrLf
    Response.Write "</script>" & vbCrLf
End Sub


Sub DelChannel()
    On Error Resume Next
    Dim ChannelID, rsChannel, sqlChannel
    ChannelID = GetUrl("iChannelID")
    If ChannelID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要删除的频道ID</li>"
    Else
        ChannelID = PE_CLng(ChannelID)
    End If
    
    If FoundErr = True Then
        Exit Sub
    End If

    sqlChannel = "Select * from PW_Channel Where ChannelID=" & ChannelID
    Set rsChannel = Server.CreateObject("Adodb.RecordSet")
    rsChannel.Open sqlChannel, Conn, 1, 3
    If rsChannel.BOF And rsChannel.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的频道！</li>"
    Else
        If rsChannel("ChannelType") = 1 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>不能删除系统频道，如果你不想用此频道，可以禁用此频道。</li>"
        Else
			If rsChannel("ChannelType") = 0 Then
				Call DelChannelDir(rsChannel("ChannelDir"))
                Dim rs
                Set rs = Conn.Execute("Select FieldName From PW_Field Where ChannelID=" & ChannelID)
                Do While Not rs.EOF
                    Conn.Execute ("alter table PW_" & Infotable & " drop COLUMN " & rs("FieldName") & "")
                    rs.MoveNext
                Loop
                Set rs = Nothing
				Conn.Execute ("delete from PW_Class where ChannelID=" & ChannelID)
				Conn.Execute ("delete from PW_Article where ChannelID=" & ChannelID)
				Conn.Execute ("delete from PW_Soft where ChannelID=" & ChannelID)
				Conn.Execute ("delete from PW_Product where ChannelID=" & ChannelID)
				Conn.Execute ("delete from PW_Photo where ChannelID=" & ChannelID)
			rsChannel.Delete
			rsChannel.Update
			Call ReloadLeft("Admin_Channel.asp")
            End If
        End If
	End If
    rsChannel.Close
    Set rsChannel = Nothing
End Sub


Sub CreateChannelDir(iChannelID, DirName, iModuleType)
    On Error Resume Next
    Dim fsfl, fl, fsfm, fm, strDir
    If Not fso.FolderExists(Server.MapPath(InstallDir & DirName)) Then
        fso.CreateFolder Server.MapPath(InstallDir & DirName)
    End If
    Select Case iModuleType
    Case 1
        strDir = "Article"
    Case 2
        strDir = "Soft"
    Case 3
        strDir = "Product"
    Case 4
        strDir = "Photo"
    End Select
    Set fsfl = fso.GetFolder(Server.MapPath(InstallDir & strDir))
    For Each fl In fsfl.Files
        If GetFileExt(fl.name) = "asp" Then
            fl.Copy Server.MapPath(InstallDir & DirName & "/" & fl.name), True
        End If
    Next
    Set fsfl = Nothing
    
    Set fl = fso.CreateTextFile(Server.MapPath(InstallDir & DirName & "/ChannelConfig.asp"), True)
    fl.WriteLine ("<" & "%")
    fl.WriteLine ("ChannelID = " & iChannelID)
    fl.WriteLine ("%" & ">")
    fl.Close
    Set fl = Nothing

End Sub

Function GetModuleTypeName(ModuleType)
    Dim strModuleType
    Select Case ModuleType
    Case 1
        strModuleType = "文章"
    Case 2
        strModuleType = "下载"
    Case 3
        strModuleType = "产品"
    Case 4
        strModuleType = "图片"
    End Select
    GetModuleTypeName = strModuleType
End Function


Sub ReloadLeft(strUrl)
    Response.Write "<script language='JavaScript' type='text/JavaScript'>" & vbCrLf
    Response.Write "  parent.left.location.reload();" & vbCrLf
    Response.Write "  window.location.href='" & strUrl & "';"
    Response.Write "</script>" & vbCrLf
End Sub

Sub DelChannelDir(DirName)
    On Error Resume Next
    If IsNull(DirName) Or Trim(DirName) = "" Then Exit Sub
    If fso.FolderExists(Server.MapPath(InstallDir & DirName)) Then
        fso.DeleteFolder Server.MapPath(InstallDir & DirName)
    End If
End Sub

Sub Channel_Fields(tChannelID,FieldArr)
	Dim oRs,i
	Set oRs=Conn.execute("Select FieldName,Title from PW_Field where ChannelID="&tChannelID)
	if not oRs.eof then
	i=0
    Response.Write "    <tr>"&vbcrlf
	do while not oRs.eof
    Response.Write "      <td><input type='checkbox' value='"& oRs("FieldName") &"' name='F_Show' " & IsFieldSelected(FieldArr, oRs("FieldName")) & ">"& oRs("Title") &"</td>"&vbcrlf
	oRs.movenext
	i=i+1
	if i mod 6=0 then
		Response.write "</tr><tr>"&vbcrlf
	end if
	loop
	Response.Write "    </tr>"
	end if
	oRs.close
	set oRs=Nothing
End Sub

Function IsFieldSelected(Compare1, Compare2)
    If FoundInArr(Compare1, Compare2, ",") = True Then
        IsFieldSelected = " checked"
    Else
        IsFieldSelected = ""
    End If
End Function

%>