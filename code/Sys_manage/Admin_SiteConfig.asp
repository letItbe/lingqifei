<!--#include file="Admin_Common.asp"-->
<%
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Channel = 0   '0--不检查，1--频道管理员，2--栏目总编，3--栏目管理员
Const PurviewLevel_Others = "SiteConfig"   '其他权限


Response.Write "<html><head><title>网站配置</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf

Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' Class='border'>" & vbCrLf
Call ShowPageTitle("网 站 信 息 配 置")
Response.Write "</table>" & vbCrLf


If Action = "SaveConfig" Then
    Call SaveConfig
Else
    Call ModifyConfig
End If
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn

Sub ModifyConfig()
    Dim sqlConfig, rsConfig
    
    sqlConfig = "select * from PW_Config"
    Set rsConfig = Server.CreateObject("ADODB.Recordset")
    rsConfig.Open sqlConfig, Conn, 1, 3
    If rsConfig.BOF And rsConfig.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>网站配置数据丢失，请使用初始数据库进行恢复。</li>"
        rsConfig.Close
        Set rsConfig = Nothing
        Exit Sub
    End If
	Dim Modules
	Modules = rsConfig("Modules")
    Response.Write "<script language='javascript'>" & vbCrLf
    Response.Write "var tID=0;" & vbCrLf
    Response.Write "function ShowTabs(ID){" & vbCrLf
    Response.Write "  if(ID!=tID){" & vbCrLf
    Response.Write "    TabTitle[tID].className='title5';" & vbCrLf
    Response.Write "    TabTitle[ID].className='title6';" & vbCrLf
    Response.Write "    Tabs[tID].style.display='none';" & vbCrLf
    Response.Write "    Tabs[ID].style.display='';" & vbCrLf
    Response.Write "    tID=ID;" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "}" & vbCrLf
	Response.Write "function SelectTemplate(formstr){" & vbCrLf
	Response.Write "  var Templateform = document.getElementById(formstr);" & vbCrLf
	Response.Write "  var arr=showModalDialog('Admin_SelectFile.asp?dialogtype=Template', '', 'dialogWidth:820px; dialogHeight:600px; help: no; scroll: yes; status: no');" & vbCrLf
	Response.Write "  if(arr!=null){" & vbCrLf
	Response.Write "    var ss=arr.split('|');" & vbCrLf
	Response.write "    var regS = new RegExp('"& Template_Dir &"',""gi"");"
	Response.write "    str=ss[0].replace(regS,'{@TemplateDir}'); "
	Response.Write "    Templateform.value=str;" & vbCrLf
	Response.Write "  }" & vbCrLf
	Response.Write "}" & vbCrLf
    Response.Write "//-->" & vbCrLf
    Response.Write "</script>" & vbCrLf
    Response.Write "<form name='myform' id='myform' method='POST' action='Admin_SiteConfig.asp' >" & vbCrLf
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr align='center' height='24'>"
    Response.Write "<td id='TabTitle' class='title6' onclick='ShowTabs(0)'>网站信息</td>" & vbCrLf
	Response.Write "<td id='TabTitle' class='title5' onclick='ShowTabs(1)'>网站选项</td>" & vbCrLf
    Response.Write "<td>&nbsp;</td></tr></table>"
    Response.Write "<table width='100%' border='0' cellpadding='5' cellspacing='1' Class='border'><tr><td class='tdbg'>" & vbCrLf
    Response.Write "<table width='95%' border='0' align='center' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>" & vbCrLf
    Response.Write "  <tbody id='Tabs' style='display:'>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='20%' class='tdbg5'><strong>网站名称：</strong></td>" & vbCrLf
    Response.Write "      <td><input name='SiteName' type='text' id='SiteName' value='" & rsConfig("SiteName") & "' size='40' maxlength='50'></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='20%' class='tdbg5'><strong>企业名称：</strong></td>" & vbCrLf
    Response.Write "      <td><input name='CompanyName' type='text' id='CompanyName' value='" & rsConfig("CompanyName") & "' size='40' maxlength='50'></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='20%' class='tdbg5'><strong>联系地址：</strong></td>" & vbCrLf
    Response.Write "      <td><input name='Address' type='text' id='Address' value='" & rsConfig("Address") & "' size='40' maxlength='50'></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='20%' class='tdbg5'><strong>联系人：</strong></td>" & vbCrLf
    Response.Write "      <td><input name='contact' type='text' id='contact' value='" & rsConfig("contact") & "' size='40' maxlength='10'></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='20%' class='tdbg5'><strong>联系电话：</strong></td>" & vbCrLf
    Response.Write "      <td><input name='Tel' type='text' id='Tel' value='" & rsConfig("Tel") & "' size='40' maxlength='50'></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='20%' class='tdbg5'><strong>联系传真：</strong></td>" & vbCrLf
    Response.Write "      <td><input name='Fax' type='text' id='Fax' value='" & rsConfig("Fax") & "' size='40' maxlength='50'></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='20%' class='tdbg5'><strong>手机：</strong></td>" & vbCrLf
    Response.Write "      <td><input name='Phone' type='text' id='Phone' value='" & rsConfig("Phone") & "' size='40' maxlength='50'></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='20%' class='tdbg5'><strong>网址：</strong></td>" & vbCrLf
    Response.Write "      <td><input name='NetAddress' type='text' id='NetAddress' value='" & rsConfig("NetAddress") & "' size='40' maxlength='50'></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='20%' class='tdbg5'><strong>邮箱：</strong></td>" & vbCrLf
    Response.Write "      <td><input name='Email' type='text' id='Email' value='" & rsConfig("Email") & "' size='40' maxlength='50'></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='20%' class='tdbg5'><strong>邮政编码：</strong></td>" & vbCrLf
    Response.Write "      <td><input name='PostCode' type='text' id='PostCode' value='" & rsConfig("PostCode") & "' size='40' maxlength='50'></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='20%' class='tdbg5'><strong>QQ号码：</strong><br>多个请用“|”隔开</td>" & vbCrLf
    Response.Write "      <td><input name='QQ' type='text' id='QQ' value='" & rsConfig("QQ") & "' size='40' maxlength='100'></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='20%' class='tdbg5'><strong>版权信息：</strong><br>支持HTML标记，不能使用双引号</td>" & vbCrLf
    Response.Write "      <td><textarea name='CopyRight' cols='60' rows='4' id='CopyRight'>" & rsConfig("CopyRight") & "</textarea></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='20%' class='tdbg5'><strong>网站META关键词：</strong><br>针对搜索引擎设置的关键词<br>多个关键词请用,号分隔</td>" & vbCrLf
    Response.Write "      <td><textarea name='Meta_Keywords' cols='60' rows='4' id='Meta_Keywords'>" & rsConfig("Meta_Keywords") & "</textarea></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='20%' class='tdbg5'><strong>网站META网页描述：</strong><br>针对搜索引擎设置的网页描述<br>多个描述请用,号分隔</td>" & vbCrLf
    Response.Write "      <td><textarea name='Meta_Description' cols='60' rows='4' id='Meta_Description'>" & rsConfig("Meta_Description") & "</textarea></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
	Response.Write "    <tr class='tdbg'>" & vbCrLf
	Response.Write "      <td width='40%' class='tdbg5'><strong>网站状态：</strong></td>" & vbCrLf
	Response.Write "      <td><input type=""radio"" value=""1"" name=""Sitestate"" "& Ischecked(rsConfig("Sitestate"),"1") &"/> 打开 <input type=""radio"" value=""0"" name=""Sitestate"" "& Ischecked(rsConfig("Sitestate"),"0") &" /> 关闭</td>" & vbCrLf
	Response.Write ""
	Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='20%' class='tdbg5'><strong>网站关闭原因：</strong><br>如果网站状态设置为关闭，请在这里设置关闭原因</td>" & vbCrLf
    Response.Write "      <td><textarea name='Closemsg' cols='60' rows='4' id='Closemsg'>" & rsConfig("Closemsg") & "</textarea></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "  </tbody>" & vbCrLf
	''''''''''''''''''''''''网站设置
	Response.Write "  <tbody id='Tabs' style='display:none'>"
	Response.Write "    <tr class='tdbg'>" & vbCrLf
	Response.Write "      <td width='40%' class='tdbg5'><strong>是否生成HTML：</strong></td>" & vbCrLf
	Response.Write "      <td><input type='radio' name='UseCreateHTML' value='0'  "& Ischecked(rsConfig("UseCreateHTML"),"0") &" /> 否&nbsp;&nbsp;<input type='radio' name='UseCreateHTML' value='1' "& Ischecked(rsConfig("UseCreateHTML"),"1") &" /> 是</td>" & vbCrLf
	Response.Write "    </tr>" & vbCrLf
	Response.Write "    <tr class='tdbg'>" & vbCrLf
	Response.Write "      <td width='40%' class='tdbg5'><strong>HTML目录：</strong></td>" & vbCrLf
	Response.Write "      <td><input name='htmlDir' type='text' id='htmlDir' value='" & rsConfig("htmlDir") & "' size='10' maxlength='10'></td>" & vbCrLf
	Response.Write "    </tr>" & vbCrLf
	Response.Write "    <tr class='tdbg'>" & vbCrLf
	Response.Write "      <td width='40%' class='tdbg5'><strong>发布文章后是否生成HTML：</strong><br>需要启用生成HTML</td>" & vbCrLf
	Response.Write "      <td><input type='radio' name='AutoCreateType' value='0' "& Ischecked(rsConfig("AutoCreateType"),"0") &" /> 否&nbsp;&nbsp;<input type='radio' name='AutoCreateType' value='1' "& Ischecked(rsConfig("AutoCreateType"),"1") &" /> 是</td>" & vbCrLf
	Response.Write "    </tr>" & vbCrLf
	Response.Write "    <tr class='tdbg'>" & vbCrLf
	Response.Write "      <td width='40%' class='tdbg5'><strong>多少天内更新的信息为新信息：</strong><br></td>" & vbCrLf
	Response.Write "      <td><input name='DaysOfNew' type='text' id='DaysOfNew' value='" & rsConfig("DaysOfNew") & "' size='10' maxlength='10'></td>" & vbCrLf
	Response.Write "    </tr>" & vbCrLf
	Response.Write "    <tr class='tdbg'>" & vbCrLf
	Response.Write "      <td width='40%' class='tdbg5'><strong>网站热点的点击数最小值：</strong><br>只有点击数达到此数值，才会作为网站的热点内容显示。</td>" & vbCrLf
	Response.Write "      <td><input name='HitsOfHot' type='text' id='HitsOfHot' value='" & rsConfig("HitsOfHot") & "' size='10' maxlength='10'></td>" & vbCrLf
	Response.Write "    </tr>" & vbCrLf
	if AdminPurview=1 then
		Response.Write "    <tr class='tdbg'>" & vbCrLf
		Response.Write "      <td width='40%' class='tdbg5'><strong>模块管理选项：</strong><br>控制网站启用的模块。</td>" & vbCrLf
		Response.Write "      <td>" & vbCrLf
		Response.Write "        <table width='100%'>" & vbCrLf
		Response.Write "          <tr>" & vbCrLf
		Response.Write "            <td><input name='Modules' type='checkbox' value='SiteConfig'" & IsModulesSelected(Modules, "SiteConfig") & ">系统设置管理</td>" & vbCrLf
		Response.Write "            <td><input name='Modules' type='checkbox' value='AD'" & IsModulesSelected(Modules, "AD") & ">网站广告管理</td>" & vbCrLf
		Response.Write "            <td><input name='Modules' type='checkbox' value='FriendSite'" & IsModulesSelected(Modules, "FriendSite") & ">友情链接管理</td>" & vbCrLf
		Response.Write "            <td><input name='Modules' type='checkbox' value='Announce'" & IsModulesSelected(Modules, "Announce") & ">网站公告管理</td>" & vbCrLf
		Response.Write "            <td><input name='Modules' type='checkbox' value='Vote'" & IsModulesSelected(Modules, "Vote") & ">网站调查管理</td>" & vbCrLf
		Response.Write "            <td><input name='Modules' type='checkbox' value='Admin'" & IsModulesSelected(Modules, "Admin") & ">管理员管理</td>" & vbCrLf
		Response.Write "          </tr>" & vbCrLf
		Response.Write "          <tr>" & vbCrLf
		Response.Write "            <td><input name='Modules' type='checkbox' value='User'" & IsModulesSelected(Modules, "User") & ">会员管理系统</td>" & vbCrLf
		Response.Write "            <td><input name='Modules' type='checkbox' value='Order'" & IsModulesSelected(Modules, "Order") & ">订单管理系统</td>" & vbCrLf
		Response.Write "            <td><input name='Modules' type='checkbox' value='Gbook'" & IsModulesSelected(Modules, "Gbook") & ">留言板</td>" & vbCrLf
		Response.Write "            <td><input name='Modules' type='checkbox' value='JobSys'" & IsModulesSelected(Modules, "JobSys") & ">人才招聘</td>" & vbCrLf
		Response.Write "            <td><input name='Modules' type='checkbox' value='DataBase'" & IsModulesSelected(Modules, "DataBase") & ">数据库管理</td>" & vbCrLf
		Response.Write "            <td><input name='Modules' type='checkbox' value='CreateHTML'" & IsModulesSelected(Modules, "CreateHTML") & ">网站生成</td>" & vbCrLf
		Response.Write "          </tr>" & vbCrLf
		Response.Write "          <tr>" & vbCrLf
		Response.Write "            <td><input name='Modules' type='checkbox' value='Nav'" & IsModulesSelected(Modules, "Nav") & ">栏目导航</td>" & vbCrLf
		Response.Write "            <td><input name='Modules' type='checkbox' value='Keywords'" & IsModulesSelected(Modules, "Keywords") & ">站内关键词</td>" & vbCrLf
		Response.Write "          </tr>" & vbCrLf
		Response.Write "        </table>" & vbCrLf
		Response.Write "      </td>" & vbCrLf
		Response.Write "    </tr>" & vbCrLf
	end if
	Response.Write "    <tr class='tdbg'>" & vbCrLf
	Response.Write "      <td width='40%' class='tdbg5'><strong>网站广告目录：</strong><br>为了不让广告拦截软件拦截网站的广告，您可以修改广告JS的存放目录（默认为AD），改过以后，需要再设置此处</td>" & vbCrLf
	Response.Write "      <td><input name='ADDir' type='text' id='ADDir' value='" & rsConfig("ADDir") & "' size='20' maxlength='20'></td>" & vbCrLf
	Response.Write "    </tr>" & vbCrLf
	Response.Write "    <tr class='tdbg'>"
	Response.Write "      <td width='200' class='tdbg5'><strong>上传文件的保存目录：</strong><br><font color='red'>你可以定期或不定期的更改上传目录，以防其他网站盗链</font></td>"
	Response.Write "      <td><input name='UploadDir' type='text' id='UploadDir' value='" & rsConfig("UploadDir") & "' size='20' maxlength='20'>&nbsp;&nbsp;<font color='red'>只能是英文和数字，不能带空格或“\”、“/”等符号。</font></td>"
	Response.Write "    </tr>" & vbCrLf
	Response.Write "    <tr class='tdbg'>"
	Response.Write "      <td width='200' class='tdbg5'><strong>允许上传的最大文件大小：</strong></td>"
	Response.Write "      <td><input name='MaxFileSize' type='text' id='MaxFileSize' value='" & rsConfig("MaxFileSize") & "' size='10' maxlength='10'> KB&nbsp;&nbsp;&nbsp;&nbsp;<font color=blue>提示：1 KB = 1024 Byte，1 MB = 1024 KB</font></td>"
	Response.Write "    </tr>" & vbCrLf
		Dim arrFileType
		If rsConfig("UpFileType") & "" = "" Then
			arrFileType = Split("gif|jpg|jpeg|jpe|bmp|png$swf$mid|mp3|wmv|asf|avi|mpg$ram|rm|ra$rar|exe|doc|zip", "$")
		Else
			arrFileType = Split(rsConfig("UpFileType"), "$")
			If UBound(arrFileType) < 4 Then
				arrFileType = Split("gif|jpg|jpeg|jpe|bmp|png$swf$mid|mp3|wmv|asf|avi|mpg$ram|rm|ra$rar|exe|doc|zip", "$")
			End If
		End If
	Response.Write "    <tr class='tdbg'>"
	Response.Write "      <td width='200' class='tdbg5'><strong>允许上传的文件类型：</strong><br>多种文件类型之间以“|”分隔</td>"
	Response.Write "      <td><table>"
	Response.Write "          <tr><td>图片类型：</td><td><input name='UpFileType' type='text' id='UpFileType' value='" & Trim(arrFileType(0)) & "' size='50' maxlength='200'></td></tr>"
	Response.Write "          <tr><td>Flash文件：</td><td><input name='UpFileType' type='text' id='UpFileType' value='" & Trim(arrFileType(1)) & "' size='50' maxlength='50'></td></tr>"
	Response.Write "          <tr><td>Windows媒体：</td><td><input name='UpFileType' type='text' id='UpFileType' value='" & Trim(arrFileType(2)) & "' size='50' maxlength='200'></td></tr>"
	Response.Write "          <tr><td>Real媒体：</td><td><input name='UpFileType' type='text' id='UpFileType' value='" & Trim(arrFileType(3)) & "' size='20' maxlength='200'></td></tr>"
	Response.Write "          <tr><td>其他文件：</td><td><input name='UpFileType' type='text' id='UpFileType' value='" & Trim(arrFileType(4)) & "' size='50' maxlength='200'></td></tr>"
	Response.Write "      </table></td>"
	Response.Write "    </tr>" & vbCrLf
	Response.Write "  </tbody>" & vbCrLf
    Response.Write "</table>" & vbCrLf
    Response.Write "</td></tr></table>" & vbCrLf
    Response.Write "<table width='100%' border='0'>" & vbCrLf
    Response.Write "    <tr>" & vbCrLf
    Response.Write "      <td height='40' align='center'>" & vbCrLf
    Response.Write "        <input name='Action' type='hidden' id='Action' value='SaveConfig'>" & vbCrLf
    Response.Write "        <input name='cmdSave' type='submit' id='cmdSave' value=' 保存设置 ' class='submit'>" & vbCrLf
    Response.Write "        <input name='Cancel' type='button' id='Cancel' value='&nbsp;取消返回&nbsp;' onclick=""window.history.go(-1)"" class='submit'>" & vbCrLf
    Response.Write "      </td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "  </table>" & vbCrLf
    Response.Write "</form>" & vbCrLf
    rsConfig.Close
    Set rsConfig = Nothing
End Sub



Sub SaveConfig()
	Dim sqlConfig, rsConfig,FoundErr,Site_Template
    FoundErr = False

    If FoundErr = True Then
        Call WriteErrMsg(ErrMsg, ComeUrl)
        Exit Sub
    End If
	
	
    sqlConfig = "select * from PW_Config"
    Set rsConfig = Server.CreateObject("ADODB.Recordset")
    rsConfig.Open sqlConfig, Conn, 1, 3

	rsConfig("SiteName") = GetForm("SiteName")
	rsConfig("CompanyName") = GetForm("CompanyName")
	rsConfig("Address") = GetForm("Address")
	rsConfig("contact") = GetForm("contact")
	rsConfig("Tel") = GetForm("Tel")
	rsConfig("Fax") = GetForm("Fax")
	rsConfig("Phone") = GetForm("Phone")
	rsConfig("NetAddress") = GetForm("NetAddress")
	rsConfig("Email") = GetForm("Email")
	rsConfig("PostCode") = GetForm("PostCode")
	rsConfig("QQ") = GetForm("QQ")
	rsConfig("CopyRight") = GetForm("CopyRight")
	rsConfig("Meta_Keywords") = GetForm("Meta_Keywords")
	rsConfig("Meta_Description") = GetForm("Meta_Description")
	rsConfig("Sitestate") = PE_CLng(GetForm("Sitestate"))
	rsConfig("Closemsg") = GetForm("Closemsg")
	rsConfig("UseCreateHTML") =	PE_CLng(GetForm("UseCreateHTML"))
	rsConfig("AutoCreateType") = PE_CLng(GetForm("AutoCreateType"))
	rsConfig("HTMLDir") = GetForm("htmldir")
	if AdminPurview=1 then
		rsConfig("Modules") = ReplaceBadChar(GetForm("Modules"))
	End if
	rsConfig("ADDir") = GetForm("ADDir")
	rsConfig("DaysOfNew") = PE_CLng(GetForm("DaysOfNew"))
	rsConfig("HitsOfHot") = PE_CLng(GetForm("HitsOfHot"))
	rsConfig("UploadDir") = Lcase(GetForm("UploadDir"))
	rsConfig("MaxFileSize") = PE_CLng(GetForm("MaxFileSize"))
	rsConfig("UpFileType") = Replace(GetForm("UpFileType"),",","$")
    rsConfig.Update
    rsConfig.Close
    Set rsConfig = Nothing
    Call WriteSuccessMsg("网站配置保存成功！", ComeUrl)
	Call ReloadLeft()
End Sub


Function IsModulesSelected(Compare1, Compare2)
    If FoundInArr(Compare1, Compare2, ",") = True Then
        IsModulesSelected = " checked"
    Else
        IsModulesSelected = ""
    End If
End Function

function  Ischecked(compare1,compare2)
	if PE_CBool(compare1) = PE_CBool(compare2) then
		Ischecked = " checked"
    Else
        Ischecked = ""
    End If
end function

Sub ReloadLeft()
    Response.Write "<script language='JavaScript' type='text/JavaScript'>" & vbCrLf
    Response.Write "  parent.left.location.reload();" & vbCrLf
    Response.Write "</script>" & vbCrLf
End Sub

%>

