<!--#include file="Admin_Common.asp"-->
<!--#include file="../Include/PW_Class.asp"-->
<%
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Channel = 0   '0--不检查，1--频道管理员，2--栏目总编，3--栏目管理员
Const PurviewLevel_Others = ""   '其他权限

if Purview_Class<>True then
    Response.write "<br><p align=center><font color='red'>对不起，你没有此项操作的权限。</font></p>"
    Call CloseConn
    Response.End
End if
Dim iOrderID
ParentID = PE_CLng(GetValue("ParentID"))

Response.Write "<html><head><title>" & ChannelName & "管理----栏目管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
Call ShowPageTitle(ChannelName & "管理----栏目管理")
Response.Write "  <tr class='tdbg'>"
Response.Write "    <td width='70' height='30'><strong>管理导航：</strong></td>"
Response.Write "    <td height='30'>"
Response.Write "<a href='Admin_Class.asp?ChannelID=" & ChannelID & "'>" & ChannelShortName & "栏目管理首页</a>&nbsp;|&nbsp;"
Response.Write "<a href='Admin_Class.asp?ChannelID=" & ChannelID & "&Action=Add'>添加" & ChannelShortName & "栏目</a>&nbsp;|&nbsp;"
Response.Write "<a href='Admin_Class.asp?ChannelID=" & ChannelID & "&Action=Order'>一级栏目排序</a>&nbsp;|&nbsp;"
Response.Write "<a href='Admin_Class.asp?ChannelID=" & ChannelID & "&Action=OrderN'>N级栏目排序</a>&nbsp;|&nbsp;"
Response.Write "<a href='Admin_Class.asp?ChannelID=" & ChannelID & "&Action=Reset'>复位所有" & ChannelShortName & "栏目</a>&nbsp;|&nbsp;"
Response.Write "<a href='Admin_Class.asp?ChannelID=" & ChannelID & "&Action=Patch'>修复栏目结构</a>"
Response.Write "    </td></tr></table>" & vbCrLf

Select Case Action
Case "Add"
    Call AddClass
Case "SaveAdd"
    Call SaveAdd
Case "Modify"
    Call Modify
Case "SaveModify"
    Call SaveModify
Case "Move"
    Call MoveClass
Case "SaveMove"
    Call SaveMove
Case "Del"
    Call DeleteClass
Case "UpOrder"
    Call UpOrder
Case "DownOrder"
    Call DownOrder
Case "Order"
    Call order
Case "UpOrderN"
    Call UpOrderN
Case "DownOrderN"
    Call DownOrderN
Case "OrderN"
    Call OrderN
Case "Reset"
    Call Reset
Case "SaveReset"
    Call SaveReset
Case "ResetChildClass"
    Call ResetChildClass
Case "Patch"
    Call Patch
Case "DoPatch"
    Call DoPatch
Case "Clear"
    Call ClearClass
Case Else
    Call main
End Select
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn



Sub main()
    Dim arrShowLine(20), i
    For i = 0 To UBound(arrShowLine)
        arrShowLine(i) = False
    Next
	Dim sqlClass, rsClass, iDepth
    sqlClass = "select * from PW_Class where ChannelID=" & ChannelID & " order by RootID,OrderID"
    Set rsClass = Conn.Execute(sqlClass)
    Response.Write "<br>" & vbCrLf
    Response.Write "<table width='100%' border='0' align='center' cellpadding='0' cellspacing='1' class='border'>"
    Response.Write "  <tr class='title' height='22'> "
    Response.Write "    <td width='30' align='center'><strong>ID</strong></td>"
    Response.Write "    <td align='center'><strong>栏目名称</strong></td>"
    Response.Write "    <td width='380' align='center'><strong>操作选项</strong></td>"
    Response.Write "  </tr>" & vbCrLf
    If rsClass.BOF And rsClass.EOF Then
        Response.Write "<tr><td colspan='10' height='50' align='center'>没有任何栏目</td></tr>"
    Else
        Do While Not rsClass.EOF
            Response.Write "<tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"
            Response.Write "    <td width='30' align='center'>" & rsClass("ClassID") & "</td>"
            Response.Write "    <td>"
            iDepth = rsClass("Depth")
            If rsClass("NextID") > 0 Then
                arrShowLine(iDepth) = True
            Else
                arrShowLine(iDepth) = False
            End If
            If iDepth > 0 Then
                For i = 1 To iDepth
                    If i = iDepth Then
                        If rsClass("NextID") > 0 Then
                            Response.Write "<img src='../images/tree_line1.gif' width='17' height='16' valign='abvmiddle'>"
                        Else
                            Response.Write "<img src='../images/tree_line2.gif' width='17' height='16' valign='abvmiddle'>"
                        End If
                    Else
                        If arrShowLine(i) = True Then
                            Response.Write "<img src='../images/tree_line3.gif' width='17' height='16' valign='abvmiddle'>"
                        Else
                            Response.Write "<img src='../images/tree_line4.gif' width='17' height='16' valign='abvmiddle'>"
                        End If
                    End If
                Next
            End If
            If rsClass("Child") > 0 Then
                Response.Write "<img src='../images/tree_folder4.gif' width='15' height='15' valign='abvmiddle'>"
            Else
                Response.Write "<img src='../images/tree_folder3.gif' width='15' height='15' valign='abvmiddle'>"
            End If
            If rsClass("Depth") = 0 Then
                Response.Write "<b>"
            End If
            Response.Write "<a href='Admin_Class.asp?Action=Modify&ChannelID=" & ChannelID & "&ClassID=" & rsClass("ClassID") & "'>" & rsClass("ClassName") & "</a>"
            If rsClass("Child") > 0 Then
                Response.Write "（" & rsClass("Child") & "）"
            End If
            If rsClass("Depth") = 0 Then
                Response.Write "</b>"
            End If
            Response.Write "</td>"
            Response.Write "<td align='left'>&nbsp;"
			If rsClass("ClassType") = 1 Then
				Response.Write "<a href='Admin_Class.asp?ChannelID=" & ChannelID & "&Action=Add&ParentID=" & rsClass("ClassID") & "'>添加子栏目</a>&nbsp;|&nbsp;"
			End If
            Response.Write "<a href='Admin_Class.asp?ChannelID=" & ChannelID & "&Action=Modify&ClassID=" & rsClass("ClassID") & "'>修改设置</a>&nbsp;|&nbsp;"
            Response.Write "<a href='Admin_Class.asp?ChannelID=" & ChannelID & "&Action=Move&ClassID=" & rsClass("ClassID") & "'>移动栏目</a>&nbsp;|&nbsp;"
            If rsClass("ParentID") = 0 And rsClass("ClassType") = 1 Then
                Response.Write "<a href='Admin_Class.asp?ChannelID=" & ChannelID & "&Action=ResetChildClass&ClassID=" & rsClass("ClassID") & "' onclick=""return confirm('“复位子栏目”将把此栏目的所有子栏目都复位成二级子栏目！请慎重操作！确定要复位子栏目吗？')"">复位子栏目</a>&nbsp;|&nbsp;"
            End If
            Response.Write "<a href='Admin_Class.asp?ChannelID=" & ChannelID & "&Action=Clear&ClassID=" & rsClass("ClassID") & "' onClick='return ConfirmDel3();'>清空</a>&nbsp;|&nbsp;"
            Response.Write "<a href='Admin_Class.asp?ChannelID=" & ChannelID & "&Action=Del&ClassID=" & rsClass("ClassID") & "' onClick='return ConfirmDel2();'>删除</a>"
            Response.Write "</td></tr>" & vbCrLf
            rsClass.MoveNext
        Loop
    End If
    rsClass.Close
    Set rsClass = Nothing
    Response.Write "</table>" & vbCrLf
    Response.Write "<script language='JavaScript' type='text/JavaScript'>" & vbCrLf
    Response.Write "function ConfirmDel2(){" & vbCrLf
    Response.Write "   if(confirm('删除栏目操作将删除此栏目中的所有子栏目和" & ChannelShortName & "，并且不能恢复！确定要删除此栏目吗？'))" & vbCrLf
    Response.Write "     return true;" & vbCrLf
    Response.Write "   else" & vbCrLf
    Response.Write "     return false;}" & vbCrLf
    Response.Write "function ConfirmDel3(){" & vbCrLf
    Response.Write "   if(confirm('清空栏目将把栏目（包括子栏目）的所有" & ChannelShortName & "放入回收站中！确定要清空此栏目吗？'))" & vbCrLf
    Response.Write "     return true;" & vbCrLf
    Response.Write "   else" & vbCrLf
    Response.Write "     return false;}" & vbCrLf
    Response.Write "</script>" & vbCrLf
End Sub



Sub AddClass()
    Call AddModify_JS
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_Class.asp?ChannelID=" & ChannelID & "'>栏目管理</a>&nbsp;&gt;&gt;&nbsp;添加栏目</td></tr></table>"
    Response.Write "<form name='form1' method='post' action='Admin_Class.asp' onsubmit='return check()'>" & vbCrLf
    Response.Write "<table width='100%' border='0' align='center' cellpadding='5' cellspacing='1' class='border'><tr class='tdbg'><td height='100' valign='top'>" & vbCrLf
    Response.Write "<table width='95%' align='center' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>" & vbCrLf
    Response.Write "  <tbody id='Tabs' style='display:'>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='300' class='tdbg5'><strong>所属栏目：</strong>"
    Response.Write "       </td>"
    Response.Write "      <td><select name='ParentID'><option value='0'>无（做为一级栏目）</option>" & GetClass_Opt(ChannelID, ParentID) & "</select> <font color=blue>不能指定为外部栏目</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='300' class='tdbg5'><strong>栏目名称：</strong></td>"
    Response.Write "      <td><input name='ClassName' type='text' size='20' maxlength='50'> <font color=red>*</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='300' class='tdbg5'><strong>栏目类型：</strong><br><font color=red>请慎重选择，栏目一旦添加后就不能再更改栏目类型。</font></td>" & vbCrLf
    Response.Write "      <td>"
    Response.Write "        <input name='ClassType' type='radio' value='1' checked onclick=""HideTabUrl('0')"">内部栏目&nbsp;"
    Response.Write "        <input name='ClassType' type='radio' value='2' onclick=""HideTabUrl('1')"">外部栏目"
    Response.Write "        &nbsp;<div id='ClassLink' style='display:none'>外部栏目的链接地址：<input name='LinkUrl' type='text' id='LinkUrl' value='' size='40' maxlength='200'></div>"
    Response.Write "      </td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='300' class='tdbg5'><strong>每页条数：</strong></td>"
    Response.Write "      <td><select name='MaxPerPage'>" & GetNumber_Option(5, 50, 20) & "</select></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='300' class='tdbg5'><strong>栏目图片地址：</strong><br>用于在栏目页显示指定的图片</td>"
    Response.Write "      <td><input name='ClassPicUrl' type='text' id='ClassPicUrl' size='50' maxlength='255'></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='300' class='tdbg5'><strong>栏目模板：</strong></td>"
    Response.Write "      <td><input name='list_template' type='text' id='list_template' maxlength='100' style=""width:300px"" readonly> <input type='button' name='Button2' class='Button' value='选择模板... ' onclick=""SelectTemplate('list_template')""> <font color='#FF0000'>*</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='300' class='tdbg5'><strong>文章模板：</strong></td>"
    Response.Write "      <td><input name='article_template' value='' type='text' id='article_template' maxlength='100' style=""width:300px"" readonly> <input type='button' name='Button2' class='Button' value='选择模板... ' onclick=""SelectTemplate('article_template')""> <font color='#FF0000'>*</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='40%' class='tdbg5'><strong>栏目META关键词：</strong><br>针对搜索引擎设置的关键词<br>多个关键词请用,号分隔</td>" & vbCrLf
    Response.Write "      <td><textarea name='Meta_Keywords' cols='60' rows='4' id='Meta_Keywords'></textarea></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='40%' class='tdbg5'><strong>栏目META网页描述：</strong><br>针对搜索引擎设置的网页描述<br>多个描述请用,号分隔</td>" & vbCrLf
    Response.Write "      <td><textarea name='Meta_Description' cols='60' rows='4' id='Meta_Description'></textarea></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "  </tbody>" & vbCrLf
    Response.Write "</table>" & vbCrLf
    Response.Write "</td></tr></table>" & vbCrLf

    Response.Write "<table width='100%' border='0' align='center'>"
    Response.Write "  <tr class='tdbg'>"
    Response.Write "    <td height='40' colspan='2' align='center'>"
    Response.Write "      <input name='Action' type='hidden' id='Action' value='SaveAdd'>"
    Response.Write "      <input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "      <input name='Add' type='submit' value=' 添 加 ' style='cursor:hand;'>&nbsp;&nbsp;<input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Class.asp?ChannelID=" & ChannelID & "'"" style='cursor:hand;'>"
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "</table>"
    Response.Write "</form>"
End Sub


Sub AddModify_JS()
    Response.Write "<script language='JavaScript' type='text/JavaScript'>" & vbCrLf
    Response.Write "function check(){" & vbCrLf
    Response.Write "  if (document.form1.ClassName.value==''){" & vbCrLf
    Response.Write "   alert('栏目名称不能为空！');" & vbCrLf
    Response.Write "   document.form1.ClassName.focus();" & vbCrLf
    Response.Write "   return false;}" & vbCrLf
    Response.Write "  if(document.form1.ClassType[1].checked==true){" & vbCrLf
    Response.Write "    if(document.form1.LinkUrl.value==''){" & vbCrLf
    Response.Write "      alert('栏目链接地址不能为空！');" & vbCrLf
    Response.Write "      document.form1.LinkUrl.focus();" & vbCrLf
    Response.Write "      return false;}" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "function HideTabUrl(tempType){" & vbCrLf
    Response.Write "  if(tempType=='0'){" & vbCrLf
    Response.Write "      document.getElementById('ClassLink').style.display='none';" & vbCrLf
    Response.Write "  }else if(tempType=='1'){" & vbCrLf
    Response.Write "  	  document.getElementById('ClassLink').style.display='';" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "}" & vbCrLf
	Response.Write "function SelectTemplate(formstr){" & vbCrLf
	Response.Write "  var Templateform = document.getElementById(formstr);" & vbCrLf
	Response.Write "  var arr=showModalDialog('Admin_SelectFile.asp?dialogtype=Template', '', 'dialogWidth:820px; dialogHeight:600px; help: no; scroll: yes; status: no');" & vbCrLf
	Response.Write "  if(arr!=null){" & vbCrLf
	Response.Write "    var ss=arr.split('|');" & vbCrLf
	Response.write "    var regS = new RegExp('"& Template_Dir &"',""gi"");"
	Response.write "    str=ss[0].replace(regS,'{@TemplateDir}'); "
	Response.Write "    Templateform.value=str;" & vbCrLf
	Response.Write "  }" & vbCrLf
	Response.Write "}" & vbCrLf
    Response.Write "</script>" & vbCrLf
End Sub


Sub Modify()
    Call AddModify_JS
    Dim ClassID, sql, rsClass, i
    ClassID = GetUrl("ClassID")
    If ClassID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
        Exit Sub
    Else
        ClassID = PE_CLng(ClassID)
    End If
    sql = "select * from PW_Class where ClassID=" & ClassID
    Set rsClass = Server.CreateObject("Adodb.recordset")
    rsClass.Open sql, Conn, 1, 1
    If rsClass.BOF And rsClass.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的栏目！</li>"
        rsClass.Close
        Set rsClass = Nothing
        Exit Sub
    End If
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_Class.asp?ChannelID=" & ChannelID & "'>栏目管理</a>&nbsp;&gt;&gt;&nbsp;修改栏目设置：<font color='red'>" & rsClass("ClassName") & "</td></tr></table>"
    Response.Write "<form name='form1' method='post' action='Admin_Class.asp' onsubmit='return check()'>" & vbCrLf
    Response.Write "<table width='100%' border='0' align='center' cellpadding='5' cellspacing='1' class='border'><tr class='tdbg'><td height='100' valign='top'>" & vbCrLf
    Response.Write "<table width='95%' align='center' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>" & vbCrLf
    Response.Write "  <tbody id='Tabs' style='display:'>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='300' class='tdbg5'><strong>所属栏目：</strong><br>如果你想改变所属栏目，请<a href='Admin_Class.asp?Action=Move&ChannelID=" & ChannelID & "&ClassID=" & ClassID & "'>点此移动栏目</a></td>"
    Response.Write "      <td>" & GetPath(rsClass("ParentID"), rsClass("ParentPath")) & "</td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='300' class='tdbg5'><strong>栏目名称：</strong></td>"
    Response.Write "      <td><input name='ClassName' type='text' value='" & rsClass("ClassName") & "' size='20' maxlength='20'> <font color=red>*</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='300' class='tdbg5'><strong>栏目类型：</strong><br><font color=red>请慎重选择，栏目一旦添加后就不能再更改栏目类型。</font></td>" & vbCrLf
    Response.Write "      <td>"
    Response.Write "        <input name='ClassType' type='radio' value='1'"
    If rsClass("ClassType") = 1 Then
        Response.Write " checked"
    Else
        Response.Write " disabled"
    End If
    Response.Write ">内部栏目&nbsp;"
    Response.Write "        <input name='ClassType' type='radio' value='2'"
    If rsClass("ClassType") = 2 Then
        Response.Write " checked"
    Else
        Response.Write " disabled"
    End If
    Response.Write ">外部栏目"
	

    Response.Write "        &nbsp;&nbsp;&nbsp;&nbsp;<div id='ClassLink'"
	If rsClass("ClassType") = 1 Then Response.Write "  style='display:none'"
	Response.Write ">外部栏目的链接地址：<input name='LinkUrl' type='text' id='LinkUrl' value='" & rsClass("LinkUrl") & "' size='40' maxlength='200'></div>"
    Response.Write "      </td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='300' class='tdbg5'><strong>每页条数：</strong></td>"
    Response.Write "      <td><select name='MaxPerPage'>" & GetNumber_Option(5, 50, rsClass("MaxPerPage")) & "</select></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='300' class='tdbg5'><strong>栏目图片地址：</strong><br>用于在栏目页显示指定的图片</td>"
    Response.Write "      <td><input name='ClassPicUrl' type='text' id='ClassPicUrl' value='" & rsClass("ClassPicUrl") & "' size='60' maxlength='255'></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='300' class='tdbg5'><strong>栏目模板：</strong></td>"
    Response.Write "      <td><input name='list_template' value='" & rsClass("Templatedir") & "' type='text' id='list_template' maxlength='100' style=""width:300px"" readonly> <input type='button' name='Button2' class='Button' value='选择模板... ' onclick=""SelectTemplate('list_template')""> <font color='#FF0000'>*</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>"
    Response.Write "      <td width='300' class='tdbg5'><strong>文章模板：</strong></td>"
    Response.Write "      <td><input name='article_template' value='" & rsClass("Templatearticle") & "' type='text' id='article_template' maxlength='100' style=""width:300px"" readonly> <input type='button' name='Button2' class='Button' value='选择模板... ' onclick=""SelectTemplate('article_template')""> <font color='#FF0000'>*</font></td>"
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='40%' class='tdbg5'><strong>栏目META关键词：</strong><br>针对搜索引擎设置的关键词<br>多个关键词请用,号分隔</td>" & vbCrLf
    Response.Write "      <td><textarea name='Meta_Keywords' cols='60' rows='4' id='Meta_Keywords'>" & rsClass("Meta_Keywords") & "</textarea></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "      <td width='40%' class='tdbg5'><strong>栏目META网页描述：</strong><br>针对搜索引擎设置的网页描述<br>多个描述请用,号分隔</td>" & vbCrLf
    Response.Write "      <td><textarea name='Meta_Description' cols='60' rows='4' id='Meta_Description'>" & rsClass("Meta_Description") & "</textarea></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "  </tbody>" & vbCrLf
    Response.Write "</table>" & vbCrLf
    Response.Write "</td></tr></table>" & vbCrLf
    Response.Write "<table width='100%' border='0' align='center'>"
    Response.Write "  <tr class='tdbg'>"
    Response.Write "    <td height='40' colspan='2' align='center'>"
    Response.Write "      <input name='Action' type='hidden' id='Action' value='SaveModify'>"
    Response.Write "      <input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "      <input name='ClassID' type='hidden' id='ClassID' value='" & rsClass("ClassID") & "'>"
    Response.Write "      <input name='Modify' type='submit' value=' 保存修改结果 ' style='cursor:hand;'>&nbsp;&nbsp;<input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Class.asp?ChannelID=" & ChannelID & "'"" style='cursor:hand;'>"
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "</table>"
    Response.Write "</form>"
    rsClass.Close
    Set rsClass = Nothing
End Sub


Sub SaveAdd()
	Dim ClassID, ClassName, ClassType, LinkUrl,ClassPicUrl,Meta_Keywords, Meta_Description,list_template,article_template,MaxPerPage
	Dim sql, rs, trs, rsClass
	Dim RootID, ParentDepth, ParentPath, ParentStr, ParentName, MaxClassID, MaxRootID, arrChildID, PrevOrderID
	Dim PrevID, NextID, Child
    ClassName = GetForm("ClassName")
    ClassType = PE_CLng(GetForm("ClassType"))
    LinkUrl = GetForm("LinkUrl")
    ClassPicUrl = GetForm("ClassPicUrl")
	MaxPerPage = PE_CLng(GetForm("MaxPerPage"))
	list_template = GetForm("list_template")
	article_template = GetForm("article_template")
    Meta_Keywords = GetForm("Meta_Keywords")
    Meta_Description = GetForm("Meta_Description")
    If ClassName = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>栏目名称不能为空！</li>"
    Else
        ClassName = ReplaceBadChar(ClassName)
    End If
    If ClassType = 2 Then
        If LinkUrl = "" Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>链接地址不能为空！</li>"
        End If
    End If
    If list_template = "" Then
		list_template = templist
    end if
    If article_template = "" Then
		article_template = temparticle
    end if
    If FoundErr = True Then
        Exit Sub
    End If

    Set trs = Conn.Execute("Select * from PW_Class Where ChannelID=" & ChannelID & " and ParentID=" & ParentID & " AND ClassName='" & ClassName & "'")
    If Not (trs.BOF And trs.EOF) Then
        FoundErr = True
        If ParentID = 0 Then
            ErrMsg = ErrMsg & "<li>已经存在一级栏目：" & ClassName & "</li>"
        Else
            ErrMsg = ErrMsg & "<li>“" & ParentName & "”中已经存在子栏目“" & ClassName & "”！</li>"
        End If
    End If
    trs.Close
    Set trs = Nothing
    If FoundErr = True Then
        Exit Sub
    End If

    Set rs = Conn.Execute("select Max(ClassID) from PW_Class")
    MaxClassID = rs(0)
    If IsNull(MaxClassID) Then
        MaxClassID = 0
    End If
    rs.Close
    Set rs = Nothing
    ClassID = MaxClassID + 1
    
    Set rs = Conn.Execute("select max(rootid) from PW_Class where ChannelID=" & ChannelID & "")
    MaxRootID = rs(0)
    If IsNull(MaxRootID) Then
        MaxRootID = 0
    End If
    rs.Close
    Set rs = Nothing
    RootID = MaxRootID + 1
	
    If ParentID > 0 Then
        Set rs = Conn.Execute("select * from PW_Class where ClassID=" & ParentID & "")
        If rs.BOF And rs.EOF Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>所属栏目已经被删除！</li>"
            rs.Close
            Set rs = Nothing
            Exit Sub
        End If
        If rs("ClassType") = 2 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>不能指定外部栏目为所属栏目！</li>"
            rs.Close
            Set rs = Nothing
            Exit Sub
        End If
        RootID = rs("RootID")
        ParentName = rs("ClassName")
        ParentDepth = rs("Depth")
        ParentPath = rs("ParentPath") & "," & rs("ClassID")   '得到此栏目的父级栏目路径
        Child = rs("Child")
        arrChildID = rs("arrChildID") & "," & ClassID
        '更新本栏目的所有上级栏目的子栏目ID数组
        Set trs = Conn.Execute("select ClassID,arrChildID from PW_Class where ClassID in (" & ParentPath & ")")
        Do While Not trs.EOF
            Conn.Execute ("update PW_Class set arrChildID='" & trs(1) & "," & ClassID & "' where ClassID=" & trs(0))
            trs.MoveNext
        Loop
        trs.Close
        If Child > 0 Then
            Dim rsPrevOrderID
            '得到父栏目的所有子栏目中最后一个栏目的OrderID
            Set rsPrevOrderID = Conn.Execute("select Max(OrderID) from PW_Class where ClassID in ( " & arrChildID & ")")
            PrevOrderID = rsPrevOrderID(0)
            Set rsPrevOrderID = Nothing
            
            '得到本栏目的上一个栏目ID
            Set trs = Conn.Execute("select top 1 ClassID from PW_Class where ChannelID=" & ChannelID & " and ParentID=" & ParentID & " order by OrderID desc")
            PrevID = trs(0)
            trs.Close
        Else
            PrevOrderID = rs("OrderID")
            PrevID = 0
        End If

        rs.Close
        Set rs = Nothing
    Else
        If MaxRootID > 0 Then
            Set trs = Conn.Execute("select ClassID from PW_Class where ChannelID=" & ChannelID & " and RootID=" & MaxRootID & " and Depth=0")
            PrevID = trs(0)
            trs.Close
        Else
            PrevID = 0
        End If
        PrevOrderID = 0
        ParentPath = "0"
    End If
    sql = "Select top 1 * from PW_Class where ChannelID=" & ChannelID & " order by ClassID desc"
    Set rsClass = Server.CreateObject("adodb.recordset")
    rsClass.Open sql, Conn, 1, 3
    rsClass.addnew
    rsClass("ChannelID") = ChannelID
    rsClass("ClassID") = ClassID
    rsClass("RootID") = RootID
    rsClass("ParentID") = ParentID
    If ParentID > 0 Then
        rsClass("Depth") = ParentDepth + 1
    Else
        rsClass("Depth") = 0
    End If
    rsClass("ParentPath") = ParentPath
    rsClass("OrderID") = PrevOrderID
    rsClass("Child") = 0
    rsClass("PrevID") = PrevID
    rsClass("NextID") = 0
    rsClass("arrChildID") = ClassID
    rsClass("ClassName") = ClassName
    rsClass("ClassType") = ClassType
    If ClassType =2 Then
        rsClass("LinkUrl") = LinkUrl
    Else
        rsClass("LinkUrl") = ""
    End If
    rsClass("ClassPicUrl") = ClassPicUrl
    rsClass("MaxPerPage") = MaxPerPage
    rsClass("Templatedir") = list_template
    rsClass("Templatearticle") = article_template
    rsClass("Meta_Keywords") = Meta_Keywords
    rsClass("Meta_Description") = Meta_Description
    rsClass.Update
    rsClass.Close
    Set rsClass = Nothing
    '更新与本栏目同一父栏目的上一个栏目的“NextID”字段值
    If PrevID > 0 Then
        Conn.Execute ("update PW_Class set NextID=" & ClassID & " where ClassID=" & PrevID)
    End If
    
    If ParentID > 0 Then
        '更新其父类的子栏目数
        Conn.Execute ("update PW_Class set child=child+1 where ClassID=" & ParentID)
        
        '更新该栏目排序以及大于本需要和同在本分类下的栏目排序序号
        Conn.Execute ("update PW_Class set OrderID=OrderID+1 where ChannelID=" & ChannelID & " and RootID=" & RootID & " and OrderID>" & PrevOrderID)
        Conn.Execute ("update PW_Class set OrderID=" & PrevOrderID & "+1 where ClassID=" & ClassID)
    End If
    Call CloseConn
    Response.Redirect "Admin_Class.asp?ChannelID=" & ChannelID
End Sub


Sub SaveModify()
	Dim ClassID, ClassName, ClassType, LinkUrl, ClassPicUrl, Meta_Keywords, Meta_Description,list_template,article_template,MaxPerPage
	Dim sql, rsClass, i, trs
    ClassID = GetForm("ClassID")
    If ClassID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
    Else
        ClassID = PE_CLng(ClassID)
    End If
	
    ClassName = GetForm("ClassName")
    ClassType = PE_CLng(GetForm("ClassType"))
    LinkUrl = GetForm("LinkUrl")
    ClassPicUrl = GetForm("ClassPicUrl")
	MaxPerPage = PE_CLng(GetForm("MaxPerPage"))
	list_template = GetForm("list_template")
	article_template = GetForm("article_template")
    Meta_Keywords = GetForm("Meta_Keywords")
    Meta_Description = GetForm("Meta_Description")
	
    If ClassName = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>栏目名称不能为空！</li>"
    Else
        ClassName = ReplaceBadChar(ClassName)
    End If
	
    If ClassType = 2 Then
        If LinkUrl = "" Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>链接地址不能为空！</li>"
        End If
    End If
    If list_template = "" Then
		list_template = templist
    end if
    If article_template = "" Then
		article_template = temparticle
    end if
    If FoundErr = True Then
        Exit Sub
    End If
    
    sql = "select * from PW_Class where ClassID=" & ClassID
    Set rsClass = Server.CreateObject("Adodb.recordset")
    rsClass.Open sql, Conn, 1, 3
    If rsClass.BOF And rsClass.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的栏目！</li>"
        rsClass.Close
        Set rsClass = Nothing
        Exit Sub
    End If

    rsClass("ClassName") = ClassName
    rsClass("LinkUrl") = LinkUrl
    rsClass("ClassPicUrl") = ClassPicUrl
	rsClass("MaxPerPage") = MaxPerPage
    rsClass("Templatedir") = list_template
	rsClass("Templatearticle") = article_template
    rsClass("Meta_Keywords") = Meta_Keywords
    rsClass("Meta_Description") = Meta_Description
    rsClass.Update
    rsClass.Close
    Set rsClass = Nothing
    If UseCreateHTML > 0 Then
        Call WriteSuccessMsg("修改栏目属性成功！记得重新生成相关文件哦！", ComeUrl)
    Else
        Call CloseConn
        Response.Redirect "Admin_Class.asp?ChannelID=" & ChannelID
    End If
End Sub
	
	
	
	
Sub DeleteClass()
    Dim sql, rsClass, trs, PrevID, NextID, ClassID, arrChildID, RootID, OrderID
    ClassID = Trim(Request("ClassID"))
    If ClassID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
        Exit Sub
    Else
        ClassID = PE_CLng(ClassID)
    End If
    sql = "select ClassID,RootID,Depth,ParentID,arrChildID,Child,PrevID,NextID,OrderID,ClassType,ParentPath from PW_Class where ClassID=" & ClassID
    Set rsClass = Conn.Execute(sql)
    If rsClass.BOF And rsClass.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>栏目不存在，或者已经被删除</li>"
        rsClass.Close
        Set rsClass = Nothing
        Exit Sub
    End If
    PrevID = rsClass("PrevID")
    NextID = rsClass("NextID")
    arrChildID = rsClass("arrChildID")
    RootID = rsClass("RootID")
    OrderID = rsClass("OrderID")
    If rsClass("Depth") > 0 Then
        Conn.Execute ("update PW_Class set child=child-1 where ClassID=" & rsClass("ParentID"))

        '更新此栏目的原来所有上级栏目的子栏目ID数组
        Set trs = Conn.Execute("select ClassID,arrChildID from PW_Class where ClassID in (" & rsClass("ParentPath") & ")")
        Do While Not trs.EOF
            Conn.Execute ("update PW_Class set arrChildID='" & RemoveClassID(trs(1), arrChildID) & "' where ClassID=" & trs(0))
            trs.MoveNext
        Loop
        trs.Close
        
        '更新与此栏目同根且排序在其之下的栏目
        Conn.Execute ("update PW_Class set OrderID=OrderID-" & UBound(Split(arrChildID, ",")) + 1 & " where ChannelID=" & ChannelID & " and RootID=" & RootID & " and OrderID>" & OrderID)

    End If
    
    '修改上一栏目的NextID和下一栏目的PrevID
    If PrevID > 0 Then
        Conn.Execute "update PW_Class set NextID=" & NextID & " where ClassID=" & PrevID
    End If
    If NextID > 0 Then
        Conn.Execute "update PW_Class set PrevID=" & PrevID & " where ClassID=" & NextID
    End If
    rsClass.Close
    Set rsClass = Nothing
	
    '删除本栏目（包括子栏目）
    Conn.Execute ("delete from PW_Class where ChannelID=" & ChannelID & " and ClassID in (" & arrChildID & ")")
    Conn.Execute ("delete from PW_Article where ChannelID=" & ChannelID & " and ClassID in (" & arrChildID & ")")
    Conn.Execute ("delete from PW_Soft where ChannelID=" & ChannelID & " and ClassID in (" & arrChildID & ")")
    Conn.Execute ("delete from PW_Product where ChannelID=" & ChannelID & " and ClassID in (" & arrChildID & ")")
    Conn.Execute ("delete from PW_Photo where ChannelID=" & ChannelID & " and ClassID in (" & arrChildID & ")")
	Call CloseConn
	Response.Redirect "Admin_Class.asp?ChannelID=" & ChannelID
	
End Sub
	
	
	
Sub MoveClass()
    Dim ClassID, sql, rsClass, i
    ClassID = GetValue("ClassID")
    If ClassID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
        Exit Sub
    Else
        ClassID = PE_CLng(ClassID)
    End If
    sql = "select * from PW_Class where ClassID=" & ClassID
    Set rsClass = Server.CreateObject("Adodb.recordset")
    rsClass.Open sql, Conn, 1, 3
    If rsClass.BOF And rsClass.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的栏目！</li>"
    Else
        Response.Write "<form name='myform' method='post' action='Admin_Class.asp'>"
        Response.Write "  <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
        Response.Write "    <tr class='title'> "
        Response.Write "      <td height='22' colspan='3' align='center'><strong>移动" & ChannelShortName & "栏目</strong></td>"
        Response.Write "    </tr>"
        Response.Write "    <tr class='tdbg'>"
        Response.Write "      <td align='left' valign='top' width='260'><strong>当前栏目：</strong><br><select name='ClassID2' size='2' style='height:330px;width:260px;' disabled>" & GetClass_Opt(ChannelID, ClassID) & "</select></td>"
        Response.Write "      <td align='center' width='70'><strong>移动到&gt;&gt;&gt;</strong></td>"
        Response.Write "      <td align='left'>"
        Response.Write "        <strong>目标栏目：</strong><font color=red>（不能指定为当前栏目的下属子栏目或外部栏目）</font><br><select name='ParentID' size='2' style='height:300px;width:260px;'><option value='0'>无（做为一级栏目）</option>" & GetClass_Opt(ChannelID, rsClass("ParentID")) & "</select>"
        Response.Write "      </td>"
        Response.Write "    </tr>"
        Response.Write "    <tr class='tdbg'>"
        Response.Write "      <td height='40' colspan='3' align='center'>"
        Response.Write "        <input name='Action' type='hidden' id='Action' value='Move'>"
        Response.Write "        <input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
        Response.Write "        <input name='ClassID' type='hidden' id='ClassID' value='" & ClassID & "'>"
        Response.Write "        <input name='Submit' type='submit' value=' 保存移动结果 ' style='cursor:hand;' onClick=""document.myform.Action.value='SaveMove';"">&nbsp;&nbsp;"
        Response.Write "        <input name='Cancel' type='button' value=' 取 消 ' style='cursor:hand;' onClick=""window.location.href='Admin_Class.asp?ChannelID=" & ChannelID & "'"">"
        Response.Write "      </td>"
        Response.Write "    </tr>"
        Response.Write "  </table>"
        Response.Write "</form>"
    End If
    rsClass.Close
    Set rsClass = Nothing
End Sub


Sub SaveMove()
    Dim ClassID, sql, rsClass, i, rsPrevOrderID
    Dim rParentID
    Dim trs, rs, ClassCount
	Dim ParentID, RootID, Depth, Child, ParentPath, ParentName, iParentPath, PrevOrderID, PrevID, NextID
	Dim ClassName, ClassType, arrChildID
    ClassID = GetForm("ClassID")
    If ClassID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
        Exit Sub
    Else
        ClassID = PE_CLng(ClassID)
    End If
    sql = "select * from PW_Class where ClassID=" & ClassID
    Set rsClass = Server.CreateObject("Adodb.recordset")
    rsClass.Open sql, Conn, 1, 3
    If rsClass.BOF And rsClass.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的栏目！</li>"
    Else
        Depth = rsClass("Depth")
        Child = rsClass("Child")
        RootID = rsClass("RootID")
        ParentID = rsClass("ParentID")
        ParentPath = rsClass("ParentPath")
        PrevID = rsClass("PrevID")
        NextID = rsClass("NextID")
        ClassName = rsClass("ClassName")
        arrChildID = rsClass("arrChildID")
        ClassType = rsClass("ClassType")
    End If
    rsClass.Close
    Set rsClass = Nothing
    
    rParentID = PE_CLng(GetForm("ParentID"))
	If rParentID = ClassID Then
		FoundErr = True
		ErrMsg = ErrMsg & "<li>所属栏目不能为自己！</li>"
	Else
		If rParentID = ParentID Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>目标栏目与当前父栏目相同，无需移动！</li>"
		End If
	End If

    If FoundErr = True Then Exit Sub
	
    If rParentID > 0 Then
        Set trs = Conn.Execute("select ClassID from PW_Class where ChannelID=" & ChannelID & " and ClassType=1 and ClassID=" & rParentID)
        If trs.BOF And trs.EOF Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>不能指定外部栏目为所属栏目</li>"
        End If
        trs.Close
        Set trs = Nothing
        If FoundInArr(arrChildID, rParentID, ",") = True Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>不能指定该栏目的下属栏目作为所属栏目</li>"
        End If
    End If
    '检查目标栏目的子栏目中是否已经存在与此栏目名称相同的栏目
    Set trs = Conn.Execute("select ClassID from PW_Class where ChannelID=" & ChannelID & " and ParentID=" & rParentID & " and ClassName='" & ClassName & "'")
    If Not (trs.BOF And trs.EOF) Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>目标栏目的子栏目中已经存在与此栏目名称相同的栏目。"
    End If
    Set trs = Nothing

    If FoundErr = True Then
        Exit Sub
    End If

    ClassCount = UBound(Split(arrChildID, ",")) + 1    '得到要移动的栏目数

    '需要更新其原来所属栏目信息，包括深度、父级ID、栏目数、排序等数据
    '需要更新当前所属栏目信息
    Dim mrs, MaxRootID
    Set mrs = Conn.Execute("select max(rootid) from PW_Class where ChannelID=" & ChannelID & "")
    MaxRootID = mrs(0)
    Set mrs = Nothing
    If IsNull(MaxRootID) Then
        MaxRootID = 0
    End If
    '更新原来同一父栏目的上一个栏目的NextID和下一个栏目的PrevID
    If PrevID > 0 Then
        Conn.Execute "update PW_Class set NextID=" & NextID & " where ClassID=" & PrevID
    End If
    If NextID > 0 Then
        Conn.Execute "update PW_Class set PrevID=" & PrevID & " where ClassID=" & NextID
    End If
	
    If ParentID > 0 And rParentID = 0 Then  '如果原来不是一级分类改成一级分类

        '更新其原来所属栏目的栏目数，排序相当于剪枝而不需考虑
        Conn.Execute ("update PW_Class set child=child-1 where ClassID=" & ParentID)

        '更新此栏目的原来所有上级栏目的子栏目ID数组
        Set trs = Conn.Execute("select ClassID,arrChildID from PW_Class where ClassID in (" & ParentPath & ")")
        Do While Not trs.EOF
            Conn.Execute ("update PW_Class set arrChildID='" & RemoveClassID(trs(1), arrChildID) & "' where ClassID=" & trs(0))
            trs.MoveNext
        Loop
        trs.Close

        '得到上一个一级分类栏目
        sql = "select ClassID,NextID from PW_Class where ChannelID=" & ChannelID & " and RootID=" & MaxRootID & " and Depth=0"
        Set rs = Server.CreateObject("Adodb.recordset")
        rs.Open sql, Conn, 1, 3
        If rs.BOF And rs.EOF Then
            PrevID = 0
        Else
            PrevID = rs(0)    '得到新的PrevID
            rs(1) = ClassID   '更新上一个一级分类栏目的NextID的值
            rs.Update
        End If
        rs.Close
        Set rs = Nothing

        MaxRootID = MaxRootID + 1

        '更新当前栏目数据
        Conn.Execute ("update PW_Class set ChannelID=" & ChannelID & ",depth=0,OrderID=0,rootid=" & MaxRootID & ",parentid=0,ParentPath='0',PrevID=" & PrevID & ",NextID=0 where ClassID=" & ClassID)

        '如果有下属栏目，则更新其下属栏目数据。下属栏目的排序不需考虑，只需更新下属栏目深度和一级排序ID(rootid)数据
        If Child > 0 Then
            ParentPath = ParentPath & ","
            arrChildID = RemoveClassID(arrChildID, ClassID) '从子栏目数组中去掉当前栏目的ID
            Set rs = Conn.Execute("select * from PW_Class where ClassID in (" & arrChildID & ")")
            Do While Not rs.EOF
                iParentPath = Replace(rs("ParentPath"), ParentPath, "")
                Conn.Execute ("update PW_Class set ChannelID=" & ChannelID & ",depth=depth-" & Depth & ",rootid=" & MaxRootID & ",ParentPath='0," & iParentPath & "' where ClassID=" & rs("ClassID"))
                rs.MoveNext
            Loop
            rs.Close
            Set rs = Nothing
        End If
    ElseIf ParentID > 0 And rParentID > 0 Then '如果是将一个分栏目移动到其他分栏目下

        '更新其原父类的子栏目数
        Conn.Execute ("update PW_Class set child=child-1 where ClassID=" & ParentID)

        '更新此栏目的原来所有上级栏目的子栏目ID数组
        Set trs = Conn.Execute("select ClassID,arrChildID from PW_Class where ClassID in (" & ParentPath & ")")
        Do While Not trs.EOF
            Conn.Execute ("update PW_Class set arrChildID='" & RemoveClassID(trs(1), arrChildID) & "' where ClassID=" & trs(0))
            trs.MoveNext
        Loop
        trs.Close

        '获得目标栏目的相关信息
        Set trs = Conn.Execute("select * from PW_Class where ClassID=" & rParentID)

        If trs("Child") > 0 Then
            '得到在目标栏目中与本栏目同级的最后一个栏目的ClassID，并更新其NextID的指向
            Set rs = Conn.Execute("select ClassID from PW_Class where ParentID=" & trs("ClassID") & " order by OrderID desc")
            PrevID = rs(0)  '得到新的PrevID
            Conn.Execute ("update PW_Class set NextID=" & ClassID & " where ClassID=" & rs(0) & "")
            Set rs = Nothing

            '得到目标栏目的子栏目的最大OrderID
            Set rsPrevOrderID = Conn.Execute("select Max(OrderID) from PW_Class where ClassID in (" & trs("arrChildID") & ")")
            PrevOrderID = rsPrevOrderID(0)
            Set rsPrevOrderID = Nothing
        Else
            PrevID = 0
            PrevOrderID = trs("OrderID")
        End If

        '更新目标栏目的子栏目数
        Conn.Execute ("update PW_Class set child=child+1 where ClassID=" & rParentID)

        '更新目标栏目及目标栏目的所有上级栏目的子栏目ID数组
        Set rs = Conn.Execute("select ClassID,arrChildID from PW_Class where ClassID in (" & trs("ParentPath") & "," & trs("ClassID") & ")")
        Do While Not rs.EOF
            Conn.Execute ("update PW_Class set arrChildID='" & rs(1) & "," & arrChildID & "' where ClassID=" & rs(0))
            rs.MoveNext
        Loop
        rs.Close


        '在获得移动过来的栏目数后更新排序在指定栏目之后的栏目排序数据
        Conn.Execute ("update PW_Class set OrderID=OrderID+" & ClassCount & "+1 where ChannelID=" & ChannelID & " and rootid=" & trs("rootid") & " and OrderID>" & PrevOrderID)
        
        '更新当前栏目数据
        Conn.Execute ("update PW_Class set ChannelID=" & ChannelID & ",depth=" & trs("depth") & "+1,OrderID=" & PrevOrderID & "+1,rootid=" & trs("rootid") & ",ParentID=" & rParentID & ",ParentPath='" & trs("ParentPath") & "," & trs("ClassID") & "',PrevID=" & PrevID & ",NextID=0 where ClassID=" & ClassID)

        '如果当前栏目有子栏目则更新子栏目数据，深度为原来的相对深度加上当前所属栏目的深度
        If Child > 0 Then
            i = 1
            arrChildID = RemoveClassID(arrChildID, ClassID) '从子栏目数组中去掉当前栏目的ID
            ParentPath = ParentPath & ","
            Set rs = Conn.Execute("select * from PW_Class where ClassID in (" & arrChildID & ") order by OrderID")
            Do While Not rs.EOF
                i = i + 1
                iParentPath = trs("ParentPath") & "," & trs("ClassID") & "," & Replace(rs("ParentPath"), ParentPath, "")
                Conn.Execute ("update PW_Class set ChannelID=" & ChannelID & ",depth=depth-" & Depth & "+" & trs("depth") & "+1,OrderID=" & PrevOrderID & "+" & i & ",rootid=" & trs("rootid") & ",ParentPath='" & iParentPath & "' where ClassID=" & rs("ClassID"))
                rs.MoveNext
            Loop
            rs.Close
        End If
        Set rs = Nothing
        trs.Close
        Set trs = Nothing
    Else    '如果原来是一级栏目改成其他栏目的下属栏目
        '获得目标栏目的相关信息
        Set trs = Conn.Execute("select * from PW_Class where ClassID=" & rParentID)

        If trs("Child") > 0 Then
            '得到在目标栏目中与本栏目同级的最后一个栏目的ClassID，并更新其NextID的指向
            Set rs = Conn.Execute("select ClassID from PW_Class where ParentID=" & trs("ClassID") & " order by OrderID desc")
            PrevID = rs(0)  '得到新的PrevID
            Conn.Execute ("update PW_Class set NextID=" & ClassID & " where ClassID=" & rs(0) & "")
            Set rs = Nothing

            '得到目标栏目的子栏目的最大OrderID
            Set rsPrevOrderID = Conn.Execute("select Max(OrderID) from PW_Class where ClassID in (" & trs("arrChildID") & ")")
            PrevOrderID = rsPrevOrderID(0)
            Set rsPrevOrderID = Nothing
        Else
            PrevID = 0
            PrevOrderID = trs("OrderID")
        End If

        '更新目标栏目的子栏目数
        Conn.Execute ("update PW_Class set child=child+1 where ClassID=" & rParentID)

        '更新目标栏目及目标栏目的所有上级栏目的子栏目ID数组
        Set rs = Conn.Execute("select ClassID,arrChildID from PW_Class where ClassID in (" & trs("ParentPath") & "," & trs("ClassID") & ")")
        Do While Not rs.EOF
            Conn.Execute ("update PW_Class set arrChildID='" & rs(1) & "," & arrChildID & "' where ClassID=" & rs(0))
            rs.MoveNext
        Loop
        rs.Close
    
        '在获得移动过来的栏目数后更新排序在指定栏目之后的栏目排序数据
        Conn.Execute ("update PW_Class set OrderID=OrderID+" & ClassCount & "+1 where ChannelID=" & ChannelID & " and rootid=" & trs("rootid") & " and OrderID>" & PrevOrderID)
        
        '更新当前栏目数据
        Conn.Execute ("update PW_Class set ChannelID=" & ChannelID & ",depth=depth+" & trs("depth") & "+1,OrderID=" & PrevOrderID + 1 & ",rootid=" & trs("rootid") & ",ParentPath='" & trs("ParentPath") & "," & trs("ClassID") & "',parentid=" & rParentID & ", PrevID=" & PrevID & ",NextID=0 where ClassID=" & ClassID & "")

        '如果当前栏目有子栏目则更新子栏目数据，深度为原来的相对深度加上当前所属栏目的深度
        Set rs = Conn.Execute("select * from PW_Class where ChannelID=" & ChannelID & " and rootid=" & RootID & " and ParentID>0 order by OrderID")
        i = 1
        Do While Not rs.EOF
            i = i + 1
            iParentPath = trs("ParentPath") & "," & trs("ClassID") & "," & Replace(rs("ParentPath"), "0,", "")
            Conn.Execute ("update PW_Class set ChannelID=" & ChannelID & ",depth=depth+" & trs("depth") & "+1,OrderID=" & PrevOrderID & "+" & i & ",rootid=" & trs("rootid") & ",ParentPath='" & iParentPath & "' where ClassID=" & rs("ClassID"))
            rs.MoveNext
        Loop
        rs.Close
        Set rs = Nothing
        trs.Close
        Set trs = Nothing
    End If
	Call CloseConn
	Response.Redirect "Admin_Class.asp?ChannelID=" & ChannelID
End Sub
        

Sub order()
    Dim sqlClass, rsClass, i, iCount, j
    sqlClass = "select * from PW_Class where ChannelID=" & ChannelID & " and ParentID=0 order by RootID"
    Set rsClass = Server.CreateObject("adodb.recordset")
    rsClass.Open sqlClass, Conn, 1, 1
    iCount = rsClass.RecordCount

    Response.Write "<br>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='0' cellspacing='1' class='border'>"
    Response.Write "  <tr class='title'>"
    Response.Write "    <td height='22' colspan='4' align='center'><strong>一 级 栏 目 排 序</strong></td>"
    Response.Write "  </tr>"
    j = 1
    Do While Not rsClass.EOF

        Response.Write "    <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"
        Response.Write "      <td width='200'>" & rsClass("ClassName") & "</td>"
     
        If j > 1 Then
            Response.Write "<form action='Admin_Class.asp?Action=UpOrder' method='post'><td width='150'>"
            Response.Write "<select name=MoveNum size=1><option value=0>向上移动</option>"
            For i = 1 To j - 1
                Response.Write "<option value=" & i & ">" & i & "</option>"
            Next
            Response.Write "</select>"
            Response.Write "<input type=hidden name=ClassID value=" & rsClass("ClassID") & "><input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
            Response.Write "<input type=hidden name=cRootID value=" & rsClass("RootID") & ">&nbsp;<input type=submit name=Submit value=修改>"
            Response.Write "</td></form>"
        Else
            Response.Write "<td width='150'>&nbsp;</td>"
        End If
        If iCount > j Then
            Response.Write "<form action='Admin_Class.asp?Action=DownOrder' method='post'><td width='150'>"
            Response.Write "<select name=MoveNum size=1><option value=0>向下移动</option>"
            For i = 1 To iCount - j
                Response.Write "<option value=" & i & ">" & i & "</option>"
            Next
            Response.Write "</select>"
            Response.Write "<input type=hidden name=ClassID value=" & rsClass("ClassID") & "><input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
            Response.Write "<input type=hidden name=cRootID value=" & rsClass("RootID") & ">&nbsp;<input type=submit name=Submit value=修改>"
            Response.Write "</td></form>"
        Else
            Response.Write "<td width='150'>&nbsp;</td>"
        End If
        Response.Write "      <td>&nbsp;</td>"
        Response.Write " </tr>"
        j = j + 1
        rsClass.MoveNext
    Loop
    Response.Write "</table>"
    rsClass.Close
    Set rsClass = Nothing
End Sub
	
Sub UpOrder()
    Dim ClassID, sqlOrder, rsOrder, MoveNum, cRootID, i, rs, PrevID, NextID
    ClassID = GetForm("ClassID")
    cRootID = GetForm("cRootID")
    MoveNum = GetForm("MoveNum")
    If ClassID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
    Else
        ClassID = PE_CLng(ClassID)
    End If
    If cRootID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>错误参数！</li>"
    Else
        cRootID = PE_CLng(cRootID)
    End If
    If MoveNum = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>错误参数！</li>"
    Else
        MoveNum = PE_CLng(MoveNum)
        If MoveNum = 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请选择要提升的数字！</li>"
        End If
    End If
    If FoundErr = True Then
        Exit Sub
    End If

    Dim mrs, MaxRootID, tRootID, tClassID, tOrderID, tPrevID
    
    '得到本栏目的PrevID,NextID
    Set rs = Conn.Execute("select PrevID,NextID from PW_Class where ClassID=" & ClassID)
    PrevID = rs(0)
    NextID = rs(1)
    rs.Close
    Set rs = Nothing
    '先修改上一栏目的NextID和下一栏目的PrevID
    If PrevID > 0 Then
        Conn.Execute "update PW_Class set NextID=" & NextID & " where ClassID=" & PrevID
    End If
    If NextID > 0 Then
        Conn.Execute "update PW_Class set PrevID=" & PrevID & " where ClassID=" & NextID
    End If

    '得到本频道最大RootID值
    Set mrs = Conn.Execute("select max(rootid) from PW_Class where ChannelID=" & ChannelID & "")
    MaxRootID = mrs(0) + 1
    '先将当前栏目移至最后，包括子栏目
    Conn.Execute ("update PW_Class set RootID=" & MaxRootID & " where ChannelID=" & ChannelID & " and RootID=" & cRootID)
    
    '然后将位于当前栏目以上的栏目的RootID依次加一，范围为要提升的数字
    sqlOrder = "select * from PW_Class where ChannelID=" & ChannelID & " and ParentID=0 and RootID<" & cRootID & " order by RootID desc"
    Set rsOrder = Server.CreateObject("adodb.recordset")
    rsOrder.Open sqlOrder, Conn, 1, 3
    If rsOrder.BOF And rsOrder.EOF Then
        Exit Sub        '如果当前栏目已经在最上面，则无需移动
    End If
    i = 1
    Do While Not rsOrder.EOF
        tRootID = rsOrder("RootID")     '得到要提升位置的RootID，包括子栏目
        Conn.Execute ("update PW_Class set RootID=RootID+1 where ChannelID=" & ChannelID & " and RootID=" & tRootID)
        i = i + 1
        If i > MoveNum Then
            tClassID = rsOrder("ClassID")
            tPrevID = rsOrder("PrevID")
            Exit Do
        End If
        rsOrder.MoveNext
    Loop
    rsOrder.Close
    Set rsOrder = Nothing
        
    '更新移动后本栏目的的PrevID和NextID，以及上一栏目的NextID和下一栏目的PrevID
    Conn.Execute ("update PW_Class set PrevID=" & tPrevID & " where ClassID=" & ClassID)
    Conn.Execute ("update PW_Class set NextID=" & tClassID & " where ClassID=" & ClassID)
    Conn.Execute ("update PW_Class set PrevID=" & ClassID & " where ClassID=" & tClassID)
    If tPrevID > 0 Then
        Conn.Execute ("update PW_Class set NextID=" & ClassID & " where ClassID=" & tPrevID)
    End If
    
    '然后再将当前栏目从最后移到相应位置，包括子栏目
    Conn.Execute ("update PW_Class set RootID=" & tRootID & " where ChannelID=" & ChannelID & " and RootID=" & MaxRootID)
	Call CloseConn
	Response.Redirect "Admin_Class.asp?ChannelID=" & ChannelID & "&Action=Order"
End Sub

Sub DownOrder()
    Dim ClassID, sqlOrder, rsOrder, MoveNum, cRootID, i, rs, PrevID, NextID
    ClassID = GetForm("ClassID")
    cRootID = GetForm("cRootID")
    MoveNum = GetForm("MoveNum")
    If ClassID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
    Else
        ClassID = PE_CLng(ClassID)
    End If
    If cRootID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>错误参数！</li>"
    Else
        cRootID = PE_CLng(cRootID)
    End If
    If MoveNum = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>错误参数！</li>"
    Else
        MoveNum = PE_CLng(MoveNum)
        If MoveNum = 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请选择要提升的数字！</li>"
        End If
    End If
    If FoundErr = True Then
        Exit Sub
    End If

    Dim mrs, MaxRootID, tRootID, tClassID, tOrderID, tNextID
    
    '得到本栏目的PrevID,NextID
    Set rs = Conn.Execute("select PrevID,NextID from PW_Class where ClassID=" & ClassID)
    PrevID = rs(0)
    NextID = rs(1)
    rs.Close
    Set rs = Nothing
    '先修改上一栏目的NextID和下一栏目的PrevID
    If PrevID > 0 Then
        Conn.Execute "update PW_Class set NextID=" & NextID & " where ClassID=" & PrevID
    End If
    If NextID > 0 Then
        Conn.Execute "update PW_Class set PrevID=" & PrevID & " where ClassID=" & NextID
    End If

    '得到本频道最大RootID值
    Set mrs = Conn.Execute("select max(rootid) from PW_Class where ChannelID=" & ChannelID & "")
    MaxRootID = mrs(0) + 1
    '先将当前栏目移至最后，包括子栏目
    Conn.Execute ("update PW_Class set RootID=" & MaxRootID & " where ChannelID=" & ChannelID & " and RootID=" & cRootID)
    
    '然后将位于当前栏目以下的栏目的RootID依次减一，范围为要下降的数字
    sqlOrder = "select * from PW_Class where ChannelID=" & ChannelID & " and ParentID=0 and RootID>" & cRootID & " order by RootID"
    Set rsOrder = Server.CreateObject("adodb.recordset")
    rsOrder.Open sqlOrder, Conn, 1, 3
    If rsOrder.BOF And rsOrder.EOF Then
        Exit Sub        '如果当前栏目已经在最下面，则无需移动
    End If
    i = 1
    Do While Not rsOrder.EOF
        tRootID = rsOrder("RootID")     '得到要提升位置的RootID，包括子栏目
        Conn.Execute ("update PW_Class set RootID=RootID-1 where ChannelID=" & ChannelID & " and RootID=" & tRootID)
        i = i + 1
        If i > MoveNum Then
            tClassID = rsOrder("ClassID")
            tNextID = rsOrder("NextID")
            Exit Do
        End If
        rsOrder.MoveNext
    Loop
    rsOrder.Close
    Set rsOrder = Nothing
    
    '更新移动后本栏目的的PrevID和NextID，以及上一栏目的NextID和下一栏目的PrevID
    Conn.Execute ("update PW_Class set PrevID=" & tClassID & " where ClassID=" & ClassID)
    Conn.Execute ("update PW_Class set NextID=" & tNextID & " where ClassID=" & ClassID)
    Conn.Execute ("update PW_Class set NextID=" & ClassID & " where ClassID=" & tClassID)
    If tNextID > 0 Then
        Conn.Execute ("update PW_Class set PrevID=" & ClassID & " where ClassID=" & tNextID)
    End If
    
    '然后再将当前栏目从最后移到相应位置，包括子栏目
    Conn.Execute ("update PW_Class set RootID=" & tRootID & " where ChannelID=" & ChannelID & " and RootID=" & MaxRootID)
	Call CloseConn
	Response.Redirect "Admin_Class.asp?ChannelID=" & ChannelID & "&Action=Order"
End Sub
	
Sub OrderN()
    Dim sqlClass, rsClass, i, iCount, trs, UpMoveNum, DownMoveNum
    sqlClass = "select * from PW_Class where ChannelID=" & ChannelID & " order by RootID,OrderID"
    Set rsClass = Server.CreateObject("adodb.recordset")
    rsClass.Open sqlClass, Conn, 1, 1
    
    Response.Write "<br>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='0' cellspacing='1' class='border'>"
    Response.Write "  <tr class='title'>"
    Response.Write "    <td height='22' colspan='4' align='center'><strong>N 级 栏 目 排 序</strong></td>"
    Response.Write "  </tr>"

    Do While Not rsClass.EOF
        Response.Write "    <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"
        Response.Write "      <td width='300'>"
        For i = 1 To rsClass("Depth")
            Response.Write "&nbsp;&nbsp;&nbsp;"
        Next
        If rsClass("Child") > 0 Then
            Response.Write "<img src='../images/tree_folder4.gif' width='15' height='15' valign='abvmiddle'>"
        Else
            Response.Write "<img src='../images/tree_folder3.gif' width='15' height='15' valign='abvmiddle'>"
        End If
        If rsClass("ParentID") = 0 Then
            Response.Write "<b>"
        End If
        Response.Write rsClass("ClassName")
        If rsClass("Child") > 0 Then
            Response.Write "(" & rsClass("Child") & ")"
        End If
        Response.Write "</td>"
        If rsClass("ParentID") > 0 Then '如果不是一级栏目，则算出相同深度的栏目数目，得到该栏目在相同深度的栏目中所处位置（之上或者之下的栏目数）
            '所能提升最大幅度应为For i=1 to 该版之上的版面数
            Set trs = Conn.Execute("select count(ClassID) from PW_Class where ParentID=" & rsClass("ParentID") & " and OrderID<" & rsClass("OrderID") & "")
            UpMoveNum = trs(0)
            If IsNull(UpMoveNum) Then UpMoveNum = 0
            If UpMoveNum > 0 Then
                Response.Write "<form action='Admin_Class.asp?Action=UpOrderN' method='post'><td width='150'>"
                Response.Write "<select name=MoveNum size=1><option value=0>向上移动</option>"
                For i = 1 To UpMoveNum
                    Response.Write "<option value=" & i & ">" & i & "</option>"
                Next
                Response.Write "</select><input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
                Response.Write "<input type=hidden name=ClassID value=" & rsClass("ClassID") & ">&nbsp;<input type=submit name=Submit value=修改>"
                Response.Write "</td></form>"
            Else
                Response.Write "<td width='150'>&nbsp;</td>"
            End If
            trs.Close
            '所能降低最大幅度应为For i=1 to 该版之下的版面数
            Set trs = Conn.Execute("select count(ClassID) from PW_Class where ParentID=" & rsClass("ParentID") & " and orderID>" & rsClass("orderID") & "")
            DownMoveNum = trs(0)
            If IsNull(DownMoveNum) Then DownMoveNum = 0
            If DownMoveNum > 0 Then
                Response.Write "<form action='Admin_Class.asp?Action=DownOrderN' method='post'><td width='150'>"
                Response.Write "<select name=MoveNum size=1><option value=0>向下移动</option>"
                For i = 1 To DownMoveNum
                    Response.Write "<option value=" & i & ">" & i & "</option>"
                Next
                Response.Write "</select><input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
                Response.Write "<input type=hidden name=ClassID value=" & rsClass("ClassID") & ">&nbsp;<input type=submit name=Submit value=修改>"
                Response.Write "</td></form>"
            Else
                Response.Write "<td width='150'>&nbsp;</td>"
            End If
            trs.Close
        Else
            Response.Write "<td colspan=2>&nbsp;</td>"
        End If
        Response.Write "      <td>&nbsp;</td>"
        Response.Write " </tr>"

        UpMoveNum = 0
        DownMoveNum = 0
        rsClass.MoveNext
    Loop
    Response.Write "</table>"
    rsClass.Close
    Set rsClass = Nothing
End Sub
	
Sub UpOrderN()
    Dim sqlOrder, rsOrder, MoveNum, ClassID, i
    Dim ParentID, OrderID, ParentPath, Child, PrevID, NextID
    ClassID = GetForm("ClassID")
    MoveNum = GetForm("MoveNum")
    If ClassID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>错误参数！</li>"
    Else
        ClassID = PE_CLng(ClassID)
    End If
    If MoveNum = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>错误参数！</li>"
    Else
        MoveNum = PE_CLng(MoveNum)
        If MoveNum = 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请选择要提升的数字！</li>"
        End If
    End If
    If FoundErr = True Then
        Exit Sub
    End If

    Dim sql, rs, trs, AddOrderNum, tClassID, tOrderID, tPrevID
    
    '要移动的栏目信息
    Set rs = Conn.Execute("select ParentID,OrderID,ParentPath,Child,PrevID,NextID from PW_Class where ClassID=" & ClassID)
    ParentID = rs(0)
    OrderID = rs(1)
    ParentPath = rs(2) & "," & ClassID
    Child = rs(3)
    PrevID = rs(4)
    NextID = rs(5)
    rs.Close
    Set rs = Nothing
    
    '获得要移动的栏目的所有子栏目数，然后加1（栏目本身），得到排序增加数（即其上栏目的OrderID增加数AddOrderNum）
    If Child > 0 Then
        Set rs = Conn.Execute("select count(*) from PW_Class where ParentPath like '%" & ParentPath & "%'")
        AddOrderNum = rs(0) + 1
        rs.Close
        Set rs = Nothing
    Else
        AddOrderNum = 1
    End If
    
    '先修改上一栏目的NextID和下一栏目的PrevID
    If PrevID > 0 Then
        Conn.Execute "update PW_Class set NextID=" & NextID & " where ClassID=" & PrevID
    End If
    If NextID > 0 Then
        Conn.Execute "update PW_Class set PrevID=" & PrevID & " where ClassID=" & NextID
    End If
    
    '和该栏目同级且排序在其之上的栏目------更新其排序，范围为要提升的数字AddOrderNum
    sql = "select ClassID,OrderID,Child,ParentPath,PrevID,NextID from PW_Class where ParentID=" & ParentID & " and OrderID<" & OrderID & " order by OrderID desc"
    Set rs = Server.CreateObject("adodb.recordset")
    rs.Open sql, Conn, 1, 3
    i = 0
    Do While Not rs.EOF
        tOrderID = rs(1)
        Conn.Execute ("update PW_Class set OrderID=OrderID+" & AddOrderNum & " where ClassID=" & rs(0))
        If rs(2) > 0 Then
            Set trs = Conn.Execute("select ClassID,OrderID from PW_Class where ParentPath like '%" & rs(3) & "," & rs(0) & "%' order by OrderID")
            If Not (trs.BOF And trs.EOF) Then
                Do While Not trs.EOF
                    Conn.Execute ("update PW_Class set OrderID=OrderID+" & AddOrderNum & " where ClassID=" & trs(0))
                    trs.MoveNext
                Loop
            End If
            trs.Close
            Set trs = Nothing
        End If
        i = i + 1
        If i >= MoveNum Then
            '获得最后一个提升序号的同级栏目信息
            tClassID = rs(0)
            tPrevID = rs(4)
            Exit Do
        End If
        rs.MoveNext
    Loop
    rs.Close
    Set rs = Nothing
    
    '更新移动后本栏目的的PrevID和NextID，以及上一栏目的NextID和下一栏目的PrevID
    Conn.Execute ("update PW_Class set PrevID=" & tPrevID & " where ClassID=" & ClassID)
    Conn.Execute ("update PW_Class set NextID=" & tClassID & " where ClassID=" & ClassID)
    Conn.Execute ("update PW_Class set PrevID=" & ClassID & " where ClassID=" & tClassID)
    If tPrevID > 0 Then
        Conn.Execute ("update PW_Class set NextID=" & ClassID & " where ClassID=" & tPrevID)
    End If
        
    '更新所要排序的栏目的序号
    Conn.Execute ("update PW_Class set OrderID=" & tOrderID & " where ClassID=" & ClassID)
    '如果有下属栏目，则更新其下属栏目排序
    If Child > 0 Then
        i = 1
        Set rs = Conn.Execute("select ClassID from PW_Class where ParentPath like '%" & ParentPath & "%' order by OrderID")
        Do While Not rs.EOF
            Conn.Execute ("update PW_Class set OrderID=" & tOrderID + i & " where ClassID=" & rs(0))
            i = i + 1
            rs.MoveNext
        Loop
        rs.Close
        Set rs = Nothing
    End If
    
    
	Call CloseConn
	Response.Redirect "Admin_Class.asp?ChannelID=" & ChannelID & "&Action=OrderN"
End Sub

Sub DownOrderN()
    Dim sqlOrder, rsOrder, MoveNum, ClassID, i
    Dim ParentID, OrderID, ParentPath, Child, PrevID, NextID
    ClassID = GetForm("ClassID")
    MoveNum = GetForm("MoveNum")
    If ClassID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>错误参数！</li>"
        Exit Sub
    Else
        ClassID = PE_CLng(ClassID)
    End If
    If MoveNum = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>错误参数！</li>"
        Exit Sub
    Else
        MoveNum = PE_CLng(MoveNum)
        If MoveNum = 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请选择要下降的数字！</li>"
            Exit Sub
        End If
    End If

    Dim sql, rs, trs, ii, tClassID, tNextID
    
    '要移动的栏目信息
    Set rs = Conn.Execute("select ParentID,OrderID,ParentPath,child,PrevID,NextID from PW_Class where ClassID=" & ClassID)
    ParentID = rs(0)
    OrderID = rs(1)
    ParentPath = rs(2) & "," & ClassID
    Child = rs(3)
    PrevID = rs(4)
    NextID = rs(5)
    rs.Close
    Set rs = Nothing

    '先修改上一栏目的NextID和下一栏目的PrevID
    If PrevID > 0 Then
        Conn.Execute "update PW_Class set NextID=" & NextID & " where ClassID=" & PrevID
    End If
    If NextID > 0 Then
        Conn.Execute "update PW_Class set PrevID=" & PrevID & " where ClassID=" & NextID
    End If
    
    '和该栏目同级且排序在其之下的栏目------更新其排序，范围为要下降的数字
    sql = "select ClassID,OrderID,child,ParentPath,PrevID,NextID from PW_Class where ParentID=" & ParentID & " and OrderID>" & OrderID & " order by OrderID"
    Set rs = Server.CreateObject("adodb.recordset")
    rs.Open sql, Conn, 1, 3
    i = 0    '同级栏目
    ii = 0   '同级栏目和子栏目
    Do While Not rs.EOF
        Conn.Execute ("update PW_Class set OrderID=" & OrderID + ii & " where ClassID=" & rs(0))
        If rs(2) > 0 Then
            Set trs = Conn.Execute("select ClassID,OrderID from PW_Class where ParentPath like '%" & rs(3) & "," & rs(0) & "%' order by OrderID")
            If Not (trs.BOF And trs.EOF) Then
                Do While Not trs.EOF
                    ii = ii + 1
                    Conn.Execute ("update PW_Class set OrderID=" & OrderID + ii & " where ClassID=" & trs(0))
                    trs.MoveNext
                Loop
            End If
            trs.Close
            Set trs = Nothing
        End If
        ii = ii + 1
        i = i + 1
        If i >= MoveNum Then
            '获得移动后本栏目的上一栏目的信息
            tClassID = rs(0)
            tNextID = rs(5)
            Exit Do
        End If
        rs.MoveNext
    Loop
    rs.Close
    Set rs = Nothing
            
    '更新移动后本栏目的的PrevID和NextID，以及上一栏目的NextID和下一栏目的PrevID
    Conn.Execute ("update PW_Class set PrevID=" & tClassID & " where ClassID=" & ClassID)
    Conn.Execute ("update PW_Class set NextID=" & tNextID & " where ClassID=" & ClassID)
    Conn.Execute ("update PW_Class set NextID=" & ClassID & " where ClassID=" & tClassID)
    If tNextID > 0 Then
        Conn.Execute ("update PW_Class set PrevID=" & ClassID & " where ClassID=" & tNextID)
    End If
    
    '更新所要排序的栏目的序号
    Conn.Execute ("update PW_Class set OrderID=" & OrderID + ii & " where ClassID=" & ClassID)
    '如果有下属栏目，则更新其下属栏目排序
    If Child > 0 Then
        i = 1
        Set rs = Conn.Execute("select ClassID from PW_Class where ParentPath like '%" & ParentPath & "%' order by OrderID")
        Do While Not rs.EOF
            Conn.Execute ("update PW_Class set OrderID=" & OrderID + ii + i & " where ClassID=" & rs(0))
            i = i + 1
            rs.MoveNext
        Loop
        rs.Close
        Set rs = Nothing
    End If
    
	Call CloseConn
	Response.Redirect "Admin_Class.asp?ChannelID=" & ChannelID & "&Action=OrderN"
End Sub
	
Sub Reset()
    Response.Write "<br>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='0' cellspacing='1' class='border'>"
    Response.Write "  <tr class='title'>"
    Response.Write "    <td height='22' colspan='3' align='center'><strong>复位所有" & ChannelShortName & "栏目</strong></td> "
    Response.Write "  </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "    <td align='center'>"
    Response.Write "      <form name='form1' method='post' action='Admin_Class.asp?Action=SaveReset'>"
    Response.Write "        <table width='80%' border='0' cellspacing='0' cellpadding='0'>"
    Response.Write "          <tr>"
    Response.Write "            <td height='150'><font color='#FF0000'><strong>注意：</strong></font><br>&nbsp;&nbsp;&nbsp;&nbsp;如果选择复位所有栏目，则所有栏目都将作为一级栏目，这时您需要重新对各个栏目进行归属的基本设置。不要轻易使用该功能，仅在做出了错误的设置而无法复原栏目之间的关系和排序的时候使用。<br><br>"
    Response.Write "            </td>"
    Response.Write "          </tr>"
    Response.Write "        </table>"
    Response.Write "        <input type='submit' name='Submit' value='复位所有栏目'> &nbsp; <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Class.asp?ChannelID=" & ChannelID & "'"" style='cursor:hand;'>"
    Response.Write "     <input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "      </form></td>"
    Response.Write "    </tr>"
    Response.Write "</table>"
End Sub
	
Sub SaveReset()
    Dim i, sql, rsClass, SuccessMsg, iCount, PrevID, NextID, trs
    sql = "select ClassID,ParentID,ClassType from PW_Class where ChannelID=" & ChannelID & " order by RootID,OrderID"
    Set rsClass = Server.CreateObject("adodb.recordset")
    rsClass.Open sql, Conn, 1, 1
    iCount = rsClass.RecordCount
    i = 1
    PrevID = 0
    Do While Not rsClass.EOF
        rsClass.MoveNext
        If rsClass.EOF Then
            NextID = 0
        Else
            NextID = rsClass(0)
        End If
        rsClass.moveprevious
        Set trs = Conn.Execute("select Count(ClassID) from PW_Class where ChannelID=" & ChannelID & " and ParentID=0 and ClassID<>" & rsClass(0) & "")
        Set trs = Nothing
        Conn.Execute ("update PW_Class set RootID=" & i & ",OrderID=0,ParentID=0,Child=0,ParentPath='0',Depth=0,PrevID=" & PrevID & ",NextID=" & NextID & ",arrChildID='" & rsClass(0) & "' where ClassID=" & rsClass(0))
        PrevID = rsClass(0)
        i = i + 1
        rsClass.MoveNext
    Loop
    rsClass.Close
    Set rsClass = Nothing
    If FoundErr = True Then
        Call WriteErrMsg(ErrMsg, ComeUrl)
    Else
        SuccessMsg = "复位成功！请返回<a href='Admin_Class.asp'>栏目管理首页</a>做栏目的归属设置。"
        Call WriteSuccessMsg(SuccessMsg, ComeUrl)
    End If
End Sub
	
Sub ResetChildClass()
    Dim ClassID, RootID, ParentPath
    Dim sql, rsClass, SuccessMsg, iCount, PrevID, NextID, i, trs
    ClassID = GetValue("ClassID")
    If ClassID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
        Exit Sub
    Else
        ClassID = PE_CLng(ClassID)
    End If
    Set rsClass = Conn.Execute("select ClassID,RootID from PW_Class where ChannelID=" & ChannelID & " and ParentID=0 and ClassID=" & ClassID)
    If rsClass.BOF And rsClass.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到指定的栏目！</li>"
    Else
        RootID = rsClass(1)
        ParentPath = "0," & rsClass(0)
    End If
    Set rsClass = Nothing
    If FoundErr = True Then Exit Sub

    sql = "select ClassID,ParentID,ClassType from PW_Class where ChannelID=" & ChannelID & " and RootID=" & RootID & " and ParentID>0 order by OrderID"
    Set rsClass = Server.CreateObject("adodb.recordset")
    rsClass.Open sql, Conn, 1, 1
    iCount = rsClass.RecordCount
    i = 1
    PrevID = 0
    Do While Not rsClass.EOF
        rsClass.MoveNext
        If rsClass.EOF Then
            NextID = 0
        Else
            NextID = rsClass(0)
        End If
        rsClass.moveprevious
        Set trs = Conn.Execute("select Count(ClassID) from PW_Class where ChannelID=" & ChannelID & " and ParentID=" & ClassID & " and ClassID<>" & rsClass(0) & "")
        Set trs = Nothing
        Conn.Execute ("update PW_Class set OrderID=" & i & ",ParentID=" & ClassID & ",Child=0,ParentPath='" & ParentPath & "',Depth=1,PrevID=" & PrevID & ",NextID=" & NextID & ",arrChildID='" & rsClass(0) & "'where ClassID=" & rsClass(0))
        PrevID = rsClass(0)
        i = i + 1
        rsClass.MoveNext
    Loop
    rsClass.Close
    Set rsClass = Nothing
    Conn.Execute ("update PW_Class set Child=" & i - 1 & " where ClassID=" & ClassID)
    
    SuccessMsg = "复位成功！请返回<a href='Admin_Class.asp'>栏目管理首页</a>做栏目的归属设置。"
    Call WriteSuccessMsg(SuccessMsg, ComeUrl)
	Call Refresh(ComeUrl,1)
End Sub


Sub Patch()
    Response.Write "<br>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='0' cellspacing='1' class='border'>"
    Response.Write "  <tr class='title'>"
    Response.Write "    <td height='22' colspan='3' align='center'><strong>修复栏目结构</strong></td> "
    Response.Write "  </tr>"
    Response.Write "    <tr class='tdbg'>"
    Response.Write "    <td align='center'>"
    Response.Write "      <form name='form1' method='post' action='Admin_Class.asp?Action=DoPatch'>"
    Response.Write "        <table width='80%' border='0' cellspacing='0' cellpadding='0'>"
    Response.Write "          <tr>"
    Response.Write "            <td height='150'><br>当栏目出现排序错误或串位的情况时，使用此功能可以修复。本操作相当安全，不会给系统带来任何负面影响。<br><br>修复过程中请勿刷新页面！"
    Response.Write "            </td>"
    Response.Write "          </tr>"
    Response.Write "        </table>"
    Response.Write "        <input type='submit' name='Submit' value='开始修复'> &nbsp; <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Class.asp?ChannelID=" & ChannelID & "'"" style='cursor:hand;'>"
    Response.Write "     <input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "      </form></td>"
    Response.Write "    </tr>"
    Response.Write "</table>"
End Sub

Sub DoPatch()
    Dim rsClass, sql, PrevID, trs
    Set rsClass = Server.CreateObject("ADODB.Recordset")
    sql = "select ClassID,RootID,OrderID,Depth,ParentID,ParentPath,Child,arrChildID,PrevID,NextID,ClassType from PW_Class where ChannelID=" & ChannelID & " and ParentID=0 order by RootID"
    rsClass.Open sql, Conn, 1, 3
    If rsClass.BOF And rsClass.EOF Then
        rsClass.Close
        Set rsClass = Nothing
        Exit Sub
    End If
    PrevID = 0
    Do While Not rsClass.EOF
        rsClass("OrderID") = 0
        rsClass("Depth") = 0
        rsClass("ParentPath") = "0"
        rsClass("PrevID") = PrevID
        rsClass("NextID") = 0
        rsClass("arrChildID") = CStr(rsClass("ClassID"))
        If PrevID <> rsClass("ClassID") And PrevID > 0 Then
            Conn.Execute ("update PW_Class set NextID=" & rsClass("ClassID") & " where ClassID=" & PrevID & "")
        End If
        PrevID = rsClass("ClassID")        
        
        rsClass.Update
        iOrderID = 1
        Call UpdateClass(rsClass("ClassID"), 1, "0")
        rsClass.MoveNext
    Loop
    rsClass.Close
    Set rsClass = Nothing
    Call WriteSuccessMsg("修复栏目结构成功！", ComeUrl)
	Call Refresh(ComeUrl,1)
End Sub

Sub UpdateClass(iParentID, iDepth, sParentPath)
    Dim rsClass, sql, PrevID, ParentPath, trs, rsChild
    ParentPath = sParentPath & "," & iParentID
    
    sql = "select ClassID,RootID,OrderID,Depth,ParentID,ParentPath,Child,arrChildID,PrevID,NextID,ClassType from PW_Class where ChannelID=" & ChannelID & " and ParentID=" & iParentID & " order by OrderID"
    Set rsClass = Server.CreateObject("ADODB.Recordset")
    rsClass.Open sql, Conn, 1, 3
    If rsClass.BOF And rsClass.EOF Then
        Conn.Execute ("update PW_Class set Child=0 where ClassID=" & iParentID & "")
    Else
        Conn.Execute ("update PW_Class set Child=" & rsClass.RecordCount & " where ClassID=" & iParentID & "")
        
        PrevID = 0
        Do While Not rsClass.EOF
            Set rsChild = Server.CreateObject("adodb.recordset")
            rsChild.Open "select arrChildID from PW_Class where ClassID in (" & ParentPath & ")", Conn, 1, 3
            Do While Not rsChild.EOF
                rsChild(0) = rsChild(0) & "," & rsClass("ClassID")
                rsChild.Update
                rsChild.MoveNext
            Loop
            rsChild.Close
            Set rsChild = Nothing
            
            rsClass("OrderID") = iOrderID
            rsClass("Depth") = iDepth
            rsClass("ParentPath") = ParentPath
            rsClass("PrevID") = PrevID
            rsClass("NextID") = 0
            rsClass("arrChildID") = CStr(rsClass("ClassID"))
            
            If PrevID <> rsClass("ClassID") And PrevID > 0 Then
                Conn.Execute ("update PW_Class set NextID=" & rsClass("ClassID") & " where ClassID=" & PrevID & "")
            End If
            PrevID = rsClass("ClassID")
                      
            rsClass.Update
            
            iOrderID = iOrderID + 1
            
            Call UpdateClass(rsClass("ClassID"), iDepth + 1, ParentPath)
            
            rsClass.MoveNext
        Loop
    End If
    rsClass.Close
    Set rsClass = Nothing
End Sub

Sub ClearClass()
    Dim rsClass, SuccessMsg, ClassID
    ClassID = GetUrl("ClassID")
    If ClassID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
        Exit Sub
    Else
        ClassID = PE_CLng(ClassID)
    End If
    Set rsClass = Conn.Execute("select arrChildID,ClassType from PW_Class where ClassID=" & ClassID)
    If rsClass.BOF And rsClass.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>栏目不存在，或者已经被删除</li>"
    Else
        Conn.Execute ("update " & SheetName & " set Deleted=" & PE_True & " where ClassID in (" & rsClass(0) & ")")
        SuccessMsg = "此栏目（包括子栏目）的所有" & ChannelShortName & "已经被移到回收站中！"
    End If
    rsClass.Close
    Set rsClass = Nothing
    
    If FoundErr = True Then Exit Sub
	Call WriteSuccessMsg(SuccessMsg, ComeUrl)
	Call Refresh(ComeUrl,1)
End Sub



Function RemoveClassID(ByVal arrClassID_Parent, ByVal arrClassID_Child)
    Dim arrClassID, arrClassID2, arrClassID3, i, j, bFound
    If IsNull(arrClassID_Parent) Then
        RemoveClassID = ""
        Exit Function
    End If
    If IsNull(arrClassID_Parent) Then
        RemoveClassID = arrClassID_Parent
        Exit Function
    End If
    If Trim(arrClassID_Parent) = Trim(arrClassID_Child) Then
        RemoveClassID = ""
        Exit Function
    End If
    arrClassID = Split(arrClassID_Parent, ",")
    arrClassID3 = ""
    If InStr(arrClassID_Child, ",") > 0 Then
        arrClassID2 = Split(arrClassID_Child, ",")
        For i = 0 To UBound(arrClassID)
            bFound = False
            For j = 0 To UBound(arrClassID2)
                If PE_CLng(arrClassID(i)) = PE_CLng(arrClassID2(j)) Then
                    bFound = True
                    Exit For
                End If
            Next
            If bFound = False Then
                If arrClassID3 = "" Then
                    arrClassID3 = arrClassID(i)
                Else
                    arrClassID3 = arrClassID3 & "," & arrClassID(i)
                End If
            End If
        Next
    Else
        For i = 0 To UBound(arrClassID)
            If PE_CLng(arrClassID(i)) <> PE_CLng(arrClassID_Child) Then
                If arrClassID3 = "" Then
                    arrClassID3 = arrClassID(i)
                Else
                    arrClassID3 = arrClassID3 & "," & arrClassID(i)
                End If
            End If
        Next
    End If
    RemoveClassID = arrClassID3
End Function

Function GetClass_Opt(iChannelID, CurrentID)
    Dim rsClass, sqlClass, strTemp, tmpDepth, i
    Dim arrShowLine(20)
    For i = 0 To UBound(arrShowLine)
        arrShowLine(i) = False
    Next
    sqlClass = "Select ClassID,ClassName,ClassType,Depth,NextID from PW_Class where ChannelID=" & iChannelID & " order by RootID,OrderID"
    Set rsClass = Conn.Execute(sqlClass)
    If rsClass.BOF And rsClass.EOF Then
        strTemp = "<option value=''>请先添加栏目</option>"
    Else
        strTemp = ""
        Do While Not rsClass.EOF
            tmpDepth = rsClass(3)
            If rsClass("NextID") > 0 Then
                arrShowLine(tmpDepth) = True
            Else
                arrShowLine(tmpDepth) = False
            End If
            strTemp = strTemp & "<option value='" & rsClass("ClassID") & "'"
            If CurrentID > 0 And rsClass("ClassID") = CurrentID Then
                 strTemp = strTemp & " selected"
            End If
            strTemp = strTemp & ">"
            
            If tmpDepth > 0 Then
                For i = 1 To tmpDepth
                    strTemp = strTemp & "&nbsp;&nbsp;"
                    If i = tmpDepth Then
                        If rsClass("NextID") > 0 Then
                            strTemp = strTemp & "├&nbsp;"
                        Else
                            strTemp = strTemp & "└&nbsp;"
                        End If
                    Else
                        If arrShowLine(i) = True Then
                            strTemp = strTemp & "│"
                        Else
                            strTemp = strTemp & "&nbsp;"
                        End If
                    End If
                Next
            End If
            strTemp = strTemp & rsClass(1)
            If rsClass(2) = 2 Then
                strTemp = strTemp & "(外)"
            End If
            strTemp = strTemp & "</option>"
            rsClass.MoveNext
        Loop
    End If
    rsClass.Close
    Set rsClass = Nothing

    GetClass_Opt = strTemp
End Function


Function GetPath(ParentID, ParentPath)
    Dim strPath, i
    If ParentID <= 0 Then
        GetPath = "无（作为一级栏目）"
        Exit Function
    End If
    Dim rsParent, sqlParent
    sqlParent = "Select * from PW_Class where ClassID in (" & ParentPath & ") order by Depth"
    Set rsParent = Conn.Execute(sqlParent)
    Do While Not rsParent.EOF
        For i = 1 To rsParent("Depth")
            strPath = strPath & "&nbsp;&nbsp;&nbsp;"
        Next
        If rsParent("Depth") > 0 Then
            strPath = strPath & "└&nbsp;"
        End If
        strPath = strPath & rsParent("ClassName") & "<br>"
        rsParent.MoveNext
    Loop
    rsParent.Close
    Set rsParent = Nothing
    GetPath = strPath
End Function

%>