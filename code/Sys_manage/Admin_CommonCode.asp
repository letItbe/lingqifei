<%

Sub CheckComeUrl()
    Dim ComeUrl, TrueSiteUrl, cUrl
    ComeUrl = Trim(Request.ServerVariables("HTTP_REFERER"))
    TrueSiteUrl = Trim(Request.ServerVariables("HTTP_HOST"))
    If ComeUrl = "" Then
        Response.Write "<br><p align=center><font color='red'>对不起，为了系统安全，不允许直接输入地址访问本系统的后台管理页面。</font></p>"
        Response.End
    Else
        cUrl = Trim("http://" & TrueSiteUrl) & ScriptName
        If LCase(Left(ComeUrl, InStrRev(ComeUrl, "/"))) <> LCase(Left(cUrl, InStrRev(cUrl, "/"))) Then
            Response.Write "<br><p align=center><font color='red'>对不起，为了系统安全，不允许从外部链接地址访问本系统的后台管理页面。</font></p>"
            Response.End
        End If
    End If
End Sub

Sub ShowPageTitle(strTitle)
    Response.Write "  <tr class='topbg'> " & vbCrLf
    Response.Write "    <td height='22' colspan='10'><table width='100%'><tr class='topbg'><td align='center'><b>" & strTitle & "</b></td></tr></table></td>" & vbCrLf
    Response.Write "  </tr>" & vbCrLf
End Sub



'**************************************************
'函数名：CheckPurview_Other
'作  用：其他权限数组检测
'参  数：AllPurviews ---- 比较数组
'        strPurview ---- 比较字符
'返回值：True  ---- 存在  False  不存在
'**************************************************
Function CheckPurview_Other(AllPurviews, strPurview)
    If IsNull(AllPurviews) Or AllPurviews = "" Or strPurview = "" Then
        CheckPurview_Other = False
        Exit Function
    End If
    CheckPurview_Other = False
    If InStr(AllPurviews, ",") > 0 Then
        Dim arrPurviews, i
        arrPurviews = Split(AllPurviews, ",")
        For i = 0 To UBound(arrPurviews)
            If Trim(arrPurviews(i)) = Trim(strPurview) Then
                CheckPurview_Other = True
                Exit For
            End If
        Next
    Else
        If Trim(AllPurviews) = Trim(strPurview) Then
            CheckPurview_Other = True
        End If
    End If
End Function


' purview_ChannelItem(adminpurview,pType)
' 作用：检查管理员权限里的具体每一个功能 浏览 添加 修改 删除 分类
' 参数:adminpurview 管理员栏目权限字符串
'      purviewType  权限类别(0为浏览，1为添加，2为修改，3为删除，4为分类)
'      ChannelID 当前栏目
' 返回值：True| false
function purview_ChannelItem(adminpurview,purviewType,ChannelID)
	dim pChannelItem
	purview_ChannelItem=false
	if adminpurview="" or IsNull(adminpurview) then exit function
	
	pChannelItem=Split(adminpurview,"$$$")
	if ubound(pChannelItem)<4 then exit function
	Select Case purviewType
		Case "0" '浏览
			if FoundInArr(pChannelItem(0), ChannelID, ",")=true then
				purview_ChannelItem=True
			end if
		Case "1" '添加	
			if FoundInArr(pChannelItem(1), ChannelID, ",")=true then
				purview_ChannelItem=True
			end if
		Case "2" '修改	
			if FoundInArr(pChannelItem(2), ChannelID, ",")=true then
				purview_ChannelItem=True
			end if
		Case "3" '删除	
			if FoundInArr(pChannelItem(3), ChannelID, ",")=true then
				purview_ChannelItem=True
			end if
		Case "4" '分类	
			if FoundInArr(pChannelItem(4), ChannelID, ",")=true then
				purview_ChannelItem=True
			end if
	End Select
	
End function

'**************************************************
'函数名：Replace_CaseInsensitive
'作  用：替换字符，大小写不敏感
'参  数：expression ---- 字符串表达式 包含要替代的子字符串
'        find ---- 被搜索的子字符串
'        replacewith ---- 用于替换的子字符串
'返回值：处理后的字符串
'**************************************************
Function Replace_CaseInsensitive(expression, find, replacewith)
    regEx.Pattern = find
    Replace_CaseInsensitive = regEx.Replace(expression, replacewith)
End Function

%>
