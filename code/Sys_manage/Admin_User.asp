<!--#include file="Admin_Common.asp"-->
<!--#include file="../Include/PW_MD5.asp"-->
<%
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Channel = 0   '0--不检查，1 检查
Const PurviewLevel_Others = "User"   '其他权限

Response.Write "<html><head><title>会员管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>" & vbCrLf
Call ShowPageTitle("会 员 管 理")
Response.Write "  <tr class='tdbg'>" & vbCrLf
Response.Write "    <td width='80' height='30'><strong>管理导航：</strong></td>" & vbCrLf
Response.Write "    <td align='left'>" & vbCrLf
Response.Write "        <a href='Admin_User.asp'>会员管理首页</a>&nbsp;|&nbsp;<a href='Admin_User.asp?Action=AddUser'>添加新会员</a>"
Response.Write "    </td>" & vbCrLf
Response.Write "  </tr>" & vbCrLf
Response.Write "</table>" & vbCrLf

Select Case Action
Case "AddUser", "Modify"
    Call ShowForm
Case "SaveAddUser", "SaveModify"
    Call SaveUser
Case "Del"
	Call Del
Case Else
    Call main
End Select
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn


Sub main()
    Dim sql, Querysql, rsUserList
    GroupID = PE_CLng(GetValue("GroupID"))
    strFileName = "Admin_User.asp?SearchType=" & SearchType & "&Field=" & strField & "&keyword=" & Keyword & "&GroupID=" & GroupID

    Call ShowJS_Main("会员")
    
    Dim rsGroup, i
    i = 1
    Response.Write "<br><table width='100%' class='border' border='0' cellpadding='2' cellspacing='1'><tr class='title'><td>| <a href='Admin_User.asp'>所有会员</a> |"
    Set rsGroup = Conn.Execute("select GroupID,GroupName,GroupIntro from PW_UserGroup order by GroupID asc")
    Do While Not rsGroup.EOF
        Response.Write " <a href='Admin_User.asp?SearchType=1&GroupID=" & rsGroup(0) & "' title='" & rsGroup(2) & "'>" & rsGroup(1) & "</a> |"
        rsGroup.MoveNext
        i = i + 1
        If i Mod 10 = 0 And Not rsGroup.EOF Then Response.Write "<br>|"
    Loop
    rsGroup.Close
    Set rsGroup = Nothing
    Response.Write "</td></tr></table>"
    
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_User.asp'>注册会员管理</a>&nbsp;&gt;&gt;&nbsp;"
	
	sql = "select  U.*,G.GroupName from PW_User U inner join PW_UserGroup G on U.GroupID=G.GroupID "
    Querysql = Querysql & " where 1=1 "
    Select Case SearchType
    Case 0
        Response.Write "所有会员"
    Case 1
        Querysql = Querysql & " and U.GroupID=" & GroupID & ""
        Response.Write GetGroupName(GroupID)
    Case 2
        If Keyword = "" Then
            Response.Write "所有会员"
        Else
			Querysql = Querysql & " and U.UserName like '%" & Keyword & "%'"
			Response.Write "用户名中含有“ <font color=red>" & Keyword & "</font> ”的会员"

		end if
    End Select
    totalPut = PE_CLng(Conn.Execute("select Count(*) from PW_User U " & Querysql)(0))

    If CurrentPage < 1 Then
        CurrentPage = 1
    End If
    If (CurrentPage - 1) * MaxPerPage > totalPut Then
        If (totalPut Mod MaxPerPage) = 0 Then
            CurrentPage = totalPut \ MaxPerPage
        Else
            CurrentPage = totalPut \ MaxPerPage + 1
        End If
    End If
	If CurrentPage > 1 Then
		Querysql = Querysql & " and U.UserID < (select min(UserID) from (select top " & ((CurrentPage - 1) * MaxPerPage) & " U.UserID from PW_User U " & Querysql & " order by U.UserID desc) as QueryUser) "
	End If
	sql = sql & Querysql & " order by U.UserID desc"
	
    Response.Write "</td></tr></table>"
    If FoundErr = True Then Exit Sub
    
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>"
    Response.Write "  <tr>"
    Response.Write "  <form name='myform' method='Post' action='Admin_User.asp'>"
    Response.Write "      <td>"
    Response.Write "      <table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
    Response.Write "        <tr class='title' height='22' align='center'>"
    Response.Write "          <td width='30'>选中</td>"
    Response.Write "          <td width='70'> 用户名</td>"
    Response.Write "          <td>所属会员组</td>"
    Response.Write "          <td width='120'>最后登录IP<br>最后登录时间</td>"
    Response.Write "          <td width='40'>登录<br>次数</td>"
    Response.Write "    	  <td width='150'>操 作</td>" & vbCrLf
    Response.Write "        </tr>"

    Set rsUserList = Server.CreateObject("Adodb.RecordSet")
    rsUserList.Open sql, Conn, 1, 1
    If rsUserList.BOF And rsUserList.EOF Then
        Response.Write "<tr><td colspan='20' height='50' align='center'>共找到 <font color=red>0</font> 个会员</td></tr>"
    Else
        Dim UserNum
        UserNum = 0
        Do While Not rsUserList.EOF
            Response.Write "      <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"" align=center>"
            Response.Write "        <td><input name='UserID' type='checkbox' onclick=""unselectall()"" id='UserID' value='" & CStr(rsUserList("UserID")) & "'></td>"
            Response.Write "        <td><a href='Admin_User.asp?Action=Modify&UserID=" & rsUserList("UserID") & "'>" & rsUserList("UserName") & "</a></td>"
            Response.Write "        <td>" & rsUserList("GroupName") & "</td>"
            Response.Write "        <td>" & rsUserList("LastLoginIP") & "<br>" & rsUserList("LastLoginTime") & "</td>"
            Response.Write "        <td>"
            If rsUserList("LoginTimes") <> "" Then
                Response.Write rsUserList("LoginTimes")
            Else
                Response.Write "0"
            End If
            Response.Write "        </td>"
            Response.Write "        <td><a href='Admin_User.asp?Action=Modify&UserID=" & rsUserList("UserID") & "'>修改</a>&nbsp;&nbsp;<a href='Admin_User.asp?Action=Del&UserID=" & rsUserList("UserID") & "'>删除</a></td>"
            Response.Write "      </tr>"

            UserNum = UserNum + 1
            If UserNum >= MaxPerPage Then Exit Do
            rsUserList.MoveNext
        Loop
    End If
    rsUserList.Close
    Set rsUserList = Nothing
    Response.Write "      </table>"
    If totalPut > 0 Then
        Response.Write "      <table width='100%' border='0' cellpadding='0' cellspacing='0'>"
        Response.Write "        <tr height='60'>"
        Response.Write "          <td width='200'><input name='chkAll' type='checkbox' id='chkAll' onclick='CheckAll(this.form);' value='checkbox'>"
        Response.Write "          选中本页显示的所有会员</td>"
        Response.Write "          <td><input type='hidden' name='Action' value=''>"
		Response.Write "          <input name='Del' type='submit' value=' 批量删除 ' onClick=""document.myform.Action.value='Del';"">&nbsp;&nbsp;&nbsp;&nbsp;"
        Response.Write "        </tr>"
        Response.Write "      </table>"
    End If
    Response.Write "      </td>"
    Response.Write "  </form>"
    Response.Write "  </tr>"
    Response.Write "</table>"
    If totalPut > 0 Then
        Response.Write ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, "个会员", True)
    End If
    Response.Write "<br>"
    Response.Write "<form name='SearchForm' action='Admin_User.asp' method='post'>" & vbCrLf
    Response.Write "<table width='100%'  border='0' align='center' cellpadding='1' cellspacing='1' class='border'>" & vbCrLf
    Response.Write "  <tr class='tdbg'>" & vbCrLf
    Response.Write "    <td width='80'>会员查询：</td>" & vbCrLf
    Response.Write "    <td>" & vbCrLf
    Response.Write "      <select name='Field' size='1'>" & vbCrLf
    Response.Write "        <option value='UserName' selected>用户名</option>" & vbCrLf
    Response.Write "      </select>" & vbCrLf
    Response.Write "      <input name='Keyword' type='text' id='Keyword' size='20' maxlength='40'>" & vbCrLf
    Response.Write "      <input type='submit' name='Submit' value='搜 索' id='Submit'>" & vbCrLf
    Response.Write "      <input type='hidden' name='SearchType' value='2'>" & vbCrLf
    Response.Write "    </td>" & vbCrLf
    Response.Write "  </tr>" & vbCrLf
    Response.Write "</table>" & vbCrLf
    Response.Write "</form>" & vbCrLf
    Response.Write "" & vbCrLf
End Sub

Sub ShowForm()
    Dim rsUser,Question,Answer,UserName,Email,TrueName,Sex,Address,Phone,Fax,ZipCode
    If Action = "AddUser" Then
        GroupID = 1
    Else
        UserID = PE_CLng(GetValue("UserID"))
        If UserID <= 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请指定会员ID！</li>"
            Exit Sub
        End If

        Set rsUser = Conn.Execute("select * from PW_User where UserID=" & UserID & "")
        If rsUser.BOF And rsUser.EOF Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>找不到指定的会员！</li>"
            rsUser.Close
            Set rsUser = Nothing
            Exit Sub
        Else
            GroupID = rsUser("GroupID")
            UserName = rsUser("UserName")
            Email = rsUser("Email")
            Question = rsUser("Question")
            Answer = rsUser("Answer")
            TrueName = rsUser("TrueName")
            Sex = rsUser("Sex")
            Address = rsUser("Address")
            Phone = rsUser("Phone")
            Fax = rsUser("Fax")
            ZipCode = rsUser("ZipCode")
        End If
        rsUser.Close
        Set rsUser = Nothing
    End If
    Response.Write "<script language=javascript>" & vbCrLf
    Response.Write "function CheckSubmit(){" & vbCrLf
    If Action = "AddUser" Then
        Response.Write "    if(document.myform.UserName.value==''){" & vbCrLf
        Response.Write "        alert('用户名不能为空！');" & vbCrLf
        Response.Write "        document.myform.UserName.focus();" & vbCrLf
        Response.Write "        return false;" & vbCrLf
        Response.Write "    }" & vbCrLf
        Response.Write "    if(document.myform.UserPassword.value==''){" & vbCrLf
        Response.Write "        alert('用户密码不能为空！');" & vbCrLf
        Response.Write "        document.myform.UserPassword.focus();" & vbCrLf
        Response.Write "        return false;" & vbCrLf
        Response.Write "    }" & vbCrLf
        Response.Write "    if(document.myform.Answer.value==''){" & vbCrLf
        Response.Write "        alert('密码提示答案不能为空！');" & vbCrLf
        Response.Write "        document.myform.Answer.focus();" & vbCrLf
        Response.Write "        return false;" & vbCrLf
        Response.Write "    }" & vbCrLf
    End If
	Response.Write "    if(document.myform.TrueName.value==''){" & vbCrLf
	Response.Write "        alert('真实姓名不能为空！');" & vbCrLf
	Response.Write "        document.myform.TrueName.focus();" & vbCrLf
	Response.Write "        return false;" & vbCrLf
	Response.Write "    }" & vbCrLf
	Response.Write "    if(document.myform.Address.value==''){" & vbCrLf
	Response.Write "        alert('联系地址不能为空！');" & vbCrLf
	Response.Write "        document.myform.Address.focus();" & vbCrLf
	Response.Write "        return false;" & vbCrLf
	Response.Write "    }" & vbCrLf
	Response.Write "    if(document.myform.Phone.value==''){" & vbCrLf
	Response.Write "        alert('联系电话不能为空！');" & vbCrLf
	Response.Write "        document.myform.Phone.focus();" & vbCrLf
	Response.Write "        return false;" & vbCrLf
	Response.Write "    }" & vbCrLf
    Response.Write "    if(document.myform.Question.value==''){" & vbCrLf
    Response.Write "        alert('密码提示问题不能为空！');" & vbCrLf
    Response.Write "        document.myform.Question.focus();" & vbCrLf
    Response.Write "        return false;" & vbCrLf
    Response.Write "    }" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "</script>" & vbCrLf
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_User.asp'>会员管理</a>&nbsp;&gt;&gt;&nbsp;"
    If Action = "AddUser" Then
        Response.Write "添加会员"
    Else
        Response.Write "修改会员信息"
    End If
    Response.Write "</td></tr></table>"
    Response.Write "<form name='myform' id='myform' action='Admin_User.asp' method='post' onSubmit='javascript:return CheckSubmit();'>" & vbCrLf
    Response.Write "<table width='100%'  border='0' align='center' cellpadding='5' cellspacing='1' class='border'><tr class='tdbg'><td height='100' valign='top'>" & vbCrLf
    Response.Write "  <table width='95%' align='center' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "        <td width='80' class='tdbg5' align='right'>会 员 组：</td>" & vbCrLf
    Response.Write "        <td><select name='GroupID' id='GroupID'>" & GetUserGroup_Option(GroupID) & "</select></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "        <td class='tdbg5' align='right'>用 户 名：</td>" & vbCrLf
    If Action = "AddUser" Then
        Response.Write "        <td><input type='text' name='UserName' size='20' maxlength='20' value='" & UserName & "'> <font color='#FF0000'>*</font></td>" & vbCrLf
    Else
        Response.Write "        <td><input type='text' name='UserName' size='20' maxlength='20' value='" & UserName & "' disabled> <font color='#FF0000'>*</font></td>" & vbCrLf
    End If
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "        <td class='tdbg5' align='right'>用户密码：</td>" & vbCrLf
    Response.Write "        <td><input name='UserPassword' type='text' id='UserPassword' size='20' maxlength='20'>"
    If Action = "AddUser" Then
        Response.Write " <font color='#FF0000'>*</font>"
    Else
        Response.Write " <font color='#FF0000'>不修改请留空</font>"
    End If
    Response.Write "</td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "        <td class='tdbg5' align='right' >真实姓名：</td>" & vbCrLf
    Response.Write "        <td><input name='TrueName' type='text' size='35' maxlength='200' value='" & TrueName & "'> <font color='#FF0000'>*</font></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "        <td class='tdbg5' align='right' >性别：</td>" & vbCrLf
    Response.Write "        <td><input name='Sex' type='radio' value='0' "
    If Sex <= 0 Or Sex > 2 Then Response.Write " checked"
    Response.Write ">保密 <input name='Sex' type='radio' value='1'"
    If Sex = 1 Then Response.Write " checked"
    Response.Write ">男 <input name='Sex' type='radio' value='2'"
    If Sex = 2 Then Response.Write " checked"
    Response.Write ">女 <font color='#FF0000'>*</font></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "        <td align='right' class='tdbg5' align='right' >联系地址：</td>" & vbCrLf
    Response.Write "        <td><input name='Address' type='text' size='60' maxlength='255' value='" & Address & "'> <font color='#FF0000'>*</font></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "    	<td class='tdbg5' align='right' >联系电话：</td>" & vbCrLf
    Response.Write "    	<td><input name='Phone' type='text' size='35' maxlength='30' value='" & Phone & "'> <font color='#FF0000'>*</font></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "    	<td class='tdbg5' align='right' >传真号码：</td>" & vbCrLf
    Response.Write "    	<td><input name='Fax' type='text' size='35' maxlength='30' value='" & Fax & "'></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "        <td class='tdbg5' align='right'>电子邮件：</td>" & vbCrLf
    Response.Write "        <td><input name='Email' type='text' id='Email' value='" & Email & "' size='35' maxlength='255'></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "        <td align='right' class='tdbg5' align='right' >邮政编码：</td>" & vbCrLf
    Response.Write "        <td><input name='ZipCode' type='text' size='35' maxlength='10' value='" & ZipCode & "'></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "        <td class='tdbg5' align='right'>提示问题：</td>" & vbCrLf
    Response.Write "        <td><input name='Question' type='text' id='Question' value='" & Question & "'  size='35'> <font color='#FF0000'>*</font></td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "    <tr class='tdbg'>" & vbCrLf
    Response.Write "        <td class='tdbg5' align='right'>提示答案：</td>" & vbCrLf
    Response.Write "        <td><input name='Answer' type='text' id='Answer' size='20'>"
    If Action = "AddUser" Then
        Response.Write " <font color='#FF0000'>*</font>"
    Else
        Response.Write " <font color='#FF0000'>不修改请留空</font>"
    End If
    Response.Write "		</td>" & vbCrLf
    Response.Write "    </tr>" & vbCrLf
    Response.Write "</table>" & vbCrLf
    Response.Write "</td></tr></table>" & vbCrLf
    Response.Write "<table width='100%'  border='0' align='center' cellpadding='5' cellspacing='1'><tr align='center'><td height='40'>" & vbCrLf
    Response.Write "    <input type='hidden' name='action' value='Save" & Action & "'><input type='hidden' name='UserID' value='" & UserID & "'>" & vbCrLf
    Response.Write "    <input type='submit' name='Submit' value='保存会员信息'>&nbsp;&nbsp;&nbsp;&nbsp;" & vbCrLf
    Response.Write "</td></tr></table>" & vbCrLf
    Response.Write "</form>" & vbCrLf
End Sub


Sub SaveUser()
	Dim UserID,sqlUser
    Dim rsUser,Question,Answer,UserName,Email,TrueName,Sex,Address,Phone,Fax,ZipCode
    UserID = PE_CLng(GetForm("UserID"))
    GroupID = PE_CLng(GetForm("GroupID"))
    UserName = ReplaceBadChar(GetForm("UserName"))
    UserPassword = ReplaceBadChar(GetForm("UserPassword"))
	TrueName = ReplaceBadChar(GetForm("TrueName"))
	Sex = PE_CLng(GetForm("Sex"))
	Address = ReplaceBadChar(GetForm("Address"))
	Phone = ReplaceBadChar(GetForm("Phone"))
	Fax = ReplaceBadChar(GetForm("Fax"))
	ZipCode = ReplaceBadChar(GetForm("ZipCode"))
    Email = ReplaceBadChar(GetForm("Email"))
    Question = ReplaceBadChar(GetForm("Question"))
    Answer = ReplaceBadChar(GetForm("Answer"))
    If Action = "SaveAdd" Then
        If UserName = "" Then
            FoundErr = True
            ErrMsg = "用户名不能为空！"
        End If
        If UserPassword = "" Then
            FoundErr = True
            ErrMsg = "密码不能为空！"
        End If
        If Answer = "" Then
            FoundErr = True
            ErrMsg = "提示答案不能为空！"
        End If
    End If
    If Question = "" Then
        FoundErr = True
        ErrMsg = "提示问题不能为空！"
    End If
    If TrueName = "" Then
        FoundErr = True
        ErrMsg = "真实姓名不能为空！"
    End If
    If Address = "" Then
        FoundErr = True
        ErrMsg = "联系地址不能为空！"
    End If
    If Phone = "" Then
        FoundErr = True
        ErrMsg = "联系电话不能为空！"
    End If
    If FoundErr Then
        Exit Sub
    End If
    If Action = "SaveAddUser" Then
        sqlUser = "SELECT * FROM PW_User Where UserName='" & UserName & "'"
        Set rsUser = Server.CreateObject("adodb.recordset")
        rsUser.Open sqlUser, Conn, 1, 3
        If rsUser.BOF And rsUser.EOF Then
            UserID = GetNewID("PW_User", "UserID")
            rsUser.addnew
            rsUser("UserID") = UserID
            rsUser("UserName") = UserName
            rsUser("LoginTimes") = 0
            rsUser("RegTime") = Now()
        Else
            FoundErr = True
            ErrMsg = "该用户名已被他人占用，请输入不同的用户名！"
        End If
    Else
        sqlUser = "SELECT * FROM PW_User Where UserID=" & UserID & ""
        Set rsUser = Server.CreateObject("adodb.recordset")
        rsUser.Open sqlUser, Conn, 1, 3
        If rsUser.BOF And rsUser.EOF Then
            FoundErr = True
            ErrMsg = "找不到指定的会员！"
        End If
    End If
    If FoundErr Then
        rsUser.Close
        Set rsUser = Nothing
        Exit Sub
    End If
    If UserPassword <> "" Then
        rsUser("UserPassword") = MD5(UserPassword, 16)
    End If
    rsUser("Question") = Question
    If Answer <> "" Then
        rsUser("Answer") = MD5(Answer, 16)
    End If
    rsUser("GroupID") = GroupID
    rsUser("Email") = Email
    rsUser("TrueName") = TrueName
    rsUser("Sex") = Sex
    rsUser("Address") = Address
    rsUser("Phone") = Phone
    rsUser("Fax") = Fax
    rsUser("ZipCode") = ZipCode
    rsUser("Email") = Email
    rsUser.Update
    rsUser.Close
    Set rsUser = Nothing
    Call WriteSuccessMsg("保存会员信息成功", "Admin_User.asp")
End Sub


Sub Del
	Dim UserID, SqlDel, RsDel
	UserID = GetValue("UserID")
    If UserID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定要删除的会员！</li>"
        Exit Sub
    End If
	SqlDel = "select * from PW_User "
    If InStr(UserID, ",") > 0 Then
        sqlDel = sqlDel & " where UserID in (" & UserID & ") order by UserID"
    Else
        sqlDel = sqlDel & " where UserID=" & UserID & " order by UserID"
    End If
    Set rsDel = Server.CreateObject("ADODB.Recordset")
    rsDel.Open sqlDel, Conn, 1, 3
	    Do While Not rsDel.EOF
			Dim tempDelName,tUserID
			tUserID = rsDel("UserID")
			tUserName = rsDel("UserName")
			Conn.Execute ("delete from PW_OrderForm where UserName='" & tUserName & "'")
			Conn.Execute ("delete from PW_Comment where UserName='" & tUserName & "'")
			Conn.Execute ("delete from PW_GuestBook where UserID='" & tUserID & "'")
		   rsDel.Delete
		   rsDel.Update
		   rsDel.movenext
   		loop
    rsDel.Close
    Set rsDel = Nothing
    Call WriteSuccessMsg("操作成功！", "Admin_User.asp")
    Call Refresh("Admin_User.asp",1)
End Sub



Function GetUserGroup_Option(CurrentGroupID)
    Dim strGroup, rsGroup
    Set rsGroup = Conn.Execute("select GroupID,GroupName from PW_UserGroup order by GroupID asc")
    Do While Not rsGroup.EOF
        strGroup = strGroup & "<option value='" & rsGroup(0) & "'"
        If rsGroup(0) = CurrentGroupID Then
            strGroup = strGroup & " selected"
        End If
        strGroup = strGroup & ">" & rsGroup(1) & "</option>"
        rsGroup.MoveNext
    Loop
    rsGroup.Close
    Set rsGroup = Nothing
    
    GetUserGroup_Option = strGroup
End Function

Function GetGroupName(iGroupID)
    If Not IsNumeric(iGroupID) Then Exit Function
    Dim rsGroup
    Set rsGroup = Conn.Execute("select GroupName from PW_UserGroup where GroupID=" & iGroupID & "")
    If rsGroup.BOF And rsGroup.EOF Then
        GetGroupName = "未知会员组"
    Else
        GetGroupName = rsGroup(0)
    End If
    Set rsGroup = Nothing
End Function
%>