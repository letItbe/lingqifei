<!--#include file="Admin_Common.asp"-->
<!--#include file="../Include/PW_FSO.asp"-->
<!--#include file="../PW_Editor/fckeditor.asp"-->
<!--#include file="../Include/PW_XmlHttp.asp"-->
<%
Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Channel = 1   '0--不检查，1 检查
Const PurviewLevel_Others = ""   '其他权限

Dim ManageType,ClassID, ProductID
Dim IncludePic, UploadFiles, DefaultPicUrl, IsThumb
Dim tClass, ClassName, RootID, ParentID, Depth, ParentPath, Child, arrChildID, ChildID



ManageType = GetValue("ManageType")
ClassID = PE_CLng(GetValue("ClassID"))
ProductID = GetValue("ProductID")
SearchType = PE_CLng(GetValue("SearchType"))

If Action = "" Then
    Action = "Manage"
End If

If IsValidID(ProductID) = False Then
    ProductID = ""
End If
FileName = "Admin_Product.asp?Action=" & Action & "&ManageType=" & ManageType & "&ChannelID=" & ChannelID 
strFileName = FileName & "&SearchType=" & SearchType & "&ClassID=" & ClassID & "&Field=" & strField & "&keyword=" & Keyword

Response.Write "<html><head><title>" & ChannelShortName & "管理</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf
Response.Write "<table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
Dim strTitle
strTitle = ChannelName & "管理----"
Select Case Action
Case "Add"
    strTitle = strTitle & "添加" & ChannelShortName
Case "Modify"
    strTitle = strTitle & "修改" & ChannelShortName
Case "SaveAdd", "SaveModify"
    strTitle = strTitle & "保存" & ChannelShortName
Case "Show"
    strTitle = strTitle & "查看" & ChannelShortName
Case "Manage"
    If ManageType = "Recyclebin" Then
        strTitle = strTitle & ChannelShortName & "回收站管理"
    Else
        strTitle = strTitle & ChannelShortName & "管理首页"
    End If
End Select
Call ShowPageTitle(strTitle)

Response.Write "  <tr class='tdbg'>"
Response.Write "    <td height='30' width='70'><strong>管理导航：</strong></td><td>"
if Purview_View=True then
	Response.Write "<a href='Admin_Product.asp?ChannelID=" & ChannelID & "'>" & ChannelShortName & "管理首页</a>"
End if
if Purview_Add=True then
	Response.Write "&nbsp;|&nbsp;<a href='Admin_Product.asp?ChannelID=" & ChannelID & "&Action=Add&ClassID=" & ClassID & "'>添加" & ChannelShortName & "</a>"
end if
if Purview_Del = True then
	Response.Write "&nbsp;|&nbsp;<a href='Admin_Product.asp?ChannelID=" & ChannelID & "&ManageType=Recyclebin&EnableSale=All'>" & ChannelShortName & "回收站管理</a>"
end if
if FoundInArr(FieldShow, "ClassID", ",") and Purview_Class=True then
	Response.Write "&nbsp;|&nbsp;<a href='Admin_Class.asp?ChannelID=" & ChannelID & "'>" & ChannelShortName & "分类管理</a>"
end if
Response.Write "</td></tr>" & vbCrLf
If Action = "Manage" Then
    Response.Write "  <tr class='tdbg'>"
    Response.Write "<form method='Get' name='SearchForm' action='" & FileName & "'>"
    Response.Write "<td><b>高级查询：</b></td><td>"
    Response.Write "<select name='Field' size='1'>"
    Response.Write "<option value='ProductName' selected>" & ChannelShortName & "名称</option>"
    Response.Write "</select>"
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "<select name='ClassID'><option value=''>所有栏目</option>" & GetClass_Option(1,ClassID) & "</select>"
    end if
    Response.Write "<input type='text' name='keyword'  size='15' value='关键字' maxlength='50' onFocus='this.select();'>"
    Response.Write "<input type='submit' name='Submit'  value='搜索'>"
    Response.Write "<input name='SearchType' type='hidden' id='SearchType' value='-1'>"
    Response.Write "<input name='ManageType' type='hidden' id='ManageType' value='" & ManageType & "'>"
    Response.Write "<input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "</td></form>"
    Response.Write "</tr>"
End If
Response.Write "</table>" & vbCrLf


Select Case Action
Case "Add"
    Call Add
Case "Modify"
    Call Modify
Case "SaveAdd", "SaveModify"
    Call SaveProduct
Case "SetOnTop", "CancelOnTop", "SetElite", "CancelElite", "SetHot", "CancelHot"
    Call SetProperty
Case "Show"
    Call Show
Case "Del"
    Call Del
Case "ConfirmDel"
    Call ConfirmDel
Case "ClearRecyclebin"
    Call ClearRecyclebin
Case "Restore"
    Call Restore
Case "RestoreAll"
    Call RestoreAll
Case Else
    Call main
End Select
If FoundErr = True Then
    Call WriteErrMsg(ErrMsg, ComeUrl)
End If
Response.Write "</body></html>"
Call CloseConn


Sub main()
    Dim rsProductList, sql, Querysql
	If ClassID > 0 Then
        Set tClass = Conn.Execute("select ClassName,RootID,ParentID,Depth,ParentPath,Child,arrChildID from PW_Class where ClassID=" & ClassID)
        If tClass.BOF And tClass.EOF Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>找不到指定的栏目</li>"
        Else
            ClassName = tClass(0)
            RootID = tClass(1)
            ParentID = tClass(2)
            Depth = tClass(3)
            ParentPath = tClass(4)
            Child = tClass(5)
            arrChildID = tClass(6)
        End If
        Set tClass = Nothing
    End If
    If FoundErr = True Then Exit Sub
    Call ShowJS_Main(ChannelShortName)
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "<br><table width='100%' border='0' align='center' cellpadding='2' cellspacing='1' class='border'>"
		Response.Write "  <tr class='title'>"
		Response.Write "    <td height='22'>" & GetRootClass() & "</td>"
		Response.Write "  </tr>" & GetChild_Root() & ""
		Response.Write "</table>"
	End if
	Response.write "<br>"
    Select Case ManageType
    Case "Recyclebin"
        Call ShowContentManagePath(ChannelShortName & "回收站管理")
    Case Else
        Call ShowContentManagePath(ChannelShortName & "管理")
    End Select
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'><tr>"
    Response.Write "    <form name='myform' method='Post' action='Admin_Product.asp' onsubmit='return ConfirmDel();'>"
    Response.Write "     <td><table class='border' border='0' cellspacing='1' width='100%' cellpadding='0'>"
    Response.Write "          <tr class='title' height='22'> "
	Response.Write "            <td height='22' width='30' align='center'><strong>选中</strong></td>"
    Response.Write "            <td width='25' align='center'><strong>ID</strong></td>"
    Response.Write "            <td align='center' ><strong>" & ChannelShortName & "名称</strong></td>"
	if FoundInArr(FieldShow, "Hits", ",") then
		Response.Write "            <td width='110' align='center' ><strong>点击数</strong></td>"
	End if
	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "            <td width='80' align='center' ><strong>" & ChannelShortName & "属性</strong></td>"
	end if
	Select Case ManageType
	Case "Recyclebin"
		Response.Write "            <td width='100' align='center' ><strong>回收站操作</strong></td>"
	Case Else
		Response.Write "            <td width='60' align='center' ><strong>操作</strong></td>"
	End Select
    Response.Write "          </tr>"
	
	If ClassID = -1 Or (ClassID > 0 And Child = 0) Then
		sql = sql & "select top " & MaxPerPage & " P.ClassID,P.ProductID,P.ProductNum,P.ProductName,P.ProductModel,P.ProductStandard,P.ProductThumb,P.Keyword,"
		sql = sql & "P.Price,P.Price_Original,P.Price_Member,P.Unit,P.OnTop,P.IsHot,P.IsElite,P.UpdateTime,P.Hits"
		sql = sql & " from PW_Product P "
	Else
		sql = sql & "select top " & MaxPerPage & " P.ClassID,P.ProductID,C.ClassName,P.ProductNum,P.ProductName,P.ProductModel,P.ProductStandard,P.ProductThumb,P.Keyword,"
		sql = sql & "P.Price,P.Price_Original,P.Price_Member,P.Unit,P.OnTop,P.IsHot,P.IsElite,P.UpdateTime,P.Hits"
		sql = sql & " from PW_Product P left join PW_Class C on P.ClassID=C.ClassID "
	End If
    Querysql = " where P.ChannelID=" & ChannelID
    If ManageType = "Recyclebin" Then
        Querysql = Querysql & " and P.Deleted=" & PE_True & ""
    Else
        Querysql = Querysql & " and P.Deleted=" & PE_False & ""
    End If
    If ClassID <> 0 Then
        If Child > 0 Then
            Querysql = Querysql & " and P.ClassID in (" & arrChildID & ")"
        Else
            Querysql = Querysql & " and P.ClassID=" & ClassID
        End If
    End If
    If Keyword <> "" Then
		Querysql = Querysql & " and P.ProductName like '%" & Keyword & "%' "
    End If
	totalPut = PE_CLng(Conn.Execute("select Count(*) from PW_Product P " & Querysql)(0))
    If CurrentPage < 1 Then
        CurrentPage = 1
    End If
    If (CurrentPage - 1) * MaxPerPage > totalPut Then
        If (totalPut Mod MaxPerPage) = 0 Then
            CurrentPage = totalPut \ MaxPerPage
        Else
            CurrentPage = totalPut \ MaxPerPage + 1
        End If
    End If
    If CurrentPage > 1 Then
            Querysql = Querysql & " and P.ProductID < (select min(ProductID) from (select top " & ((CurrentPage - 1) * MaxPerPage) & " P.ProductID from PW_Product P " & Querysql & " order by P.ProductID desc) as QueryProduct)"
    End If
	sql = sql & Querysql & " order by P.ProductID desc"
    Set rsProductList = Server.CreateObject("ADODB.Recordset")
    rsProductList.Open sql, Conn, 1, 1
    
    If rsProductList.BOF And rsProductList.EOF Then
        totalPut = 0
        Response.Write "<tr class='tdbg'><td colspan='20' align='center'><br>"

        If ClassID > 0 Then
            Response.Write "此栏目及其子栏目中没有任何"
        Else
            Response.Write "没有任何"
        End If
		Response.Write ChannelShortName & "！"
		Response.Write "<br><br></td></tr>"
    Else
        Dim ProductNum
        ProductNum = 0
        Do While Not rsProductList.EOF
            Response.Write "      <tr class='tdbg' onmouseout=""this.className='tdbg'"" onmouseover=""this.className='tdbgmouseover'"">"
			Response.Write "        <td width='30' align='center'><input name='ProductID' type='checkbox' onclick='CheckItem(this)' id='ProductID' value='" & rsProductList("ProductID") & "'></td>"
			Response.Write "        <td width='25' align='center'>" & rsProductList("ProductID") & "</td>"
            Response.Write "        <td>"
			If rsProductList("ClassID") <> ClassID And ClassID <> -1 Then
				Response.Write "<a href='" & FileName & "&ClassID=" & rsProductList("ClassID") & "'>["
				If rsProductList("ClassName") <> "" Then
					Response.Write rsProductList("ClassName")
				Else
					Response.Write "<font color='gray'>不属于任何栏目</font>"
				End If
				Response.Write "]</a>&nbsp;"
			End If
            Response.Write "<a href='Admin_Product.asp?ChannelID=" & ChannelID & "&Action=Show&ProductID=" & rsProductList("ProductID") & "'"
            Response.Write " title='" & ChannelShortName & "名称：" & rsProductList("ProductName") & vbCrLf
			if FoundInArr(FieldShow, "ProductModel", ",") then
				Response.Write ChannelShortName & "型号：" & rsProductList("ProductModel") & vbCrLf
			end if
			if FoundInArr(FieldShow, "ProductStandard", ",") then
				Response.Write ChannelShortName & "规格：" & rsProductList("ProductStandard") & vbCrLf
			end if
			if FoundInArr(FieldShow, "Keyword", ",") then
				Response.Write "关 键 字：" & Mid(rsProductList("Keyword"), 2, Len(rsProductList("Keyword")) - 2) & vbCrLf
			end if
			if FoundInArr(FieldShow, "UpdateTime", ",") then
				Response.Write "更新时间：" & rsProductList("UpdateTime") & vbCrLf
			end if
            Response.Write "'>"
            Response.Write rsProductList("ProductName") & "</a>"
            Response.Write "</td>"
			if FoundInArr(FieldShow, "Hits", ",") then
				Response.Write "      <td align='center'>" & rsProductList("Hits") & "</td>"
			end if
			if FoundInArr(FieldShow, "Property", ",") then
                Response.Write "    <td width='80' align='center'>"
                If rsProductList("OnTop") = True Then
                    Response.Write "<font color=blue>顶</font> "
                Else
                    Response.Write "&nbsp;&nbsp;"
                End If
                If rsProductList("IsHot") = True Then
                    Response.Write "<font color=red>热</font> "
                Else
                    Response.Write "&nbsp;&nbsp;"
                End If
                If rsProductList("IsElite") = True Then
                    Response.Write "<font color=green>荐</font> "
                Else
                    Response.Write "&nbsp;&nbsp;"
                End If
                If Trim(rsProductList("ProductThumb")) <> "" Then
                    Response.Write "<font color=blue>图</font>"
                Else
                    Response.Write "&nbsp;&nbsp;"
                End If
                Response.Write "    </td>"
			end if
			Select Case ManageType
			Case "Recyclebin"
				if Purview_Del then
					Response.Write "<td width='100' align='center'>"
					Response.Write "<a href='Admin_Product.asp?ChannelID=" & ChannelID & "&Action=ConfirmDel&ProductID=" & rsProductList("ProductID") & "' onclick=""return confirm('确定要彻底删除此" & ChannelShortName & "吗？彻底删除后将无法还原！');"">彻底删除</a> "
					Response.Write "<a href='Admin_Product.asp?ChannelID=" & ChannelID & "&Action=Restore&ProductID=" & rsProductList("ProductID") & "'>还原</a>"
					Response.Write "</td>"
				end if
			Case Else
				Response.Write "    <td width='60' align='center'>"
				if Purview_Modify then
					Response.Write "<a href='Admin_Product.asp?ChannelID=" & ChannelID & "&Action=Modify&ProductID=" & rsProductList("ProductID") & "'>修改</a>&nbsp;"
				end if
				if Purview_Del then
					Response.Write "<a href='Admin_Product.asp?ChannelID=" & ChannelID & "&Action=Del&ProductID=" & rsProductList("ProductID") & "' onclick=""return confirm('确定要删除此" & ChannelShortName & "吗？删除后你还可以从回收站中还原。');"">删除</a>&nbsp;"
				end if
				Response.Write "</td>"
			End Select
            Response.Write "</tr>"

            ProductNum = ProductNum + 1
            If ProductNum >= MaxPerPage Then Exit Do
            rsProductList.MoveNext
        Loop
    End If
    rsProductList.Close
    Set rsProductList = Nothing
    Response.Write "</table>"
    Response.Write "<table width='100%' border='0' cellpadding='0' cellspacing='0'>"
    Response.Write "  <tr height='30'>"
	Response.Write "    <td width='200'><input name='chkAll' type='checkbox' id='chkAll' onclick='CheckAll(this.form)' value='checkbox'>选中本页显示的所有" & ChannelShortName & "</td>"
    Response.Write "    <td align='left'>"
    Select Case ManageType
    Case "Recyclebin"
        if Purview_Del then
            Response.Write "<input name='submit1' type='submit' id='submit1' onClick=""document.myform.Action.value='ConfirmDel'"" value=' 彻底删除 '>&nbsp;"
            Response.Write "<input name='Submit2' type='submit' id='Submit2' onClick=""document.myform.Action.value='ClearRecyclebin'"" value='清空回收站'>&nbsp;&nbsp;&nbsp;&nbsp;"
            Response.Write "<input name='Submit3' type='submit' id='Submit3' onClick=""document.myform.Action.value='Restore'"" value='还原选定的" & ChannelShortName & "'>&nbsp;"
            Response.Write "<input name='Submit4' type='submit' id='Submit4' onClick=""document.myform.Action.value='RestoreAll'"" value='还原所有" & ChannelShortName & "'>"
        End If
     Case Else
            Response.Write "<input type='submit' name='submit3' value='设为推荐' onClick=""document.myform.Action.value='SetElite'"">&nbsp;"
            Response.Write "<input type='submit' name='submit4' value='取消推荐' onClick=""document.myform.Action.value='CancelElite'"">&nbsp;"
            Response.Write "<input type='submit' name='submit3' value='设为热卖' onClick=""document.myform.Action.value='SetHot'"">&nbsp;"
            Response.Write "<input type='submit' name='submit4' value='取消热卖' onClick=""document.myform.Action.value='CancelHot'"">&nbsp;"
	 	if Purview_Del then
            Response.Write "<input type='submit' name='submit5' value='批量删除' onClick=""document.myform.Action.value='Del'"">&nbsp;"
        End If
    End Select
    Response.Write "<input name='Action' type='hidden' id='Action' value=''>"
    Response.Write "<input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "  </td></tr>"
    Response.Write "</table>"
    Response.Write "</td>"
    Response.Write "</form></tr></table>"
    If totalPut > 0 Then
        Response.Write ShowPage(strFileName, totalPut, MaxPerPage, CurrentPage, True, True, ChannelItemUnit & ChannelShortName & "", True)
    End If
	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "<br><b>" & ChannelShortName & "属性中的各项含义：</b><br>"
		Response.Write "<font color=blue>顶</font>----固顶" & ChannelShortName & "，<font color=red>热</font>----热门" & ChannelShortName & "，<font color=green>荐</font>----推荐" & ChannelShortName & "，<font color=blue>图</font>----有" & ChannelShortName & "缩略图<br><br>"
	end if
End Sub



Sub ShowJS_Product()
    Response.Write "<script language = 'JavaScript'>" & vbCrLf
    Response.Write "function SelectProduct(){" & vbCrLf
    Response.Write "  var arr=showModalDialog('Admin_SelectFile.asp?ChannelID=" & ChannelID & "&dialogtype=productthumb', '', 'dialogWidth:820px; dialogHeight:600px; help: no; scroll: yes; status: no');" & vbCrLf
    Response.Write "  if(arr!=null){" & vbCrLf
    Response.Write "    var ss=arr.split('|');" & vbCrLf
    Response.Write "    document.myform.ProductThumb.value=ss[0];" & vbCrLf
    Response.Write "  }" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "function CheckForm()" & vbCrLf
    Response.Write "{" & vbCrLf
	if FoundInArr(FieldShow, "Title", ",") then
		Response.Write "  if (document.myform.ProductName.value==''){" & vbCrLf
		Response.Write "    alert('" & ChannelShortName & "名称不能为空！');" & vbCrLf
		Response.Write "    document.myform.ProductName.focus();" & vbCrLf
		Response.Write "    return false;" & vbCrLf
		Response.Write "  }" & vbCrLf
	End if
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "  var obj=document.myform.ClassID;" & vbCrLf
		Response.Write "  var iCount=0;" & vbCrLf
		Response.Write "  for(var i=0;i<obj.length;i++){" & vbCrLf
		Response.Write "    if(obj.options[i].selected==true){" & vbCrLf
		Response.Write "      iCount=iCount+1;" & vbCrLf
		Response.Write "    }" & vbCrLf
		Response.Write "  }" & vbCrLf
		Response.Write "  if (iCount==0){" & vbCrLf
		Response.Write "    alert('请选择所属栏目！');" & vbCrLf
		Response.Write "    document.myform.ClassID.focus();" & vbCrLf
		Response.Write "    return false;" & vbCrLf
		Response.Write "  }" & vbCrLf
	End if
    Dim rsField
    Set rsField = Conn.Execute("select * from PW_Field where ChannelID=" & ChannelID & "")
    Do While Not rsField.EOF
        If rsField("ShowOnForm") = True And rsField("EnableNull") = False  Then
			Response.Write "  if (document.myform."&rsField("FieldName")&".value==''){" & vbCrLf
			Response.Write "    alert('"&rsField("Title")&"不能为空！');" & vbCrLf
			Response.Write "    document.myform."&rsField("FieldName")&".focus();" & vbCrLf
			Response.Write "    return false;" & vbCrLf
			Response.Write "  }" & vbCrLf
        End If
        rsField.MoveNext
    Loop
	if FoundInArr(FieldShow, "Keyword", ",") then
		Response.Write "  if (document.myform.Keyword.value==''){" & vbCrLf
		Response.Write "    alert('关键字不能为空！');" & vbCrLf
		Response.Write "    document.myform.Keyword.focus();" & vbCrLf
		Response.Write "    return false;" & vbCrLf
		Response.Write "  }" & vbCrLf
	end if
	if FoundInArr(FieldShow, "Unit", ",") then
		Response.Write "  if (document.myform.Unit.value==''){" & vbCrLf
		Response.Write "    alert('" & ChannelShortName & "单位不能为空！');" & vbCrLf
		Response.Write "    document.myform.Unit.focus();" & vbCrLf
		Response.Write "    return false;" & vbCrLf
		Response.Write "  }" & vbCrLf
	end if
	if FoundInArr(FieldShow, "Price", ",") then
		Response.Write "  if (document.myform.Price.value==''){" & vbCrLf
		Response.Write "    alert('" & ChannelShortName & "当前零售价不能为空！');" & vbCrLf
		Response.Write "    document.myform.Price.focus();" & vbCrLf
		Response.Write "    return false;" & vbCrLf
		Response.Write "  }" & vbCrLf
	end if
	if FoundInArr(FieldShow, "Price_Original", ",") then
		Response.Write "  if (document.myform.Price_Original.value==''){" & vbCrLf
		Response.Write "    alert('" & ChannelShortName & "原始零售价不能为空！');" & vbCrLf
		Response.Write "    document.myform.Price_Original.focus();" & vbCrLf
		Response.Write "    return false;" & vbCrLf
		Response.Write "  }" & vbCrLf
	end if
	if FoundInArr(FieldShow, "Price_Member", ",") then
		Response.Write "  if (document.myform.Price_Member.value==''){" & vbCrLf
		Response.Write "    alert('" & ChannelShortName & "会员零售价不能为空！');" & vbCrLf
		Response.Write "    document.myform.Price_Member.focus();" & vbCrLf
		Response.Write "    return false;" & vbCrLf
		Response.Write "  }" & vbCrLf
	end if
	if FoundInArr(FieldShow, "Price_Member", ",") then
		Response.Write "  var oEditor  =FCKeditorAPI.GetInstance(""ProductExplain"");" & vbCrLf
		Response.Write "  var Content =oEditor.GetXHTML();" & vbCrLf
		Response.Write "  if (Content==null || Content==''){" & vbCrLf
		Response.Write "     alert('" & ChannelShortName & "详细说明不能为空！');" & vbCrLf
		Response.Write "     oEditor.Focus();" & vbCrLf
		Response.Write "     return(false);" & vbCrLf
		Response.Write "  }" & vbCrLf
	end if
    Response.Write "}" & vbCrLf
    Response.Write "function regInput(obj, reg, inputStr)" & vbCrLf
    Response.Write "{" & vbCrLf
    Response.Write "    var docSel = document.selection.createRange()" & vbCrLf
    Response.Write "    if (docSel.parentElement().tagName != ""INPUT"")    return false" & vbCrLf
    Response.Write "    oSel = docSel.duplicate()" & vbCrLf
    Response.Write "    oSel.text = """"" & vbCrLf
    Response.Write "    var srcRange = obj.createTextRange()" & vbCrLf
    Response.Write "    oSel.setEndPoint(""StartToStart"", srcRange)" & vbCrLf
    Response.Write "    var str = oSel.text + inputStr + srcRange.text.substr(oSel.text.length)" & vbCrLf
    Response.Write "    return reg.test(str)" & vbCrLf
    Response.Write "}" & vbCrLf
    Response.Write "</script>" & vbCrLf
End Sub

Sub Add()
	Call ShowJS_Product()
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_Product.asp?ChannelID=" & ChannelID & "'>" & ChannelName & "管理</a>&nbsp;&gt;&gt;&nbsp;添加" & ChannelShortName & "</td></tr></table>"
    Response.Write "<form method='POST' name='myform' onSubmit='return CheckForm();' action='Admin_Product.asp' target='_self'>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='5' cellspacing='0' class='border'>"
    Response.Write "  <tr align='center'>"
    Response.Write "    <td class='tdbg'>"
    Response.Write "      <table width='98%' border='0' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>"
	
	if FoundInArr(FieldShow, "ProductNum", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "编号：</td>"
		Response.Write "            <td><input name='ProductNum' type='text' value='" & GetNumString() & "' size='20' maxlength='20' disabled> <input type='checkbox' name='AutoGetNum' value='Yes' checked onclick='document.myform.ProductNum.disabled=this.checked'>自动编号</td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "Title", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "名称：</td>"
		Response.Write "            <td><input name='ProductName' type='text' value='' autocomplete='off' size='50' maxlength='255'><font color='#FF0000'>*</font><input type='button' name='checksame' value='检查是否存在相同的" & ChannelShortName & "名' onclick=""showModalDialog('Admin_CheckSameTitle.asp?ModuleType=" & ModuleType & "&Title='+document.myform.ProductName.value,'checksame','dialogWidth:350px; dialogHeight:250px; help: no; scroll: no; status: no');"">"
		Response.Write "            </td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>所属栏目：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <select name='ClassID'>" & GetClass_Option(1,ClassID) & "</select>" &vbcrlf
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Keyword", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>关键字：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='Keyword' type='text' style=""clear:both"" id='Keyword' value='" & Trim(Session("Keyword")) & "' size='50' maxlength='255'> <font color='#FF0000'>*</font> "
		Response.Write "              <font color='#0000FF'>用来查找相关" & ChannelShortName & "，可输入多个关键字，中间用<font color='#FF0000'>“|”</font>隔开。不能出现&quot;'&?;:()等字符。</font>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Unit", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "单位：</td>"
		Response.Write "            <td><input name='Unit' type='text' value='" & Session("Unit") & "' size='10' maxlength='20' style='text-align: center;'> &lt;&lt; <select name='UnitList' onchange='document.myform.Unit.value=this.value;'>" & GetAllUnit() & "</select></td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "Weight", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "重量：</td>"
		Response.Write "            <td><input name='Weight' type='text' value='' size='10' maxlength='10' style='text-align: center;'> 千克（Kg）</td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "ProductModel", ",") then
        Response.Write "          <tr class='tdbg'>"
        Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "型号：</td>"
        Response.Write "            <td><input name='ProductModel' type='text' value='' size='50' maxlength='50'> &lt;&lt; <select name='ModelList' onchange='document.myform.ProductModel.value=this.value;'>" & GetAllModel() & "</select></td>"
        Response.Write "          </tr>"
	End if
	if FoundInArr(FieldShow, "ProductStandard", ",") then
        Response.Write "          <tr class='tdbg'>"
        Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "规格：</td>"
        Response.Write "            <td><input name='ProductStandard' type='text' value='' size='50' maxlength='50'> &lt;&lt; <select name='StandardList' onchange='document.myform.ProductStandard.value=this.value;'>" & GetAllStandard() & "</select></td>"
        Response.Write "          </tr>"
	End if
	if FoundInArr(FieldShow, "ProducerName", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>生 产 商：</td>"
		Response.Write "            <td><input name='ProducerName' type='text' value='" & Trim(Session("ProducerName")) & "' size='50' maxlength='50'></td>"
		Response.Write "          </tr>"
	End if
	if FoundInArr(FieldShow, "TrademarkName", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>品牌/商标：</td>"
		Response.Write "            <td><input name='TrademarkName' type='text' value='" & Trim(Session("TrademarkName")) & "' size='50' maxlength='50'></td>"
		Response.Write "          </tr>"
	End if
	Call ShowTabs_MyField_Add()
	if FoundInArr(FieldShow, "Price", ",") or FoundInArr(FieldShow, "Price_Original", ",") or FoundInArr(FieldShow, "Price_Member", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "价格：</td>"
		Response.Write "            <td><table width='100%' border='0' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>"
		if FoundInArr(FieldShow, "Price_Original", ",") then
			Response.Write "<tr valign='top' class='tdbg'><td align='right' width='80'><font color='red'>原始零售价：</font></td><td align='left'><input name='Price_Original' type='text' value='' size='10' maxlength='10' onchange='getprice(this.value)' style='text-align: right;' onKeyPress= ""return regInput(this,/^\d*\.?\d{0,2}$/,String.fromCharCode(event.keyCode))"" onpaste= ""return regInput(this,/^\d*\.?\d{0,2}$/,window.clipboardData.getData('Text'))"" ondrop= ""return regInput(this,    /^\d*\.?\d{0,2}$/,event.dataTransfer.getData('Text'))""><font color='red'>元 *</font></td></tr>"
		end if
		if FoundInArr(FieldShow, "Price", ",") then
		Response.Write "<tr valign='top' class='tdbg'><td align='right' width='80'><font color='blue'>当前零售价：</font></td><td align='left'><input name='Price' type='text' value='' size='10' maxlength='10' style='text-align: right;' onKeyPress= ""return regInput(this,/^\d*\.?\d{0,2}$/,String.fromCharCode(event.keyCode))"" onpaste= ""return regInput(this,/^\d*\.?\d{0,2}$/,window.clipboardData.getData('Text'))"" ondrop= ""return regInput(this,    /^\d*\.?\d{0,2}$/,event.dataTransfer.getData('Text'))""><font color='blue'>元  *</font></td></tr>"
		end if
		if FoundInArr(FieldShow, "Price_Member", ",") then
			Response.Write "<tr valign='top' class='tdbg'><td align='right' width='80'>会员零售价：</td><td align='left'><input name='Price_Member' type='text' value='' size='10' maxlength='10' style='text-align: right;' onKeyPress= ""return regInput(this,/^\d*\.?\d{0,2}$/,String.fromCharCode(event.keyCode))"" onpaste= ""return regInput(this,/^\d*\.?\d{0,2}$/,window.clipboardData.getData('Text'))"" ondrop= ""return regInput(this,    /^\d*\.?\d{0,2}$/,event.dataTransfer.getData('Text'))"">元 *</td></tr>"
		end if
		Response.Write "          </table></td></tr>"
	End if
	if FoundInArr(FieldShow, "ProductIntro", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' valign='middle' class='tdbg5'>" & ChannelShortName & "简介：</td>"
		Response.Write "            <td>用于首页及栏目页显示，不要超过255个字符<br><textarea name='ProductIntro' cols='67' rows='3' id='ProductIntro'></textarea></td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "ProductExplain", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' valign='bottom' class='tdbg5'>详细介绍：<br><br>"
		If IsObjInstalled("Microsoft.XMLHTTP") = True Then
			Response.Write "<table><tr><td><input type='checkbox' name='SaveRemotePic' value='Yes' checked ></td><td>自动下载" & ChannelShortName & "介绍里的图片</td>"
			Response.Write "</tr></table>"
		End IF	
		Response.Write "<iframe id='frmPreview' width='120' height='150' frameborder='1' src='Admin_imgPreview.asp'></iframe></td>"
		Response.Write "            <td  valign='top'>" &vbcrlf
		Call ShowFckEditor("Default","","ProductExplain","100%","300")
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "ProductThumb", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "缩略图：</td>"
		Response.Write "            <td><input name='ProductThumb' type='text' id='ProductThumb' size='40' maxlength='200'> <input type='button' name='Button2' value='从已上传图片中选择' onclick='SelectProduct()' class='Button' >&nbsp;不填默认抓取内容中的第一张图片"
		Response.Write "              <br>上传缩略图：<iframe style='top:2px' id='UploadFiles' src='upload.asp?ChannelID=" & ChannelID & "&dialogtype=Productpic' frameborder=0 scrolling=no width='450' height='25'></iframe>"
		Response.Write "            </td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "属性：</td>"
		Response.Write "            <td> <input name='OnTop' type='checkbox' id='OnTop' value='Yes'> 固顶" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp; "
		Response.Write "<input name='IsHot' type='checkbox' id='IsHot' value='Yes'> 热门" & ChannelShortName & "&nbsp;&nbsp;&nbsp; "
		Response.Write "<input name='IsElite' type=checkbox id='IsElite' value='Yes'> 推荐" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp;  "
		Response.Write "          </td></tr>"
	end if
	if FoundInArr(FieldShow, "Hits", ",") then
        Response.Write "          <tr class='tdbg'>" &vbcrlf
        Response.Write "            <td width='120' align='right' class='tdbg5'>点击数初始值：</td>" &vbcrlf
        Response.Write "            <td>" &vbcrlf
        Response.Write "              <input name='Hits' type='text' id='Hits' value='0' size='10' maxlength='10' style='text-align:center'>&nbsp;&nbsp; <font color='#0000FF'>这功能是提供给管理员作弊用的。不过尽量不要用呀！^_^</font>"
        Response.Write "            </td>" &vbcrlf
        Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "UpdateTime", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>录入时间：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='UpdateTime' type='text' id='UpdateTime' value='" & Now() & "' maxlength='50'> 时间格式为“年-月-日 时:分:秒”，如：<font color='#0000FF'>2003-5-12 12:32:47</font>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
    Response.Write "        </table>" &vbcrlf
    Response.Write "      </td>" &vbcrlf
    Response.Write "    </tr>" &vbcrlf
    Response.Write "  </table>" &vbcrlf
    Response.Write "  <p align='center'>"
    Response.Write "   <input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "   <input name='Action' type='hidden' id='Action' value='SaveAdd'>"
    Response.Write "   <input name='add' type='submit'  id='Add' value=' 添 加 ' style='cursor:hand;'>&nbsp;&nbsp;"
    Response.Write "   <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Product.asp?ChannelID=" & ChannelID & "&Action=Manage';"" style='cursor:hand;'>"
    Response.Write "  </p><br>"
    Response.Write "</form>"
End Sub




Sub Modify()
    Dim rsProduct
    If ProductID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定要修改的" & ChannelShortName & "ID</li>"
        Exit Sub
    Else
        ProductID = PE_CLng(ProductID)
    End If
    Set rsProduct = Conn.Execute("select * from PW_Product where ProductID=" & ProductID & "")
    If rsProduct.BOF And rsProduct.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到" & ChannelShortName & "</li>"
        rsProduct.Close
        Set rsProduct = Nothing
        Exit Sub
    End If

    ClassID = rsProduct("ClassID")
    If FoundErr = True Then
        rsProduct.Close
        Set rsProduct = Nothing
        Exit Sub
    End If
    Call ShowJS_Product
    Response.Write "<br><table width='100%'><tr><td align='left'>您现在的位置：<a href='Admin_Product.asp?ChannelID=" & ChannelID & "'>" & ChannelName & "管理</a>&nbsp;&gt;&gt;&nbsp;修改" & ChannelShortName & "</td></tr></table>"
    Response.Write "<form method='POST' name='myform' onSubmit='return CheckForm();' action='Admin_Product.asp' target='_self'>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='5' cellspacing='0' class='border'>"
    Response.Write "  <tr align='center'>"
    Response.Write "    <td class='tdbg'>"
    Response.Write "      <table width='98%' border='0' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>"
	
	if FoundInArr(FieldShow, "ProductNum", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "编号：</td>"
		Response.Write "            <td><input name='ProductNum' type='text' value='" & PE_HTMLEncode(rsProduct("ProductNum")) & "' size='20' maxlength='20'></td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "Title", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "名称：</td>"
		Response.Write "            <td><input name='ProductName' type='text' value='" & rsProduct("ProductName") & "' autocomplete='off' size='50' maxlength='255'><font color='#FF0000'>*</font>"
		Response.Write "            </td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>所属栏目：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <select name='ClassID'>" & GetClass_Option(1,ClassID) & "</select>" &vbcrlf
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Keyword", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>关键字：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='Keyword' type='text' style=""clear:both"" id='Keyword' value='" & Mid(rsProduct("Keyword"), 2, Len(rsProduct("Keyword")) - 2) & "' size='50' maxlength='255'> <font color='#FF0000'>*</font> "
		Response.Write "              <font color='#0000FF'>用来查找相关" & ChannelShortName & "，可输入多个关键字，中间用<font color='#FF0000'>“|”</font>隔开。不能出现&quot;'&?;:()等字符。</font>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "Unit", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "单位：</td>"
		Response.Write "            <td><input name='Unit' type='text' value='" & rsProduct("Unit") & "' size='10' maxlength='20' style='text-align: center;'> &lt;&lt; <select name='UnitList' onchange='document.myform.Unit.value=this.value;'>" & GetAllUnit() & "</select></td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "Weight", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "重量：</td>"
		Response.Write "            <td><input name='Weight' type='text' value='" & rsProduct("Weight") & "' size='10' maxlength='10' style='text-align: center;'> 千克（Kg）</td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "ProductModel", ",") then
        Response.Write "          <tr class='tdbg'>"
        Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "型号：</td>"
        Response.Write "            <td><input name='ProductModel' type='text' value='"&rsProduct("ProductModel")&"' size='50' maxlength='50'> &lt;&lt; <select name='ModelList' onchange='document.myform.ProductModel.value=this.value;'>" & GetAllModel() & "</select></td>"
        Response.Write "          </tr>"
	End if
	if FoundInArr(FieldShow, "ProductStandard", ",") then
        Response.Write "          <tr class='tdbg'>"
        Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "规格：</td>"
        Response.Write "            <td><input name='ProductStandard' type='text' value='"& rsProduct("ProductStandard") &"' size='50' maxlength='50'> &lt;&lt; <select name='StandardList' onchange='document.myform.ProductStandard.value=this.value;'>" & GetAllStandard() & "</select></td>"
        Response.Write "          </tr>"
	End if
	if FoundInArr(FieldShow, "ProducerName", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>生 产 商：</td>"
		Response.Write "            <td><input name='ProducerName' type='text' value='" & rsProduct("ProducerName") & "' size='50' maxlength='50'></td>"
		Response.Write "          </tr>"
	End if
	if FoundInArr(FieldShow, "TrademarkName", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>品牌/商标：</td>"
		Response.Write "            <td><input name='TrademarkName' type='text' value='" & rsProduct("TrademarkName") & "' size='50' maxlength='50'></td>"
		Response.Write "          </tr>"
	End if
	Call ShowTabs_MyField_Modify(rsProduct)
	if FoundInArr(FieldShow, "Price", ",") or FoundInArr(FieldShow, "Price_Original", ",") or FoundInArr(FieldShow, "Price_Member", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "价格：</td>"
		Response.Write "            <td><table width='100%' border='0' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>"
		if FoundInArr(FieldShow, "Price_Original", ",") then
			Response.Write "<tr valign='top' class='tdbg'><td align='right' width='80'><font color='red'>原始零售价：</font></td><td align='left'><input name='Price_Original' type='text' value='" & rsProduct("Price_Original") & "' size='10' maxlength='10' onchange='getprice(this.value)' style='text-align: right;' onKeyPress= ""return regInput(this,/^\d*\.?\d{0,2}$/,String.fromCharCode(event.keyCode))"" onpaste= ""return regInput(this,/^\d*\.?\d{0,2}$/,window.clipboardData.getData('Text'))"" ondrop= ""return regInput(this,    /^\d*\.?\d{0,2}$/,event.dataTransfer.getData('Text'))""><font color='red'>元 *</font></td></tr>"
		end if
		if FoundInArr(FieldShow, "Price", ",") then
		Response.Write "<tr valign='top' class='tdbg'><td align='right' width='80'><font color='blue'>当前零售价：</font></td><td align='left'><input name='Price' type='text' value='" & rsProduct("Price") & "' size='10' maxlength='10' style='text-align: right;' onKeyPress= ""return regInput(this,/^\d*\.?\d{0,2}$/,String.fromCharCode(event.keyCode))"" onpaste= ""return regInput(this,/^\d*\.?\d{0,2}$/,window.clipboardData.getData('Text'))"" ondrop= ""return regInput(this,    /^\d*\.?\d{0,2}$/,event.dataTransfer.getData('Text'))""><font color='blue'>元  *</font></td></tr>"
		end if
		if FoundInArr(FieldShow, "Price_Member", ",") then
			Response.Write "<tr valign='top' class='tdbg'><td align='right' width='80'>会员零售价：</td><td align='left'><input name='Price_Member' type='text' value='" & rsProduct("Price_Member") & "' size='10' maxlength='10' style='text-align: right;' onKeyPress= ""return regInput(this,/^\d*\.?\d{0,2}$/,String.fromCharCode(event.keyCode))"" onpaste= ""return regInput(this,/^\d*\.?\d{0,2}$/,window.clipboardData.getData('Text'))"" ondrop= ""return regInput(this,    /^\d*\.?\d{0,2}$/,event.dataTransfer.getData('Text'))"">元 *</td></tr>"
		end if
		Response.Write "          </table></td></tr>"
	End if
	if FoundInArr(FieldShow, "ProductIntro", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' valign='middle' class='tdbg5'>" & ChannelShortName & "简介：</td>"
		Response.Write "            <td>用于首页及栏目页显示，不要超过255个字符<br><textarea name='ProductIntro' cols='67' rows='3' id='ProductIntro'>" & rsProduct("ProductIntro") & "</textarea></td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "ProductExplain", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' valign='bottom' class='tdbg5'>详细介绍：<br><br>"
		If IsObjInstalled("Microsoft.XMLHTTP") = True Then
			Response.Write "<table><tr><td><input type='checkbox' name='SaveRemotePic' value='Yes' checked></td><td>自动下载" & ChannelShortName & "介绍里的图片</td>"
			Response.Write "</tr></table>"
		End IF	
		Response.Write "<iframe id='frmPreview' width='120' height='150' frameborder='1' src='Admin_imgPreview.asp'></iframe></td>"
		Response.Write "            <td  valign='top'>" &vbcrlf
		Call ShowFckEditor("Default",GetPhotoUrl(rsProduct("ProductExplain")),"ProductExplain","100%","300")
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "ProductThumb", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "缩略图：</td>"
		Response.Write "            <td><input name='ProductThumb' type='text' id='ProductThumb' size='40' maxlength='200' value='" & GetPhotoUrl(rsProduct("ProductThumb")) & "' onPropertyChange=""frmPreview.img.src=((this.value == '') ? '../images/nopic.gif' : this.value);""> <input type='button' name='Button2' value='从已上传图片中选择' onclick='SelectProduct()'>"
		Response.Write "              从本产品中选择：<select name='ProductThumbList' id='ProductThumbList' onChange=""ProductThumb.value=this.value;frmPreview.img.src=((this.value == '') ? '../images/nopic.gif' : this.value);"">"
		Response.Write "                <option value=''"
		If GetPhotoUrl(rsProduct("ProductThumb")) = "" Then Response.Write "selected"
		Response.Write ">不指定缩略图</option>"
		Dim UploadFiles
		UploadFiles = GetPhotoUrl(rsProduct("UploadFiles"))
		If UploadFiles <> "" Then
			Dim IsOtherUrl
			IsOtherUrl = True
			If InStr(UploadFiles, "|") > 1 Then
				Dim arrUploadFiles, intTemp
				arrUploadFiles = Split(UploadFiles, "|")
				For intTemp = 0 To UBound(arrUploadFiles)
					If GetPhotoUrl(rsProduct("ProductThumb")) = arrUploadFiles(intTemp) Then
						Response.Write "<option value='" & arrUploadFiles(intTemp) & "' selected>" & arrUploadFiles(intTemp) & "</option>"
						IsOtherUrl = False
					Else
						Response.Write "<option value='" & arrUploadFiles(intTemp) & "'>" & arrUploadFiles(intTemp) & "</option>"
					End If
				Next
			Else
				If UploadFiles = GetPhotoUrl(rsProduct("ProductThumb")) Then
					Response.Write "<option value='" & UploadFiles & "' selected>" & UploadFiles & "</option>"
					IsOtherUrl = False
				Else
					Response.Write "<option value='" & UploadFiles & "'>" & UploadFiles & "</option>"
				End If
			End If
			If IsOtherUrl = True And GetPhotoUrl(rsProduct("ProductThumb")) <> "" Then
				Response.Write "<option value='" & GetPhotoUrl(rsProduct("ProductThumb")) & "' selected>" & GetPhotoUrl(rsProduct("ProductThumb")) & "</option>"
			End If
		End If
		Response.Write "              </select>"
		Response.Write "              <br>上传缩略图：<iframe style='top:2px' id='UploadFiles' src='upload.asp?ChannelID=" & ChannelID & "&dialogtype=Productpic' frameborder=0 scrolling=no width='450' height='25'></iframe>"
		Response.Write "            </td>"
		Response.Write "          </tr>"
	end if
	if FoundInArr(FieldShow, "Property", ",") then
		Response.Write "          <tr class='tdbg'>"
		Response.Write "            <td width='120' align='right' class='tdbg5'>" & ChannelShortName & "属性：</td>"
		Response.Write "            <td> <input name='OnTop' type='checkbox' id='OnTop' value='Yes'"
		If rsProduct("OnTop") = True Then
			Response.Write " checked"
		End If
		Response.Write ">"
		Response.Write "              固顶" & ChannelShortName & "&nbsp;&nbsp;&nbsp;&nbsp; <input name='IsHot' type='checkbox' id='IsHot' value='Yes'"
		If rsProduct("IsHot") = True Then
			Response.Write " checked"
		End If
		Response.Write ">"
		Response.Write "              热门" & ChannelShortName & "&nbsp;&nbsp;&nbsp; <input name='IsElite' type=checkbox id='IsElite' value='Yes'"
		If rsProduct("IsElite") = True Then
			Response.Write " checked"
		End If
		Response.Write ">"
		Response.Write "              推荐" & ChannelShortName & ""
		Response.Write "          </td></tr>"
	end if
	if FoundInArr(FieldShow, "Hits", ",") then
        Response.Write "          <tr class='tdbg'>" &vbcrlf
        Response.Write "            <td width='120' align='right' class='tdbg5'>点击数初始值：</td>" &vbcrlf
        Response.Write "            <td>" &vbcrlf
        Response.Write "              <input name='Hits' type='text' id='Hits' value='" & rsProduct("Hits") & "' size='10' maxlength='10' style='text-align:center'>&nbsp;&nbsp; <font color='#0000FF'>这功能是提供给管理员作弊用的。不过尽量不要用呀！^_^</font>"
        Response.Write "            </td>" &vbcrlf
        Response.Write "          </tr>" &vbcrlf
	end if
	if FoundInArr(FieldShow, "UpdateTime", ",") then
		Response.Write "          <tr class='tdbg'>" &vbcrlf
		Response.Write "            <td width='120' align='right' class='tdbg5'>录入时间：</td>" &vbcrlf
		Response.Write "            <td>" &vbcrlf
		Response.Write "              <input name='UpdateTime' type='text' id='UpdateTime' value='" & rsProduct("UpdateTime") & "' maxlength='50'> 时间格式为“年-月-日 时:分:秒”，如：<font color='#0000FF'>2003-5-12 12:32:47</font>"
		Response.Write "            </td>" &vbcrlf
		Response.Write "          </tr>" &vbcrlf
	end if
    Response.Write "        </table>" &vbcrlf
    Response.Write "      </td>" &vbcrlf
    Response.Write "    </tr>" &vbcrlf
    Response.Write "  </table>" &vbcrlf
    Response.Write "  <p align='center'>"
    Response.Write "   <input name='Action' type='hidden' id='Action' value='SaveModify'>"
    Response.Write "   <input name='ChannelID' type='hidden' id='ChannelID' value='" & ChannelID & "'>"
    Response.Write "   <input name='ProductID' type='hidden' id='ProductID' value='" & rsProduct("ProductID") & "'>"
    Response.Write "   <input name='Save' type='submit' onClick=""document.myform.Action.value='SaveModify';"" value='保存修改结果' style='cursor:hand;'>&nbsp;"
    Response.Write "   <input name='Cancel' type='button' id='Cancel' value=' 取 消 ' onClick=""window.location.href='Admin_Product.asp?ChannelID=" & ChannelID & "&Action=Manage';"" style='cursor:hand;'>"
    Response.Write "  </p><br>"
    Response.Write "</form>"
    Response.Write "<script language='javascript'>setTimeout('setpic()',1000);" & vbCrLf
    Response.Write "function setpic(){" & vbCrLf
    If rsProduct("ProductThumb") <> "" Then
    	Response.Write "frmPreview.img.src='" & GetPhotoUrl(rsProduct("ProductThumb")) & "';"
    End If
    Response.Write "}" & vbCrLf
    Response.Write "</script>"
    rsProduct.Close
    Set rsProduct = Nothing
End Sub



Sub SaveProduct()
    Dim rsProduct, sql, trs, i
    Dim ProductID, ClassID
	Dim ProductNum, AutoGetNum
	Dim ProductName, ProductModel, ProductStandard, Keyword
    Dim ProducerName, TrademarkName
    Dim ProductIntro, ProductExplain, ProductThumb, arrUploadFiles
    Dim Price, Price_Original, Price_Member, Unit
	Dim Weight, UpdateTime
    ProductID = PE_CLng(GetForm("ProductID"))
    ClassID = GetForm("ClassID")
    ProductNum = ReplaceBadChar(GetForm("ProductNum"))
    AutoGetNum = GetForm("AutoGetNum")


    ProductName = PE_HTMLEncode(GetForm("ProductName"))
    ProductModel = PE_HTMLEncode(GetForm("ProductModel"))
    ProductStandard = GetForm("ProductStandard")
    ProducerName = PE_HTMLEncode(GetForm("ProducerName"))
    TrademarkName = PE_HTMLEncode(GetForm("TrademarkName"))
    Keyword = ReplaceBadChar(GetForm("Keyword"))
    ProductIntro = GetForm("ProductIntro")
	
    For i = 1 To Request.Form("ProductExplain").Count
        ProductExplain = ProductExplain & Request.Form("ProductExplain")(i)
    Next
	ProductExplain = SetPhotoUrl(ProductExplain) '替换地址为{PW_Photourl}
    If GetForm("SaveRemotePic") = "Yes" Then
        ProductExplain = ReplaceRemoteUrl(ProductExplain)
    End If
	
	ProductThumb = GetForm("ProductThumb")
	'从内容中提取出图片
	Dim Mat,FilesArray
	UploadFiles=""
    regEx.Pattern ="<img [^>]*\bsrc=""([^"">]+)""[^>]+>" 
    Set Matches = regEx.Execute(ProductExplain)
	If   Matches.Count> 0   Then 
		For Each Mat In Matches
		if UploadFiles="" then
			UploadFiles=Mat.SubMatches(0)   
		else
			UploadFiles=UploadFiles&"|"&Mat.SubMatches(0)  
		end if 
		next
	end if
	if UploadFiles<>"" and ProductThumb="" then
		If InStr(UploadFiles, "|") = 0 Then
			ProductThumb = UploadFiles
		Else
			FilesArray = Split(UploadFiles, "|")
			ProductThumb = FilesArray(0)
		End If
	End if

    Price = PE_CDbl(GetForm("Price"))
    Price_Original = PE_CDbl(GetForm("Price_Original"))
    Price_Member = PE_CDbl(GetForm("Price_Member"))
    Unit = GetForm("Unit")
    Weight = PE_CDbl(GetForm("Weight"))
    UpdateTime = PE_CDate(GetForm("UpdateTime"))
    If Action = "SaveAdd" Then
		if FoundInArr(FieldShow, "ProductNum", ",") then
			If AutoGetNum = "Yes" Then
				ProductNum = GetNumString()
			Else
				If ProductNum = "" Then
					FoundErr = True
					ErrMsg = ErrMsg & "<li>请输入" & ChannelShortName & "编号！"
				Else
					Set trs = Conn.Execute("select top 1 ProductID from PW_Product where ProductNum='" & ProductNum & "'")
					If Not (trs.BOF And trs.EOF) Then
						FoundErr = True
						ErrMsg = ErrMsg & "<li>输入的" & ChannelShortName & "编号已经存在！</li>"
					End If
					Set trs = Nothing
				End If
			End If
		end if
    Else
		if FoundInArr(FieldShow, "ProductNum", ",") then
			If ProductNum = "" Then
				FoundErr = True
				ErrMsg = ErrMsg & "<li>请输入" & ChannelShortName & "编号！"
			Else
				Set trs = Conn.Execute("select top 1 ProductID from PW_Product where ProductID<>" & ProductID & " and ProductNum='" & ProductNum & "'")
				If Not (trs.BOF And trs.EOF) Then
					FoundErr = True
					ErrMsg = ErrMsg & "<li>输入的" & ChannelShortName & "编号已经存在！</li>"
				End If
				Set trs = Nothing
			End If
		end if
    End If
	if FoundInArr(FieldShow, "Title", ",") then
		If ProductName = "" Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>" & ChannelShortName & "名称不能为空！</li>"
		End If
	End if
	if FoundInArr(FieldShow, "ClassID", ",") then
		If ClassID = "" Or IsNull(ClassID) Or Not IsNumeric(ClassID) Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>未指定所属栏目！</li>"
		Else
			ClassID = PE_CLng(ClassID)
			Select Case ClassID
			Case -1
                ClassName = "不指定任何栏目"
                Depth = -1
                ParentPath = ""
			Case Else
				Set tClass = Conn.Execute("select ClassName,ClassType,Depth,ParentID,ParentPath,Child from PW_Class where ClassID=" & ClassID)
				If tClass.BOF And tClass.EOF Then
					FoundErr = True
					ErrMsg = ErrMsg & "<li>找不到指定的栏目！</li>"
				Else
					ClassName = tClass("ClassName")
					Depth = tClass("Depth")
					ParentPath = tClass("ParentPath")
					ParentID = tClass("ParentID")
					Child = tClass("Child")
				End If
            Set tClass = Nothing
			End Select
		end if
	else
		ClassID = -1
		Depth = -1
		ParentPath = ""
	end if
	if FoundInArr(FieldShow, "Keyword", ",") then
		If Keyword = "" Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>关键字不能为空！</li>"
		End If
	end if
	if FoundInArr(FieldShow, "ProductIntro", ",") then
		If ProductIntro <> "" Then
			If Len(ProductIntro) > 255 Then
				FoundErr = True
				ErrMsg = ErrMsg & "<li>" & ChannelShortName & "简介不能超过255字符！</li>"
			End If
		End If
	end if
	if FoundInArr(FieldShow, "ProductExplain", ",") then
		If ProductExplain = "" Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>" & ChannelShortName & "详细说明不能为空！</li>"
		End If
	end if
	if FoundInArr(FieldShow, "Price", ",") then
		If Price_Original = "" Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>请输入当前价格！</li>"
		End If
	end if
	if FoundInArr(FieldShow, "Price_Original", ",") then
		If Price_Original = "" Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>请输入原始价格！</li>"
		End If
	end if
	if FoundInArr(FieldShow, "Price_Member", ",") then
		If Price_Original = "" Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>请输入会员价格！</li>"
		End If
	end if
	if FoundInArr(FieldShow, "Unit", ",") then
		If Unit = "" Then
			FoundErr = True
			ErrMsg = ErrMsg & "<li>" & ChannelShortName & "单位不能为空！</li>"
		Else
			Session("Unit") = Unit
		End If
	end if
    Dim rsField
    Set rsField = Conn.Execute("select * from PW_Field where ChannelID=" & ChannelID & "")
    Do While Not rsField.EOF
        If rsField("EnableNull") = False and rsField("ShowOnForm") = True Then
            If GetForm(rsField("FieldName")) = "" Then
                FoundErr = True
                ErrMsg = ErrMsg & "<li>请输入" & rsField("Title") & "！</li>"
            End If
        End If
        rsField.MoveNext
    Loop
    
    If FoundErr = True Then
        Exit Sub
    End If
	
    Keyword = "|" & ReplaceBadChar(Keyword) & "|"

    Set rsProduct = Server.CreateObject("adodb.recordset")
    If Action = "SaveAdd" Then
        ProductID = GetNewID("PW_Product", "ProductID")
        sql = "select top 1 * from PW_Product"
        rsProduct.Open sql, Conn, 1, 3
        rsProduct.AddNew
        rsProduct("ProductID") = ProductID
        rsProduct("ChannelID") = ChannelID
    ElseIf Action = "SaveModify" Then
        If ProductID <= 0 Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>不能确定ProductID的值</li>"
        Else
            sql = "select * from PW_Product where ProductID=" & ProductID
            rsProduct.Open sql, Conn, 1, 3
            If rsProduct.BOF And rsProduct.EOF Then
                FoundErr = True
                ErrMsg = ErrMsg & "<li>找不到此" & ChannelShortName & "，可能已经被其他人删除。</li>"
                rsProduct.Close
                Set rsProduct = Nothing
                Exit Sub
            End If
        End If
    End If

    rsProduct("ClassID") = ClassID
    rsProduct("ProductNum") = ProductNum
    rsProduct("ProductName") = ProductName
    rsProduct("ProductModel") = ProductModel
    rsProduct("ProductStandard") = ProductStandard
    rsProduct("Keyword") = Keyword
    rsProduct("ProducerName") = ProducerName
    rsProduct("TrademarkName") = TrademarkName
    rsProduct("ProductIntro") = ProductIntro
    rsProduct("ProductExplain") = ProductExplain
    rsProduct("ProductThumb") = ProductThumb
    rsProduct("UploadFiles") = UploadFiles
    rsProduct("Price_Original") = Price_Original
    rsProduct("Price") = Price
    rsProduct("Price_Member") = Price_Member
    rsProduct("Unit") = Unit
    rsProduct("Weight") = Weight
    rsProduct("OnTop") = PE_CBool(GetForm("OnTop"))
    rsProduct("IsHot") = PE_CBool(GetForm("IsHot"))
    rsProduct("IsElite") = PE_CBool(GetForm("IsElite"))
    rsProduct("Hits") = PE_CLng(GetForm("Hits"))
    rsProduct("UpdateTime") = UpdateTime
    rsProduct("Deleted") = False
    If Not (rsField.BOF And rsField.EOF) Then
        rsField.MoveFirst
        Do While Not rsField.EOF
			If FoundInArr(FieldShow, rsField("FieldName"), ",") then
				If GetForm(rsField("FieldName")) <> "" or rsField("EnableNull")=True  Then
					rsProduct(Trim(rsField("FieldName"))) = GetForm(rsField("FieldName"))
				End If
			End if
            rsField.MoveNext
        Loop
    End If
	rsField.Close
    Set rsField = Nothing
    rsProduct.Update
    rsProduct.Close
    Set rsProduct = Nothing
    Response.Write "<br><br>"
    Response.Write "<table class='border' align=center width='500' border='0' cellpadding='2' cellspacing='1'>"
    Response.Write "  <tr align=center>"
    Response.Write "    <td  height='22' colspan='2' align='center' class='title'> "
    If Action = "SaveAdd" Then
        Response.Write "<b>添加" & ChannelShortName & "成功</b>"
    Else
        Response.Write "<b>修改" & ChannelShortName & "成功</b>"
    End If
    Response.Write "    </td>"
    Response.Write "  </tr>"
	if FoundInArr(FieldShow, "ClassID", ",") then
		Response.Write "        <tr class='tdbg'>"
		Response.Write "          <td width='100' align='right' class='tdbg5'><strong>所属栏目：</strong></td>"
		Response.Write "          <td width='400'>" & ShowClassPath() & "</td>"
		Response.Write "        </tr>"
	end if
	if FoundInArr(FieldShow, "Title", ",") then
		Response.Write "  <tr class='tdbg'> "
		Response.Write "    <td width='100' align='right' class='tdbg5'><strong>" & ChannelShortName & "名称：</strong></td>"
		Response.Write "    <td width='250'>" & PE_HTMLEncode(ProductName) & "</td>"
		Response.Write "  </tr>"
	end if
	if FoundInArr(FieldShow, "Keyword", ",") then
		Response.Write "        <tr class='tdbg'>"
		Response.Write "          <td width='100' align='right' class='tdbg5'><strong>关 键 字：</strong></td>"
		Response.Write "          <td width='400'>" & Mid(Keyword, 2, Len(Keyword) - 2) & "</td>"
		Response.Write "        </tr>"
	end if
    Response.Write "  <tr class='tdbg'> "
    Response.Write "    <td height='40' colspan='3' align='center'>"
    Response.Write "【<a href='Admin_Product.asp?ChannelID=" & ChannelID & "&Action=Modify&ProductID=" & ProductID & "'>修改此" & ChannelShortName & "</a>】&nbsp;"
    Response.Write "【<a href='Admin_Product.asp?ChannelID=" & ChannelID & "&Action=Add&ClassID=" & ClassID & "'>继续添加" & ChannelShortName & "</a>】&nbsp;"
    Response.Write "【<a href='Admin_Product.asp?ChannelID=" & ChannelID & "&Action=Manage&ClassID=" & ClassID & "'>" & ChannelShortName & "管理</a>】&nbsp;"
    Response.Write "【<a href='Admin_Product.asp?ChannelID=" & ChannelID & "&Action=Show&ProductID=" & ProductID & "'>查看" & ChannelShortName & "信息</a>】"
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "</table>" & vbCrLf
    Session("Keyword") = Trim(Request("Keyword"))
    Session("ProductModel") = ProductModel
    Session("ProductStandard") = ProductStandard
    Session("ProducerName") = ProducerName
    Session("TrademarkName") = TrademarkName
	If UseCreateHTML > 0 And ObjInstalled_FSO = True And AutoCreateType = 1 Then
       Response.Write "<br><iframe id='CreateProduct' width='100%' height='210' frameborder='0' src='Admin_CreateProduct.asp?ChannelID=" & ChannelID & "&Action=CreateProduct2&ClassID=" & ClassID & "&ProductID=" & ProductID & "&ShowBack=No'></iframe>"
    End If
End Sub


'******************************************************************************************
'以下为设置固顶、推荐等属性使用的函数，各模块实现过程类似，修改时注意同时修改各模块内容。
'******************************************************************************************

Sub SetProperty()
    If ProductID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定" & ChannelShortName & "！</li>"
    End If
    If Action = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>参数不足！</li>"
    End If
    If FoundErr = True Then Exit Sub
    
    Dim sqlProperty, rsProperty
    If InStr(ProductID, ",") > 0 Then
        ProductID = ReplaceBadChar(ProductID)
        sqlProperty = "select * from PW_Product where ProductID in (" & ProductID & ")"
    Else
        ProductID = PE_CLng(ProductID)
        sqlProperty = "select * from PW_Product where ProductID=" & ProductID
    End If
    Set rsProperty = Server.CreateObject("ADODB.Recordset")
    rsProperty.Open sqlProperty, Conn, 1, 3
    Do While Not rsProperty.EOF
            Select Case Action
            Case "SetOnTop"
                rsProperty("OnTop") = True
            Case "CancelOnTop"
                rsProperty("OnTop") = False
            Case "SetHot"
                rsProperty("IsHot") = True
            Case "CancelHot"
                rsProperty("IsHot") = False
            Case "SetElite"
                rsProperty("IsElite") = True
            Case "CancelElite"
                rsProperty("IsElite") = False
            End Select
            rsProperty.Update
        rsProperty.MoveNext
    Loop
    rsProperty.Close
    Set rsProperty = Nothing

    Call WriteSuccessMsg("操作成功！", "Admin_Product.asp?ChannelID=" & ChannelID & "")
    Call Refresh("Admin_Product.asp?ChannelID=" & ChannelID,1)
End Sub


Sub Del()
    If ProductID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定" & ChannelShortName & "！</li>"
        Exit Sub
    End If
    
    Dim sqlDel, rsDel
    Dim trs
    sqlDel = "select P.ProductID,P.ProductName,P.Deleted from PW_Product P "
    If InStr(ProductID, ",") > 0 Then
        sqlDel = sqlDel & " where P.ProductID in (" & ProductID & ") order by P.ProductID"
    Else
        sqlDel = sqlDel & " where P.ProductID=" & ProductID
    End If
    Set rsDel = Server.CreateObject("ADODB.Recordset")
    rsDel.Open sqlDel, Conn, 1, 3
    Do While Not rsDel.EOF
        	rsDel("Deleted") = True
        rsDel.Update
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing
    Call WriteSuccessMsg("操作成功！", "Admin_Product.asp?ChannelID=" & ChannelID & "")
    Call Refresh("Admin_Product.asp?ChannelID=" & ChannelID,1)
End Sub

Sub ConfirmDel()
    If ProductID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定" & ChannelShortName & "！</li>"
        Exit Sub
    End If
    
    Dim sqlDel, rsDel
    sqlDel = "select UploadFiles,ProductThumb from PW_Product where ProductID in (" & ProductID & ")"
    Set rsDel = Conn.Execute(sqlDel)
    Do While Not rsDel.EOF
         Call DelFiles(GetUploadFiles(GetPhotoUrl(rsDel("UploadFiles")), GetPhotoUrl(rsDel("ProductThumb"))))
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing
    Conn.Execute ("delete from PW_Product where ProductID in (" & ProductID & ")")
    Conn.Execute ("delete from PW_Comment where ModuleType=" & ModuleType & " and InfoID in (" & ProductID & ")")
    Call CloseConn
    Response.Redirect ComeUrl
End Sub


Sub ClearRecyclebin()
    Dim sqlDel, rsDel
    ProductID = ""
    sqlDel = "select ProductID,UploadFiles,ProductThumb from PW_Product where Deleted=" & PE_True & " and ChannelID=" & ChannelID
    Set rsDel = Conn.Execute(sqlDel)
    Do While Not rsDel.EOF
        If ProductID = "" Then
            ProductID = rsDel(0)
        Else
            ProductID = ProductID & "," & rsDel(0)
        End If
        Call DelFiles(GetUploadFiles(GetPhotoUrl(rsDel("UploadFiles")), GetPhotoUrl(rsDel("ProductThumb"))))
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing
    If ProductID <> "" Then
        Conn.Execute ("delete from PW_Product where Deleted=" & PE_True & " and ChannelID=" & ChannelID & "")
        Conn.Execute ("delete from PW_Comment where ModuleType=" & ModuleType & " and InfoID in (" & ProductID & ")")
    End If
    Call CloseConn
    Response.Redirect ComeUrl
End Sub


Sub Restore()
    If ProductID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请先选定" & ChannelShortName & "！</li>"
        Exit Sub
    End If
    
    Dim sqlDel, rsDel
    If InStr(ProductID, ",") > 0 Then
        sqlDel = "select * from PW_Product where ProductID in (" & ProductID & ")"
    Else
        sqlDel = "select * from PW_Product where ProductID=" & ProductID
    End If
    Set rsDel = Server.CreateObject("ADODB.Recordset")
    rsDel.Open sqlDel, Conn, 1, 3
    Do While Not rsDel.EOF
        rsDel("Deleted") = False
        rsDel.Update
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing

    Call WriteSuccessMsg("操作成功！", "Admin_Product.asp?ChannelID=" & ChannelID)
    Call Refresh("Admin_Product.asp?ChannelID=" & ChannelID,1)
End Sub



Sub RestoreAll()
    Dim sqlDel, rsDel
    sqlDel = "select * from PW_Product where Deleted=" & PE_True & " and ChannelID=" & ChannelID
    Set rsDel = Server.CreateObject("ADODB.Recordset")
    rsDel.Open sqlDel, Conn, 1, 3
    Do While Not rsDel.EOF
        rsDel("Deleted") = False
        rsDel.Update
        rsDel.MoveNext
    Loop
    rsDel.Close
    Set rsDel = Nothing
    Call WriteSuccessMsg("操作成功！", "Admin_Product.asp?ChannelID=" & ChannelID)
    Call Refresh("Admin_Product.asp?ChannelID=" & ChannelID,1)
End Sub


Sub Show()
    If ProductID = "" Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>请指定" & ChannelShortName & "ID！</li>"
        Exit Sub
    End If
	
    Dim rsProduct
    Set rsProduct = Conn.Execute("Select * from PW_Product where ProductID=" & ProductID & "")
    If rsProduct.BOF And rsProduct.EOF Then
        FoundErr = True
        ErrMsg = ErrMsg & "<li>找不到" & ChannelShortName & "！</li>"
        rsProduct.Close
        Set rsProduct = Nothing
        Exit Sub
    End If
    ClassID = rsProduct("ClassID")
	Select Case ClassID
        Case 0
            FoundErr = True
            ErrMsg = ErrMsg & "<li>指定的栏目不允许此操作！</li>"
		Case -1
			ClassName = "不指定任何栏目"
		Case Else
            Set tClass = Conn.Execute("select ClassName,ClassType,Depth,ParentID,ParentPath,Child from PW_Class where ClassID=" & ClassID)
            If tClass.BOF And tClass.EOF Then
                FoundErr = True
                ErrMsg = ErrMsg & "<li>找不到指定的栏目！</li>"
            Else
                ClassName = tClass("ClassName")
                Depth = tClass("Depth")
                ParentPath = tClass("ParentPath")
                ParentID = tClass("ParentID")
                Child = tClass("Child")
            End If
            Set tClass = Nothing
	end select
    If FoundErr = True Then
        rsProduct.Close
        Set rsProduct = Nothing
        Exit Sub
    End If
    Response.Write "<br>您现在的位置：&nbsp;<a href='Admin_Product.asp?ChannelID=" & ChannelID & "'>" & ChannelShortName & "管理</a>&nbsp;&gt;&gt;&nbsp;"
    If ParentID > 0 Then
        Dim sqlPath, rsPath
        sqlPath = "select ClassID,ClassName from PW_Class where ClassID in (" & ParentPath & ") order by Depth"
        Set rsPath = Conn.Execute(sqlPath)
        Do While Not rsPath.EOF
            Response.Write "<a href='Admin_Product.asp?ChannelID=" & ChannelID & "&ClassID=" & rsPath(0) & "'>" & rsPath(1) & "</a>&nbsp;&gt;&gt;&nbsp;"
            rsPath.MoveNext
        Loop
        rsPath.Close
        Set rsPath = Nothing
    End If
    Response.Write "<a href='Admin_Product.asp?ChannelID=" & ChannelID & "&ClassID=" & ClassID & "'>" & ClassName & "</a>&nbsp;&gt;&gt;&nbsp;查看" & ChannelShortName & "信息："
    Response.Write rsProduct("ProductName") & "<br><br>"
    Response.Write "<table width='100%' border='0' align='center' cellpadding='5' cellspacing='0' class='border'>"
    Response.Write "  <tr align='center'>"
    Response.Write "    <td class='tdbg' height='200' valign='top'>"
    Response.Write "      <table width='98%' border='0' cellpadding='2' cellspacing='1' bgcolor='#FFFFFF'>"
    Response.Write "        <tbody id='Tabs' style='display:'>" & vbCrLf
	if FoundInArr(FieldShow, "ProductNum", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>" & ChannelShortName & "编号：</td>"
		Response.Write "  <td>" & PE_HTMLEncode(rsProduct("ProductNum")) & "</td>"
		Response.Write "  <td colspan='2' rowspan='6' align=center valign='middle'>" & "<img src='" & GetPhotoUrl(rsProduct("ProductThumb")) & "' width='150'><br>" & PE_HTMLEncode(rsProduct("ProductName")) & "</td>"
		Response.Write "</tr>"
	end if
 	if FoundInArr(FieldShow, "Title", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>" & ChannelShortName & "名称：</td>"
		Response.Write "  <td><strong>" & PE_HTMLEncode(rsProduct("ProductName")) & "</strong></td>"
		Response.Write "</tr>"
	end if
 	if FoundInArr(FieldShow, "Keyword", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>关键字：</td>"
		Response.Write "  <td>" & Mid(rsProduct("Keyword"), 2, Len(rsProduct("Keyword")) - 2) & "</td>"
		Response.Write "</tr>"
	end if
 	if FoundInArr(FieldShow, "Unit", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>" & ChannelShortName & "单位：</td>"
		Response.Write "  <td>" & PE_HTMLEncode(rsProduct("Unit")) & "</td>"
		Response.Write "</tr>"
	end if
 	if FoundInArr(FieldShow, "Property", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>" & ChannelShortName & "属性：</td>"
		Response.Write "  <td width='200'>"
		If rsProduct("OnTop") = True Then
			Response.Write "<font color=blue>顶</font> "
		Else
			Response.Write "&nbsp;&nbsp;&nbsp;"
		End If
		If rsProduct("IsHot") = True Then
			Response.Write "<font color=red>热</font> "
		Else
			Response.Write "&nbsp;&nbsp;&nbsp;"
		End If
		If rsProduct("IsElite") = True Then
			Response.Write "<font color=green>荐</font> "
		Else
			Response.Write "&nbsp;&nbsp;&nbsp;"
		End If
		If Trim(rsProduct("ProductThumb")) <> "" Then
			Response.Write "<font color=blue>图</font>"
		Else
			Response.Write "&nbsp;&nbsp;"
		End If
		Response.Write "</td>"
		Response.Write "</tr>"
	end if
 	if FoundInArr(FieldShow, "Weight", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>" & ChannelShortName & "重量：</td>"
		Response.Write "  <td width='200'>" & rsProduct("Weight") & " Kg</td>"
		Response.Write "</tr>"
	end if
 	if FoundInArr(FieldShow, "Price", ",") or FoundInArr(FieldShow, "Price_Original", ",") or FoundInArr(FieldShow, "Price_Member", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>" & ChannelShortName & "价格：</td>"
		Response.Write "  <td colspan='3'>"
		if FoundInArr(FieldShow, "Price_Original", ",") then
			Response.Write "原始零售价：<font color='red'><STRIKE>" & Price2Str(rsProduct("Price_Original")) & "</STRIKE></font>元&nbsp;&nbsp;&nbsp;&nbsp;"
		End if
		if FoundInArr(FieldShow, "Price", ",") then
			Response.Write "当前零售价：<font color='blue'>" & Price2Str(rsProduct("Price")) & "</font>元&nbsp;&nbsp;&nbsp;&nbsp;"
		End if
		if FoundInArr(FieldShow, "Price_Member", ",") then
			Response.Write "会员零售价：" & Price2Str(rsProduct("Price_Member")) & "&nbsp;&nbsp;&nbsp;&nbsp;"
		end if
		Response.Write "</td></tr>"
	end if
 	if FoundInArr(FieldShow, "ProducerName", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>生 产 商：</td>"
		Response.Write "  <td colspan='3'>" & PE_HTMLEncode(rsProduct("ProducerName")) & "</td>"
		Response.Write "</tr>"
	end if
 	if FoundInArr(FieldShow, "TrademarkName", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>品牌/商标：</td>"
		Response.Write "  <td colspan='3'>" & PE_HTMLEncode(rsProduct("TrademarkName")) & "</td>"
		Response.Write "</tr>"
	end if
 	if FoundInArr(FieldShow, "ProductModel", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>" & ChannelShortName & "型号：</td>"
		Response.Write "  <td colspan='3'>" & PE_HTMLEncode(rsProduct("ProductModel")) & "</td>"
		Response.Write "</tr>"
	end if
 	if FoundInArr(FieldShow, "ProductStandard", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td width='100' align='right' class='tdbg5'>" & ChannelShortName & "规格：</td>"
		Response.Write "  <td colspan='3'>" & PE_HTMLEncode(rsProduct("ProductStandard")) & "</td>"
		Response.Write "</tr>"
	end if
 	if FoundInArr(FieldShow, "ProductIntro", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td align='right' class='tdbg5'>" & ChannelShortName & "简介：</td>"
		Response.Write "  <td colspan='3'>" & PE_HTMLEncode(rsProduct("ProductIntro")) & "</td>"
		Response.Write "</tr>"
	end if
 	if FoundInArr(FieldShow, "ProductExplain", ",") then 
		Response.Write "<tr class='tdbg'>"
		Response.Write "  <td align='right' class='tdbg5'>" & ChannelShortName & "说明：</td>"
		Response.Write "  <td colspan='3'>" & GetPhotoUrl(rsProduct("ProductExplain")) & "</td>"
		Response.Write "</tr>"
	end if
    Response.Write "        </tbody>" & vbCrLf
    Response.Write "      </table>"
    Response.Write "    </td>"
    Response.Write "  </tr>"
    Response.Write "</table>" & vbCrLf
    Response.Write "<form name='formA' method='get' action='Admin_Product.asp'><p align='center'>"
    Response.Write "<input type='hidden' name='ChannelID' value='" & ChannelID & "'>"
    Response.Write "<input type='hidden' name='ProductID' value='" & ProductID & "'>"
    Response.Write "<input type='hidden' name='Action' value=''>" & vbCrLf
    If rsProduct("Deleted") = False Then
		if Purview_Del then
            Response.Write "<input type='submit' name='submit' value=' 删 除 ' onclick=""document.formA.Action.value='Del'"">&nbsp;&nbsp;"
		end if
		if FoundInArr(FieldShow, "Property", ",") then
            If rsProduct("OnTop") = False Then
                Response.Write "<input type='submit' name='submit' value='设为固顶' onclick=""document.formA.Action.value='SetOnTop'"">&nbsp;&nbsp;"
            Else
                Response.Write "<input type='submit' name='submit' value='取消固顶' onclick=""document.formA.Action.value='CancelOnTop'"">&nbsp;&nbsp;"
            End If
            If rsProduct("IsHot") = False Then
                Response.Write "<input type='submit' name='submit' value='设为热卖' onclick=""document.formA.Action.value='SetHot'"">&nbsp;&nbsp;"
            Else
                Response.Write "<input type='submit' name='submit' value='取消热卖' onclick=""document.formA.Action.value='CancelHot'"">&nbsp;&nbsp;"
            End If
            If rsProduct("isElite") = False Then
                Response.Write "<input type='submit' name='submit' value='设为推荐' onclick=""document.formA.Action.value='SetElite'"">&nbsp;&nbsp;"
            Else
                Response.Write "<input type='submit' name='submit' value='取消推荐' onclick=""document.formA.Action.value='CancelElite'"">&nbsp;&nbsp;"
            End If
		end if
    Else
		if Purview_Del then
            Response.Write "<input type='submit' name='submit' value='彻底删除' onclick=""if(confirm('确定要彻底删除此" & ChannelShortName & "吗？彻底删除后将无法还原！')==true){document.formA.Action.value='ConfirmDel';}"">&nbsp;&nbsp;"
            Response.Write "<input type='submit' name='submit' value=' 还 原 ' onclick=""document.formA.Action.value='Restore'"">"
		end if
    End If
    Response.Write "<input type='submit' name='submit' value=' 返 回 ' onclick=""window.location.href='Admin_product.asp'"">&nbsp;&nbsp;"
    Response.Write "</p></form>"
End Sub


Function Price2Str(val1)
    If IsNull(val1) Then
        Price2Str = ""
        Exit Function
    End If
    If val1 = 0 Then
        Price2Str = ""
        Exit Function
    End If
    Price2Str = FormatNumber(val1, 2, vbTrue, vbFalse, vbTrue)
End Function

Function GetUploadFiles(PhotoUrls, PhotoThumb)
    Dim arrPhotoUrls, iTemp, strUrls
    strUrls = ""
    strUrls = strUrls & PhotoThumb
    arrPhotoUrls = Split(PhotoUrls, "|")
    For iTemp = 0 To UBound(arrPhotoUrls)
    	strUrls = strUrls & "|" & arrPhotoUrls(iTemp)
    Next
    GetUploadFiles = strUrls
End Function




Function GetAllUnit()
    Dim rsUnit, strUnit
    Set rsUnit = Conn.Execute("select DISTINCT Unit from PW_Product")
    strUnit = strUnit & "<option value=''>请选择</option>"
    Do While Not rsUnit.EOF
        strUnit = strUnit & "<option value='" & rsUnit(0) & "'>" & rsUnit(0) & "</option>"
        rsUnit.MoveNext
    Loop
    Set rsUnit = Nothing
    GetAllUnit = strUnit
End Function

Function GetAllModel()
    Dim rsModel, strModel
    Set rsModel = Conn.Execute("select DISTINCT top 50 ProductModel from PW_Product")
    Do While Not rsModel.EOF
        strModel = strModel & "<option value='" & rsModel(0) & "'>" & rsModel(0) & "</option>"
        rsModel.MoveNext
    Loop
    Set rsModel = Nothing
    GetAllModel = strModel
End Function

Function GetAllStandard()
    Dim rsStandard, strStandard
    Set rsStandard = Conn.Execute("select DISTINCT top 50 ProductStandard from PW_Product")
    Do While Not rsStandard.EOF
        strStandard = strStandard & "<option value='" & rsStandard(0) & "'>" & rsStandard(0) & "</option>"
        rsStandard.MoveNext
    Loop
    Set rsStandard = Nothing
    GetAllStandard = strStandard
End Function

%>