<!--#include file="Admin_commonC.asp"-->
<%

Const NeedCheckComeUrl = True   '是否需要检查外部访问
Const PurviewLevel_Channel = 0   '0--不检查，1 检查
Const PurviewLevel_Others = ""   '其他权限


'定义生成相关的变量
Dim CreateType, tmpFileName, tmpPageTitle, tmpNavPath, IsAutoCreate
Dim Pages
Dim TotalCreate, CurrentCreatePage, iCount, iTotalPage
Dim strUrlParameter


ClassID = GetValue("ClassID")
IsAutoCreate = False
CreateType = GetValue("CreateType")
If CreateType = "" Then
    CreateType = 1
Else
    CreateType = PE_CLng(CreateType)
End If
CurrentCreatePage = PE_CLng(GetValue("CreatePage"))

Response.Write "<html><head><title>" & ChannelShortName & "生成</title>" & vbCrLf
Response.Write "<meta http-equiv='Content-Type' content='text/html; charset=gb2312'>" & vbCrLf
Response.Write "<link href='Admin_Style.css' rel='stylesheet' type='text/css'>" & vbCrLf
Response.Write "</head>" & vbCrLf
Response.Write "<body leftmargin='2' topmargin='0' marginwidth='0' marginheight='0'>" & vbCrLf


Sub CreateIndex()
	Dim tmpDir
    If ChannelID <> PrevChannelID Then
        Call GetChannel(ChannelID)
        PrevChannelID = ChannelID
    End If
    If UseCreateHTML = 0 Then
        Response.Write "因为设置了“不生成HTML”，所以不用生成HTML。"
        Exit Sub
    End If

    Response.Write "<b>正在生成此频道的首页（" & ChannelUrl & "/Index.html）……"
    MaxPerPage = MaxPerPage_Channel
	strPageTitle = SiteName
	strNavPath = "您现在的位置：&nbsp;<a href='" & InstallDir & "'>" & SiteName & "</a>"
	CurrentPage = 1
	strFileName = ChannelUrl_ASPFile & "/Index.asp"
	strPageTitle = strPageTitle & " >> " & ChannelName
    strNavPath = strNavPath & "&nbsp;" & strNavLink & "&nbsp;<a  href='" & ChannelUrl & "/Index.html'>" & ChannelName & "</a>"
	tmpDir = ChannelUrl&"/"
	If CreateMultiFolder(tmpDir) = False Then
		Response.Write "请检查服务器。系统不能创建生成文件所需要的文件夹。"
		Exit Sub
	End If
    Call PW_Content.GetHTML_Index
	Call GetModelFront_Html
    Call WriteToFile(ChannelUrl & "/Index.html", strHtml)
	
    Response.Write "……………………生成首页成功！</b>" & vbCrLf
End Sub

Sub CreateClass()
    'On Error Resume Next
    If ChannelID <> PrevChannelID Then
        Call GetChannel(ChannelID)
        PrevChannelID = ChannelID
    End If
    If UseCreateHTML = 0 Then
        Response.Write "<b>因为设置了“不生成HTML”，所以不用生成栏目页。</b><br>"
        Exit Sub
    End If

    Dim rsCreate, sql
	Dim tmpDir
	If IsAutoCreate = False Then
		Response.Write "<b>正在生成栏目列表页面……请稍候！<font color='red'>在此过程中请勿刷新此页面！！！</font></b><br>"
	End If
	Response.Flush
    sql = "select * from PW_Class where ClassType=1 and ChannelID=" & ChannelID
    Select Case CreateType
    Case 1 '选定的栏目
        If IsValidID(ClassID) = False Then
            FoundErr = True
            ErrMsg = ErrMsg & "<li>请指定要生成的栏目ID</li>"
            Exit Sub
        End If
        If Left(ClassID, 1) = "," Then
            ClassID = Right(ClassID, Len(ClassID) - 1)
        End If
        If InStr(ClassID, ",") > 0 Then
            sql = sql & " and ClassID in (" & ClassID & ")"
        Else
            sql = sql & " and ClassID=" & ClassID & ""
        End If
    Case 2 '所有栏目
         
    Case Else
        Response.Write "参数错误！"
        Exit Sub
    End Select
    sql = sql & " order by RootID,OrderID"
    Set rsCreate = Server.CreateObject("ADODB.Recordset")
    rsCreate.Open sql, Conn, 1, 1
    If rsCreate.Bof And rsCreate.EOF Then
        TotalCreate = 0
        rsCreate.Close
        Set rsCreate = Nothing
        Exit Sub
    Else
        TotalCreate = rsCreate.RecordCount
    End If
    
    Call MoveRecord(rsCreate)
    Call ShowTotalCreate("个栏目")
    
    Do While Not rsCreate.EOF
        PageTitle = ""
        FoundErr = False
        If ChannelID <> PrevChannelID Then
            Call GetChannel(ChannelID)
            PrevChannelID = ChannelID
        End If
        CurrentPage = 1
        strPageTitle = tmpPageTitle
        strNavPath = tmpNavPath
        ClassID = rsCreate("ClassID")
        strFileName = ChannelUrl_ASPFile & "/list.asp?ClassID=" & ClassID
        Call GetClass
		tmpDir = ChannelUrl&"/"
        If CreateMultiFolder(tmpDir) = False Then
            Response.Write "请检查服务器。系统不能创建生成文件所需要的文件夹。"
            Exit Sub
        End If
        tmpFileName = ChannelUrl &"/"& GetListFileName(ClassID, CurrentPage, CurrentPage) & ".html"
        Call PW_Content.GetHtml_Class
		Call GetModelFront_Html()
        Call WriteToFile(tmpFileName, strHtml)

        iCount = iCount + 1
        Response.Write "<li>成功生成第 <font color='red'><b>" & iCount & " </b></font>个栏目的列表：" & tmpFileName & "</li><br>" & vbCrLf
        Response.Flush
	
        If UseCreateHTML > 0  And (IsAutoCreate = False Or (IsAutoCreate = True And AutoCreateType = 1)) then
            If TotalPut Mod MaxPerPage = 0 Then
                Pages = TotalPut \ MaxPerPage
            Else
                Pages = TotalPut \ MaxPerPage + 1
            End If
            If Pages > 1 Then
                For CurrentPage = 2 To Pages
                    If ChannelID <> PrevChannelID Then
                        Call GetChannel(ChannelID)
                        PrevChannelID = ChannelID
                    End If
                    tmpFileName = ChannelUrl &"/"& GetListFileName(ClassID, CurrentPage, Pages) & ".html"
					strNavPath = tmpNavPath
					Call PW_Content.GetHtml_Class
					Call GetModelFront_Html()
					Call WriteToFile(tmpFileName, strHtml)
					Response.Write "&nbsp;&nbsp;&nbsp;成功生成第 <font color='red'><b>" & iCount & " </b></font>个栏目的第 <font color='blue'>" & CurrentPage & "</font> 页列表：" & tmpFileName & "<br>" & vbCrLf
					Response.Flush
                Next
            End If
        End If
        ClassShowType = 1
        rsCreate.MoveNext
        If iCount Mod MaxPerPage_Create = 0 Then Exit Do
    Loop
    rsCreate.Close
    Set rsCreate = Nothing
End Sub

Sub CreateSiteIndex()
    Response.Write "<br><iframe id='CreateSiteIndex' width='100%' height='30' frameborder='0' src='Admin_CreateSiteIndex.asp'></iframe>"
End Sub


Sub ShowProcess()
	Dim iCreatePage
	If CreateType = 9 Then
		iCreatePage = CurrentCreatePage
	Else
		iCreatePage = CurrentCreatePage + 1
    End If
	strFileName = "Admin_Create" & ModuleName & ".asp?Action=" & Action & "&CreateType=" & CreateType & "&ChannelID=" & ChannelID & "&ClassID=" & Trim(Request("ClassID")) & "&CreatePage=" & iCreatePage & strUrlParameter
    strFileName = Replace(strFileName, " ", "")
    If CurrentCreatePage < iTotalPage Then
        If SleepTime > 0 Then
            Response.Write "<p align='center'>" & SleepTime & "秒后将自动继续生成下一页！</p>" & vbCrLf
        End If
        Call Refresh(strFileName,SleepTime)
    Else
        Response.Write "<p align='center'>已经生成所有页面！</p>" & vbCrLf
        If Trim(Request("ShowBack")) <> "No" And CreateType <> 7 And CreateType <> 8 Then
            Response.Write "<p align='center'><a href='Admin_CreateHTML.asp?ChannelID=" & ChannelID & "'>【返回】</a></p>" & vbCrLf
        End If
    End If
End Sub

Sub MoveRecord(rsCreate)
	If (TotalCreate Mod MaxPerPage_Create) = 0 Then
        iTotalPage = TotalCreate \ MaxPerPage_Create
    Else
        iTotalPage = TotalCreate \ MaxPerPage_Create + 1
    End If
    If CurrentCreatePage < 1 Then
        CurrentCreatePage = 1
    End If
	If CreateType = 9 Then
		Exit Sub
	End If
    If (CurrentCreatePage - 1) * MaxPerPage_Create > TotalCreate Then
        CurrentCreatePage = iTotalPage
    End If
    If CurrentCreatePage > 1 Then
        If (CurrentCreatePage - 1) * MaxPerPage_Create < TotalCreate Then
			rsCreate.Move (CurrentCreatePage - 1) * MaxPerPage_Create
        Else
            CurrentCreatePage = 1
        End If
    End If
    iCount = (CurrentCreatePage - 1) * MaxPerPage_Create
End Sub

Sub ShowTotalCreate(ItemName)
	If IsAutoCreate = False Then
		Response.Write "总共需要生成 <font color='red'><b>" & TotalCreate & "</b></font> " & ItemName
		Response.Write "，每页生成 <font color='red'><b>" & MaxPerPage_Create & "</b></font> " & ItemName
		Response.Write "，共需要分 <font color='red'><b>" & iTotalPage & "</b></font> 页生成"
		Response.Write "，当前正在生成 <font color='red'><b>" & CurrentCreatePage & "</b></font> 页<br>" & vbCrLf
		Response.Flush
    End If
End Sub

%>