
function echo(obj,html)
{
	$(obj).innerHTML=html;
}

function fopen(obj)
{
	$(obj).style.display="";
	$(obj).style.left = ( screen.width - 20 - 450 ) / 2;
	$(obj).style.top = 50;
	$(obj).style.width = "450px";
}
function fclose(obj)
{
	$(obj).style.display="none";
	$(obj).innerHTML="";
}


var DS_x,DS_y;
function dsMove(obj) //实现层的拖移
{
	if(event.button==1)
	{
		var X=$(obj).clientLeft;
		var Y=$(obj).clientTop;
		$(obj).style.pixelLeft=X+(event.x-DS_x);
		$(obj).style.pixelTop=Y+(event.y-DS_y);
	}
}



function createAjaxObj()
{
var XmlHttp;
	//windows
	try {
		XmlHttp = new ActiveXObject("Msxml2.XMLHTTP.3.0");
	} catch (e) {
		try {
			XmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try {
				XmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e) {
				XmlHttp = false;
			}
		}
	}
	//other
	if (!XmlHttp && typeof XMLHttpRequest != "undefined")
	{
		try {
			XmlHttp = new XMLHttpRequest();
		} catch (e) {
			XmlHttp = false;
		}
	}
	return XmlHttp;
}


function getdata(url,obj)
{
		
	var xmlhttp=createAjaxObj();
	if(!xmlhttp)
	{
		alert("你的浏览器不支持XMLHTTP！！");
		return;
	}
	xmlhttp.abort();
	xmlhttp.onreadystatechange=requestdata;
	xmlhttp.open("GET",url+"?math="+new Date().getTime(),true);
	xmlhttp.setRequestHeader("Content-type", "text/html;charset=gb2312");
	xmlhttp.setRequestHeader("If-Modified-Since","0");
	xmlhttp.send(null);
	function requestdata()
	{
		
			fopen(obj);
			echo(obj,"Loading......");
			if(xmlhttp.readyState==4)
			{
				if(xmlhttp.status==200)
				{
					echo(obj,xmlhttp.responseText);
					
				}
			}
		
	}
}


function postdata(url,obj)
{
	if(isNotEmpty("UserName","姓名","UserName_Msg") && isTel("Tel","电话号码","Tel_Msg") && checkIsInteger("Oicq","QQ号码","Oicq_Msg") && checkEmail("Email","邮箱","Email_Msg")  && isNotEmpty("Title","标题","Title_Msg") && isNotEmpty("Content","内容","Content_Msg") && IsCheckCode("CheckCode","验证码","CheckCode_Msg")){
		var params="UserName="+encodeURIComponent(encodeURIComponent($("UserName").value));
		params+="&Sex="+encodeURIComponent(encodeURIComponent(GetRadio("Sex")));
		params+="&Tel="+encodeURIComponent(encodeURIComponent($("Tel").value));
		params+="&Oicq="+encodeURIComponent(encodeURIComponent($("Oicq").value));
		params+="&Email="+encodeURIComponent(encodeURIComponent($("Email").value));
		params+="&Title="+encodeURIComponent(encodeURIComponent($("Title").value));
		params+="&Content="+encodeURIComponent(encodeURIComponent($("Content").value));
		params+="&CheckCode="+encodeURIComponent(encodeURIComponent($("CheckCode").value));
		params+="&UserID="+encodeURIComponent(encodeURIComponent($("UserID").value));
		params+="&KindID="+encodeURIComponent(encodeURIComponent($("KindID").value));
		var xmlhttp=createAjaxObj();
		if(!xmlhttp)
		{
			alert("你的浏览器不支持XMLHTTP！！");
			return;
		}
		xmlhttp.abort();
		xmlhttp.onreadystatechange=requestdata;
		xmlhttp.open("POST", url+"?timeStamp=" + new Date().getTime(), true);
		xmlhttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
		xmlhttp.setRequestHeader("If-Modified-Since","0");
		xmlhttp.send(params);
		function requestdata()
		{
			$("Submit").disabled = true;
			$("Submit").value = "提交中...";
			$("Close").disabled = true;
			if(xmlhttp.readyState==4)
			{
				//alert(xmlhttp.status);
				if(xmlhttp.status==200)
				{
					var texts;
					texts=xmlhttp.responseText.split("|");
					if(texts[0]=="0"){
						$("Submit").disabled = false;
						$("Close").disabled = false;
						$("Submit").value = "提交";
						alert(texts[1]);
					}else if(texts[0]=="1"){
						alert(texts[1]);
						fclose(obj)
					}else{
						alert("未知错误!");
						fclose(obj)
					}
				}
			}
		}
	}
}

