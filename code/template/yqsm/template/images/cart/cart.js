Object.extend = function(destination, source) {
	for (var property in source) {
		destination[property] = source[property];
	}
	return destination;
}

Object.each = Array.prototype.each = function(obj, func){
	if(typeof(func) != 'function' || typeof(obj) != 'object'){
		return false;
	}
	for (var key in obj) {
		func(obj[key], key);
	}
}

/**
 * Cart
 */
var Cart = Cart || {
	storage : null,
	stack : {},
	
	init : function(){
		this.storage = Cart.CookieStorage;
		this.stack = this.storage.load();
	},
	
	_exist : function(key){
		return typeof(this.stack[key]) == 'object' && this.stack[key];
	},
	
	each : function(func){
		Object.each(this.stack, func);
	},
	
	add : function(goods, quantity){
		if(typeof(goods) != 'object' || !goods){
			return false;
		}
		if(typeof quantity == 'undefined'){
			quantity = 1;
		}
		this.stack[goods.key()] = {'goods' : goods, 'quantity' : quantity};
		this.change();
	},
	
	del : function(key){
		if(this._exist(key)){
			this.stack[key] = null;
		}
		var _tmp = {};
		this.each(function(v, k){
			if(v){
				_tmp[k] = v;
			}
		});
		this.stack = _tmp;
		this.change();
	},
	
	quantity : function(key, nums){
		if(this._exist(key)){
			this.stack[key].quantity = nums;
			this.change();
		}
	},
	
	clear : function(){
		this.stack = {};
		this.change();
	},
	
	totalGoods : function(){
		var len = 0;
		this.each(function(v, k){
			len++;
		});
		return len;
	},
	
	totalQuantity : function(){
		var quantity = 0;
		this.each(function(cartItem, key){
			quantity += cartItem.quantity
		});
		return quantity;
	},
	
	totalCost : function(){
		var total = 0.00;
		this.each(function(cartItem, key){
			total += cartItem.quantity * parseFloat(cartItem.goods.get('price'));
		});
		return total;
	},
	
	save : function(){
		this.storage.save(this.stack);
	},
	
	change : function(){
		if(typeof(this.onchange)){
			this.onchange();
		}
	}
};

/**
 * Cart.Goods
 */
Cart.Goods = Cart.Goods || function(options){
	this.params = {'id':0, 'name':'', 'price':0.00};
	this.paramskey = 'id';
	this.set(options);
}
Cart.Goods.prototype = {
	set : function(key, value){
		if(typeof key == 'object'){
			Object.extend(this.params, key || {});
		}else{
			this.params[key] = value;
		}
	},
	
	get : function(key){
		return this.params[key];
	},
	
	key : function(){
		return '__key' + this.params[this.paramskey];
	},
	
	each : function(func){
		Object.each(this.params, func);
	}
}

/**
 * Cart.CookieStorage
 */
Cart.CookieStorage = Cart.CookieStorage || {
	cookieName : '__jc001__cart',
	
	load : function(){
		var cookiestr = this._read(this.cookieName);
		if(cookiestr.length == 0){
			return {};
		}
		var cart = JSON.parse(cookiestr);
		Object.each(cart, function(v, k){
			v.goods = new Cart.Goods(v.goods.params);
		})
		return cart;
	},
	
	save : function(cart){
		this._save(this.cookieName, JSON.stringify(cart));
	},

	_save : function(name, value, hours){
		var expire = "";
		if(hours != null){
			expire = new Date((new Date()).getTime() + hours * 3600000);
			expire = "; expires=" + expire.toGMTString();
		}
		document.cookie = name + "=" + escape(value) + expire + ';path=/';
	},
	
	_read : function(name){
		var cookieValue = "";
		var search = name + "=";
		if(document.cookie.length > 0){ 
			offset = document.cookie.indexOf(search);
			if (offset != -1){ 
				offset += search.length;
				end = document.cookie.indexOf(";", offset);
				if (end == -1) end = document.cookie.length;
					cookieValue = unescape(document.cookie.substring(offset, end))
			}
		}
		return cookieValue;
	}
}


Cart.Printer = Cart.Printer || function(cart){
	this.cart = cart;
	this.header = {'id':'商品编号', 'name':'商品名称', 'price':'商品价格', quantity : '商品数量', del : '删除'};
	this.fieldName = 'cart_goods';
}
Cart.Printer.prototype = {
	setHeader : function(key, value){
		if(typeof key == 'object'){
			Object.extend(this.header, key || {});
		}else{
			this.header[key] = value;
		}
	},
	
	show : function(id){
		if(typeof this.cart != 'object'){
			return false;
		}
		var doc = document;
		var tbl = doc.createElement('table');
		var tr = tbl.insertRow(-1);
		tr.className = 'align_Center Thead';
		Object.each(this.header, function(name, k){
			var cell = tr.insertCell(-1);
			cell.innerHTML = name;
		});
		
		var _this = this;
		this.cart.each(function(itm, key){
			var _tr = tbl.insertRow(-1);
			itm.goods.each(function(v, k){
				if(typeof(_this.header[k]) != 'undefined'){
					var cell = _tr.insertCell(-1);
					cell.innerHTML = v;
				}
			})
			
			// quantity
			var id = itm.goods.get('id');
			var input = doc.createElement('input');
			input.type = 'text';
			input.name = _this.fieldName + '[' + id + ']';
			input.size = 8;
			input.value =  itm.quantity;
			var cell = _tr.insertCell(-1);
			cell.insertBefore(input, null);
			
			// utype
			var inputUtype = doc.createElement('input');
			inputUtype.type = 'hidden';
			inputUtype.name = 'utype[' + id + ']';
			inputUtype.value =  itm.goods.get('utype');
			cell.insertBefore(inputUtype, null);
			
			// shopid
			var inputUtype = doc.createElement('input');
			inputUtype.type = 'hidden';
			inputUtype.name = 'shopid[' + id + ']';
			inputUtype.value =  itm.goods.get('shopid');
			cell.insertBefore(inputUtype, null);
			
			var ov = itm.quantity;
			input.onchange = function(){
				var v = parseInt(this.value);
				if(v > 0){
					_this.cart.quantity(itm.goods.key(), v);
					_this.cart.save();
					this.value = v;
				}else{
					this.value = ov;
				}
			}
			
			// del
			var delinput = doc.createElement('input');
			delinput.type = 'button';
			delinput.value =  _this.header.del;
			delinput.onclick = function(){
				_this.cart.del(itm.goods.key());
				_this.cart.save();
				_tr.style.display = 'none';
				window.location.reload();
			}
			var del = _tr.insertCell(-1);
			del.insertBefore(delinput, null);
			
			// class name
			_tr.onmouseover = function(){
				this.className = 'sel';
			};
			_tr.onmouseout = function(){
				this.className = '';
			}
		})

		if(typeof(id) == 'string'){
			id = doc.getElementById(id);
		}
		if(typeof(id) != 'object' || id == null){
			id = doc.body;
		}else{
			id.innerHTML = '';	
		}
		id.insertBefore(tbl, null);
	}
}

Cart.init();
